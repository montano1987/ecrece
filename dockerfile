FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/ecrece-2.1.3.RELEASE.war app.war
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]