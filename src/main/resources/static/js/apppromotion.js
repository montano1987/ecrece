//inicio Promotion
angular.module('app').controller("PromotionController", function ($scope, $filter,toast, $location, $window, PromotionCRUDService, ConfigurationCRUDService, NotificationCRUDService, BusinessCRUDService) {
	
	$scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    $scope.today = $filter('date')(new Date(), "yyyy-MM-dd");

	var locdir = window.location;
	 var pathNamedir = locdir.pathname.substring(0, locdir.pathname.lastIndexOf('/') + 1);
	 var dir = locdir.href.substring(0, locdir.href.length - ((locdir.pathname + locdir.search + locdir.hash).length - pathNamedir.length));
	 
    $scope.dateini = new Date();
    $scope.dateini.setDate($scope.dateini.getDate()+ 1);
    //modal adicionar
    $scope.AdicionarModal = function () {
    	$scope.clearfield();
        $scope.showModal = true;
	};
    $scope.CerrarModal = function () {
		$scope.clearfield();
		$scope.IsNewRecord = 1;
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clearfield();      
    };
    $scope.clearfield = function () { 
    	console.log(dir)
    	if($scope.Rad.toradio == 2)
    		{
    		$scope.Promotion = {
        			idpromotion: 0,
        			daysPromotion:0,
        			codePromotion:"", 
        			date_start_promotion: $scope.dateini,
        			date_end_promotion:"",  
        			desc_promotion:"",
        			activePromotion :false,
        			business :$scope.Business,
        			title_promotion :"",
        			urldir:dir+'#!/promotiondetail/',
                };	
    		}
    	else
    		{
    		$scope.Promotion = {
        			idpromotion: 0,
        			daysPromotion:0,
        			codePromotion:"", 
        			date_start_promotion: $scope.dateini,
        			date_end_promotion:"",  
        			desc_promotion:"",
        			activePromotion :false,
        			business : null,
        			title_promotion :"",
        			urldir:dir+'#!/promotiondetail/',
                };	
    		}
    	
    	$scope.verMessageError = false;
	};
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Promotions, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
    
//    busqueda
    $scope.searchtitle = function () {    	
    	if ( ($scope.search_promotiontitle == undefined  || $scope.search_promotiontitle =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.title_promotion != null)
						{
							if(item.title_promotion.toUpperCase().search($scope.search_promotiontitle.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
    $scope.searchcodigo = function () {    	
    	if ( ($scope.search_promotioncode == undefined  || $scope.search_promotioncode =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.title_promotion != null)
						{
							if(item.codePromotion.toUpperCase().search($scope.search_promotioncode.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
    $scope.searchdstar = function () {   
    	if ( ($scope.search_promotiondatestart == undefined  || $scope.search_promotiondatestart =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{	    	
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.date_start_promotion != null)
						{
							if(angular.equals($filter('date')(item.date_start_promotion, "yyyy-MM-dd"), $filter('date')($scope.search_promotiondatestart, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
    $scope.searchdend = function () {   
    	if ( ($scope.search_promotiondateend == undefined  || $scope.search_promotiondateend =="") )
		{
		$scope.callto($scope.Rad.toradio)
		}
		else
		{    	
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.date_end_promotion != null)
						{
							
							if(angular.equals($filter('date')(item.date_end_promotion, "yyyy-MM-dd"), $filter('date')($scope.search_promotiondateend, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
      
    $scope.changedateend = function (){
    	$scope.lesszero = false;
    	if($scope.Promotion.daysPromotion > 0 && $scope.Promotion.daysPromotion != "")
    		{
				var newdate = new  Date($scope.Promotion.date_start_promotion)
    			$scope.Promotion.date_end_promotion = new Date(newdate.setDate(newdate.getDate()+$scope.Promotion.daysPromotion));
    		}
    	if($scope.Promotion.daysPromotion <= 0 || $scope.Promotion.daysPromotion == null || $scope.Promotion.daysPromotion == undefined)
    		{
    			$scope.lesszero = true;
    			$scope.Promotion.date_end_promotion = "";
    		}
    }    
    $scope.fordelete = function (promo){
    	var todaynow = new Date();
    	if(promo.activePromotion != true && $filter('date')(todaynow, 'yyyy-MM-dd') > $filter('date')(promo.date_start_promotion, 'yyyy-MM-dd'))
    	{
    		return false;    		
    	}
    	else
		{
			return true;
		}
    }    
    $scope.foredit = function (promo){
    	var todaynow = new Date();
    	if(promo.activePromotion != true && ($filter('date')(todaynow, 'yyyy-MM-dd') > $filter('date')(promo.date_start_promotion, 'yyyy-MM-dd') ||  $filter('date')(todaynow, 'yyyy-MM-dd') < $filter('date')(promo.date_end_promotion, 'yyyy-MM-dd')))
    	{
    		return false;    		
    	}
    	else
		{
			return true;
		}
    }
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
	 $scope.showcheck = function () {
		 console.log($scope.Rad.toradio)
			if($scope.Rad.toradio == 1)
    		{
    			$scope.showbotomadd= false;
    		}
    	else if($scope.Rad.toradio == 2 && $scope.user.role.desc_rol == "ROLE_ADMIN")
    		{
    			$scope.showbotomadd= false;
    		}    	
    	else
    		{
    			$scope.showbotomadd= true;
    		}
    }
	 
//	 var loc = window.location;
//	 var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
//	 var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	 
	$scope.callto = function (radiodata) {
		if(radiodata == 1)
			{
			$scope.changetoradiouser();
			}
		if(radiodata == 2)
			{
			$scope.changetoradiobusiness();
			}
	    };
	    $scope.showcheck = function () {
	    	$scope.showbotomadd= true;	    		
	    }
	 $scope.changetoradiobusiness = function ()
	    {		 
				var recordsGet = PromotionCRUDService.getAllPromotionsbyBusiness($scope.Business.idbusiness);
				recordsGet.then(function (d) {
						$scope.Promotions = d.data;
						$scope.original = $scope.Promotions;
						$scope.search();
						console.log($scope.Promotions)
						
						var recordsGet = BusinessCRUDService.getFuncionalitybyBusiness($scope.Business.idbusiness);
						recordsGet.then(function (b) {
							console.log(b.data)
							var funcio = $scope.filterUsers = $filter('filter')(b.data, function (item) {
										if(item.idfuncionality == 3)
										{
											return true;
										}						
										return false;
									});
							if(funcio.length > 0)
								{
								$scope.showbotomadd= false;
								}
							else
								{
									$scope.showbotomadd= true;
								}
						},
								function (errorPl) {
							$log.error('Error ', errorPl);
						});
						
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
	    }
	    $scope.changetoradiouser = function ()
	    {
	    	if ($scope.user.role.desc_rol==="ROLE_ADMIN")
			{
				var recordsGet = PromotionCRUDService.getAllPromotionsbyUserAdmin();
				recordsGet.then(function (d) {
						$scope.Promotions = d.data;
						$scope.original = $scope.Promotions;
						$scope.search();
						console.log($scope.Promotions)
						$scope.showbotomadd= false;
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});	
			}
		else
			{
				var recordsGet = PromotionCRUDService.getAllPromotionsbyUser($window.sessionStorage.iduser);
				recordsGet.then(function (d) {
						$scope.Promotions = d.data;
						$scope.original = $scope.Promotions;
						$scope.search();
						console.log($scope.Promotions)
						$scope.showbotomadd= true;
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});	 
			}	
	    }
	
	//limpiar registro
    $scope.clear = function () {
    	$scope.isadmin = false;
		$scope.IsNewRecord = 1;
		$scope.lesszero = false;
		$scope.showbotomadd= true;
		
		$scope.Business = {
				idbusiness: 0,
		}
		$scope.user = JSON.parse($window.sessionStorage.userData);
		$scope.Rad = {
				toradio:1,
		}
        $scope.Promotion = {
			idpromotion: 0,
			daysPromotion:"",
			codePromotion:"", 
			date_start_promotion: new Date(),
			date_end_promotion:"",  
			desc_promotion:"",
			activePromotion :false,
			business :"",
			title_promotion :"",
			urldir:dir+'#!/promotiondetail/',
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
    	$scope.clear();
    	
    	var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
			if ($scope.user.role.desc_rol==="ROLE_ADMIN")
			{
				$scope.showbotomadd = false;
				var recordsGet = BusinessCRUDService.getallBusiness()
				recordsGet.then(function (d) {
					$scope.BusinessList = d.data;
					$scope.Business = d.data[0];
					$scope.callto($scope.Rad.toradio);
				},
					function (errorPl) {
						console.log( errorPl)
					});
			}
			else
			{	
				var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
				recordsGet.then(function (d) {
					$scope.BusinessList = d.data;
					$scope.Business = d.data[0];
					$scope.callto($scope.Rad.toradio);
				},
					function (errorPl) {
						console.log( errorPl)
					});
			}
			   	
		},
		function (errorPl) {
			$log.error('Error ', errorPl);
		});
		
    }

    //guardar prestamos
    $scope.save = function (Promotion) {
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1 ) { 
			
            var promisePost = PromotionCRUDService.addPromotion(Promotion);
            promisePost.then(function (d) {                
                $scope.Promotion.idpromotion = d.data.idpromotion;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                toast({
		    	     duration  : 5000,
		    	     message   : $filter('translate')("PROMOTION_INSERT"),
		    	     className : "alert-success",
		    	     position: "center",
		    	     dismissible: true,
		    	   });

                $scope.callto($scope.Rad.toradio);
                
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else {
			 //Else Edit the record
					
            var promisePut = PromotionCRUDService.updatePromotion($scope.Promotion.idpromotion, Promotion);
            promisePut.then(function (d) {
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
                $scope.callto($scope.Rad.toradio);
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
	};
	
	 $scope.notificationforallpromotion = function (Promotion) {
		  var promisePost = NotificationCRUDService.promotionforall(Promotion);
		  promisePost.then(function (d) { 
			  console.log("all fine") 
			  }, function (err) {
                console.log(err);
            });  
    }
    //Eliminar
    $scope.delete = function (Promotion) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = PromotionCRUDService.deletePromotion(Promotion.idpromotion);
    	            promiseDelete.then(function (d) {           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 2000);                               
    	                })();   
    	                $scope.callto($scope.Rad.toradio);
    	            }, function (err) {
    	                $scope.errorMessage = "PROMOTION_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		}); 
    }
    //detalles de uno
    $scope.get = function (Promotion) {       							
				$scope.Promotion = {
					idpromotion: Promotion.idpromotion,
					daysPromotion :Promotion.daysPromotion,
					codePromotion :Promotion.codePromotion, 
					date_start_promotion : new Date (Promotion.date_start_promotion),
					date_end_promotion : new Date (Promotion.date_end_promotion),  
					desc_promotion :Promotion.desc_promotion,  
					activePromotion :Promotion.activePromotion,
					business :Promotion.business,
					title_promotion :Promotion.title_promotion,
				};
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            
    }
    $scope.showclone = function (Promotion) {
		$scope.clone(Promotion);
		$scope.showModal = true;
	}
    $scope.clone = function (Promotion) {
    	console.log(Promotion)
		 $scope.Promotion = {
				 idpromotion: 0,
				 daysPromotion: Promotion.daysPromotion,
				 codePromotion: Promotion.codePromotion,
				 activePromotion: false,
				date_start_promotion: new Date(),
				date_end_promotion: "",				
				business :Promotion.business,
				title_promotion :Promotion.title_promotion,
				desc_promotion: Promotion.desc_promotion,
		};
		$scope.IsNewRecord = 1;
		$scope.verMessage = false;        
	}    

    $scope.goDetail = function (idpromotion) {
		$location.path("/promotiondetail/"+idpromotion); 
	}
    $scope.back = function () {
    	$window.history.back(-1);	
	}
});

angular.module('app').service('PromotionCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {	
    this.getPromotion = function getPromotion(idpromotion){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/promotion/details/'+idpromotion
        });
	}
    
    this.getAll = function getAll(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/promotionsall'
        });
	}
    
    this.getAllPromotionsbyBusiness = function getAllPromotionsbyBusiness(idbusiness){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/promotion/business/'+idbusiness
          });
    }
    this.addPromotion = function addPromotion(Promotion){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/promotion',
          data: Promotion
        });
    }
	
    this.deletePromotion = function deletePromotion(idpromotion){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/promotion/'+idpromotion
        })
    }
	
    this.updatePromotion = function updatePromotion(idpromotion, Promotion){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/promotion/'+idpromotion,
          data: Promotion
        })
    }
	
    this.getAllPromotions = function getAllPromotions(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/promotions'
        });
    }
    this.getAllPromotionsbyUser = function getAllPromotionsbyUser(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/promotions/user/'+iduser
        });
    }
    this.getAllPromotionsbyUserAdmin = function getAllPromotionsbyUserAdmin(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/promotions/useradmin'
        });
    }
    
    this.SendNotification = function SendNotification(Notification){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/promotions/notificationforall'
        });
    }
    this.getPromotionbybusiness = function getPromotionbybusiness(idbusiness){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/promotions/business/'+idbusiness
        });
    }
    
    

}]);
//fin Promotion
angular.module('app').controller("PromotionDetailController", function ($scope,$window,RELATIVE_DIR,$location,PromotionCRUDService) {
		
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idpromotion =urlParts[urlParts.length - 1];
	$scope.Reservation;
	loadRecords();
	function loadRecords(){
	      var recordsGet = PromotionCRUDService.getPromotion(idpromotion);        
	        recordsGet.then(function (d) {
	        	$scope.PromotionDetail = d.data;
	        	if($scope.PromotionDetail.business != null)
	    		{
	    		$scope.pathimage = RELATIVE_DIR+"/files/"+$scope.PromotionDetail.business.name_image;
	    		}			
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	  }	
	
	$scope.showavatar = function () {
		if($scope.pathimage != undefined)
			{
				return true;
			}
		else
			{
				return false;
			}
	}
	
	$scope.out = function () {
		$location.path('/home');
	}
	$scope.back = function () {
		$window.history.back(-1);	
	}
});

angular.module('app').controller("PromotionListBusinessController", function ($scope,$filter, $window,RELATIVE_DIR,$location,PromotionCRUDService,ConfigurationCRUDService, BusinessCRUDService) {
	
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idbusiness =urlParts[urlParts.length - 1];
	
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Promotions, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.goDetail = function (idpromotion) {
			$location.path("/promotiondetail/"+idpromotion); 
	}
//  busqueda
    $scope.searchtitle = function () {    	
    	if ( ($scope.search_promotiontitle == undefined  || $scope.search_promotiontitle =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.title_promotion != null)
						{
							if(item.title_promotion.toUpperCase().search($scope.search_promotiontitle.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
    $scope.searchcodigo = function () {    	
    	if ( ($scope.search_promotioncode == undefined  || $scope.search_promotioncode =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.title_promotion != null)
						{
							if(item.codePromotion.toUpperCase().search($scope.search_promotioncode.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
    $scope.searchdstar = function () {   
    	if ( ($scope.search_promotiondatestart == undefined  || $scope.search_promotiondatestart =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{	    	
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.date_start_promotion != null)
						{
							if(angular.equals($filter('date')(item.date_start_promotion, "yyyy-MM-dd"), $filter('date')($scope.search_promotiondatestart, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
    $scope.searchdend = function () {   
    	if ( ($scope.search_promotiondateend == undefined  || $scope.search_promotiondateend =="") )
		{
		$scope.callto($scope.Rad.toradio)
		}
		else
		{    	
			$scope.filterPromotions = $filter('filter')($scope.original, function (item) 
					{
					if(item.date_end_promotion != null)
						{
							
							if(angular.equals($filter('date')(item.date_end_promotion, "yyyy-MM-dd"), $filter('date')($scope.search_promotiondateend, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Promotions = $scope.filterPromotions;
			$scope.search();
		}
	}
      
    $scope.changedateend = function (){
    	$scope.lesszero = false;
    	if($scope.Promotion.daysPromotion > 0 && $scope.Promotion.daysPromotion != "")
    		{
				var newdate = new  Date($scope.Promotion.date_start_promotion)
    			$scope.Promotion.date_end_promotion = new Date(newdate.setDate(newdate.getDate()+$scope.Promotion.daysPromotion));
    		}
    	if($scope.Promotion.daysPromotion <= 0 || $scope.Promotion.daysPromotion == null || $scope.Promotion.daysPromotion == undefined)
    		{
    			$scope.lesszero = true;
    			$scope.Promotion.date_end_promotion = "";
    		}
    } 
	
	loadRecords();
	function loadRecords(){
		if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;	
			if(idbusiness != -1)
				{
				  var recordsGet = PromotionCRUDService.getPromotionbybusiness(idbusiness);        
			        recordsGet.then(function (d) {
			        	$scope.Promotions = d.data;
						$scope.search();
						console.log($scope.Promotions)		
			        },
			        function (errorPl) {
			            $log.error('Error ', errorPl);
			        });
			        var recordsGet = BusinessCRUDService.getBusiness(idbusiness)
					recordsGet.then(function (d) {
						$scope.busi = d.data;
					 },
			        function (errorPl) {
			            $log.error('Error ', errorPl);
			        });
				}
				
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	  }	
	
});