'use strict';

//inicio categoria
angular.module('app').controller("CateController", function ($scope, $filter,$window,$location, CategoryCRUDService, ConfigurationCRUDService) {
	    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clear();
		loadRecords();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
    };
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Categorias, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
		});		
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
  $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
	$scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
   
    $scope.searchcategory = function () {
		if ($scope.search_categ == "" || $scope.search_categ == undefined)
			{
				loadRecords();
			}
		else
			{
				$scope.filterCategorias = $filter('filter')($scope.original, function (item) {
							if(item.name_category.toUpperCase().search($scope.search_categ.toUpperCase()) > -1)
							{
								return true;
							}								
						return false;
					});
				
					$scope.Categorias = $scope.filterCategorias;
					$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
    
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	//limpiar registro
    $scope.clear = function () {	
        $scope.IsNewRecord = 1;
        $scope.Cate = {
            idcategory: 0,
            name_category: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			if(d.data[0].recordbypage===null)
				$scope.itemsPerPage = 0;
			else
				 $scope.itemsPerPage = d.data[0].recordbypage;
        var recordsGet = CategoryCRUDService.getAllCategories();
        recordsGet.then(function (d) {
				$scope.Categorias = d.data;
				$scope.original = $scope.Categorias;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
		});
    }
    //guardar prestamos
    $scope.save = function (Cate) {
    	var CateInser = {
    			idcategory: Cate.idcategory,
    			name_category: Cate.name_category,
            };
    	var cateequal = $scope.Categorias.filter(item => item.name_category === CateInser.name_category);
    	if(cateequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "CATEGORY_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
    		//si IsNewRecord == 1 adiciono si no edito
            if ($scope.IsNewRecord === 1) {
                var promisePost = CategoryCRUDService.addCategory(CateInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.Cate.idcategory = d.data.idcategory;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.verMessage = true;
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 2000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });
            } else { //Else Edit the record
                var promisePut = CategoryCRUDService.updateCategory($scope.Cate.idcategory,$scope.Cate.name_category );
                promisePut.then(function (d) {
                    loadRecords();
                    $scope.Message = "SUCCESSFULY_EDIT";
                    $scope.verMessage = true;
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 2000);
                               
                })();
                }, function (err) {
                    $scope.errorMessage = "ERROR_DATA_EDIT";
                    console.log(err);
                    $scope.verMessageError = true;
                });
            }
    		}
        
    };
    //Eliminar
    $scope.delete = function (Category) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    			    var promiseDelete = CategoryCRUDService.deleteCategory(Category.idcategory);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 3000);
    	                               
    	                })();                
    	            }, function (err) {
    	                $scope.errorMessage = "CATEGORY_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }
    		  
    		});        
    }
    //detalles de uno
    $scope.get = function (Category) {
        var promiseGetSingle = CategoryCRUDService.getCategory(Category.idcategory);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Cate = {
                    idcategory: res.idcategory,
                    name_category: res.name_category
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "CATEGORY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});

angular.module('app').service('CategoryCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getCategory = function getCategory(idcategory){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/categories/'+idcategory
        });
	}
    this.goIndex1 = function goIndex1(){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/index1/'
          });
    }
	
    this.addCategory = function addCategory(Category){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/categories',
          data: Category
        });
    }
	
    this.deleteCategory = function deleteCategory(idcategory){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/categories/'+idcategory
        })
    }
	
    this.updateCategory = function updateCategory(idcategory,name_category){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/categories/'+idcategory,
          data: { idcategory:idcategory, name_category:name_category}
        })
    }
	
    this.getAllCategories = function getAllCategories(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/categories'
        });
    }
    this.findall = function findall(desccategory){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/categories/findall/'+desccategory
        });
    }

}]);
//fin categoria

//controlador Configuration
angular.module('app').controller("ConfigurationController", function ($scope,$window,$location, $rootScope, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
    };
    $scope.back = function () {
		$window.history.back(-1);	
	}
	
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.Configuration = {
        		id_configuration: 0,
    			latitude: 0,
    			longitude: 0,
    			ptos_star:0,
				daysfuncionality:0,
				recordbypage: 0,
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
        $scope.clear();
        var recordsGet = ConfigurationCRUDService.getAllConfiguration();
        recordsGet.then(function (d) {
                $scope.configurations = d.data
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
    }
    //guardar prestamos
    $scope.save = function (Configuration) {
    	var ConfigurationInser = {
    			id_configuration: Configuration.id_configuration,
    			latitude: Configuration.latitude,
    			longitude: Configuration.longitude,
    			ptos_star:Configuration.ptos_star,
				daysfuncionality:Configuration.daysfuncionality,
				recordbypage : Configuration.recordbypage,
            };
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
        	console.log(ConfigurationInser);
            var promisePost = ConfigurationCRUDService.addConfiguration(ConfigurationInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Configuration.id_configuration = d.data.id_configuration;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = ConfigurationCRUDService.updateConfiguration(Configuration);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    };
    //Eliminar

    $scope.delete = function (Configuration) {
    	swal({
  		  title: "",
  		  text: $filter('translate')("WANT_DELETE"),
  		  type: "error",
  		  showCancelButton: true,
  		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
  		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
  		  confirmButtonText: $filter('translate')("DELETE"),
  		  cancelButtonText: $filter('translate')("CANCEL"),
  		  closeOnConfirm: true,
  		  closeOnCancel: true
  		},
  		function(isConfirm){
  			if (isConfirm) {
  				 var promiseDelete = ConfigurationCRUDService.deleteConfiguration(Configuration);
  	            promiseDelete.then(function (d) {  
  	            	loadRecords();           	
  	                $scope.Message = "SUCCESSFULY_DELETE";
  	                $scope.verMessage = true;
  	                (function next() {                   
  	                        setTimeout(function () {
  	                        	$scope.verMessage = false;
  	                            $scope.$digest();
  	                        }, 2000);
  	                               
  	                })();
  	            }, function (err) {
  	                $scope.errorMessage = "CATEGORY_ERR_DELETE";
  	                console.log(err);
  	                $scope.verMessageError = true;
  	            });
  			  }  		  
  		});  
       
    }
    //detalles de uno
    $scope.get = function (Configuration) {
        var promiseGetSingle = ConfigurationCRUDService.getConfiguration(Configuration);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                console.log(d.data)
                $scope.Configuration = {
                		id_configuration: res.id_configuration,
                		latitude: res.latitude,
                		longitude: res.longitude,
                		ptos_star: res.ptos_star,
						daysfuncionality:res.daysfuncionality,
						recordbypage : res.recordbypage,
                };
                console.log($scope.Configuration)
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "CATEGORY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('ConfigurationCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getConfiguration = function getConfiguration(Configuration){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/configuration/'+Configuration.id_configuration
        });
	}
	
    this.addConfiguration = function addConfiguration(Configuration){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/configuration',
          data: Configuration
        });
    }
	
    this.deleteConfiguration = function deleteConfiguration(Configuration){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/configuration/'+Configuration.id_configuration
        })
    }
	
    this.updateConfiguration = function updateConfiguration(Configuration){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/configuration/'+Configuration.id_configuration,
          data: Configuration
        })
    }
	
    this.getAllConfiguration = function getAllConfiguration(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/configuration'
        });
    }

}]);
//fin configuration
//Inicio paquete puntos
angular.module('app').controller("PointPackageController", function ($scope, $filter,$window,$location, PointPackageCRUDService,ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
    };
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Paquetes, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
	// paginado fin
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
	$scope.groupedItems = [];
	$scope.itemsPerPage = 0;
	$scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.PointPackage = {
        		id_package: 0,
        		point_value: 0,
        		cost: 0,
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
				$scope.itemsPerPage = d.data[0].recordbypage;
				
				var recordsGet = PointPackageCRUDService.getAllPointPackage();
				recordsGet.then(function (d) {
						$scope.Paquetes = d.data;
						console.log($scope.Paquetes)
						$scope.original = $scope.Paquetes;
						$scope.search();				
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
		});
        
    }
    //guardar prestamos
    $scope.save = function (PointPackage) {
    	var PointPackageInser = {
    			id_package: PointPackage.id_package,
        		point_value: PointPackage.point_value,
        		cost: PointPackage.cost,
            };
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
            var promisePost = PointPackageCRUDService.addPointPackage(PointPackageInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.PointPackage.id_package = d.data.id_package;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = PointPackageCRUDService.updatePointPackage($scope.PointPackage );
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    };
    //Eliminar
    $scope.delete = function (PointPackage) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = PointPackageCRUDService.deletePointPackage(PointPackage);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 2000);
    	                               
    	                })();

    	            }, function (err) {
    	                $scope.errorMessage = "PACKAGE_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (PointPackage) {
        var promiseGetSingle = PointPackageCRUDService.getPointPackage(PointPackage);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.PointPackage = {
                    id_package: res.id_package,
            		point_value: res.point_value,
            		cost: res.cost,
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "PACKAGE_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('PointPackageCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getPointPackage = function getPointPackage(PointPackage){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/pointpackage/'+PointPackage.id_package
        });
	}
    this.getPointPackageid = function getPointPackageid(id_package){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/pointpackage/'+id_package
        });
	}
	
    this.addPointPackage = function addPointPackage(PointPackage){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/pointpackage',
          data: PointPackage
        });
    }
	
    this.deletePointPackage = function deletePointPackage(PointPackage){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/pointpackage/'+PointPackage.id_package
        })
    }
	
    this.updatePointPackage = function updatePointPackage(PointPackage){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/pointpackage/'+PointPackage.id_package,
          data: PointPackage
        })
    }
	
    this.getAllPointPackage = function getAllPointPackage(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/pointpackage'
        });
    }

}]);
//fin paquete puntos

//country inicio****************************
angular.module('app').controller("CountryController", function ($scope,$filter,$window,$location, CountryCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clear();		
		$scope.showModal = false;
		
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Paises, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin	

    $scope.searchcountry = function () {
		if ($scope.search_pais == "" || $scope.search_pais == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterPaises = $filter('filter')($scope.original, function (item) {	
					if(item.name_country.toUpperCase().search($scope.search_pais.toUpperCase()) > -1)
							{
								return true;
							}
														
					return false;
				});
		$scope.Paises = $scope.filterPaises;
		$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
		
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.Country = {
        		idcountry: 0,
        		name_country: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
			var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				$scope.itemsPerPage = d.data[0].recordbypage;

				var recordsGet = CountryCRUDService.getAllCountry();
				recordsGet.then(function (d) {
						$scope.Paises = d.data;
						$scope.original = $scope.Paises;
						$scope.search();
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			});
    }
    //guardar prestamos
    $scope.save = function (Countr) {
    	
    	var CountryInser = {
    			idcountry: Countr.idcountry,
    			name_country: Countr.name_country,
            };
        //si IsNewRecord == 1 adiciono si no edito
    	var countryequal = $scope.Paises.filter(item => item.name_country === CountryInser.name_country);
    	if(countryequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "COUNTRY_EXIST";
            $scope.verMessageError = true;
    		}
    	else
    		{
    		if ($scope.IsNewRecord === 1) {
        		var promisePost = CountryCRUDService.addCountry(CountryInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.Country.idcountry = d.data.idcountry;
                    $scope.verMessage = true;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });        		
        	
        } else { //Else Edit the record
        	
            var promisePut = CountryCRUDService.updateCountry($scope.Country.idcountry,$scope.Country.name_country );
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
        
    };
    //Eliminar
    $scope.delete = function (Country) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = CountryCRUDService.deleteCountry(Country.idcountry);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                        }, 3000);
    		                               
    		                })();

    		            }, function (err) {
    		                $scope.errorMessage = "COUNTRY_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (Country) {
        var promiseGetSingle = CountryCRUDService.getCountry(Country.idcountry);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Country = {
                		idcountry: res.idcountry,
                		name_country: res.name_country
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "COUNTRY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('CountryCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getCountry = function getCountry(idcountry){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/countries/'+idcountry
        });
	}
    this.findall = function findall(descountry){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/countries/findall/'+descountry
        });
	}
	
    this.addCountry = function addCountry(Country){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/countries',
          data: Country
        });
    }
	
    this.deleteCountry = function deleteCountry(idcountry){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/countries/'+idcountry
        })
    }
	
    this.updateCountry = function updateCountry(idcountry,name_country){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/countries/'+idcountry,
          data: {idcountry, name_country:name_country}
        })
    }
	
    this.getAllCountry = function getAllCountry(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/countries'
        });
    }

}]);
//country fin

//ScheduleJobOffer inicio****************************
angular.module('app').controller("ScheduleJobOfferController", function ($scope,$filter,$window,$location, ScheduleJobOfferCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clear();		
		$scope.showModal = false;
		
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.ScheduleJobOffers, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin	

    $scope.searchdesc_schedulejob = function () {
		if ($scope.desc_schedulejob == "" || $scope.desc_schedulejob == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterScheduleJobOffers = $filter('filter')($scope.original, function (item) {	
					if(item.desc_schedu.toUpperCase().search($scope.desc_schedulejob.toUpperCase()) > -1)
							{
								return true;
							}
														
					return false;
				});
		$scope.ScheduleJobOffers = $scope.filterScheduleJobOffers;
		$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
		
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.ScheduleJobOffer = {
        		idschedulejoboffer: 0,
        		desc_schedu: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
			var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				$scope.itemsPerPage = d.data[0].recordbypage;

				var recordsGet = ScheduleJobOfferCRUDService.getAllScheduleJobOffer();
				recordsGet.then(function (d) {
						$scope.ScheduleJobOffers = d.data;
						$scope.original = $scope.ScheduleJobOffers;
						$scope.search();
						console.log($scope.ScheduleJobOffers)
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			});
    }
    //guardar prestamos
    $scope.save = function (ScheduleJobOfferRequest) {
    	
    	var ScheduleJobOfferInser = {
    			idschedulejoboffer: ScheduleJobOfferRequest.idschedulejoboffer,
    			desc_schedu: ScheduleJobOfferRequest.desc_schedu,
            };
        //si IsNewRecord == 1 adiciono si no edito
    	var schedujobequal = $scope.ScheduleJobOffers.filter(item => item.desc_schedu === ScheduleJobOfferInser.desc_schedu);
    	if(schedujobequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "SCHEDULEJOBOFFER_EXIST";
            $scope.verMessageError = true;
    		}
    	else
    		{
    		if ($scope.IsNewRecord === 1) {
        		var promisePost = ScheduleJobOfferCRUDService.addScheduleJobOffer(ScheduleJobOfferInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.ScheduleJobOffer.idschedulejoboffer = d.data.idschedulejoboffer;
                    $scope.verMessage = true;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });        		
        	
        } else { //Else Edit the record
        	
            var promisePut = ScheduleJobOfferCRUDService.updateScheduleJobOffer(ScheduleJobOfferInser);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
        
    };
    //Eliminar
    $scope.delete = function (ScheduleJobOffer) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = ScheduleJobOfferCRUDService.deleteScheduleJobOffer(ScheduleJobOffer.idschedulejoboffer);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                        }, 3000);
    		                               
    		                })();

    		            }, function (err) {
    		                $scope.errorMessage = "SCHEDULEJOBOFFER_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (ScheduleJobOfferRequ) {
    	 $scope.IsNewRecord = 0;
    	 $scope.showModal = true;
    	 $scope.verMessage = false;
    	 
    	console.log(ScheduleJobOfferRequ)
    	 $scope.ScheduleJobOffer = {
                		idschedulejoboffer: ScheduleJobOfferRequ.idschedulejoboffer,
                		desc_schedu: ScheduleJobOfferRequ.desc_schedu
                };       
    }
});
angular.module('app').service('ScheduleJobOfferCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getScheduleJobOffer = function getScheduleJobOffer(idschedulejoboffer){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/schedulejoboffer/'+idschedulejoboffer
        });
	}
	
    this.addScheduleJobOffer = function addScheduleJobOffer(ScheduleJobOffer){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/schedulejoboffer',
          data: ScheduleJobOffer
        });
    }
	
    this.deleteScheduleJobOffer = function deleteScheduleJobOffer(idschedulejoboffer){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/schedulejoboffer/'+idschedulejoboffer
        })
    }
	
    this.updateScheduleJobOffer= function updateScheduleJobOffer(ScheduleJobOffer){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/schedulejoboffer/'+ScheduleJobOffer.idschedulejoboffer,
          data: ScheduleJobOffer
        })
    }
	
    this.getAllScheduleJobOffer = function getAllScheduleJobOffer(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/schedulejoboffer'
        });
    }

}]);
//ScheduleJobOffer fin
//Schoollevel inicio****************************
angular.module('app').controller("SchoollevelController", function ($scope,$filter,$window,$location, SchoollevelCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clear();		
		$scope.showModal = false;
		
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Schoollevels, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin	

    $scope.searchdesc_schoollevel = function () {
		if ($scope.desc_schoollevelmodel == "" || $scope.desc_schoollevelmodel == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterSchoollevels = $filter('filter')($scope.original, function (item) {	
					if(item.desc_schoollevel.toUpperCase().search($scope.desc_schoollevelmodel.toUpperCase()) > -1)
							{
								return true;
							}
														
					return false;
				});
		$scope.Schoollevels = $scope.filterSchoollevels;
		$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
		
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.Schoollevel = {
        		idschoollevel: 0,
        		desc_schoollevel: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
			var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				$scope.itemsPerPage = d.data[0].recordbypage;

				var recordsGet = SchoollevelCRUDService.getAllSchoollevel();
				recordsGet.then(function (d) {
						$scope.Schoollevels = d.data;
						$scope.original = $scope.Schoollevels;
						$scope.search();
						console.log($scope.Schoollevels)
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			});
    }
    //guardar prestamos
    $scope.save = function (SchoollevelsRequest) {
    	
    	var SchoollevelsInser = {
    			idschoollevel: SchoollevelsRequest.idschoollevel,
    			desc_schoollevel: SchoollevelsRequest.desc_schoollevel,
            };
        //si IsNewRecord == 1 adiciono si no edito
    	var schoollevelequal = $scope.Schoollevels.filter(item => item.desc_schedu === SchoollevelsRequest.desc_schoollevel);
    	if(schoollevelequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "SCHOOLLEVEL_EXIST";
            $scope.verMessageError = true;
    		}
    	else
    		{
    		if ($scope.IsNewRecord === 1) {
        		var promisePost = SchoollevelCRUDService.addSchoollevel(SchoollevelsInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.Schoollevel.idschoollevel = d.data.idschoollevel;
                    $scope.verMessage = true;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });        		
        	
        } else { //Else Edit the record
        	
            var promisePut = SchoollevelCRUDService.updateSchoollevel(SchoollevelsInser);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
        
    };
    //Eliminar
    $scope.delete = function (Schoollevel) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = SchoollevelCRUDService.deleteSchoollevel(Schoollevel.idschoollevel);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                        }, 3000);
    		                               
    		                })();

    		            }, function (err) {
    		                $scope.errorMessage = "SCHEDULEJOBOFFER_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (SchoollevelRequ) {
    	 $scope.IsNewRecord = 0;
    	 $scope.showModal = true;
    	 $scope.verMessage = false;
    	 
    	console.log(SchoollevelRequ)
    	 $scope.Schoollevel = {
			    		idschoollevel: SchoollevelRequ.idschoollevel,
						desc_schoollevel: SchoollevelRequ.desc_schoollevel,
                };       
    }
});
angular.module('app').service('SchoollevelCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getSchoollevel = function getSchoollevel(idschoollevel){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/schoollevel/'+idschoollevel
        });
	}
	
    this.addSchoollevel = function addSchoollevel(Schoollevel){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/schoollevel',
          data: Schoollevel
        });
    }
	
    this.deleteSchoollevel = function deleteSchoollevel(idschoollevel){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/schoollevel/'+idschoollevel
        })
    }
	
    this.updateSchoollevel= function updateSchoollevel(Schoollevel){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/schoollevel/'+Schoollevel.idschoollevel,
          data: Schoollevel
        })
    }
	
    this.getAllSchoollevel = function getAllSchoollevel(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/schoollevel'
        });
    }

}]);
//Schoollevel fin

//ContractType inicio****************************
angular.module('app').controller("ContractTypeController", function ($scope,$filter,$window,$location, ContractTypeCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clear();		
		$scope.showModal = false;
		
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.ContractTypes, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin	

    $scope.searchdesc_contracttype = function () {
		if ($scope.desc_contracttypemodel == "" || $scope.desc_contracttypemodel == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterContractTypes = $filter('filter')($scope.original, function (item) {	
					if(item.desc_contracttype.toUpperCase().search($scope.desc_contracttypemodel.toUpperCase()) > -1)
							{
								return true;
							}
														
					return false;
				});
		$scope.ContractTypes = $scope.filterContractTypes;
		$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
		
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.ContractType = {
        		idcontracttype: 0,
        		desc_contracttype: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
			var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				$scope.itemsPerPage = d.data[0].recordbypage;

				var recordsGet = ContractTypeCRUDService.getAllContractType();
				recordsGet.then(function (d) {
						$scope.ContractTypes = d.data;
						$scope.original = $scope.ContractTypes;
						$scope.search();
						console.log($scope.ContractTypes)
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			});
    }
    //guardar prestamos
    $scope.save = function (ContractTypeRequest) {
    	
    	var ContractTypeInser = {
    			idcontracttype: ContractTypeRequest.idcontracttype,
    			desc_contracttype: ContractTypeRequest.desc_contracttype,
            };
        //si IsNewRecord == 1 adiciono si no edito
    	var contracttypeequal = $scope.ContractTypes.filter(item => item.desc_contracttype === ContractTypeRequest.desc_contracttype);
    	if(contracttypeequal.length > 0  && $scope.IsNewRecord === 1)
    		{
	    		$scope.errorMessage = "CONTRACTTYPE_EXIST";
	            $scope.verMessageError = true;
    		}
    	else
    		{
    		if ($scope.IsNewRecord === 1) {
        		var promisePost = ContractTypeCRUDService.addContractType(ContractTypeInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.ContractType.idcontracttype = d.data.idcontracttype;
                    $scope.verMessage = true;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });        		
        	
        } else { //Else Edit the record
        	
            var promisePut = ContractTypeCRUDService.updateContractType(ContractTypeInser);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
        
    };
    //Eliminar
    $scope.delete = function (ContractType) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = ContractTypeCRUDService.deleteContractType(ContractType.idcontracttype);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                        }, 3000);
    		                               
    		                })();

    		            }, function (err) {
    		                $scope.errorMessage = "SCHEDULEJOBOFFER_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (ContractTypeRequ) {
    	 $scope.IsNewRecord = 0;
    	 $scope.showModal = true;
    	 $scope.verMessage = false;
    	 
    	console.log(ContractTypeRequ)
    	 $scope.ContractType = {
    					idcontracttype: ContractTypeRequ.idcontracttype,
    					desc_contracttype: ContractTypeRequ.desc_contracttype,
                };       
    }
});
angular.module('app').service('ContractTypeCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getContractType = function getContractType(idcontracttype){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/contracttype/'+idcontracttype
        });
	}
	
    this.addContractType = function addContractType(ContractType){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/contracttype',
          data: ContractType
        });
    }
	
    this.deleteContractType = function deleteContractType(idcontracttype){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/contracttype/'+idcontracttype
        })
    }
	
    this.updateContractType= function updateContractType(ContractType){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/contracttype/'+ContractType.idcontracttype,
          data: ContractType
        })
    }
	
    this.getAllContractType = function getAllContractType(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/contracttype'
        });
    }

}]);
//ContractType fin

//ProfessionalExperience inicio****************************
angular.module('app').controller("ProfessionalExperienceController", function ($scope,$filter,$window,$location, ProfessionalExperienceCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clear();		
		$scope.showModal = false;
		
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.ProfessionalExperiences, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin	

    $scope.searchdesc_professionalexperience = function () {
		if ($scope.desc_professionalexperiencemodel == "" || $scope.desc_professionalexperiencemodel == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterProfessionalExperiences = $filter('filter')($scope.original, function (item) {	
					if(item.desc_professionalexperience.toUpperCase().search($scope.desc_professionalexperiencemodel.toUpperCase()) > -1)
							{
								return true;
							}
														
					return false;
				});
		$scope.ProfessionalExperiences = $scope.filterProfessionalExperiences;
		$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
		
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.ProfessionalExperience = {
        		idprofessionalexperience: 0,
        		desc_professionalexperience: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
			var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				$scope.itemsPerPage = d.data[0].recordbypage;

				var recordsGet = ProfessionalExperienceCRUDService.getAllProfessionalExperience();
				recordsGet.then(function (d) {
						$scope.ProfessionalExperiences = d.data;
						$scope.original = $scope.ProfessionalExperiences;
						$scope.search();
						console.log($scope.ProfessionalExperiences)
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			});
    }
    //guardar prestamos
    $scope.save = function (ProfessionalExperienceRequest) {
    	
    	var ProfessionalExperienceInser = {
    			idprofessionalexperience: ProfessionalExperienceRequest.idprofessionalexperience,
    			desc_professionalexperience: ProfessionalExperienceRequest.desc_professionalexperience,
            };
        //si IsNewRecord == 1 adiciono si no edito
    	var professionalexperienceequal = $scope.ProfessionalExperiences.filter(item => item.desc_professionalexperience === ProfessionalExperienceRequest.desc_professionalexperience);
    	if(professionalexperienceequal.length > 0  && $scope.IsNewRecord === 1)
    		{
	    		$scope.errorMessage = "PROFESSIONALEXPERIENCE_EXIST";
	            $scope.verMessageError = true;
    		}
    	else
    		{
    		if ($scope.IsNewRecord === 1) {
        		var promisePost = ProfessionalExperienceCRUDService.addProfessionalExperience(ProfessionalExperienceInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.ProfessionalExperience.idprofessionalexperience = d.data.idprofessionalexperience;
                    $scope.verMessage = true;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });        		
        	
        } else { //Else Edit the record
        	
            var promisePut = ProfessionalExperienceCRUDService.updateProfessionalExperience(ProfessionalExperienceInser);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
        
    };
    //Eliminar
    $scope.delete = function (ProfessionalExperience) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = ProfessionalExperienceCRUDService.deleteProfessionalExperience(ProfessionalExperience.idprofessionalexperience);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                        }, 3000);
    		                               
    		                })();

    		            }, function (err) {
    		                $scope.errorMessage = "SCHEDULEJOBOFFER_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (ProfessionalExperienceRequ) {
    	 $scope.IsNewRecord = 0;
    	 $scope.showModal = true;
    	 $scope.verMessage = false;
    	 
    	console.log(ProfessionalExperienceRequ)
    	 $scope.ProfessionalExperience = {
		    		idprofessionalexperience: ProfessionalExperienceRequ.idprofessionalexperience,
		    		desc_professionalexperience: ProfessionalExperienceRequ.desc_professionalexperience,
                };       
    }
});
angular.module('app').service('ProfessionalExperienceCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getProfessionalExperience = function getProfessionalExperience(idprofessionalexperience){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/professionalexperience/'+idprofessionalexperience
        });
	}
	
    this.addProfessionalExperience = function addProfessionalExperience(ProfessionalExperience){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/professionalexperience',
          data: ProfessionalExperience
        });
    }
	
    this.deleteProfessionalExperience = function deleteProfessionalExperience(idprofessionalexperience){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/professionalexperience/'+idprofessionalexperience
        })
    }
	
    this.updateProfessionalExperience= function updateProfessionalExperience(ProfessionalExperience){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/professionalexperience/'+ProfessionalExperience.idprofessionalexperience,
          data: ProfessionalExperience
        })
    }
	
    this.getAllProfessionalExperience = function getAllProfessionalExperience(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/professionalexperience'
        });
    }

}]);
//ProfessionalExperience fin

//rol inicio****************************
angular.module('app').controller("RolController", function ($scope, $filter,$window,$location, RolCRUDService,ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Roles, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin

	$scope.searchrol = function () {
		if ($scope.search_Rol == "" || $scope.search_Rol == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterRoles = $filter('filter')($scope.original, function (item) {
				if($scope.search_Rol != undefined || $scope.search_Rol != "" )
					{
						if(item.desc_rol.toUpperCase().search($scope.search_Rol.toUpperCase()) > -1)
						{
							return true;
						}
					}								
					return false;
				});
			
				$scope.Roles = $scope.filterRoles;
				$scope.search();
			}		
	}
	$scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.Rol = {
        		idrol: 0,
        		desc_rol: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;
		
        var recordsGet = RolCRUDService.getAllRol();
        recordsGet.then(function (d) {
				$scope.Roles = d.data;
				$scope.original = $scope.Roles;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			
			});
    }
    //guardar prestamos
    $scope.save = function (Rol) {
    	
    	var RolInser = {
    			idrol: Rol.idrol,
    			desc_rol: Rol.desc_rol,
            };
    	var rolequal = $scope.Roles.filter(item => item.desc_rol === RolInser.desc_rol);
    	if(rolequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "ROL_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {          
            var promisePost = RolCRUDService.addRol(RolInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Rol.idrol = d.data.idrol;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
        	
            var promisePut = RolCRUDService.updateRol($scope.Rol.idrol,$scope.Rol.desc_rol );
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (Rol) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = RolCRUDService.deleteRol(Rol.idrol);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 3000);
    	                               
    	                })();

    	               
    	                
    	            }, function (err) {
    	                $scope.errorMessage = "ROL_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		});     	
       
    }
    //detalles de uno
    $scope.get = function (Rol) {
        var promiseGetSingle = RolCRUDService.getRol(Rol.idrol);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Rol = {
                		idrol: res.idrol,
                		desc_rol: res.desc_rol
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "ROL_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('RolCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getRol = function getRol(idrol){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/roles/'+idrol
        });
	}
	
    this.addRol = function addRol(Rol){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/roles',
          data: Rol
        });
    }
	
    this.deleteRol = function deleteRol(idrol){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/roles/'+idrol
        })
    }
	
    this.updateRol = function updateRol(idrol,desc_rol){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/roles/'+idrol,
          data: {idrol, desc_rol: desc_rol}
        })
    }
	
    this.getAllRol = function getAllRol(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/roles'
        });
    }

}]);
//rol fin

//typenotification inicio****************************
angular.module('app').controller("TypeNotificationController", function ($scope, $filter,$window,$location, TypeNotificationCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.TipoNotificaciones, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    
    $scope.searchnotitype = function () {
    	if (($scope.search_desc_notification_type == undefined || $scope.search_desc_notification_type == "") )
		{
			loadRecords();
		}
	else
		{
		$scope.filterTipoNotificaciones = $filter('filter')($scope.original, function (item) {
					if(item.desc_notification_type.toUpperCase().search($scope.search_desc_notification_type.toUpperCase()) > -1)
					{
						return true;
					}					
					return false;
				});
		$scope.TipoNotificaciones = $scope.filterTipoNotificaciones;
		$scope.search();
		}			
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {

        $scope.IsNewRecord = 1;
        $scope.TypeNotification = {
        		idnotifications_type: 0,
        		desc_notification_type: "",
        		name_notification_type:"",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = TypeNotificationCRUDService.getAllTypeNotification();
        recordsGet.then(function (d) {
				$scope.TipoNotificaciones = d.data;
				$scope.original = $scope.TipoNotificaciones;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
		});
    }
    //guardar prestamos
    $scope.save = function (TypeNotification) {
    	var TypeNotificationInser = {  
    		idnotifications_type:TypeNotification.idnotifications_type,
    			desc_notification_type: TypeNotification.desc_notification_type,
    			name_notification_type: TypeNotification. name_notification_type,
            };
    	var notitypeequal = $scope.TipoNotificaciones.filter(item => item.desc_notification_type === TypeNotificationInser.desc_notification_type);
    	
    	if(notitypeequal.length > 0  && $scope.IsNewRecord === 1)
    		{
            $scope.verMessageError = true;
            $scope.errorMessage = "NOTIFICATION_TYPE_EXISTS";
    		}
    	else
    		{
    		//si IsNewRecord == 1 adiciono si no edito
            if ($scope.IsNewRecord === 1) {
                var promisePost = TypeNotificationCRUDService.addTypeNotification(TypeNotificationInser);
                promisePost.then(function (d) {
                    loadRecords();
                    $scope.TypeNotification.idnotifications_type = d.data.idnotifications_type;
                    $scope.Message = "SUCCESSFULY_SAVE";
                    $scope.verMessage = true;
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA_SAVE";
                    $scope.verMessageError = true;
                });
            } else { //Else Edit the record
                var promisePut = TypeNotificationCRUDService.updateTypeNotification($scope.TypeNotification);
                promisePut.then(function (d) {
                    loadRecords();
                    $scope.Message = "SUCCESSFULY_EDIT";
                    $scope.verMessage = true;
                    $scope.showModal = false;
                    (function next() {                   
                        setTimeout(function () {
                        	$scope.verMessage = false;
                            $scope.$digest();
                        }, 3000);
                               
                })();
                }, function (err) {
                    $scope.errorMessage = "SUCCESSFULY_EDIT";
                    console.log(err);
                    $scope.verMessageError = true;
                });
            }
    		}
    };
    //Eliminar
    $scope.delete = function (TypeNotification) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				  var promiseDelete = TypeNotificationCRUDService.deleteTypeNotification(TypeNotification.idnotifications_type);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                        }, 3000);
    		                               
    		                })();                
    		            }, function (err) {
    		                $scope.errorMessage = "NOTIFICATION_TYPE_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
  //detalles de uno
    $scope.get = function (TypeNotification) {
        var promiseGetSingle = TypeNotificationCRUDService.getTypeNotification(TypeNotification.idnotifications_type);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.TypeNotification = {
                		idnotifications_type: res.idnotifications_type,
                		desc_notification_type: res.desc_notification_type,
                		name_notification_type: res.name_notification_type
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "NOTIFICATION_TYPE_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('TypeNotificationCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getTypeNotification = function getTypeNotification(idnotifications_type){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/typeNotification/'+idnotifications_type
        });
	}
   
    this.addTypeNotification = function addTypeNotification(TypeNotification){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/typeNotification',
          data: TypeNotification
        });
    }
	
    this.deleteTypeNotification = function deleteTypeNotification(idnotifications_type){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/typeNotification/'+idnotifications_type
        })
    }
	
    this.updateTypeNotification = function updateTypeNotification(notification_type){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/typeNotification/'+notification_type.idnotifications_type,
          data: notification_type
        })
    }
	
    this.getAllTypeNotification = function getAllTypeNotification(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/typeNotification'
        });
    }

}]);
//typenotification fin

//userstate inicio****************************
angular.module('app').controller("UserStatesController", function ($scope, $filter,$window,$location, UserStatesCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.UserStatess, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
  $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    $scope.searchuserstate = function () {
		if ( ($scope.search_name_user_state == undefined  || $scope.search_name_user_state =="") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterUserStatess = $filter('filter')($scope.original, function (item) {
							if(item.name_user_state.toUpperCase().search($scope.search_name_user_state.toUpperCase()) > -1)
								{
									return true;
								}							
						return false;
					});
			$scope.UserStatess = $scope.filterUserStatess;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 10;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {

        $scope.IsNewRecord = 1;
        $scope.UserStates = {
        		iduser_state: 0,
        		name_user_state: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = UserStatesCRUDService.getAllUserStates();
        recordsGet.then(function (d) {
				$scope.UserStatess = d.data;
				$scope.original = $scope.UserStatess;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
		});
    }
    //guardar prestamos
    $scope.save = function (UserStates) {
    	
    	var UserStatesInser = {  
    			iduser_state:UserStates.iduser_state,
    			name_user_state: UserStates.name_user_state,
            };
    	var userstateequal = $scope.UserStatess.filter(item => item.name_user_state === UserStatesInser.name_user_state);
    	if(userstateequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "USER_STATE_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
            var promisePost = UserStatesCRUDService.addUserStates(UserStatesInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.UserStates.iduser_state = d.data.iduser_state;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = UserStatesCRUDService.updateUserStates($scope.UserStates.iduser_state,$scope.UserStates.name_user_state);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (UserStates) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = UserStatesCRUDService.deleteUserStates(UserStates.iduser_state);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 3000);
    	                               
    	                })();               
    	                
    	            }, function (err) {
    	                $scope.errorMessage = "NOTIFICATION_TYPE_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		}); 
       
    }
  //detalles de uno
    $scope.get = function (UserStates) {
        var promiseGetSingle = UserStatesCRUDService.getUserStates(UserStates.iduser_state);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.UserStates = {
                		iduser_state: res.iduser_state,
                		name_user_state: res.name_user_state
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "NOTIFICATION_TYPE_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('UserStatesCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getUserStates = function getUserStates(iduser_state){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/usersStates/'+iduser_state
        });
	}
   
    this.addUserStates = function addUserStates(UserStates){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/usersStates',
          data: UserStates
        });
    }
	
    this.deleteUserStates = function deleteUserStates(iduser_state){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/usersStates/'+iduser_state
        })
    }
	
    this.updateUserStates = function updateUserStates(iduser_state,name_user_state){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/usersStates/'+iduser_state,
          data: {iduser_state:iduser_state, name_user_state:name_user_state}
        })
    }
	
    this.getAllUserStates = function getAllUserStates(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/usersStates'
        });
    }

}]);
//userstate fin

//busisnestate inicio****************************
angular.module('app').controller("BusinessStatesController", function ($scope, $filter,$window,$location, BusinessStatesCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.BusinessStatess, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
      
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
        if($scope.pagedItems.length < $scope.itemsPerPage)
        	{
        	$scope.setPage(0);
        	}        
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
    
	// paginado fin
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

	$scope.searchbusinesstate = function () {
		if ($scope.search_name_state_business == "" || $scope.search_name_state_business == undefined)
			{
				loadRecords();
			}
		else
			{
			$scope.filterBusinessStatess = $filter('filter')($scope.original, function (item) {
				if($scope.search_name_state_business != undefined || $scope.search_name_state_business != "" )
					{
						if(item.name_state_business.toUpperCase().search($scope.search_name_state_business.toUpperCase()) > -1)
						{
							return true;
						}
					}								
					return false;
				});
			
				$scope.BusinessStatess = $scope.filterBusinessStatess;
				$scope.search();
			}		
	}
	$scope.back = function () {
		$window.history.back(-1);	
	}
	
    //limpiar registro
    $scope.clear = function () {
	
        $scope.IsNewRecord = 1;
        $scope.BusinessStates = {
        		idstate_business: 0,
        		name_state_business: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
	//cargar todos los prestamos

    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = BusinessStatesCRUDService.getAllBusinessStates();
        recordsGet.then(function (d) {
				$scope.BusinessStatess = d.data
				$scope.original = $scope.BusinessStatess;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			},
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
    }
    //guardar prestamos
    $scope.save = function (BusinessStates) {
    	
    	var BusinessStatesInser = {  
    			idstate_business:BusinessStates.idstate_business,
    			name_state_business: BusinessStates.name_state_business,
            };
    	var bustatesequal = $scope.BusinessStatess.filter(item => item.name_state_business === BusinessStatesInser.name_state_business);
    	if(bustatesequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "BUSINESS_STATE_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
        	console.log(BusinessStatesInser.idstate_business,BusinessStatesInser.name_state_business);
            var promisePost = BusinessStatesCRUDService.addBusinessStates(BusinessStatesInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.BusinessStates.idstate_business = d.data.idstate_business;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = BusinessStatesCRUDService.updateBusinessStates($scope.BusinessStates.idstate_business,$scope.BusinessStates.name_state_business);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (BusinessStates) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = BusinessStatesCRUDService.deleteBusinessStates(BusinessStates.idstate_business);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 3000);
    	                               
    	                })();               
    	                
    	            }, function (err) {
    	                $scope.errorMessage = "USER_STATE_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		});        
    }
  //detalles de uno
    $scope.get = function (BusinessStates) {
        var promiseGetSingle = BusinessStatesCRUDService.getBusinessStates(BusinessStates.idstate_business);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.BusinessStates = {
                		idstate_business: res.idstate_business,
                		name_state_business: res.name_state_business
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "USER_STATE_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('BusinessStatesCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getBusinessStates = function getBusinessStates(idstate_business){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/businessStates/'+idstate_business
        });
	}
   
    this.addBusinessStates = function addBusinessStates(BusinessStates){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/businessStates',
          data: BusinessStates
        });
    }
	
    this.deleteBusinessStates = function deleteBusinessStates(idstate_business){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/businessStates/'+idstate_business
        })
    }
	
    this.updateBusinessStates = function updateBusinessStates(idstate_business,name_state_business){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/businessStates/'+idstate_business,
          data: {idstate_business:idstate_business, name_state_business:name_state_business}
        })
    }
	
    this.getAllBusinessStates = function getAllBusinessStates(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/businessStates'
        });
    }
    this.findall = function findall(descbusinessstate){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/businessStates/findall/'+descbusinessstate
        });
    }

}]);
//businessStates fin

//paymentforms inicio****************************
angular.module('app').controller("PaymentFormsController", function ($scope, $filter,$window,$location, PaymentFormsCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.PaymentFormss, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    
    $scope.searchpayment = function () {
		if ( ($scope.search_name_payment_form == undefined  || $scope.search_name_payment_form =="") && ($scope.desc_payment_form == undefined || $scope.desc_payment_form == "") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterPaymentFormss = $filter('filter')($scope.original, function (item) {				
					if(($scope.search_name_payment_form != undefined  ) && ($scope.desc_payment_form != undefined ) )
						{
						
							if(item.name_payment_form.toUpperCase().search($scope.search_name_payment_form.toUpperCase()) > -1 && item.desc_payment_form.toUpperCase().search($scope.desc_payment_form.toUpperCase()) > -1)
							{
								return true;
							}
						}
						if(($scope.search_name_payment_form != undefined || $scope.search_name_payment_form !="")  && ($scope.desc_payment_form == undefined || $scope.desc_payment_form == ""))
							{
							if(item.name_payment_form.toUpperCase().search($scope.search_name_payment_form.toUpperCase()) > -1)
								{
									return true;
								}
							}
						if(($scope.desc_payment_form != undefined || $scope.desc_payment_form != "") && ($scope.search_name_payment_form == undefined || $scope.search_name_payment_form =="") )
						{
							if(item.desc_payment_form.toUpperCase().search($scope.desc_payment_form.toUpperCase()) > -1)
							{
								return true;
							}
						}								
						return false;
					});
			$scope.PaymentFormss = $scope.filterPaymentFormss;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.PaymentForms = {
        		idpayment_form: 0,
                name_payment_form: "",
                desc_payment_form: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = PaymentFormsCRUDService.getAllPaymentForms();
        recordsGet.then(function (d) {
				$scope.PaymentFormss = d.data;
				$scope.original = $scope.PaymentFormss;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			});
    }
    //guardar prestamos
    $scope.save = function (PaymentForms) {
    	
    	var PaymentFormsInser = {  
    			idpayment_form: PaymentForms.idpayment_form,
                name_payment_form: PaymentForms.name_payment_form,
                desc_payment_form: PaymentForms.desc_payment_form,
            };
    	var Pformequal = $scope.PaymentFormss.filter(item => item.name_payment_form === PaymentFormsInser.name_payment_form);
    	if(Pformequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "PAYMENT_FORM_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
        	
            var promisePost = PaymentFormsCRUDService.addPaymentForms(PaymentFormsInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.PaymentForms.idpayment_form = d.data.idpayment_form;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = PaymentFormsCRUDService.updatePaymentForms($scope.PaymentForms.idpayment_form,$scope.PaymentForms.name_payment_form,$scope.PaymentForms.desc_payment_form);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (PaymentForms) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = PaymentFormsCRUDService.deletePaymentForms(PaymentForms.idpayment_form);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 3000);
    	                               
    	                })();               
    	                
    	            }, function (err) {
    	                $scope.errorMessage = "PAYMENT_FORM_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		}); 
       
    }
  //detalles de uno
    $scope.get = function (PaymentForms) {
        var promiseGetSingle = PaymentFormsCRUDService.getPaymentForms(PaymentForms.idpayment_form);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.PaymentForms = {
                		idpayment_form: res.idpayment_form,
                        name_payment_form: res.name_payment_form,
                        desc_payment_form: res.desc_payment_form,
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "PAYMENT_FORM_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('PaymentFormsCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getPaymentForms = function getPaymentForms(idpayment_form){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/paymentForms/'+idpayment_form
        });
	}
   
    this.addPaymentForms = function addPaymentForms(PaymentForms){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/paymentForms',
          data: PaymentForms
        });
    }
	
    this.deletePaymentForms = function deletePaymentForms(idpayment_form){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/paymentForms/'+idpayment_form
        })
    }
	
    this.updatePaymentForms = function updatePaymentForms(idpayment_form,name_payment_form,desc_payment_form){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/paymentForms/'+idpayment_form,
          data: {idpayment_form: idpayment_form, name_payment_form: name_payment_form,desc_payment_form: desc_payment_form }
        })
    }
	
    this.getAllPaymentForms = function getAllPaymentForms(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/paymentForms'
        });
    }

}]);
//PaymentFormss fin

// SubCategoia inicio****************************
angular.module('app').controller("SubcategoryController", function ($scope, $filter,$window,$location, SubcategoryCRUDService, CategoryCRUDService, ConfigurationCRUDService) {
	
	
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.SubCategorias, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
  $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    $scope.searchsub = function () {
		if ( ($scope.search_name_subcategory == undefined  || $scope.search_name_subcategory =="") && ($scope.search_name_category == undefined || $scope.search_name_category == "") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterSubCategorias = $filter('filter')($scope.original, function (item) {				
					if(($scope.search_name_subcategory != undefined  ) && ($scope.search_name_category != undefined ) )
						{						
							if(item.name_subcategory.toUpperCase().search($scope.search_name_subcategory.toUpperCase()) > -1 && item.category.name_category.toUpperCase().search($scope.search_name_category.toUpperCase()) > -1)
							{
								return true;
							}
						}
						if(($scope.search_name_subcategory != undefined || $scope.search_name_subcategory !="")  && ($scope.search_name_category == undefined || $scope.search_name_category == ""))
							{
							if(item.name_subcategory.toUpperCase().search($scope.search_name_subcategory.toUpperCase()) > -1)
								{
									return true;
								}
							}
						if(($scope.search_name_category != undefined || $scope.search_name_category != "") && ($scope.search_name_subcategory == undefined || $scope.search_name_subcategory =="") )
						{
							if(item.category.name_category.toUpperCase().search($scope.search_name_category.toUpperCase()) > -1)
							{
								return true;
							}
						}								
						return false;
					});
			$scope.SubCategorias = $scope.filterSubCategorias;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.Subcategory = {
        		idsubcategory: 0,
                Category: $scope.primeracategoria,
                name_subcategory: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = SubcategoryCRUDService.getAllSubcategory();
        recordsGet.then(function (d) {
				$scope.SubCategorias = d.data;
				console.log($scope.SubCategorias)
				$scope.original = $scope.SubCategorias;
				$scope.search();
                
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
        var recordsGet = CategoryCRUDService.getAllCategories();
        recordsGet.then(function (d) {
                $scope.Categorias = d.data;
                if (d.data.length!=0) {
                     $scope.primeracategoria=d.data[0].idcategory;
                } else {
                    $scope.primeracategoria=0;
                }
               
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			});

    }
    //guardar prestamos
    $scope.save = function (Subcategory) {
    	var SubcategoryInser = {  
    			idsubcategory: Subcategory.idsubcategory,
                Category: Subcategory.Category,
                name_subcategory: Subcategory.name_subcategory,
            };
    	var subcateequal = $scope.SubCategorias.filter(item => item.name_subcategory === SubcategoryInser.name_subcategory);
    
    	if(subcateequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "SUBCATEGORY_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
        	
            var promisePost = SubcategoryCRUDService.addSubcategory(SubcategoryInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Subcategory.idsubcategory = d.data.idsubcategory;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = SubcategoryCRUDService.updateSubcategory($scope.Subcategory.idsubcategory,$scope.Subcategory.Category,$scope.Subcategory.name_subcategory);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (Subcategory) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = SubcategoryCRUDService.deleteSubcategory(Subcategory.idsubcategory);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                            
    	                        }, 3000);
    	                               
    	                })();               
    	                
    	            }, function (err) {
    	                $scope.errorMessage = "SUBCATEGORY_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		}); 
       
    }
  //detalles de uno
    $scope.get = function (Subcategory) {
        var promiseGetSingle = SubcategoryCRUDService.getSubcategory(Subcategory.idsubcategory);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Subcategory = {
                            idsubcategory: res.idsubcategory,
                            Category: res.category.idcategory,
                            name_subcategory: res.name_subcategory,                   
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
                console.log($scope.Subcategory.Category);
            },
            function (errorPl) {
                $scope.errorMessage = "SUBCATEGORY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('SubcategoryCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getSubcategory = function getSubcategory(idsubcategory){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/subcategories/'+idsubcategory
        });
	}
	this.getCategorySubcategory = function getCategorySubcategory(idsubcategory){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/subcategories/category/'+idsubcategory
        });
	}
    this.addSubcategory = function addSubcategory(Subcategory){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/categories/'+Subcategory.Category+'/subcategories',
          data: Subcategory
        });
    }
	
    this.deleteSubcategory = function deleteSubcategory(idsubcategory){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/subcategories/'+idsubcategory
        })
    }
	
    this.updateSubcategory = function updateSubcategory(idsubcategory,Category,name_subcategory){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/categories/'+Category+'/subcategories/'+idsubcategory,
          data: {idsubcategory: idsubcategory, idcategory_fk: Category,name_subcategory: name_subcategory }
        })
    }
	
    this.getAllSubcategory = function getAllSubcategory(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/subcategories'
        });
    }

}]);
// SubCategoia fin

//Province inicio****************************
angular.module('app').controller("ProvinceController", function ($scope, $filter,$window,$location, ProvinceCRUDService, CountryCRUDService, ConfigurationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Provinces, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    
    $scope.searchprov = function () {
		if ( ($scope.search_name_province == undefined  || $scope.search_name_province =="") && ($scope.search_pais == undefined || $scope.search_pais == "") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterProvinces = $filter('filter')($scope.original, function (item) {				
					if(($scope.search_name_province != undefined  ) && ($scope.search_pais != undefined ) )
						{						
							if(item.name_province.toUpperCase().search($scope.search_name_province.toUpperCase()) > -1 && item.country.name_country.toUpperCase().search($scope.search_pais.toUpperCase()) > -1)
							{
								return true;
							}
						}
						if(($scope.search_name_province != undefined || $scope.search_name_province !="")  && ($scope.search_pais == undefined || $scope.search_pais == ""))
							{
							if(item.name_province.toUpperCase().search($scope.search_name_province.toUpperCase()) > -1)
								{
									return true;
								}
							}
						if(($scope.search_pais != undefined || $scope.search_pais != "") && ($scope.search_name_province == undefined || $scope.search_name_province =="") )
						{
							if(item.country.name_country.toUpperCase().search($scope.search_pais.toUpperCase()) > -1)
							{
								return true;
							}
						}								
						return false;
					});
			$scope.Provinces = $scope.filterProvinces;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {	

        $scope.IsNewRecord = 1;
        $scope.Province = {
        		idprovince: 0,
                Country: $scope.primerpais,
                name_province: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = ProvinceCRUDService.getAllProvince();
        recordsGet.then(function (d) {
				$scope.Provinces = d.data;
				$scope.original = $scope.Provinces;
				console.log($scope.Provinces)
				$scope.search();                
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
        var recordsGet = CountryCRUDService.getAllCountry();
        recordsGet.then(function (d) {
                $scope.Countries = d.data
                if (d.data.length!=0) {
                    $scope.primerpais=d.data[0].idcountry;
                } else {
                     $scope.primerpais=0;
                }
                
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			
			});

    }
    //guardar prestamos
    $scope.save = function (Province) {
    	var ProvinceInser = {  
    			idprovince: Province.idprovince,
                Country: Province.Country,
                name_province: Province.name_province,
            };
    	var provinceequal = $scope.Provinces.filter(item => item.name_province === ProvinceInser.name_province);
    	if(provinceequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "PROVINCE_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
        	console.log(ProvinceInser.Country);
            var promisePost = ProvinceCRUDService.addProvince(ProvinceInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Province.idprovince = d.data.idprovince;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                        
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = ProvinceCRUDService.updateProvince($scope.Province.idprovince,$scope.Province.Country,$scope.Province.name_province);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                        
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (Province) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = ProvinceCRUDService.deleteProvince(Province.idprovince);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                            
    		                        }, 3000);
    		                               
    		                })();               
    		                
    		            }, function (err) {
    		                $scope.errorMessage = "PROVINCE_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 
       
    }
  //detalles de uno
    $scope.get = function (Province) {
        var promiseGetSingle = ProvinceCRUDService.getProvince(Province.idprovince);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Province = {
                            idprovince: res.idprovince,
                            Country: res.country.idcountry,
                            name_province: res.name_province,                   
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "SUBCATEGORY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('ProvinceCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getProvince = function getProvince(idprovince){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/provinces/'+idprovince
        });
	}
   
    this.addProvince = function addProvince(Province){
        console.log(Province.Country,Province.name_province,Province.idprovince);
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/countries/'+Province.Country+'/provinces',
          data: Province
        });
    }
	
    this.deleteProvince = function deleteProvince(idprovince){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/provinces/'+idprovince
        })
    }
	
    this.updateProvince = function updateProvince(idprovince,Country,name_province){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/countries/'+Country+'/provinces/'+idprovince,
          data: {idprovince: idprovince, idcountry: Country,name_province: name_province }
        })
    }
	
    this.getAllProvince = function getAllProvince(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/provinces'
        });
    }

}]);
// Province fin
// Municipality inicio****************************
angular.module('app').controller("MunicipalityController", function ($scope, $filter,$window,$location, MunicipalityCRUDService, ProvinceCRUDService, ConfigurationCRUDService ) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
		$scope.showModal = false;		
	};
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Municipalities, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    
    $scope.searchmunicipality = function () {
		if ( ($scope.search_name_municipality == undefined  || $scope.search_name_municipality =="") && ($scope.search_name_province == undefined || $scope.search_name_province == "") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterMunicipalities = $filter('filter')($scope.original, function (item) {					
					if(($scope.search_name_municipality != undefined ) && ($scope.search_name_province != undefined ))
						{
							if(item.name_municipality.toUpperCase().search($scope.search_name_municipality.toUpperCase()) > -1 && item.province.name_province.toUpperCase().search($scope.search_name_province.toUpperCase()) > -1)
							{
								return true;
							}
						}
						if(($scope.search_name_municipality != undefined || $scope.search_name_municipality !="")  && ($scope.search_name_province == undefined || $scope.search_name_province == ""))
							{
							if(item.name_municipality.toUpperCase().search($scope.search_name_municipality.toUpperCase()) > -1)
								{
									return true;
								}
							}
						if(($scope.search_name_province != undefined || $scope.search_name_province != "" ) && ($scope.search_name_municipality == undefined  || $scope.search_name_municipality =="") )
						{
							if(item.province.name_province.toUpperCase().search($scope.search_name_province.toUpperCase()) > -1)
							{
								return true;
							}
						}								
						return false;
					});
			$scope.Municipalities = $scope.filterMunicipalities;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {

        $scope.IsNewRecord = 1;
        $scope.Municipality = {
        		idmunicipality: 0,
                Province: $scope.primeraprovincia,
                name_municipality: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = MunicipalityCRUDService.getAllMunicipality();
        recordsGet.then(function (d) {
				$scope.Municipalities = d.data;
				$scope.original = $scope.Municipalities;
				$scope.search();              
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			
		 });	
        var recordsGet = ProvinceCRUDService.getAllProvince();
        recordsGet.then(function (d) {
                $scope.Provinces = d.data
                if(d.data.length!=0)
                {$scope.primeraprovincia=d.data[0].idprovince}
                else
                {$scope.primeraprovincia=0}
                
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });

    }
    //guardar prestamos
    $scope.save = function (Municipality) {
    	var MunicipalityInser = {  
    			idmunicipality: Municipality.idmunicipality,
                Province: Municipality.Province,
                name_municipality: Municipality.name_municipality,
            };
    	var muniequal = $scope.Municipalities.filter(item => item.name_municipality === MunicipalityInser.name_municipality);
    	if(muniequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "MUNICIPALITY_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
            var promisePost = MunicipalityCRUDService.addMunicipality(MunicipalityInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Municipality.idmunicipality = d.data.idmunicipality;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                        
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = MunicipalityCRUDService.updateMunicipality($scope.Municipality.idmunicipality,$scope.Municipality.Province,$scope.Municipality.name_municipality);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                        
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (Municipality) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = MunicipalityCRUDService.deleteMunicipality(Municipality.idmunicipality);
    		            promiseDelete.then(function (d) {  
    		            	loadRecords();           	
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {                   
    		                        setTimeout(function () {
    		                        	$scope.verMessage = false;
    		                            $scope.$digest();
    		                            
    		                        }, 3000);
    		                               
    		                })();               
    		                
    		            }, function (err) {
    		                $scope.errorMessage = "SUBCATEGORY_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		});        
    }
  //detalles de uno
    $scope.get = function (Municipality) {
        var promiseGetSingle = MunicipalityCRUDService.getMunicipality(Municipality.idmunicipality);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Municipality = {
                            idmunicipality: res.idmunicipality,
                            Province: res.province.idprovince,
                            name_municipality: res.name_municipality,                   
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "SUBCATEGORY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('MunicipalityCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getMunicipality = function getMunicipality(idmunicipality){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/municipality/'+idmunicipality
        });
	}
	this.findall = function findall(municipality, province){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/municipality/findall/'+municipality+'/'+province
        });
	}
	this.getProvinceMunicipality = function getProvinceMunicipality(idmunicipality){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/municipality/province/'+idmunicipality
        });
	}
    this.addMunicipality = function addMunicipality(Municipality){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/provinces/'+Municipality.Province+'/municipality',
          data: Municipality
        });
    }
	
    this.deleteMunicipality = function deleteMunicipality(idmunicipality){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/municipality/'+idmunicipality
        })
    }
	
    this.updateMunicipality = function updateMunicipality(idmunicipality,Province,name_municipality){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/provinces/'+Province+'/municipality/'+idmunicipality,
          data: {idmunicipality: idmunicipality, idprovince: Province,name_municipality: name_municipality }
        })
    }
	
    this.getAllMunicipality = function getAllMunicipality(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/municipality'
        });
    }

}]);
// Municipality fin

//Funcionality inicio****************************
angular.module('app').controller("FuncionalityController", function ($scope, $filter,$window,$location, FuncionalityCRUDService, ConfigurationCRUDService) {


    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
		$scope.showModal = false;

	};
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};

	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Funcionalidades, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
    
    $scope.searchfuncionality = function () {
		if ( ($scope.search_funcionalidadname == undefined  || $scope.search_funcionalidadname =="") && ($scope.search_funcionalidaddesc == undefined || $scope.search_funcionalidaddesc == "") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterFuncionalidades = $filter('filter')($scope.original, function (item) {				
					if(($scope.search_funcionalidadname != undefined  ) && ($scope.search_funcionalidaddesc != undefined ) )
						{						
							if(item.nameFuncionality.toUpperCase().search($scope.search_funcionalidadname.toUpperCase()) > -1 && item.descFuncionality.toUpperCase().search($scope.search_funcionalidaddesc.toUpperCase()) > -1)
							{
								return true;
							}
						}
						if(($scope.search_funcionalidadname != undefined || $scope.search_funcionalidadname !="")  && ($scope.search_funcionalidaddesc == undefined || $scope.search_funcionalidaddesc == ""))
							{
							if(item.nameFuncionality.toUpperCase().search($scope.search_funcionalidadname.toUpperCase()) > -1)
								{
									return true;
								}
							}
						if(($scope.search_funcionalidaddesc != undefined || $scope.search_funcionalidaddesc != "") && ($scope.search_funcionalidadname == undefined || $scope.search_funcionalidadname =="") )
						{
							if(item.descFuncionality.toUpperCase().search($scope.search_funcionalidaddesc.toUpperCase()) > -1)
							{
								return true;
							}
						}								
						return false;
					});
			$scope.Funcionalidades = $scope.filterFuncionalidades;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
    //limpiar registro
    $scope.clear = function () {
		$scope.IsNewRecord = 1;
        $scope.Funcionality = {
            idfuncionality: 0,
            descFuncionality: "",
            nameFuncionality:"",
            pointFuncionality:0,
            point_plus:0,
            point_less:0,
            funcionality_days:0,
            sistema:false,
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = FuncionalityCRUDService.getAllFuncionality();
        recordsGet.then(function (d) {
				$scope.Funcionalidades = d.data;
				$scope.original = $scope.Funcionalidades;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
			
		});
    }
    //guardar prestamos
    $scope.save = function (Funcionality) {

        var FuncionalityInser = {
            idfuncionality: Funcionality.idfuncionality,
            descFuncionality: Funcionality.descFuncionality,
            nameFuncionality:Funcionality.nameFuncionality,
            pointFuncionality:Funcionality.pointFuncionality,
            point_plus:Funcionality.point_plus,
            point_less:Funcionality.point_less,
            funcionality_days:Funcionality.funcionality_days,
            sistema :Funcionality.sistema,
        };
        var funcequal = $scope.Funcionalidades.filter(item => item.nameFuncionality === FuncionalityInser.nameFuncionality);
        console.log($scope.IsNewRecord)
    	if(funcequal.length > 0 && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "FUNCIONALITY_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
            var promisePost = FuncionalityCRUDService.addFuncionality(FuncionalityInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Funcionality.idfuncionality = d.data.idfuncionality;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {
                    setTimeout(function () {
                        $scope.verMessage = false;
                        $scope.$digest();
                        
                    }, 3000);

                })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record

            var promisePut = FuncionalityCRUDService.updateFuncionality(Funcionality);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {
                    setTimeout(function () {
                        $scope.verMessage = false;
                        $scope.$digest();
                        
                    }, 3000);

                })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (Funcionality) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				 var promiseDelete = FuncionalityCRUDService.deleteFuncionality(Funcionality.idfuncionality);
    		            promiseDelete.then(function (d) {
    		                loadRecords();
    		                $scope.Message = "SUCCESSFULY_DELETE";
    		                $scope.verMessage = true;
    		                (function next() {
    		                    setTimeout(function () {
    		                        $scope.verMessage = false;
    		                        $scope.$digest();
    		                    }, 3000);

    		                })();

    		            }, function (err) {
    		                $scope.errorMessage = "FUNCIONALITY_ERR_DELETE";
    		                console.log(err);
    		                $scope.verMessageError = true;
    		            });
    			  }  		  
    		}); 

    }
    //detalles de uno
    $scope.get = function (Funcionality) {
        var promiseGetSingle = FuncionalityCRUDService.getFuncionality(Funcionality.idfuncionality);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.Funcionality = {
                    idfuncionality: res.idfuncionality,
                    descFuncionality: res.descFuncionality,
                    nameFuncionality:res.nameFuncionality,
                    pointFuncionality:res.pointFuncionality,
                    point_plus:res.point_plus,
                    point_less:res.point_less,
                    funcionality_days:res.funcionality_days,
                    sistema :res.sistema,
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "FUNCIONALITY_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('FuncionalityCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {

    this.getFuncionality = function getFuncionality(idfuncionality){
        return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/funcionalities/'+idfuncionality
        });
    }
    this.findall = function findall(namefunc, descfunc, pointfunc, plusfunc, lessfunc, dayfunc){
        return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/funcionalities/findall/'+namefunc+'/'+descfunc+'/'+pointfunc+'/'+plusfunc+'/'+lessfunc+'/'+dayfunc
        });
    }

    this.addFuncionality = function addFuncionality(funcionality){
        return $http({
            method: 'POST',
            url: RELATIVE_DIR+'/api/funcionalities',
            data: funcionality
        });
    }

    this.deleteFuncionality = function deleteFuncionality(idfuncionality){
        return $http({
            method: 'DELETE',
            url: RELATIVE_DIR+'/api/funcionalities/'+idfuncionality
        })
    }

    this.updateFuncionality = function updateFuncionality(Funcionality){
        return $http({
            method: 'PUT',
            url: RELATIVE_DIR+'/api/funcionalities/'+Funcionality.idfuncionality,
            data: Funcionality
        })
    }

    this.getAllFuncionality = function getAllFuncionality(){
        return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/funcionalities'
        });
    }
    this.getAllFuncionalityNotSystem = function getAllFuncionalityNotSystem(){
        return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/funcionalities/notsystem'
        });
    }    
    this.getAllFuncionalitySystem = function getAllFuncionalitySystem(){
        return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/funcionalities/system'
        });
    }

}]);
// Funcionality fin

//controlador State Package
angular.module('app').controller("StatePackageController", function ($scope, $rootScope, StatePackageCRUDService, ConfigurationCRUDService) {
	$scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
		$scope.showModal = false;

    };
    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.StatePackage = {
        		id_state_package: 0,
        		state: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
        $scope.clear();
        var recordsGet = StatePackageCRUDService.getAllStatePackage();
        recordsGet.then(function (d) {
                $scope.StatePackages = d.data
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
    }
    //guardar prestamos
    $scope.save = function (StatePackage) {
    	var StatePackageInser = {
    			id_state_package: StatePackage.id_state_package,
    			state: StatePackage.state,
            };
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
            var promisePost = StatePackageCRUDService.addStatePackage(StatePackageInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.StatePackage.id_state_package = d.data.id_state_package;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = StatePackageCRUDService.updateStatePackage(StatePackage);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 2000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    };
    //Eliminar
    $scope.delete = function (StatePackage) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = StatePackageCRUDService.deleteStatePackage(StatePackage);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 2000);
    	                               
    	                })();
    	                }, function (err) {
    	                $scope.errorMessage = "PACKAGE_STATE_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		}); 
       
    }
    //detalles de uno
    $scope.get = function (StatePackage) {
        var promiseGetSingle = StatePackageCRUDService.getStatePackage(StatePackage);
        promiseGetSingle.then(function (d) {
                var res = d.data;
                $scope.StatePackage = {
                		id_state_package: res.id_state_package,
                		state: res.state,
                };
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;
            },
            function (errorPl) {
                $scope.errorMessage = "PACKAGE_STATE_NFOUND";
                console.log(errorPl);
                $scope.verMessageError = true;
            });
    }
});
angular.module('app').service('StatePackageCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
    this.getStatePackage = function getStatePackage(StatePackage){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/statepackage/'+StatePackage.id_state_package
        });
	}
	
    this.addStatePackage = function addStatePackage(StatePackage){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/statepackage',
          data: StatePackage
        });
    }
	
    this.deleteStatePackage = function deleteStatePackage(StatePackage){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/statepackage/'+StatePackage.id_state_package
        })
    }
	
    this.updateStatePackage = function updateStatePackage(StatePackage){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/statepackage/'+StatePackage.id_state_package,
          data: StatePackage
        })
    }
	
    this.getAllStatePackage = function getAllStatePackage(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/statepackage'
        });
    }

}]);
//fin categoria

//NotificationState inicio****************************
angular.module('app').controller("NotificationStateController", function ($scope, $filter,$window,$location, NotificationStateCRUDService, ConfigurationCRUDService) {
	    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    //modal adicionar
    $scope.AdicionarModal = function () {
        $scope.clear();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.clear();
        $scope.showModal = false;
	};
	$scope.LimpiarModal = function () {
        $scope.clear();      
	};
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.NotificationStates, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	// paginado fin
    
    $scope.searchname = function () {
		if ( ($scope.search_name == undefined  || $scope.search_name =="") && ($scope.search_desc == undefined || $scope.search_desc == "") )
			{
				loadRecords();
			}
		else
			{
			$scope.filterNotificationState = $filter('filter')($scope.original, function (item) {				
					if(($scope.search_name != undefined  ) && ($scope.search_desc != undefined ) )
						{						
							if(item.nameNotificationstate.toUpperCase().search($scope.search_name.toUpperCase()) > -1 && item.descNotificationstate.toUpperCase().search($scope.search_desc.toUpperCase()) > -1)
							{
								return true;
							}
						}
						if(($scope.search_name!= undefined  || $scope.search_name !="")  && ($scope.search_desc != undefined || $scope.search_desc == ""))
							{
							if(item.nameNotificationstate.toUpperCase().search($scope.search_name.toUpperCase()) > -1)
								{
									return true;
								}
							}
						if(($scope.search_desc != undefined || $scope.search_desc != "") && ($scope.search_name == undefined || $scope.search_name =="") )
						{
							if(item.descNotificationstate.toUpperCase().search($scope.search_desc.toUpperCase()) > -1)
							{
								return true;
							}
						}								
						return false;
					});
			$scope.NotificationStates = $scope.filterNotificationState;
			$scope.search();
			}		
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

    //limpiar registro
    $scope.clear = function () {
        $scope.IsNewRecord = 1;
        $scope.NotificationState = {
        		idnotificationstate: 0,
        		nameNotificationstate: "",
        		descNotificationstate: "",
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		 var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

        var recordsGet = NotificationStateCRUDService.getAllNotificationStates();
        recordsGet.then(function (d) {
	        		$scope.NotificationStates = d.data;
	        		console.log($scope.NotificationStates)
					$scope.original = $scope.NotificationStates
					$scope.search();
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
				});
			});
    }
    //guardar prestamos
    $scope.save = function (NotificationState) {
    	
    	var NotificationStateInser = {  
    			idnotificationstate: NotificationState.idnotificationstate,
    			nameNotificationstate: NotificationState.nameNotificationstate,
    			descNotificationstate: NotificationState.descNotificationstate,
            };
    	var NStateequal = $scope.NotificationStates.filter(item => item.nameNotificationstate === NotificationState.nameNotificationstate);
    	if(NStateequal.length > 0  && $scope.IsNewRecord === 1)
    		{
    		$scope.errorMessage = "NOTIFICATION_STATE_FORM_EXISTS";
            $scope.verMessageError = true;
    		}
    	else
    		{
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
        	
            var promisePost = NotificationStateCRUDService.addNotificationState(NotificationStateInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.NotificationState.idnotificationstate = d.data.idnotificationstate;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = NotificationStateCRUDService.updateNotificationState(NotificationState);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    		}
    };
    //Eliminar
    $scope.delete = function (NotificationState) {
    	swal({
    		  title: "",
    		  text: $filter('translate')("WANT_DELETE"),
    		  type: "error",
    		  showCancelButton: true,
    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
    		  confirmButtonText: $filter('translate')("DELETE"),
    		  cancelButtonText: $filter('translate')("CANCEL"),
    		  closeOnConfirm: true,
    		  closeOnCancel: true
    		},
    		function(isConfirm){
    			if (isConfirm) {
    				var promiseDelete = NotificationStateCRUDService.deleteNotificationState(NotificationState.idnotificationstate);
    	            promiseDelete.then(function (d) {  
    	            	loadRecords();           	
    	                $scope.Message = "SUCCESSFULY_DELETE";
    	                $scope.verMessage = true;
    	                (function next() {                   
    	                        setTimeout(function () {
    	                        	$scope.verMessage = false;
    	                            $scope.$digest();
    	                        }, 3000);
    	                               
    	                })();               
    	                
    	            }, function (err) {
    	                $scope.errorMessage = "NOTIFICATION_STATE_ERR_DELETE";
    	                console.log(err);
    	                $scope.verMessageError = true;
    	            });
    			  }  		  
    		});        
    }
  //detalles de uno
    $scope.get = function (NotificationState) {
    			
    			$scope.NotificationState = {
			        		idnotificationstate: NotificationState.idnotificationstate,
			        		nameNotificationstate: NotificationState.nameNotificationstate,
			        		descNotificationstate: NotificationState.descNotificationstate,
			        };
    			console.log($scope.NotificationState)
                $scope.showModal = true;
                $scope.IsNewRecord = 0;
                $scope.verMessage = false;            
    }
});
angular.module('app').service('NotificationStateCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
   
    this.addNotificationState = function addNotificationState(NotificationState){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/notificationStates',
          data: NotificationState
        });
    }
	
    this.deleteNotificationState = function deleteNotificationState(idnotificationstate){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/notificationStates/'+idnotificationstate
        })
    }
	
    this.updateNotificationState = function updateNotificationState(NotificationState){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/notificationStates/'+NotificationState.idnotificationstate,
          data: NotificationState
        })
    }	
    this.getAllNotificationStates = function getAllNotificationStates(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/notificationStates'
        });
    }

}]);
//NotificationState fin
