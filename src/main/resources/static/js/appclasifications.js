//inicio Promotion
angular.module('app').controller("CalificationController", function ($scope, $filter, $window,$location, BusinessCRUDService, CalificationController, ConfigurationCRUDService) {
	
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Clasificacion, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin	
//       busqueda
    $scope.searchcbusi = function () {    	
    	if ( ($scope.search_business == undefined  || $scope.search_business =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterClasificacion = $filter('filter')($scope.original, function (item) 
					{
					if(item.business.name_business != null)
						{
							if(item.business.name_business.toUpperCase().search($scope.search_business.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Clasificacion = $scope.filterClasificacion;
			$scope.search();
		}
	}
    $scope.searchcuser = function () {    	
    	if ( ($scope.search_user == undefined  || $scope.search_user =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterClasificacion = $filter('filter')($scope.original, function (item) 
					{
					if(item.user.nameUser != null)
						{
							if(item.user.nameUser.toUpperCase().search($scope.search_user.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Clasificacion = $scope.filterClasificacion;
			$scope.search();
		}
	}
    
    $scope.changetoradiobusiness = function ()
    {    	
        var recordsGet = CalificationController.getAllCalificationbybusiness($scope.Business.idbusiness);
		recordsGet.then(function (d) {
				$scope.Clasificacion = d.data;
				$scope.original = $scope.Clasificacion;
				$scope.search();
			},
			function (errorPl) {
				$log.error( errorPl);
			});
    }
    $scope.changetoradiouser = function ()
    {

    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
		{	
    		var recordsGet = CalificationController.getAll();
			recordsGet.then(function (d) {
					$scope.Clasificacion = d.data;
					$scope.original = $scope.Clasificacion;
					$scope.search();
				},
				function (errorPl) {
					$log.error( errorPl);
				});	
		}
    	else
    	{
    		  var recordsGet = CalificationController.getAllCalificationbyuser($window.sessionStorage.iduser);
				recordsGet.then(function (d) {
						$scope.Clasificacion = d.data;
						$scope.original = $scope.Clasificacion;
						$scope.search();
					},
					function (errorPl) {
						$log.error( errorPl);
					});	
    		}			
    }
    
    $scope.callto = function (radiodata,business) {
		if(radiodata == 1)
			{
			$scope.changetoradiouser();
			}
		if(radiodata == 2)
			{
			$scope.changetoradiobusiness(business);
			}
	    }
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
	//limpiar registro
    $scope.clear = function () {
    	$scope.Business = {};
    	$scope.user = JSON.parse($window.sessionStorage.userData);		
    	$scope.Rad = {
				toradio:1,
		}
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
    	$scope.clear();
    	var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
		    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
	    		{
		    		 var recordsGet = CalificationController.getAll();
						recordsGet.then(function (d) {
								$scope.Clasificacion = d.data;
								$scope.original = $scope.Clasificacion;
								$scope.search();
							},
							function (errorPl) {
								$log.error( errorPl);
							});	
	    		}
		    	else
		    	{		
				        var recordsGet = CalificationController.getAllCalificationbyuser($window.sessionStorage.iduser);
						recordsGet.then(function (d) {
								$scope.Clasificacion = d.data;
								$scope.original = $scope.Clasificacion;
								$scope.search();
							},
							function (errorPl) {
								$log.error( errorPl);
							});	
		    		}
		    	},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
    	
    	if ($scope.user.role.desc_rol==="ROLE_ADMIN")
		{
			$scope.showbotomadd = false;
			var recordsGet = BusinessCRUDService.getallBusiness()
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];				
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
		else
		{
			var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];			
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
    	
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
    			    	
		},
		function (errorPl) {
			$log.error('Error ', errorPl);
		});
    }
    $scope.back = function () {
		$window.history.back(-1);	
	}
});

angular.module('app').service('CalificationController',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {	
        
    this.getAll = function getAll(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/clasification'
        });
	}
    
    this.getAllCalificationbyuser = function getAllCalificationbyuser(iduser){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/clasificationbyuser/'+iduser
          });
    }
    this.getAllCalificationbybusiness = function getAllCalificationbybusiness(idbusiness){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/clasificationbybusiness/'+idbusiness
          });
    }
    

}]);
