//offer Controller  ***************************
angular.module('app').controller("OfferController", function ($scope,$window, $location, $filter, OfferCRUDService,BusinessCRUDService, ConfigurationCRUDService, NotificationCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    $scope.dateini = new Date();
    $scope.dateini.setDate($scope.dateini.getDate()+ 1);
    
	var locdir = window.location;
	 var pathNamedir = locdir.pathname.substring(0, locdir.pathname.lastIndexOf('/') + 1);
	 var dir = locdir.href.substring(0, locdir.href.length - ((locdir.pathname + locdir.search + locdir.hash).length - pathNamedir.length));
    
    $scope.today = moment().format('YYYY-MM-DD');
    //modal adicionar
    $scope.AdicionarModal = function () {
    	$scope.clearfield();
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
		$scope.clearfield();		
		$scope.showModal = false;
		
	};
	$scope.LimpiarModal = function () {
        $scope.clearfield();      
    };
	$scope.clearfield = function () {
		if($scope.Rad.toradio == 2)
		{
			$scope.Offer = {
	        		idoffer: 0,
					descOffer: "",
					nameOffer: "",
					activeOffer: false,
					date_start: $scope.dateini,
					date_end: "",
					daysOffer:0,
					business: $scope.Business.bus,
					urldir:dir+'#!/offerdetail/',
					};	
			console.log($scope.Offer)
		}
	else
		{
		$scope.Offer = {
        		idoffer: 0,
				descOffer: "",
				nameOffer: "",
				activeOffer: false,
				date_start: $scope.dateini,
				date_end: "",
				daysOffer:0,
				business: null,
				urldir:dir+'#!/offerdetail/',
				};
		}
		
	      
	};

	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Offers, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
//    buscar
    $scope.searchoffer = function () {    	
    	if ( ($scope.search_offer == undefined  || $scope.search_offer =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterOffers = $filter('filter')($scope.original, function (item) 
					{
					if(item.nameOffer != null)
						{
							if(item.nameOffer.toUpperCase().search($scope.search_offer.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Offers = $scope.filterOffers;
			$scope.search();
		}
	}
    $scope.searchdesc = function () {    	
    	if ( ($scope.search_descr == undefined  || $scope.search_descr =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterOffers = $filter('filter')($scope.original, function (item) 
					{
					if(item.nameOffer != null)
						{
							if(item.descOffer.toUpperCase().search($scope.search_descr.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Offers = $scope.filterOffers;
			$scope.search();
		}
	}
    $scope.searchdstar = function () {   
    	if ( ($scope.search_startdate == undefined  || $scope.search_startdate =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{	    	
			$scope.filterOffers = $filter('filter')($scope.original, function (item) 
					{
					if(item.date_start != null)
						{
							if(angular.equals($filter('date')(item.date_start, "yyyy-MM-dd"), $filter('date')($scope.search_startdate, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Offers = $scope.filterOffers;
			$scope.search();
		}
	}
    $scope.searchdend = function () {   
    	if ( ($scope.search_enddate == undefined  || $scope.search_enddate =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{	    	
			$scope.filterOffers = $filter('filter')($scope.original, function (item) 
					{
					if(item.date_end != null)
						{
							if(angular.equals($filter('date')(item.date_end, "yyyy-MM-dd"), $filter('date')($scope.search_enddate, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Offers = $scope.filterOffers;
			$scope.search();
		}
	}
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
	 var loc = window.location;
	 var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
	 var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	
	$scope.callto = function (radiodata) {
		if(radiodata == 1)
			{
			$scope.changetoradiouser();
			}
		if(radiodata == 2)
			{
			$scope.changetoradiobusiness();
			}
	    };	    
	
	$scope.changetoradiobusiness = function ()
    {
		var recordsGet = OfferCRUDService.getAllOffersbyBusiness($scope.Business.bus.idbusiness);
		recordsGet.then(function (d) {
				$scope.Offers = d.data;
				$scope.original = $scope.Offers;				
				$scope.search();	
				console.log($scope.Offers)
				
				var recordsGet = BusinessCRUDService.getFuncionalitybyBusiness($scope.Business.bus.idbusiness);
				recordsGet.then(function (b) {
					console.log(b.data)
					var funcio = $scope.filterUsers = $filter('filter')(b.data, function (item) {
								if(item.idfuncionality == 3)
								{
									return true;
								}						
								return false;
							});
					if(funcio.length > 0)
						{
						$scope.showbotomadd= false;
						}
					else
						{
							$scope.showbotomadd= true;
						}
				},
						function (errorPl) {
					$log.error('Error ', errorPl);
				});
			},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});		
    }
    $scope.changetoradiouser = function ()
    {
    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
		{	
    		$scope.isadmin = true;				
    		var recordsGet = OfferCRUDService.getAll();
				recordsGet.then(function (d) {
						$scope.Offers = d.data;
						$scope.original = $scope.Offers;
						$scope.search();
						console.log($scope.Offers)
						$scope.showbotomadd= false;
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
		}
    	else
    	{
			var recordsGet = OfferCRUDService.getAllOffersbyUser($window.sessionStorage.iduser)
				recordsGet.then(function (d) {
					$scope.Offers = d.data;
					$scope.original = $scope.Offers;
					$scope.search();
					console.log($scope.Offers)	
					$scope.showbotomadd= true;
				},
					function (errorPl) {
						console.log( errorPl)
					});
    		}			
    }    
		
    //limpiar registro
    $scope.clear = function () {		
    	$scope.isadmin = false;
		$scope.IsNewRecord = 1;
		$scope.user = JSON.parse($window.sessionStorage.userData);
		$scope.Rad = {
				toradio:1,
		}
		$scope.Business = {
				bus: null,
		}
        $scope.Offer = {
        		idoffer: 0,
				descOffer: "",
				nameOffer: "",
				activeOffer: false,
				date_start: $scope.dateini,
				date_end: "",
				daysOffer:0,
				business:null,
				urldir:dir+'#!/offerdetail/',
		};
		
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
        $scope.showbotomadd= true;
    }
    $scope.IsNewRecord = 1;
    
    $scope.showcheck = function () { 
    	$scope.showbotomadd= true;    		
    }
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
		$scope.clear();	
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;

				
		    	},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
		
		if ($scope.user.role.desc_rol==="ROLE_ADMIN")
		{
			$scope.showbotomadd = false;
			var recordsGet = BusinessCRUDService.getallBusiness()
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];	
				$scope.callto($scope.Rad.toradio);
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
		else
		{
			
			var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];	
				$scope.callto($scope.Rad.toradio);
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
	}
		
    $scope.changedateend = function (){
    	$scope.lesszero = false;
    	if($scope.Offer.daysOffer > 0 && $scope.Offer.daysOffer != "")
    		{
				var newdate = new  Date($scope.Offer.date_start)
    			$scope.Offer.date_end = new Date(newdate.setDate(newdate.getDate()+$scope.Offer.daysOffer));
    		}
    	if($scope.Offer.daysOffer <= 0 || $scope.Offer.daysOffer == null || $scope.Offer.daysOffer == undefined)
    		{
    			$scope.lesszero = true;
    			$scope.Offer.date_end = "";
    		}
    }   
    
	$scope.foredit = function (offer){
    	var today = new Date();
    	if(offer.activeOffer != true && (new Date(offer.date_start) > today || new Date(offer.date_end)<today))
    	{
    		return false;    		
    	}
    	else
    		{
    			return true;
    		}
    }
	$scope.fordelete = function (offer){
    	var today = new Date();
    	if(offer.activeOffer != true && $filter('date')(today, 'yyyy-MM-dd') > $filter('date')(offer.date_start, 'yyyy-MM-dd'))
    	{
    		return false;    		
    	}
    	else
		{
			return true;
		}
    }
	
    //guardar prestamos
    $scope.save = function (Offer) {    	
	 console.log(Offer)
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
		
            var promisePost = OfferCRUDService.addOffer(Offer);
            promisePost.then(function (d) {            								
                $scope.Offer.idoffer = d.data.idoffer;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
                $scope.callto($scope.Rad.toradio);
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
		} else { //Else Edit the record
			
            var promisePut = OfferCRUDService.updateOffer(Offer);
            promisePut.then(function (d) {
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                   
                    setTimeout(function () {
                    	$scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
                $scope.callto($scope.Rad.toradio);
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    };
    //Eliminar
    $scope.delete = function (Offer) {
    	swal({
  		  title: "",
  		  text: $filter('translate')("WANT_DELETE"),
  		  type: "error",
  		  showCancelButton: true,
  		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
  		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
  		  confirmButtonText: $filter('translate')("DELETE"),
  		  cancelButtonText: $filter('translate')("CANCEL"),
  		  closeOnConfirm: true,
  		  closeOnCancel: true
  		},
  		function(isConfirm){
  			if (isConfirm) {
  				 var promiseDelete = OfferCRUDService.deleteOffer(Offer.idoffer);
  	            promiseDelete.then(function (d) {  
  	            	       	
  	                $scope.Message = "SUCCESSFULY_DELETE";
  	                $scope.verMessage = true;
  	                (function next() {                   
  	                        setTimeout(function () {
  	                        	$scope.verMessage = false;
  	                            $scope.$digest();
  	                        }, 3000);
  	                               
  	                })();
  	                $scope.callto($scope.Rad.toradio);
  	            }, function (err) {
  	                $scope.errorMessage = "OFFER_ERR_DELETE";
  	                console.log(err);
  	                $scope.verMessageError = true;
  	            });
  			  }  		  
  		});    
       
    }
    //detalles de uno
    $scope.get = function (Offer) {
		 $scope.Offer = {
        		idoffer: Offer.idoffer,
				descOffer: Offer.descOffer,
				nameOffer: Offer.nameOffer,
				activeOffer: Offer.activeOffer,
				date_start: new Date(Offer.date_start),
				date_end: new Date(Offer.date_end),
				daysOffer: Math.round((new Date(Offer.date_end).getTime() - new Date(Offer.date_start).getTime())/ (1000*60*60*24)),
				business :Offer.business,
		};
		$scope.showModal = true;
		$scope.IsNewRecord = 0;
		$scope.verMessage = false;
        
	}

	 $scope.clone = function (Offer) {
		 console.log(Offer)
		 $scope.Offer = {
        		idoffer: 0,
				descOffer: Offer.descOffer,
				nameOffer: Offer.nameOffer,
				activeOffer: false,
				date_start: new Date(),
				date_end: "",
				daysOffer: Math.round((new Date(Offer.date_end).getTime() - new Date(Offer.date_start).getTime())/ (1000*60*60*24)),
				business :Offer.business,
		};
		$scope.showModal = true;
		$scope.IsNewRecord = 1;
		$scope.verMessage = false;        
	}

	
	$scope.notificationforalloffer = function (Offer) {
		  var promisePost = NotificationCRUDService.offerforall(Offer);
		  promisePost.then(function (d) { 
			  console.log("all fine") 
			  }, function (err) {
                console.log(err);
            });  
    }
	
	$scope.showclone = function (Offer) {
		$scope.clone(Offer);
		$scope.showModal = true;
	}
	 $scope.goDetail = function (idoffer) {
			$location.path("/offerdetail/"+idoffer); 
		}
	 $scope.back = function () {
			$window.history.back(-1);	
		}
});
angular.module('app').service('OfferCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
	
	this.getAll = function getAll(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/offersall'
        });
	}
	
    this.getOffer = function getOffer(idoffer){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/offers/'+idoffer
        });
	}
	
    this.addOffer = function addOffer(Offer){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/offers',
          data: Offer
        });
    }
	
    this.deleteOffer = function deleteOffer(idoffer){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/offers/'+idoffer
        })
    }
	
    this.updateOffer = function updateOffer(offer){
		console.log(offer)
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/offers/'+offer.idoffer,
          data: offer
        })
    }
	
    this.getAllOffersbyBusiness = function getAllOffersbyBusiness(idbusiness){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/offers/business/'+idbusiness
        });
    }
    
    this.getAllOffersActive = function getAllOffersActive(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/offers/active'
        });
    }
    this.getAllOffersbyUser = function getAllOffersbyUser(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/offers/user/'+iduser
        });
    }
    this.getOfferbybusiness = function getOfferbybusiness(idbusiness){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/offers/all/business/'+idbusiness
        });
    }
    

}]);
//Oferta fin

angular.module('app').controller("OfferDetailController", function ($scope,$window,RELATIVE_DIR,$location,OfferCRUDService) {
	
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idoffer =urlParts[urlParts.length - 1];
	loadRecords();
	function loadRecords(){
		console.log(idoffer)
	      var recordsGet = OfferCRUDService.getOffer(idoffer);        
	        recordsGet.then(function (d) {
	        	$scope.OfferDetail = d.data;
	        	if($scope.OfferDetail.business != null)
	    		{
	    		$scope.pathimage = RELATIVE_DIR+"/files/"+$scope.OfferDetail.business.name_image;
	    		}			
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	  }	
	
	$scope.showavatar = function () {
		if($scope.pathimage != undefined)
			{
				return true;
			}
		else
			{
				return false;
			}
	}
	
	$scope.out = function () {
		$location.path('/home');
	}
	$scope.back = function () {
				$window.history.back(-1);	
			
	}
});

angular.module('app').controller("OfferListBusinessController", function ($scope,$filter, $window,RELATIVE_DIR,$location,OfferCRUDService, ConfigurationCRUDService, BusinessCRUDService) {
	
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idbusiness =urlParts[urlParts.length - 1];
	
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Offers, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.goDetail = function (idoffer) {
		$location.path("/offerdetail/"+idoffer); 
	}
	//    buscar
$scope.searchoffer = function () {    	

		$scope.filterOffers = $filter('filter')($scope.original, function (item) 
				{
				if(item.nameOffer != null)
					{
						if(item.nameOffer.toUpperCase().search($scope.search_offer.toUpperCase()) > -1)
						{
							return true;
						}	
					}
											
					return false;
					});
		$scope.Offers = $scope.filterOffers;
		$scope.search();
	
}
$scope.searchdesc = function () {    	
	
		$scope.filterOffers = $filter('filter')($scope.original, function (item) 
				{
				if(item.nameOffer != null)
					{
						if(item.descOffer.toUpperCase().search($scope.search_descr.toUpperCase()) > -1)
						{
							return true;
						}	
					}
											
					return false;
					});
		$scope.Offers = $scope.filterOffers;
		$scope.search();
	
}
$scope.searchdstar = function () {   
	    	
		$scope.filterOffers = $filter('filter')($scope.original, function (item) 
				{
				if(item.date_start != null)
					{
						if(angular.equals($filter('date')(item.date_start, "yyyy-MM-dd"), $filter('date')($scope.search_startdate, "yyyy-MM-dd")))
						{
							return true;
						}	
					}
											
					return false;
					});
		$scope.Offers = $scope.filterOffers;
		$scope.search();
	
}
$scope.searchdend = function () {   
	    	
		$scope.filterOffers = $filter('filter')($scope.original, function (item) 
				{
				if(item.date_end != null)
					{
						if(angular.equals($filter('date')(item.date_end, "yyyy-MM-dd"), $filter('date')($scope.search_enddate, "yyyy-MM-dd")))
						{
							return true;
						}	
					}
											
					return false;
					});
		$scope.Offers = $scope.filterOffers;
		$scope.search();
	
}
	
    loadRecords();
	function loadRecords(){
		if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;	
			if(idbusiness != -1)
				{
				  var recordsGet = OfferCRUDService.getOfferbybusiness(idbusiness);        
			        recordsGet.then(function (d) {
			        	$scope.Offers = d.data;
			        	$scope.original = $scope.Offers;
						$scope.search();	
			        },
			        function (errorPl) {
			            $log.error('Error ', errorPl);
			        });
			        var recordsGet = BusinessCRUDService.getBusiness(idbusiness)
					recordsGet.then(function (d) {
						$scope.busi = d.data;
					 },
			        function (errorPl) {
			            $log.error('Error ', errorPl);
			        });
				}
				
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	  }	
    
});