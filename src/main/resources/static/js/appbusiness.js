angular.module('app').controller("ActivateFuncionalityController", function ( $scope,toast,$timeout,$window, $window,NotificationCRUDService, ConfigurationCRUDService, $filter, $location, BusinessCRUDService,
	FuncionalityCRUDService){
	$scope.selection = [];
	$scope.selectionfuncionality = [];
	$scope.Funcionalitiestoo = [];
//	buscar
	$scope.searchcbusi = function () {    	
    	if ( ($scope.search_name_business == undefined  || $scope.search_name_business =="") )
			{
    		loadAllbusines();
			}
    	else
		{
			$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
					{
					if(item.name_business != null)
						{
							if(item.name_business.toUpperCase().search($scope.search_name_business.toUpperCase()) > -1)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Business = $scope.filterBusiness;
		}
	}
	$scope.searchdir = function () {    	
    	if ( ($scope.search_dirbusines == undefined  || $scope.search_dirbusines =="") )
			{
    		loadAllbusines();
			}
    	else
		{
			$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
					{
					if(item.dir_business != null)
						{
							if(item.dir_business.toUpperCase().search($scope.search_dirbusines.toUpperCase()) > -1)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Business = $scope.filterBusiness;
		}
	}
	$scope.searchfuncio = function () {    	
    	if ( ($scope.search_funcio == undefined  || $scope.search_funcio =="") )
			{
    		loadAllbusines();
			}
    	else
		{    		
			$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
					{
					if(item.funcionality.length > 0)
						{
							var isin = false;
							angular.forEach(item.funcionality, function (funcio) {
								if(funcio.nameFuncionality.toUpperCase().search($scope.search_funcio.toUpperCase()) > -1)
								{
									isin = true;
									return ;
								}	
							});
							return isin;
						}												
						return false;
						});
			$scope.Business = $scope.filterBusiness;
		}
	}
	$scope.searchstate = function () {    	
    	if ( ($scope.search_state == undefined  || $scope.search_state =="") )
			{
    		loadAllbusines();
			}
    	else
		{
			$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
					{
					if(item.businessStates != null)
						{
							if(item.businessStates.name_state_business.toUpperCase().search($scope.search_state.toUpperCase()) > -1)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Business = $scope.filterBusiness;
		}
	}
	$scope.goDetail= function (item) {
		$scope.markersbusi = [];
		$scope.BusinessDetail = item;
		console.log($scope.BusinessDetail)
		 var promiseGetSingle = NotificationCRUDService.getSubcategoryBusiness(item.idbusiness);
	 	 	        promiseGetSingle.then(function (d) {
	 	 	        	console.log(d.data)
	 	 	        	$scope.addSubCategorias = d.data;
	 	 	        },
	 	            function (errorPl) {
	 	                console.log(errorPl);
	 	            });
		if(item.latitude != null)
 		{
 		$scope.markersbusi.push({
			lat: parseFloat(item.latitude),
			lon: parseFloat(item.longitude),
			label: {
//                message:  "<img src=\"examples/images/notredame.jpg\" />",
				message:  item.name_business,
                show: false,
                showOnMouseOver: true
            }
		});	
 		$scope.center.lat = parseFloat(item.latitude);
		$scope.center.lon = parseFloat(item.longitude);
		$scope.showModalBusiness = true;
 		}
 	else
 		{
 		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
			$scope.showModalBusiness = true;
			},
			function (errorPl) {
				console.log("error" + errorPl)
			});
 		}	
		$timeout(function () {
			$scope.showmap = true;
			}, 500);
	};
	$scope.AdicionarModal = function () {
		$scope.showModal = true;
	};
	$scope.CerrarModal = function () {
		$scope.showModal = false;
		$scope.selectAll = false;
		$scope.selectionfuncionality = [];
		$scope.showModalBusiness = false;
	};
//	$scope.CerrarModalBusiness = function () {
//		$scope.showModalBusiness = false;	
//	};
	 angular.extend($scope, {
		   center:{
		      lat: 0,
		      lon: 0,
		      zoom: 12
		    }
		  });
	
	$scope.clear = function () {		
	$scope.selectAll = false;	
	$scope.selectAllBusiness = false;
	$scope.verMessage = false;
	$scope.showModalBusiness = false;
	$scope.markersbusi = [];
	$scope.showmap = false;
	
	$scope.FuncionalityDateEnd = {
			idfuncionality: 0,
			descFuncionality: "",
			nameFuncionality: "",
			pointFuncionality: 0,
			point_plus: 0,
			point_less: 0,
			dateend: new Date(),

		};
	$scope.Businessadd = {
			idbusiness: 0,
			iduser: 0,
			name_business: "",
			desc_business: "",
			code_qr: "",
			dir_business: "",
			web_site: "",
			licence_business: "",
			phone_business: "",
			municipality: 0,
			path_image: "",
			name_image: "",
			latitude: 0,
			longitude: 0,
			funcionality:[]
		};
		$scope.Business = [];
	}		
	//cargar todos los negocios
	function loadAllbusines() {
		$scope.Business = [];
		var recordsGet = BusinessCRUDService.getallBusinessbyActive();
		recordsGet.then(function (d) {
			$scope.templist = d.data;
			angular.forEach($scope.templist, function (bus) {
				var recordsGet = BusinessCRUDService.getFuncionalitybyBusiness(bus.idbusiness);
				recordsGet.then(function (d) {
					$scope.Businessadd = {
							idbusiness: bus.idbusiness,
							iduser: bus.iduser,
							name_business: bus.name_business,
							desc_business: bus.desc_business,
							code_qr: bus.code_qr,
							dir_business: bus.dir_business,
							web_site: bus.web_site,
							licence_business: bus.licence_business,
							phone_business: bus.phone_business,
							municipality: bus.municipality,
							path_image: bus.path_image,
							name_image: bus.name_image,
							latitude: bus.latitude,
							longitude: bus.longitude,
							funcionality:d.data
						};
					$scope.Business.push($scope.Businessadd)
				},
						function (errorPl) {
					$log.error('Error ', errorPl);
				});
			});
			$scope.original = $scope.Business;
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	
	loadAllfuncionalities();
	function loadAllfuncionalities() {
		if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();
		loadAllbusines();
		var recordsGet = FuncionalityCRUDService.getAllFuncionalityNotSystem();
		recordsGet.then(function (d) {
			$scope.Funcionalities = d.data
			angular.forEach($scope.Funcionalities, function (funcio) {
				var dat = new Date();
				dat.setDate(dat.getDate() + funcio.funcionality_days);
				$scope.FuncionalityDateEnd = {
					idfuncionality: funcio.idfuncionality,
					descFuncionality: funcio.descFuncionality,
					nameFuncionality: funcio.nameFuncionality,
					pointFuncionality: funcio.pointFuncionality,
					point_plus: funcio.point_plus,
					point_less: funcio.point_less,
					dateend: dat,

				};
				//                    	console.log($scope.FuncionalityDateEnd);
				$scope.Funcionalitiestoo.push($scope.FuncionalityDateEnd);
			});
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
		$scope.toggleSelectionFuncionality = function toggleSelectionFuncionality(Funcionality) {
		var idx = $scope.selectionfuncionality.indexOf(Funcionality);

		// Is currently selected
		if (idx > -1) {
			$scope.selectionfuncionality.splice(idx, 1);
		}

		// Is newly selected
		else {
			$scope.selectionfuncionality.push(Funcionality);
		}
	};
	$scope.selectedallFuncionalities = function () {
		if($scope.selectAll==true)
		{
			$scope.selectionfuncionality.splice(0);
		}
	else
		{
			angular.forEach($scope.Funcionalitiestoo, function (func) {
				$scope.selectionfuncionality.push(func);
			});
		}
	$scope.selectAll = !$scope.selectAll;
		
	};

	$scope.toggleSelection = function toggleSelection(Business) {
		var idx = $scope.selection.indexOf(Business);
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		else {
			$scope.selection.push(Business);
		}
	};
	$scope.selectedallBusiness = function () {
			if($scope.selectAllBusiness==true)
				{
					$scope.selection.splice(0);
				}
			else
				{
					angular.forEach($scope.Business, function (busi) {
						$scope.selection.push(busi);
					});
				}
			$scope.selectAllBusiness = !$scope.selectAllBusiness;			
		};
		$scope.sendnotification = function(idbusiness,listfuncionality){
			var promiseGetSingle = BusinessCRUDService.sendnotificationfuncionality(idbusiness,listfuncionality );
            promiseGetSingle.then(function (d) {                	
            },        
                function (errorPl) {
                console.log(errorPl);
            });			
		}

	$scope.asigfuncionality = function () {
		angular.forEach($scope.selection, function (selec) {
			angular.forEach($scope.selectionfuncionality, function (funcio) {
				var promisePost = BusinessCRUDService.asigfuncionality(funcio.idfuncionality, selec.idbusiness, $filter('date')(new Date($filter('date')(new Date(funcio.dateend), 'medium'))));
				promisePost.then(function (d) {
					console.log("todo bien")
				}, function (err) {
					console.log(err);
					$scope.selectionfuncionality = [];
					$scope.selection = [];
					$scope.selectAll = false;
					$scope.errorMessage = "FUNCIONALITIES_ASSIGN";
					$scope.verMessageError = true;
				});
			})
			$scope.sendnotification(selec.idbusiness,$scope.selectionfuncionality)
		})
		
		$scope.CerrarModal();
		$scope.selectionfuncionality = [];
		$scope.selection = [];	
		loadAllbusines();
		toast({
   	     duration  : 5000,
   	     message   : $filter('translate')("FUNC_ASIG"),
   	     className : "alert-success",
   	     position: "center",
   	     dismissible: true,
   	   });
				
	};
	$scope.back = function () {
		$window.history.back(-1);	
	}

});

//Business inicio****************************
angular.module('app').controller("BusinessController", function ( $scope,$rootScope, toast, $window, $filter, $location, BusinessCRUDService, ProvinceCRUDService,
	MunicipalityCRUDService, SubcategoryCRUDService, CategoryCRUDService, ConfigurationCRUDService, FuncionalityCRUDService) {
		
	angular.extend($scope, {
		center:{
		      lat: 0,
		      lon: 0,
		      zoom: 12
        },
        defaults: {
            events: {
                map: [ 'singleclick']
            }
        },
        mouseclickposition: {},
        projection: 'EPSG:4326'
    });

    $scope.$on('openlayers.map.singleclick', function(event, data) {
        $scope.$apply(function() {
            if ($scope.projection === data.projection) {
                $scope.mouseclickposition = data.coord;
            } else {
                var p = ol.proj.transform([ data.coord[0], data.coord[1] ], data.projection, $scope.projection);
                $scope.mouseclickposition = {
                    lat: p[1],
                    lon: p[0],
                    projection: $scope.projection
                }
            }
            $scope.Business.latitude = $scope.mouseclickposition.lat;
            $scope.Business.longitude = $scope.mouseclickposition.lon;
            $scope.businessmark = {
            	lat: $scope.Business.latitude,
                lon: $scope.Business.longitude,
            }
        });
    });
	$scope.showModal = false;
	$scope.verMessage = false;
	$scope.verMessageError = false;
	$scope.errorMessage = "";
	$scope.addSubCategorias = [];
	$scope.mostrarboton = false;
	$scope.addSchedule = [];
	$scope.diainicial = new Date();
	$scope.selection = [];
	$scope.selectionfuncionality = [];
	$scope.Funcionalitiestoo = [];
	$scope.diaseleccionados = [];

	$scope.AdicionarModal = function () {
		$scope.clearmodalschedule();
		$scope.showModal = true;
	};
	$scope.CerrarModal = function () {
		$scope.showModal = false;
	};
	$scope.toshowbottomaddschedule = function ()
	{	
		if ($scope.diaseleccionados.length != 0)
			{
				if($scope.Business.tfhours == true)
					{
						return false;
					}
				else
					{
							if($scope.Business.weekday == 1)
							{
								
								if($scope.Business.h_star != "" && $scope.Business.h_end  != "")	
									{
										return false;
									}
								return true;
							}
						if($scope.Business.weekday == 2)
						{
							if($scope.Business.h_star != "" && $scope.Business.h_end  != ""
									&& $scope.Business.h_star1 != "" && $scope.Business.h_end1 != "" )	
								{
									return false;
								}
							return true;
						}
						if($scope.Business.weekday == 3)
						{
							if($scope.Business.h_star != "" && $scope.Business.h_end  != ""
								&& $scope.Business.h_star1 != "" && $scope.Business.h_end1 != ""
									&& $scope.Business.h_star2 != "" && $scope.Business.h_end2 != "")	
								{
									return false;
								}
							return true
							;
						}
					}
								
			}
			return true;
	}
	$scope.deletehorario = function(schedule)
	{
		var idx = $scope.addfortableSchedule.indexOf(schedule)
		$scope.addfortableSchedule.splice(idx,1);
	}
	$scope.disabledsave = function()
	{
		if($scope.addfortableSchedule.length > 0)
		{
			return false;
			}
		return true;
	}
	
	$scope.adicionarhorario = function () {
		if($scope.addfortableSchedule.length == 0)
		{
			angular.forEach($scope.diaseleccionados, function (day) {
					$scope.Schedulefortable = {
							h_star : $scope.Business.h_star ,
							h_end: $scope.Business.h_end,
							h_star1: $scope.Business.h_star1,
							h_end1: $scope.Business.h_end1,
							h_star2: $scope.Business.h_star2,
							h_end2: $scope.Business.h_end2,
					weekday:day.id,	
					tfhours:$scope.Business.tfhours,
				};
					console.log($scope.Schedulefortable)
			$scope.addfortableSchedule.push($scope.Schedulefortable);
			});
			$scope.diaseleccionados = [];
			$scope.selectAll = false;
			$scope.selecttfhours = false;
			$scope.Business.weekday = 0;
			 $scope.Business.h_star="";
			 $scope.Business.h_end="";
			  $scope.Business.h_star1="";
			 $scope.Business.h_end1="";
			  $scope.Business.h_star2="";
			 $scope.Business.h_end2="";
			 $scope.Business.tfhours = false;
			 console.log($scope.addfortableSchedule)
		}
		else
		{
			angular.forEach($scope.diaseleccionados, function (day) {
				var Schedule = $scope.addfortableSchedule.filter(elem => elem.weekday === day.id);
				var idx = $scope.addfortableSchedule.indexOf(Schedule[0])
					if(idx > -1)
					{
						$scope.addfortableSchedule.splice(idx,1);
					}				
				});

			angular.forEach($scope.diaseleccionados, function (day) {
					$scope.Schedulefortable = {
							h_star : $scope.Business.h_star ,
							h_end: $scope.Business.h_end,
							h_star1: $scope.Business.h_star1,
							h_end1: $scope.Business.h_end1,
							h_star2: $scope.Business.h_star2,
							h_end2: $scope.Business.h_end2,
					weekday:day.id,
					tfhours:$scope.Business.tfhours,
				};
			$scope.addfortableSchedule.push($scope.Schedulefortable);
			});
			$scope.diaseleccionados = [];
			$scope.selectAll = false;
			$scope.selecttfhours = false;
			$scope.Business.weekday = 0;
			 $scope.Business.h_star="";
			 $scope.Business.h_end="";
			  $scope.Business.h_star1="";
			 $scope.Business.h_end1="";
			  $scope.Business.h_star2="";
			 $scope.Business.h_end2="";
			 $scope.Business.tfhours = false;
			 console.log($scope.addfortableSchedule)
		}
				
	};

	$scope.daysselected = function daysselected(Day) {
		var idx = $scope.diaseleccionados.indexOf(Day);
		if (idx > -1) {
			$scope.diaseleccionados.splice(idx, 1);
		}
		else {
			$scope.diaseleccionados.push(Day);
		}
	};

	$scope.selectedalldays = function () {
		if($scope.selectAll==true)
		{
			$scope.diaseleccionados.splice(0);
		}
	else
		{
			angular.forEach($scope.dias, function (funcio) {
				$scope.diaseleccionados.push(funcio);
			});
		}
	$scope.selectAll = !$scope.selectAll;		
	};
	
	$scope.tfhours = function () {	
		$scope.Business.weekday = 0
		$scope.selecttfhours = !$scope.selecttfhours;
		$scope.Business.tfhours = $scope.selecttfhours;	
	};

	$scope.out = function () {
		swal({
	  		  title: "",
	  		  text: $filter('translate')("WANT_OUT_BUSINES"),
	  		  type: "warning",
	  		  showCancelButton: true,
	  		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
	  		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
	  		  confirmButtonText: $filter('translate')("ACCEPT"),
	  		  cancelButtonText: $filter('translate')("CANCEL"),
	  		  closeOnConfirm: true,
	  		  closeOnCancel: true
	  		},
	  		function(isConfirm){
	  			if (isConfirm) {
	  				$window.history.back(-1);
	  			  }  		  
	  		}); 		
	}
	$scope.clearmodalschedule = function()
	{
		$scope.selectAll = false;
		$scope.selecttfhours = false;
		$scope.diaseleccionados = [];
		$scope.Business.weekday = 0;
		 $scope.Business.h_star="";
		 $scope.Business.h_end="";
		  $scope.Business.h_star1="";
		 $scope.Business.h_end1="";
		  $scope.Business.h_star2="";
		 $scope.Business.h_end2="";
		 $scope.Business.tfhours=false;
	}

	//limpiar registro
	$scope.clear = function () {
		$scope.vererrorprovince = false;
		$scope.maxlen_namebusi = false;
		$scope.vererrormunicipality = false;
		$scope.maxlen_web_site = false;
		$scope.maxlen_licence_business = false;
		$scope.maxlen_phone_business = false;

		$scope.errorinser = false;
		$scope.addfortableSchedule = [];
		$scope.mySchedule = [];
		$scope.diaseleccionados = [];
		$scope.selectAll = false;
		$scope.selecttfhours = false;
		$scope.addSchedule = [];
		
		var dia = new Array(7);
		dia[0] = { id: 0, name: "Domingo" };
		dia[1] = { id: 1, name: "Lunes" };
		dia[2] = { id: 2, name: "Martes" };
		dia[3] = { id: 3, name: "Miércoles" };
		dia[4] = { id: 4, name: "Jueves" };
		dia[5] = { id: 5, name: "Viernes" };
		dia[6] = { id: 6, name: "Sábado" };
		$scope.dias = dia;
		$scope.newSchedule = {
			weekday:"",
			stime :"",
			etime :"",
			stime1:"",
			etime1:"",
			stime2:"",
			etime2:"",
		};

		$scope.selection = [];		 
		$scope.idsuer = 0;
		$scope.IsNewRecord = 1;
		$scope.FuncionalityDateEnd = {
			idfuncionality: 0,
			descFuncionality: "",
			nameFuncionality: "",
			pointFuncionality: 0,
			point_plus: 0,
			point_less: 0,
			dateend: new Date(),
		};
		$scope.Business = {
			idbusiness: 0,
			iduser: $window.sessionStorage.iduser,
			name_business: "",
			desc_business: "",
			code_qr: "",
			dir_business: "",
			web_site: "",
			licence_business: "",
			phone_business: "",
			municipality: 0,
			Province: 0,
			Category: 0,
			Subcategory: 0,
			path_image: "",
			name_image: "",
			latitude: 0,
			longitude: 0,
			active: false,
			h_end: 0,
			h_star: 0,
			weekday: -1,
			tfhours : false,
		};
		 
		$scope.verMessage = false;
		$scope.verMessageError = false;
		$scope.errorMessage = "";
		$scope.verMessageImage = false;
		$scope.MessageImage = "";
		$scope.verMessageSubcategory = false;
		$scope.MessageSubcategory = "";
	}
	$scope.IsNewRecord = 1;
	loadAllbusines();

	//cargar todos los negocios
	function loadAllbusines() {
		var recordsGet = BusinessCRUDService.getallBusiness();
		recordsGet.then(function (d) {
			$scope.AllBusiness = d.data
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}

	loadAllfuncionalities();
	function loadAllfuncionalities() {
		var recordsGet = FuncionalityCRUDService.getAllFuncionalityNotSystem();
		recordsGet.then(function (d) {
			$scope.Funcionalities = d.data
			angular.forEach($scope.Funcionalities, function (funcio) {

				var dat = new Date();
				dat.setDate(dat.getDate() + funcio.funcionality_days);

				$scope.FuncionalityDateEnd = {
					idfuncionality: funcio.idfuncionality,
					descFuncionality: funcio.descFuncionality,
					nameFuncionality: funcio.nameFuncionality,
					pointFuncionality: funcio.pointFuncionality,
					point_plus: funcio.point_plus,
					point_less: funcio.point_less,
					dateend: dat,

				};
				$scope.Funcionalitiestoo.push($scope.FuncionalityDateEnd);
			});
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}


	$scope.toggleSelectionFuncionality = function toggleSelectionFuncionality(Funcionality) {
		var idx = $scope.selectionfuncionality.indexOf(Funcionality);

		if (idx > -1) {
			$scope.selectionfuncionality.splice(idx, 1);
		}
		else {
			$scope.selectionfuncionality.push(Funcionality);
		}
	};
	$scope.selectedallFuncionalities = function () {
		if($scope.selectAll==true)
		{
			$scope.selectionfuncionality.splice(0);
		}
	else
		{
			angular.forEach($scope.Funcionalitiestoo, function (funcio) {
				$scope.selectionfuncionality.push(funcio);
			});
		}
	$scope.selectAll = !$scope.selectAll;		
	};

	$scope.toggleSelection = function toggleSelection(Business) {
		var idx = $scope.selection.indexOf(Business);

		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		else {
			$scope.selection.push(Business);
		}
	};
	$scope.selectedallBusiness = function () {
		if($scope.selectAllBusiness==true)
		{
			$scope.selection.splice(0);
		}
	else
		{
			angular.forEach($scope.Business, function (funcio) {
				$scope.selection.push(funcio);
			});
		}
	$scope.selectAllBusiness = !$scope.selectAllBusiness;
		
	};
	loadRecords();

	//cargar todos los prestamos
	function loadRecords() {
		$scope.clear();
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
		},
			function (errorPl) {
				console.log("error" + errorPl)
			});

		var recordsGet = MunicipalityCRUDService.getProvinceMunicipality($scope.Business.Province);
		recordsGet.then(function (d) {
			$scope.Municipios = d.data				
			},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
		var recordsGet = ProvinceCRUDService.getAllProvince();
		recordsGet.then(function (d) {
			$scope.Provincias = d.data			
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
		var recordsGet = SubcategoryCRUDService.getCategorySubcategory($scope.Business.Category);
		recordsGet.then(function (d) {
			$scope.Subcategorias = d.data;			
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
		var recordsGet = CategoryCRUDService.getAllCategories();
		recordsGet.then(function (d) {
			$scope.Categorias = d.data;
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	$scope.changecategory = function () {
		var recordsGet = SubcategoryCRUDService.getCategorySubcategory($scope.Business.Category);
		recordsGet.then(function (d) {
			$scope.Subcategorias = d.data
			angular.forEach($scope.addSubCategorias, function (value, key) {
				var Subcatego = $scope.Subcategorias.filter(elem => elem.idsubcategory === value.idsubcategory);
				if (Subcatego.length != 0) {
					$scope.Subcategorias.splice($scope.Subcategorias.indexOf(Subcatego));
				}
			});
			if (d.data.length != 0) {
				$scope.Business.Subcategory = d.data[0].idsubcategory;

			} else {
				$scope.Business.Subcategory = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	$scope.changeprovince = function () {
		var recordsGet = MunicipalityCRUDService.getProvinceMunicipality($scope.Business.Province);
		recordsGet.then(function (d) {
			$scope.Municipios = d.data
			if (d.data.length != 0) {
				$scope.Business.municipality = d.data[0].idmunicipality;

			} else {
				$scope.Business.municipality = 0;
			}
			$scope.vererrorprovince = false;
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	$('#file-input').change(function (e) {
		addImage(e);
	});

	function addImage(e) {
		var file = e.target.files[0],
			imageType = /image.*/;

		if (!file.type.match(imageType))
			return;

		var reader = new FileReader();
		reader.onload = fileOnload;
		reader.readAsDataURL(file);
	}

	function fileOnload(e) {
		var result = e.target.result;
		$('#imgSalida').attr("src", result);
	}
	//guardar prestamos
	$scope.addsubcategorybusiness = function (Business) {
		
		var Subcategoryforadd = $scope.Subcategorias.filter(elem => elem.idsubcategory === Business.Subcategory);
	
		$scope.addSubCategorias.push(Subcategoryforadd[0]);
		$scope.Subcategorias.splice($scope.Subcategorias.indexOf(Subcategoryforadd[0]),1);
		if ($scope.Subcategorias.length === 0) {
			$scope.mostrarboton = true;
		}
		else {
			$scope.Business.Subcategory = $scope.Subcategorias[0].idsubcategory;
			$scope.mostrarboton = false;			
		}
	}	
	$scope.deleteaddsubcategory = function (Subcategory) {
		$scope.addSubCategorias.splice($scope.addSubCategorias.indexOf(Subcategory),1);	
		 $scope.changecategory();
	}
	$scope.save = function (Business) {
		 var locdir = window.location;
		 var pathNamedir = locdir.pathname.substring(0, locdir.pathname.lastIndexOf('/') + 1);
		 var dir = locdir.href.substring(0, locdir.href.length - ((locdir.pathname + locdir.search + locdir.hash).length - pathNamedir.length));
		 
		var file = $scope.myFile;
		if(Business.name_business.length > 254)
			{
				$scope.maxlen_namebusi = true;
			}
		else if(Business.licence_business.length > 254)
		{
			$scope.maxlen_licence_business = true;
		}
		else if(Business.phone_business.length > 10)
		{
			$scope.maxlen_phone_business = true;
		}
		else if(Business.web_site.length > 254)
		{
			$scope.maxlen_web_site = true;
		}
		else if($scope.Business.Province == 0)
			{
			$scope.vererrorprovince = true;
			}
		else if($scope.Business.municipality == 0)
			{
			$scope.vererrormunicipality = true;
			}
		else if ($scope.addSubCategorias.length == 0) {
			$scope.verMessageSubcategory = true;
			$scope.MessageSubcategory = "SUBCATEGORY_REQUIRED";
			$scope.verMessageImage = false;
		}
		else if (file == undefined) {
			$scope.verMessageImage = true;
			$scope.MessageImage = "BUSINESS_IMAGE_REQUIRED";
			$scope.verMessageSubcategory = false;
		}
		else {
			$scope.verMessageImage = false;
			$scope.verMessageSubcategory = false;	
			if(Business.licence_business == undefined)
			{
				Business.licence_business = "";
			}
			if(Business.licence_business == undefined)
			{
				Business.licence_business = "";
			}
			if(Business.phone_business == undefined)
			{
				Business.phone_business = "";
			}
			if(Business.web_site == undefined)
			{
				Business.web_site = "";
			}			
			var BusinessInser = {
				idbusiness: Business.idbusiness,
				iduser: $window.sessionStorage.iduser,
				name_business: Business.name_business,
				desc_business: Business.desc_business,
				code_qr: Business.code_qr,
				dir_business: Business.dir_business,
				web_site: Business.web_site,
				licence_business: Business.licence_business,
				phone_business: Business.phone_business,
				municipality: Business.municipality,
				Province: Business.Province,
				Category: Business.Category,
				Subcategory: Business.Subcategory,
				name_image: Business.name_business,
				active: Business.active,
				latitude: Business.latitude,
				longitude: Business.longitude,
			};
			console.log(BusinessInser)
			if (BusinessInser.latitude === undefined) {
				BusinessInser.latitude = 0;
				BusinessInser.longitude = 0;
			}
			if ($scope.IsNewRecord === 1) {
				var promisePost = BusinessCRUDService.uploadFileToUrl(file, BusinessInser);
				promisePost.then(function (d) {
					$scope.newBusiness = d.data;

					angular.forEach($scope.addSubCategorias, function (sub) {	
						$scope.insertbusinesssubcategory(sub.idsubcategory, $scope.newBusiness)
					});

					$scope.insertnotification($scope.newBusiness, BusinessInser.iduser);

					angular.forEach($scope.addfortableSchedule, function (Schedu) {	
							var promisePost = BusinessCRUDService.saveSchedule($scope.newBusiness.idbusiness,Schedu);
							promisePost.then(function (d) {
							}, function (err) {
								console.log(err);
								$scope.errorMessage = $filter('translate')("ERROR_DATA_SAVE");
								$scope.verMessageError = true;
							});		
						
						});	
					dir=dir+'#!/mybusiness/'+ $scope.newBusiness.idbusiness+'/qr';
					$scope.ModDir = {
							dir:dir,
							idmoddir:0,
					};
					
					var promisePost = BusinessCRUDService.createqr($scope.newBusiness.idbusiness,$scope.ModDir);
					promisePost.then(function (d) {
					}, function (err) {
						console.log(err);
					})
					
					if(!$scope.errorinser)
					{
						$window.sessionStorage.setItem('busninessnumber', parseInt($window.sessionStorage.busninessnumber + 1));				
						$location.path("/home");
						toast({
                 	     duration  : 5000,
                 	     message   : $filter('translate')("BUSINESS_FOR_CHECK"),
                 	     className : "alert-success",
                 	     position: "center",
                 	     dismissible: true,
                 	   });
						var promisePost = BusinessCRUDService.getallBusinessbyUsertoLogin($window.sessionStorage.iduser);
						promisePost.then(function (d) {
							
							$window.sessionStorage.setItem('busninessnumber', d.data.length);
    	                	$rootScope.$broadcast('busninessnumber', { a: $window.sessionStorage.busninessnumber })
    	                	 if (d.data.length ===1)
    	                		{	
    	                		 	$window.sessionStorage.setItem('idbusiness', d.data[0].idbusiness);    	                		
    	                		}
    	                	}
						, function (err) {
							console.log(err);
						});	
					}					
				}, function (err) {
					var exced = "Maximum upload size exceeded";
					$scope.showmaxsize = false;
					$scope.messagemaxsize = $filter('translate')("MAXSIZE");
					if(err.data.message.toUpperCase().search(exced.toUpperCase()) > -1)
						{
							$scope.showmaxsize = true;
						}
					console.log(err.data.message);
					$scope.errorMessage = $filter('translate')("ERROR_DATA_SAVE");
					$scope.verMessageError = true;
				});
			} 
		}
	};
	$scope.asigfuncionality = function () {
		angular.forEach($scope.selection, function (selec) {
			angular.forEach($scope.selectionfuncionality, function (funcio) {
				var promisePost = BusinessCRUDService.asigfuncionality(funcio.idfuncionality, selec.idbusiness, $filter('date')(new Date($filter('date')(new Date(funcio.dateend), 'medium'))));
				promisePost.then(function (d) {
					$scope.CerrarModal();
					$scope.selectionfuncionality = [];
					$scope.selection = [];

				}, function (err) {
					console.log(err);
					$scope.errorMessage = $filter('translate')("FUNCIONALITIES_ASSIGN");
					$scope.verMessageError = true;
				});
			})
		})
	};

	$scope.insertbusinesssubcategory = function (idsubcategory, Business) {
		var promisePost = BusinessCRUDService.addSubcategory(idsubcategory, Business);
		promisePost.then(function (d) {
		}, function (err) {
			console.log(err);
			$scope.errorinser = true;
			$scope.errorMessage = $filter('translate')("ERROR_DATA_SAVE");
			$scope.verMessageError = true;
		});
	};

	$scope.insertnotification = function (Business, iduser) {
		var recordsGet = BusinessCRUDService.addNoti(Business, iduser);
		recordsGet.then(function (d) {
		}, function (err) {
			console.log(err);
			$scope.errorinser = true;
			$scope.errorMessage = $filter('translate')("ERROR_DATA_SAVE");
			$scope.verMessageError = true;
		});
	};

});
angular.module('app').directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function () {
				scope.$apply(function () {
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);
angular.module('app').service('BusinessCRUDService', ['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {


	this.addBusiness = function addBusiness(Business) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/municipality/' + Business.municipality,
			data: Business
		});
	}
	this.getBusiness = function getBusiness(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/' + idbusiness
		});
	}
	this.getallBusiness = function getallBusiness() {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business'
		});
	}
	this.getallMyBusiness = function getallMyBusiness(iduser) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/user/'+iduser+'/mybusiness'
		});
	}	
	this.getallBusinessbyUser = function getallBusinessbyUser(iduser) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/user/'+iduser
		});
	}
	this.getallBusinessbyUsertoLogin = function getallBusinessbyUsertoLogin(iduser) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/user/login/'+iduser
		});
	}
	this.getallBusinessbyActive = function getallBusinessbyActive(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/business/active'
        });
	}
 	this.getBusinessCategoryActive = function getBusinessCategoryActive(idcategory){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/category/business/active'+idcategory
        });
	}
    
	this.saveSchedule = function saveSchedule(idbusiness,Schedule) {
		console.log("asd")
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/schedule',
			data: Schedule
		});
	}

	this.addSubcategory = function addSubcategory(idsubcategory, Business) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/' + Business.idbusiness + '/subcategory/' + idsubcategory
		});
	}

	this.addNoti = function addNoti(Business, iduser) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/' + Business.idbusiness + '/notification/user/' + iduser,
			data: Business
		});
	}
	this.getfuncionalitiesbybusiness = function getfuncionalitiesbybusiness(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/' + idbusiness + '/funcionalities',
		});
	}
	this.asigfuncionality = function asigfuncionality(idfuncionality, idbusiness, dateend) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/asigfuncionality/' + idfuncionality + '/business/' + idbusiness + '/dateend/' + dateend
		});
	}
	this.buyfuncionality = function buyfuncionality( idbusiness, idfuncionality,dateend) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/asigfuncionality/'+idfuncionality+'/business/'+idbusiness+'/dateend/'+dateend+'/user'
		});
	}

	this.clasification = function clasification(idbusiness, clasification, iduser) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/' + idbusiness + '/clasification/' + clasification + '/user/' + iduser
		});
	}

	this.avgclasifications = function avgclasifications(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/' + idbusiness + '/avgclasification/'
		});
	}

	this.getTopBusiness = function getTopBusiness() {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/top/',
		});
	}

	this.uploadFileToUrl = function (file, Business) {
		var fd = new FormData();
		fd.append('file', file);
		fd.append('formulario', angular.toJson(Business));
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/municipality/' + Business.municipality + '/user/' + Business.iduser,
			data: fd,
			transformRequest: angular.identity,
			headers: { 'Content-Type': undefined }
		});
	}

	this.withoutuploadFileToUrl = function (Business) {
		var fd = new FormData();
		fd.append('formulario', angular.toJson(Business));
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/edit/withoutfile',
			data: fd,
			transformRequest: angular.identity,
			headers: { 'Content-Type': undefined }
		});
	}

	this.withuploadFileToUrl = function (file, Business) {

		var fd = new FormData();
		fd.append('file', file);
		fd.append('formulario', angular.toJson(Business));
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/edit/withfile',
			data: fd,
			transformRequest: angular.identity,
			headers: { 'Content-Type': undefined }
		});
	}

	this.getSchedules = function getSchedules(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/schedule/'+ idbusiness
		});
	}
	this.deletesubcategoryforbusiness = function deletesubcategoryforbusiness(iduser) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/deletesubcategory/'+ iduser
		});
	}

	this.deletescheduleforbusiness = function deletescheduleforbusiness(iduser) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/deleteschedule/'+ iduser
		});
	}

	this.savesearch = function savesearch(SearchBusiness) {	
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/search',
			data: SearchBusiness
		});
	}

	this.saveReplyComent = function saveReplyComent(reply,coment, iduser) {	
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/replycoment/idcoment/'+coment.idcoment+"/iduser/"+iduser,
			data:reply
		});
	}

	this.savecoment= function savecoment(coment, iduser) {	
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/coment/user/'+iduser,
			data: coment
		});
	}
	this.getreplysbycoment = function getreplysbycoment(idcoment) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/getreplys/'+ idcoment
		});
	}
	this.reportBusiness = function reportBusiness(idbusines, iduser,ModReport) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusines+'/report/user/'+iduser,
			data: ModReport
		});
	}
	this.saveReservationCharacteristics = function saveReservationCharacteristics(Characteristics, idbusiness) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/reservationcharacteristics',
			data: Characteristics
		});
	}
	
	this.actualizarReservationCharacteristics = function actualizarReservationCharacteristics(Characteristics, idbusiness) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/reservationcharacteristics/actualizar',
			data: Characteristics
		});
	}
	
	this.loadreservationcharacteristics = function loadreservationcharacteristics(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/loadreservationcharacteristics'
		});
	}
	this.madereservation = function madereservation(idbusiness, Reservation, iduser) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/madereservation/iduserreservation/'+iduser,
			data: Reservation
		});
	}
	this.detailreservation = function detailreservation(idreservation) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/detailreservation/'+idreservation
		});
	}
	this.sendemailreservation = function sendemailreservation(idreservation) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/sendmailreservation/'+idreservation
		});
	}
	this.acceptreservation = function acceptreservation(idreservation) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/acceptreservation/'+idreservation
		});
	}
	this.sendmailacceptreservation = function sendmailacceptreservation(idreservation) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/sendmailacceptreservation/'+idreservation
		});
	}
	this.sendmailrejectreservation = function sendmailrejectreservation(idreservation) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/sendmailrejectreservation/'+idreservation
		});
	}
	this.sendnotificationrejectreservation = function sendnotificationrejectreservation(idreservation) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/sendnotificationrejectreservation/'+idreservation
		});
	}
	this.getlistreservation = function getlistreservation(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/listreservation'
		});
	}
	this.sendnotificationfuncionality = function sendnotificationfuncionality(idbusiness,listfuncionality) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/listfuncionality',
			data: listfuncionality
		});
	}
	
	this.getFuncionalitybyBusiness = function getFuncionalitybyBusiness(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/listfuncionality/singel'
		});
	}
	
	this.getIsFavorite = function getIsFavorite(idbusiness,iduser) {		
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/favorite/business/'+idbusiness+'/user/'+iduser
		});
	}
	this.checkFavorite = function checkFavorite(idbusiness,iduser) {		
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/favorite/check/business/'+idbusiness+'/user/'+iduser
		});
	}
	this.unCheckFavorite = function unCheckFavorite(idbusiness,iduser) {		
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/favorite/uncheck/business/'+idbusiness+'/user/'+iduser
		});
	}
	this.countcalification = function countcalification(idbusiness) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/' + idbusiness + '/countcalification/'
		});
	}	
	this.lastsearch = function lastsearch(iduser) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/lastsearch/'+iduser
		});
	}
	this.favo = function favo(iduser) {
		return $http({
			method: 'GET',
			url: RELATIVE_DIR+'/api/business/favo/'+iduser
		});
	}
	this.createqr = function createqr(idbusiness, ModDir) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/business/'+idbusiness+'/createqr',
			data: ModDir
		});
	}
	this.savejob = function savejob(Job) {
		return $http({
			method: 'POST',
			url: RELATIVE_DIR+'/api/job',
			data: Job
		});
	}


}]);
// Business fin

angular.module('app').directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
			max: '=',
			readonly: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function (index) {
				if(scope.readonly != false)
				{
					scope.ratingValue = index + 1;
					scope.onRatingSelected({
						rating: index + 1
					});
				}
                
            };
            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});
angular.module('app').controller("BusinessDetailController", function ($scope, $rootScope, $window,toast, $filter, $location, BusinessCRUDService, ConfigurationCRUDService,
	FuncionalityCRUDService, ComentCRUDService, RELATIVE_DIR, NotificationCRUDService, VisitsService, LoginCRUDService, PromotionCRUDService, OfferCRUDService, $timeout,RegisterUserCRUDService,
	$compile, uiCalendarConfig,ScheduleJobOfferCRUDService, SchoollevelCRUDService, ContractTypeCRUDService, ProfessionalExperienceCRUDService) {
	
	var pathname = window.location.href;
	var urlParts = pathname.split('/');
	var idbusiness = parseInt(urlParts[urlParts.length - 1], 10);
	
	var locdir = window.location;
	 var pathNamedir = locdir.pathname.substring(0, locdir.pathname.lastIndexOf('/') + 1);
	 var dir = locdir.href.substring(0, locdir.href.length - ((locdir.pathname + locdir.search + locdir.hash).length - pathNamedir.length));
	
	
	 $scope.showroute = function()
	 {	 
		 $("#ShowRoute").on("shown.bs.modal", function () { 
			 $window.sessionStorage.setItem('BusinessRoute', JSON.stringify($scope.Business));
			 if($scope.Business.latitude != null)
				 {
				 $rootScope.$broadcast('forshowmap', { d: "si" })
				
				 }				
				
			}).modal('show');
	 }
	 $scope.back = function () {
			$window.history.back(-1);	
		}
	 $scope.changedateendpromo = function (){
	    	$scope.lesszero = false;
	    	if($scope.Promotion.daysPromotion > 0 && $scope.Promotion.daysPromotion != "")
	    		{
					var newdate = new  Date($scope.Promotion.date_start_promotion)
	    			$scope.Promotion.date_end_promotion = new Date(newdate.setDate(newdate.getDate()+$scope.Promotion.daysPromotion));
	    		}
	    	if($scope.Promotion.daysPromotion <= 0 || $scope.Promotion.daysPromotion == null || $scope.Promotion.daysPromotion == undefined)
	    		{
	    			$scope.lesszero = true;
	    			$scope.Promotion.date_end_promotion = "";
	    		}
	    }
	 $scope.changedateendoffer = function (){
	    	$scope.lesszerooffer = false;
	    	if($scope.Offer.daysOffer > 0 && $scope.Offer.daysOffer != "")
	    		{
					var newdate = new  Date($scope.Offer.date_start)
	    			$scope.Offer.date_end = new Date(newdate.setDate(newdate.getDate()+$scope.Offer.daysOffer));
	    		}
	    	if($scope.Offer.daysOffer <= 0 || $scope.Offer.daysOffer == null || $scope.Offer.daysOffer == undefined)
	    		{
	    			$scope.lesszerooffer = true;
	    			$scope.lesszerooffer.date_end = "";
	    		}
	    }	
    $scope.getSelectedRating = function (rating) {		
			var recordsGet = BusinessCRUDService.clasification(idbusiness, rating, $window.sessionStorage.iduser)
			recordsGet.then(function (d) {				
				 var recordsGet2 = BusinessCRUDService.avgclasifications(parseInt(idbusiness));
					recordsGet2.then(function (d) {
						 $scope.forreadonly = false;
						 $scope.showModalRating = false;
						$scope.avgcalification = d.data;
						$scope.iscalification = true;
						var recordsGet2 = BusinessCRUDService.countcalification(parseInt(idbusiness));
						recordsGet2.then(function (b) {
							$scope.countcalification = b.data;
						},
						function (errorPl) {
							console.log( errorPl)
						});
					},
					function (errorPl) {
						console.log( errorPl)
					});
			},
				function (errorPl) {
					console.log("error" + errorPl)
				});
		
	}	
	$scope.fordesabled = function (funcionality) {		
	angular.forEach($scope.funcionalityselected, function (funcio) {
			$scope.arre.push(funcio)
		});
		var n = $scope.arre.findIndex(function (item) {
			return item.idfuncionality.idfuncionality === funcionality.idfuncionality;
		});
		if (n >-1)
		{
			$scope.arre = []
			return false
		}
		if ($scope.ptosuser >= funcionality.pointFuncionality ) {	
			$scope.arre = []
			return true;
		}
		$scope.arre = [];
		return false;
	};
	$scope.desabledallbotton = function () { 
		var varforshow = false;		
		angular.forEach($scope.Funcionalidades, function (funcio) {
			if ($scope.ptosuser >= funcio.pointFuncionality) {
				varforshow = true;
			}	
		});
		return varforshow;
	}
	$scope.fornotenoughpoint = function (funcionality) {
		angular.forEach($scope.funcionalityselected, function (funcio) {
			$scope.arre.push(funcio)
		});
		var n = $scope.arre.findIndex(function (item) {
			return item.idfuncionality.idfuncionality === funcionality.idfuncionality;
		});
		if (n === -1 && $scope.ptosuser >= funcionality.pointFuncionality) {
			return false;
		}
		if (n > -1) { return false; }
		else {
			return true;
		}
	};

	$scope.edit = function () {
		if($scope.mybu)
			{
//			 $window.sessionStorage.setItem('idbusiness', $scope.Business.idbusiness);
				$location.path('/businessedit/'+$scope.Business.idbusiness);
			}
		else
			{
			if($scope.shownotedit)
				{
				$scope.shownotedit = false;
					toast({
		        	     duration  : 4000,
		        	     message   : $filter('translate')("BUSINESS_NOT_EDIT"),
		        	     className : "alert-danger",
		        	     position: "center",
		        	     dismissible: true,
		        	   });
				}			
			}
	};
	$scope.closemodalreport = function () {
		$scope.showModalReport = false;
	}
	$scope.clearmodalreport = function () {
		$scope.ModReport={
				idmodreport:0,
				desc_report:"",
		};		
	}
	$scope.showreportmodal = function () {
		$scope.clearmodalreport();
		$scope.showModalReport = true;
		
	}
	$scope.Report = function (ModReport) {
			var promisePost = BusinessCRUDService.reportBusiness(idbusiness, $window.sessionStorage.iduser,ModReport );
					promisePost.then(function (d) {
						$scope.closemodalreport();
						$scope.forreport = false;
						toast({
	             	     duration  : 3000,
	             	     message   : $filter('translate')("BUSINESS_REPORTED"),
	             	     className : "alert-success",
	             	     position: "center",
	             	     dismissible: true,
	             	   });
					}, function (err) {
				console.log(err);
			});	 	
				
	};
	$scope.AdicionarModal = function () {
		$scope.showModal = true;
		$scope.selectAll = false;
		$scope.verMessageError = false;
		$scope.selection = [];
		if($scope.funcionalityselected.length == $scope.Funcionalidades.length)
		{
			$scope.showselectallfuncionality = false;
		}
	};
	$scope.AdicionarModalComent = function () {
		if($window.sessionStorage.iduser != $scope.Business.iduser.iduser || $scope.reply == true)
			{
				$scope.showModalComent = true;
			}
		else
			{
				if($scope.showtoastcomment)
				{
					$scope.showtoastcomment = false;
					toast({
			    	     duration  : 4000,
			    	     message   : $filter('translate')("OWNER_NOT_COMMENT"),
			    	     className : "alert-success",
			    	     position: "center",
			    	     dismissible: true,
			    	   });
				}
				$(document).ready(function(){
					$('.fixed-top').removeClass('modal-opened');
				  });
					

			}		
	};
	$scope.AdicionarModalReservation = function () {		
		$scope.showModalReservation = true;
	};
	$scope.AdicionarModalCalification = function () {
		$scope.forreadonly = true;
		$scope.rating = 1;	
		$scope.showModalRating = true;
	};
	
	$scope.CerrarModalComent = function () {
		$scope.showModalComent = false;
		$scope.showModalReservation = false;
		$scope.showModalMadeReservation = false;
		$scope.showModalFullCanlendar = false;
		$scope.showModalPromotion = false;
		$scope.showModalOffer = false;
		$scope.reply= false;
		$scope.coment= true;
		$scope.verMessageError = false;
		
		$scope.LimpiarModalComent();
		$scope.LimpiarModalMadeReservation();
		
	};
	$scope.CerrarModal = function () {
		$scope.showModal = false;
		$scope.verMessageError = false;
	};
	
	$scope.LimpiarModalComent = function () {
		$scope.Coment.desc_coment = "";
	};
	$scope.clearfieldpromo = function () {    
		$scope.today = $filter('date')(new Date(), "yyyy-MM-dd");

	    $scope.dateini = new Date();
	    $scope.dateini.setDate($scope.dateini.getDate()+ 1);
	    $scope.lesszero = false;
    	$scope.Promotion = {
    			idpromotion: 0,
    			daysPromotion:0,
    			codePromotion:"", 
    			date_start_promotion: $scope.dateini,
    			date_end_promotion:"",  
    			desc_promotion:"",
    			activePromotion :false,
    			business :$scope.Business,
    			title_promotion :"",
    			urldir:dir+'#!/promotiondetail/',
            };		   
	};
	$scope.clearfieldoffer = function () {
		$scope.today = $filter('date')(new Date(), "yyyy-MM-dd");
		$scope.lesszero = false;
	    $scope.dateini = new Date();
	    $scope.dateini.setDate($scope.dateini.getDate()+ 1);
	    
		$scope.Offer = {
        		idoffer: 0,
				descOffer: "",
				nameOffer: "",
				activeOffer: false,
				date_start: $scope.dateini,
				date_end: "",
				daysOffer:0,
				business: $scope.Business,
				urldir:dir+'#!/offerdetail/',
				};      
	};
	$scope.showmodalpromociones = function (){
		$scope.clearfieldpromo();
		$scope.showModalPromotion = true;
	}
	$scope.showmodalofertas = function (){
		$scope.clearfieldoffer();
		$scope.showModalOffer = true;
	}
	$scope.gonewbusiness = function () {
		$location.path('/business');
	}
	$scope.clonebusines = function () {
//		$window.sessionStorage.setItem('idbusiness', $scope.Business.idbusiness);
		$location.path('/businessclone/'+$scope.Business.idbusiness);
	}
	$scope.showmodaljob = function (){
		
		$scope.clearfieldjob();
		$scope.showModalJob = true;
	}
	$scope.CerrarModalJob = function (){
			
			$scope.showModalJob = false;
	}
	$scope.savejob = function (Job){
		
		var promisePost = BusinessCRUDService.savejob(Job);
		promisePost.then(function (d) {  
			$scope.CerrarModalJob();
			toast({
	    	     duration  : 5000,
	    	     message   : $filter('translate')("JOB_OFFER_INSERT"),
	    	     className : "alert-success",
	    	     position: "center",
	    	     dismissible: true,
	    	   });
          }, function (err) {
              console.log(err);
          });
	}
	$scope.clearfieldjob = function (){
		$scope.daysfuncionality =  $filter('filter')($scope.funcionalityselected, function (item) 
					{
						if(item.idfuncionality.nameFuncionality == "Empleo")
						{
							return true;
						}													
						return false;
						});
			$scope.dayshow = new Date ($scope.daysfuncionality[0].date_end);
			$scope.dayblock = new Date ($scope.daysfuncionality[0].date_end);			
			$scope.dayblock.setDate($scope.dayblock.getDate()+1);
			
			$scope.Job = {
    				titlejob:"",
    				descjob:"",
    				experiencejob:"",
    				dateend: $scope.dayshow,
    				phonejob:"",
    				salaryjob: 0,
    				business: $scope.Business,
    				schedulejoboffer: null,
    				sex: null,
    				maxage:0,
    				minage:0,
    				schoollevel:null,
    				contracttype:null,
    				otherrequirement: "",
    				professionalexperience: null
    		};
	}

	$scope.isowner = function () {
		if($window.sessionStorage.iduser != undefined)
			{
			if($window.sessionStorage.iduser == $scope.Business.iduser.iduser)
				{
					return false;
				}
			else
				{
					return true;
			}
			}
		else
			{
			return true;
			}
		return false;	
	}
	
	$scope.AdicionarModalFullCalendar = function () {
		$scope.reserdetail = {
				title: "",
                start: "",
                end: ""
		};
		var listshowrese=[];
		var indice = 0
		var urltogo = dir+'#!/detailreservation/'
		angular.forEach($scope.listreservation, function (reser) {	
			if($window.sessionStorage.iduser == reser.idreservation || $window.sessionStorage.iduser == reser.business.user.iduser)
				{
					$scope.reserdetail = {
							title:reser.usertoreservation.nameUser,							
			                start: $filter('date')(reser.date, "yyyy-MM-dd"),
			                url: urltogo+reser.idreservation,
			                end: $filter('date')(reser.dateend, "yyyy-MM-dd")
					};
				}
			else
				{
					$scope.reserdetail = {
							title: $filter('translate')("RESERVATION")+(indice+1),
							start: $filter('date')(reser.date, "yyyy-MM-dd"),
			                end: $filter('date')(reser.dateend, "yyyy-MM-dd")
					};
				}
	        
	        listshowrese.push($scope.reserdetail);
       	});
		console.log($window.sessionStorage.language)
		$("#fullcalen").on("shown.bs.modal", function () { 
			$('#calendar').fullCalendar({
		        header: {
		          left: 'prev,next',
		          center: 'title',
		          right: ''
		        },
		        locale: 'es',
		        height: 500,
		        defaultDate: new Date(),
		        navLinks: true, // can click day/week names to navigate views
		        eventLimit: false, // allow "more" link when too many events 
		        events:listshowrese
		      });		
	}).modal('show');
		
	};
	$scope.AdicionarModalForItem = function (finconality) {
		if($window.sessionStorage.userData !=undefined )
			{
				if(finconality.nameFuncionality == "Domicilio")
				{
					if($scope.showtoastdomicilio)
					{
						$scope.showtoastdomicilio = false;
							toast({
				        	     duration  : 4000,
				        	     message   : $filter('translate')("DOMICILE_ON_RESERVATION"),
				        	     className : "alert-success",
				        	     position	: "center",
				        	     dismissible: true,
				        	   });
					}	
					$(document).ready(function(){
						$('.fixed-top').removeClass('modal-opened');
					  });
				}
				if(finconality.nameFuncionality == "Comentario")
				{
					$scope.AdicionarModalComent();
				}
				if(finconality.nameFuncionality == "Reservaciones" && $window.sessionStorage.iduser == $scope.Business.iduser.iduser)
				{
					$scope.loadreservationcharacteristis();					
				}
				if(finconality.nameFuncionality == "Reservaciones" && $window.sessionStorage.iduser != $scope.Business.iduser.iduser)
				{		
						 $scope.Reservation.schedule="";
						 $scope.Reservation.scheduleend="";
					
					$scope.loadmadereservation();			
				}
				if(finconality.nameFuncionality == "Promociones" && $window.sessionStorage.iduser == $scope.Business.iduser.iduser)
				{
					$scope.showmodalpromociones();			
				}
				if(finconality.nameFuncionality == "Promociones" && $window.sessionStorage.iduser != $scope.Business.iduser.iduser)
				{					
					$location.path("/listpromobybusiness/"+$scope.Business.idbusiness)	
					$(document).ready(function(){
						$('.fixed-top').removeClass('modal-opened');
					  });
				}
				if(finconality.nameFuncionality == "Ofertas" && $window.sessionStorage.iduser == $scope.Business.iduser.iduser)
				{
					$scope.showmodalofertas();			
				}
				if(finconality.nameFuncionality == "Empleo")
				{
					if($window.sessionStorage.iduser == $scope.Business.iduser.iduser)
						{
							$scope.showmodaljob();
						}
					else
						{
							if($scope.showtoastemployee)
							{
								$scope.showtoastemployee = false;
								toast({
						    	     duration  : 5000,
						    	     message   : $filter('translate')("ONLY_OWNER"),
						    	     className : "alert-danger",
						    	     position: "center",
						    	     dismissible: true,
						    	   });
							}
							
							$(document).ready(function(){
								$('.fixed-top').removeClass('modal-opened');
							  });
						}
				}
				if(finconality.nameFuncionality == "Ofertas" && $window.sessionStorage.iduser != $scope.Business.iduser.iduser)
				{
					$location.path("/listofferbybusiness/"+$scope.Business.idbusiness)		
					$(document).ready(function(){
						$('.fixed-top').removeClass('modal-opened');
					  });
				}
				if(finconality.nameFuncionality == "Calificaciones" )
				{
					if($window.sessionStorage.iduser != $scope.Business.iduser.iduser)
						{
						if($scope.iscalification == false )
							{
								$scope.AdicionarModalCalification();
							}
						else
							{
								if($scope.showtoastcalification)
								{
									$scope.showtoastcalification = false;
									 toast({
							    	     duration  : 5000,
							    	     message   : $filter('translate')("BUSINESS_CALIFICATION"),
							    	     className : "alert-danger",
							    	     position: "center",
							    	     dismissible: true,
							    	   });
								}
							 
							  $(document).ready(function(){
									$('.fixed-top').removeClass('modal-opened');
								  });
							}
						}
					else{
							if($scope.showtoastcalification)
							{
								$scope.showtoastcalification = false;
								 toast({
						    	     duration  : 5000,
						    	     message   : $filter('translate')("OWNER_NOT_CALIFICATION"),
						    	     className : "alert-danger",
						    	     position: "center",
						    	     dismissible: true,
						    	   });
							}						
					  $(document).ready(function(){
							$('.fixed-top').removeClass('modal-opened');
						  });
					}

													
				}
			}
		else{
			$window.sessionStorage.setItem('toredirect', '/mybusiness/'+$scope.Business.idbusiness);
			$('#Login').modal('show');
			$(document).ready(function(){
				$('.fixed-top').removeClass('modal-opened');
			  });
		}		
	};
	$scope.deletecoment = function (coment) {
		swal({
  		  title: "",
  		  text: $filter('translate')("DELETE_COMENT"),
  		  type: "error",
  		  showCancelButton: true,
  		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
  		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
  		  confirmButtonText: $filter('translate')("DELETE"),
  		  cancelButtonText: $filter('translate')("CANCEL"),
  		  closeOnConfirm: true,
  		  closeOnCancel: true
  		},
  		function(isConfirm){
  			if (isConfirm) {
  			    var promiseDelete = ComentCRUDService.deletecoment(coment);
  	            promiseDelete.then(function (d) {  
  	            	$scope.loadallcomentforbusines();
  	                $scope.Message = "SUCCESSFULY_DELETE";                
  	            }, function (err) {
  	                console.log(err);
  	            });
  			  }
  		  
  		}); 	
	};	
	$scope.replycoment = function (coment) {
		$scope.comenttoreply = coment;	
		$scope.reply= true;
		$scope.coment= false;	
		$scope.AdicionarModalComent();				
	};	
	$scope.saveReplyComent = function (reply) {		
	console.log(reply)
		var promisePost = BusinessCRUDService.saveReplyComent(reply,$scope.comenttoreply,$window.sessionStorage.iduser);
					promisePost.then(function (d) {
						$scope.loadallcomentforbusines();
						$scope.CerrarModalComent();
					}, function (err) {
				console.log(err);
			});			
	};
	$scope.checkfavorite = function () {		
		var promisePost = BusinessCRUDService.checkFavorite($scope.Business.idbusiness,$scope.userlog );
				promisePost.then(function (d) {
					$scope.favo = d.data;
				}, function (err) 
				{
					console.log(err);
				});			
	};
	$scope.uncheckfavorite = function () {		
		var promisePost = BusinessCRUDService.unCheckFavorite($scope.Business.idbusiness,$scope.userlog);
				promisePost.then(function (d) {
					$scope.favo = d.data;
				}, function (err) 
				{
					console.log(err);
				});			
	};	
	
	$scope.savepromotion = function(Promotion)	{
			if(Promotion.daysPromotion>0 && (Promotion.date_end_promotion == "" || Promotion.date_end_promotion == null))
			{
				var newdate = new  Date(Promotion.date_start_promotion)
				Promotion.date_end_promotion = new Date(newdate.setDate(newdate.getDate()+Promotion.daysPromotion));
			}
	        var promisePost = PromotionCRUDService.addPromotion(Promotion);
	        promisePost.then(function (d) {                
	            $scope.Promotion.idpromotion = d.data.idpromotion;
	            $scope.CerrarModalComent();
	            toast({
		    	     duration  : 5000,
		    	     message   : $filter('translate')("PROMOTION_INSERT"),
		    	     className : "alert-success",
		    	     position: "center",
		    	     dismissible: true,
		    	   });

	            
	        }, function (err) {
	            console.log(err);
	        });
	}
	$scope.saveoffer = function(Offer)
	{
		if(Offer.daysOffer>0 && (Offer.date_end == "" || Offer.date_end == null))
		{
			var newdate = new  Date(Offer.date_start)
			Offer.date_end = new Date(newdate.setDate(newdate.getDate()+Offer.daysOffer));
		}
        var promisePost = OfferCRUDService.addOffer(Offer);
        promisePost.then(function (d) {        	
						
            $scope.Offer.idoffer = d.data.idoffer;
            $scope.CerrarModalComent();
            toast({
	    	     duration  : 5000,
	    	     message   : $filter('translate')("OFFER_INSERT"),
	    	     className : "alert-success",
	    	     position: "center",
	    	     dismissible: true,
	    	   });
        }, function (err) {
            console.log(err);
        });
	}
   $scope.changedateend = function (){
    	$scope.lesszero = false;
    	if($scope.Reservation.night > 0 && $scope.Reservation.night != "")
    		{
				var newdate = new  Date($scope.Reservation.date)
    			$scope.Reservation.dateend = new Date(newdate.setDate(newdate.getDate()+$scope.Reservation.night));
    		}
    	if($scope.Reservation.night <= 0 || $scope.Reservation.night == null || $scope.Reservation.night == undefined)
    		{
    			$scope.lesszero = true;
    			$scope.Reservation.dateend = "";
    		}
    } 
	
	$scope.toreadonly = function(funcionality)
	{	
		
		var toretun = true;
				angular.forEach($scope.funcionalityselected, function (funcuser) {	
			        if(funcuser.idfuncionality.idfuncionality === funcionality.idfuncionality )
			        	{			        		
			        		toretun = false;
			        	}
			        /*if(funcionality.nameFuncionality == "Domicilio")
			        	{
			        		toretun = true;
			        	}*/
		       	});			
			return toretun;	
	}
	$scope.saveMadeReservation = function (Reservation) {	
		$scope.errormessagedateend = false;
		$scope.errormessagepersonsamount = false;
		$scope.errormessageschedule = false;
		$scope.errormessagescheduleend = false;
		$scope.errormessagearticle = false;
	
		if($scope.Characteristics.night == true )
			{
				if(Reservation.night == "" || Reservation.night < 1 || Reservation.night == undefined)
					{
						$scope.errormessagedateend = true;	
					}							
			}
		if($scope.Characteristics.personsamount == true)
		{
			if(Reservation.personsamount == "" || Reservation.personsamount < 1 || Reservation.personsamount == undefined)
			{
			$scope.errormessagepersonsamount = true;
			}			
		}
		if($scope.Characteristics.schedule == true )
		{
			if(Reservation.schedule == "" || Reservation.schedule == undefined)
				{
					$scope.errormessageschedule = true;
				}			
		}
		if($scope.Characteristics.scheduleend == true )
		{
			if(Reservation.scheduleend == "" || Reservation.scheduleend == undefined)
				{
					$scope.errormessagescheduleend = true;
				}			
		}
		if($scope.Characteristics.article == true )
		{
			if(Reservation.article == "" || Reservation.article == undefined)
				{
					$scope.errormessagearticle = true;
				}			
		}
		if($scope.errormessagearticle == true || $scope.errormessagescheduleend == true || $scope.errormessageschedule == true
				|| $scope.errormessagepersonsamount == true || $scope.errormessagedateend == true)
			{
				return;
			}
		if(Reservation.night == "" || Reservation.night == null )
			{
				var newdate = new  Date($scope.Reservation.date);
				var newdateend = new  Date($scope.Reservation.dateend);		
				$scope.Reservation.night = (newdateend.getDate()- newdate.getDate());
			}
		if(($scope.Reservation.night!="" || $scope.Reservation.night == null) && Reservation.dateend == "")
			{
				var newdate = new  Date($scope.Reservation.date);
				$scope.Reservation.dateend = new Date(newdate.setDate(newdate.getDate()+$scope.Reservation.night));
			}
		
		var loc = window.location;
	    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
	    $scope.Reservation.urldir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	    
	   $scope.Reservation.urldir =$scope.Reservation.urldir+'#!/detailreservation/';
	    	    
		var recordsPost = BusinessCRUDService.madereservation(idbusiness,$scope.Reservation,$window.sessionStorage.iduser);
		recordsPost.then(function (d) {		
			toast({
	    	     duration  : 5000,
	    	     message   : $filter('translate')("RESERVATION_SENT"),
	    	     className : "alert-success",
	    	     position: "center",
	    	     dismissible: true,
	    	   });
			$scope.showModalMadeReservation = false;
			$scope.LimpiarModalMadeReservation ();
			$scope.sendemailreservation(d.data.idreservation);
		},
		function (errorPl) {
			console.log( errorPl)
		});	
	
	};
	$scope.sendemailreservation = function (idreservation){
		var recordsPost = BusinessCRUDService.sendemailreservation(idreservation);
		recordsPost.then(function (d) {		
			console.log("send")
		},
		function (errorPl) {
			console.log( errorPl)
		});	
	}
	$scope.loadmadereservation = function ()
	{
		var recordsGet = BusinessCRUDService.loadreservationcharacteristics(idbusiness);
		recordsGet.then(function (d) {
			$scope.Characteristics = {
				date:d.data.date,
				dateend:d.data.dateend,
				night:d.data.night,
				personsamount:d.data.personsamount,
				description:d.data.description,
				schedule:d.data.schedule,
				scheduleend:d.data.scheduleend,
				article:d.data.article,
				idreservationcharacteristics:d.data.idreservationcharacteristics,
				domicile:d.data.domicile,
		};
		$scope.showModalMadeReservation = true;
		},
		function (errorPl) {
			console.log( errorPl)
		});		
	}
	
	$scope.showreport = function () {		
		if($window.sessionStorage.iduser != undefined && $window.sessionStorage.iduser.length > 0 && $scope.forreport)
		{
			return true;
		}	
		else{
			return false;
		}
	};
	$scope.LimpiarModalReservation = function()
	{
		$scope.Characteristics = {
				date:true,
				dateend:false,
				night:false,
				personsamount:false,
				description:true,
				schedule:false,
				scheduleend:false,
				article:false,
				domicile:false,
		};
	}
	$scope.LimpiarModalMadeReservation = function()
	{
		$scope.Reservation = {
				idreservation:0,
				date:$scope.dateini,
				dateend:"",
				night:"",
				personsamount:"",
				description:"",
				schedule:"",
				scheduleend:"",
				article:"",
				urldir:"",
				accept:false,
				domicile:false,
				immediate:false,
				dir_reservation:"",
		};
		$scope.errormessagedateend = false;
		$scope.errormessagepersonsamount = false;
		$scope.errormessageschedule = false;
		$scope.errormessagescheduleend = false;
		$scope.errormessagearticle = false;
	}
	
	$scope.loadreservationcharacteristis = function ()
	{
		var recordsGet = BusinessCRUDService.loadreservationcharacteristics(idbusiness);
		recordsGet.then(function (d) {
			if(d.data.date != undefined)
				{
					$scope.Characteristics = {
						date:d.data.date,
						dateend:d.data.dateend,
						night:d.data.night,
						personsamount:d.data.personsamount,
						description:d.data.description,
						schedule:d.data.schedule,
						scheduleend:d.data.scheduleend,
						article:d.data.article,
						idreservationcharacteristics:d.data.idreservationcharacteristics,
						domicile:d.data.domicile,
				};
				}
		
			$scope.AdicionarModalReservation();
		},
		function (errorPl) {
			console.log( errorPl)
		});		
	}
	$scope.saveReservationCharacteristics = function(Characteristics)
	{
		if(Characteristics.idreservationcharacteristics > 0)
			{
				var recordsPost = BusinessCRUDService.actualizarReservationCharacteristics(Characteristics, idbusiness);
				recordsPost.then(function (d) {
					console.log(d.data)
					$scope.CerrarModalComent();
				},
				function (errorPl) {
					console.log( errorPl)
				});
			}
		var recordsPost = BusinessCRUDService.saveReservationCharacteristics(Characteristics, idbusiness);
		recordsPost.then(function (d) {
			console.log(d.data)
			$scope.CerrarModalComent();
		},
		function (errorPl) {
			console.log( errorPl)
		});
	}

	//limpiar registro
	$scope.clear = function () {
		$scope.selection = [];
		$scope.arre = [];
		$scope.ptosuser = $window.sessionStorage.ptosUser;
		$scope.iscalification = false;
		$scope.forreadonly = false;	
		$scope.rating = 1;	
			
		$scope.showtoastcomment = true;
		$scope.showtoastemployee = true;
		$scope.showtoastcalification = true;
		$scope.showtoastdomicilio = true;
		$scope.shownotedit = true;
		$scope.today = new Date();
		$scope.listdateend= [];
		$scope.pathimage = RELATIVE_DIR+"/files/";
		$scope.today = new Date();
		$scope.mybu = false;
		$scope.showcalification = false;
		$scope.showcalificatioavg = false;
		$scope.ptosuser = $window.sessionStorage.ptosUser;
		$scope.userlog = $window.sessionStorage.iduser;
		$scope.errormessagedateend = false;
		$scope.errormessagepersonsamount = false;
		$scope.errormessageschedule = false;
		$scope.errormessagescheduleend = false;
		$scope.errormessagearticle = false;
		$scope.coments = [];
		$scope.listreservation = [];
		$scope.showModalComent = false;
		$scope.showModalReservation = false;
		$scope.showModalMadeReservation = false;
		$scope.showModalOffer = false;
		$scope.showModalReport = false;
		$scope.showModalJob = false;
		$scope.showimage = false;
		$scope.notlogin = true;
		$scope.reply= false;
		$scope.coment= true;	
		$scope.forreport = true;
		$scope.selectAll = false;
		$scope.dateini = new Date();
	    $scope.dateini.setDate($scope.dateini.getDate()+ 1);
	    $scope.showobtainfuncionality = false;
	    $scope.showselectallfuncionality = true;
	    $scope.logged = false;
	    $scope.isadmin = false;
	    $scope.disableroute = true;
	    
	    if($scope.userlog!=undefined)
	    	{
	    		$scope.logged= true;
	    		if(JSON.parse($window.sessionStorage.userData).role.desc_rol == "ROLE_ADMIN")
	    			{
	    				$scope.isadmin = true;
	    			}
	    		
	    	}
	    $scope.imgcheck = RELATIVE_DIR+"/img/favorite_check.png";
	    $scope.imguncheck = RELATIVE_DIR+"/img/favorite_uncheck.png";
	    $scope.imgcalendar = RELATIVE_DIR+"/img/iconcalendar.png";
	    $scope.inreview = false;
	  
	    $scope.favo = false;	
	    
	    $scope.todayreservation = moment().format('YYYY-MM-DD');
		if($window.sessionStorage.userData!=undefined)
		{
			$scope.notlogin = false;
		}
		$scope.Characteristics = {
				date:true,
				dateend:false,
				night:false,
				personsamount:false,
				description:true,
				schedule:false,
				scheduleend:false,
				article:false,
				domicile:false,
		};
		$scope.Job = {
				titlejob:"",
				descjob:"",
				experiencejob:"",
				dateend: $scope.dayshow,
				phonejob:"",
				salaryjob: 0,
				business: $scope.Business,
				schedulejoboffer: null,
				sex: null,
				maxage:0,
				minage:0,
				schoollevel:null,
				contracttype:null,
				otherrequirement: "",
				professionalexperience: null
		};
		$scope.Reservation = {
				idreservation :0,
				date:$scope.dateini,
				dateend:"",
				night:"",
				personsamount:"",
				description:"",
				schedule:"",
				scheduleend:"",
				article:"",
				urldir:"",
				accept:false,
				domicile:false,
				immediate:false,
				dir_reservation:"",
		};
		$scope.Coment = {
        		idcoment :0,
 	        	date_coment :new Date(),
 	        	desc_coment :"",
 	        	business :0,
// 	        	iduser :JSON.parse($window.sessionStorage.userData),
		};
		$scope.Visit = {
				idvisits :0,
				business :0,
				create_date :new Date(),
		};
		$scope.Business = {
			idbusiness: 0,
			iduser: 0,
			name_business: "",
			desc_business: "",
			code_qr: "",
			dir_business: "",
			web_site: "",
			licence_business: "",
			phone_business: "",
			municipality: 0,
			Province: 0,
			Category: 0,
			Subcategory: 0,
			path_image: "",
			name_image: "",
			latitude: 0,
			longitude: 0,
			active: false,
			h_end: 0,
			h_star: 0,
			weekday: -1,
			businessStates: 0,
		};	
		$scope.markersbusi = [];
		$scope.addfortableSchedule = [];
		angular.extend($scope, {
			   center:{
			      lat: 0,
			      lon: 0,
			      zoom: 12
			    }
			  });
		$scope.showhorario = 1;
	}
	loadRecords();
	//cargar todos los prestamos
	function loadRecords() {
		if(isNaN(idbusiness))
			{
				$location.path('/home');
			}
		$scope.clear();	
		 $scope.Sexs = [{
 	  		"idsex": 0, "namesex": "Masculino" },
 	  {"idsex": 1, "namesex": "Femenino"}];	 
		var recordsGet = ScheduleJobOfferCRUDService.getAllScheduleJobOffer()
		recordsGet.then(function (b) {
			$scope.ScheduleJobOffer = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	
    	var recordsGet = SchoollevelCRUDService.getAllSchoollevel()
		recordsGet.then(function (b) {
			$scope.Schoollevel = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	var recordsGet = ContractTypeCRUDService.getAllContractType()
		recordsGet.then(function (b) {
			$scope.ContractType = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	var recordsGet = ProfessionalExperienceCRUDService.getAllProfessionalExperience()
		recordsGet.then(function (b) {
			$scope.ProfessionalExperience = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
			},
			function (errorPl) {
				console.log("error" + errorPl)
			});
			var recordsGet1 = BusinessCRUDService.getlistreservation(idbusiness)
			recordsGet1.then(function (d) {
				$scope.listreservation = d.data;			
			},
			function (errorPl) {
				console.log( errorPl)
			});	
			if($scope.logged)
				{
					var recordsGet1 = BusinessCRUDService.getIsFavorite(idbusiness,$scope.userlog)
					recordsGet1.then(function (d) {
						$scope.favo = d.data;
					},
					function (errorPl) {
						console.log( errorPl)
					});
				}
		var recordsGet = BusinessCRUDService.getBusiness(idbusiness)
		recordsGet.then(function (d) {	
			if($scope.userlog!=undefined && d.data.user.iduser != $scope.userlog)
			{
				var recordsPost = VisitsService.savevisits($scope.userlog,d.data.idbusiness )
				recordsPost.then(function (d) {
				},
				function (errorPl) {
					console.log(errorPl)
				});
			}			
			if(d.data.businessStates.idstate_business == 2)
			{
				$scope.inreview = true;
			}
			var busi = d.data;
			console.log(d.data)
			$scope.Business = {
				idbusiness: busi.idbusiness,
				name_business: busi.name_business,
				desc_business: busi.desc_business,
				dir_business: busi.dir_business,
				web_site: busi.web_site,
				licence_business: busi.licence_business,
				phone_business: busi.phone_business,
				latitude: busi.latitude,
				longitude: busi.longitude,
				name_image:$scope.pathimage+busi.name_image,
				subcategory:busi.subcategory,
				iduser: busi.user,				
			};
			if($scope.Business.latitude != null && $scope.Business.longitude != null)
				{
					$scope.disableroute = false;
					
				}
			var promiseGetSingle = NotificationCRUDService.getSubcategoryBusiness(idbusiness);
		        promiseGetSingle.then(function (d) {
		        	if(d.data != undefined)
		        		{
		        			$scope.Business.subcategory = d.data;
		        		}
		        },
	         function (errorPl) {
	             console.log(errorPl);
	         });
			if($scope.Business.name_image.length>0)			
			{
				$scope.showimage = true;
			}			
			$scope.Coment = {
        		idcoment :0,
 	        	date_coment :new Date(),
 	        	desc_coment :"",
 	        	title_coment :"",
 	        	business :busi,
// 	        	iduser :JSON.parse($window.sessionStorage.userData),
		};
			$scope.FuncionalityUserBusiness = {
					idfunc_business:0,
					date_start : "",
					date_end : "",
					active : true,
					idfuncionality : 0,
					idbusiness : 0,
			}

			if (busi.user.iduser === parseInt($window.sessionStorage.iduser)) {
				$scope.mybu = true;
			}
			if ($scope.mybu == true && busi.businessStates.idstate_business == 1) {
				$scope.showobtainfuncionality = true;
			}
			if (busi.latitude != null) {
				$scope.center.lat = parseFloat(busi.latitude);
				$scope.center.lon = parseFloat(busi.longitude);
				$scope.markersbusi.push({
					lat: parseFloat(busi.latitude),
					lon: parseFloat(busi.longitude),
					label: {
						message:  busi.name_business,
                        show: false,
                        showOnMouseOver: true
                    }
				});	
			}			
			var recordsGet1 = ComentCRUDService.getAllComentforBusiness(parseInt(busi.idbusiness));
			recordsGet1.then(function (d) {
				$scope.coments = d.data;
				console.log($scope.coments)
			},
			function (errorPl) {
				console.log( errorPl)
			});			
			var recordsGet2 = BusinessCRUDService.avgclasifications(parseInt(busi.idbusiness));
				recordsGet2.then(function (d) {
					$scope.avgcalification = d.data;
				},
				function (errorPl) {
					console.log( errorPl)
				});
				var recordsGet2 = BusinessCRUDService.countcalification(parseInt(busi.idbusiness));
				recordsGet2.then(function (d) {
					$scope.countcalification = d.data;
					console.log($scope.countcalification)
				},
				function (errorPl) {
					console.log( errorPl)
				});
				
				var recordsGet = BusinessCRUDService.getfuncionalitiesbybusiness(busi.idbusiness)
				recordsGet.then(function (d) {
					$scope.funcionalityselected = d.data;	
					console.log($scope.funcionalityselected)
					angular.forEach($scope.funcionalityselected, function (funcio) {
						if(funcio.idfuncionality.nameFuncionality == "Calificaciones")		
						{						
								$scope.showcalification = true;													
						}
					});					
				},
					function (errorPl) {
						console.log( errorPl)
					});
		},
			function (errorPl) {
				console.log( errorPl)
			});
		var recordsGet = FuncionalityCRUDService.getAllFuncionalityNotSystem();
		recordsGet.then(function (d) {
			$scope.Funcionalidades = d.data			
			angular.forEach($scope.Funcionalidades, function (funcio) {	
				var newdate = new Date();				 
				 temp = new Date(newdate.setDate(newdate.getDate()+funcio.funcionality_days));
					$scope.listdateend.push(temp);
			});
			
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});		
		
		var recordsGet = BusinessCRUDService.getSchedules(idbusiness);
		recordsGet.then(function (d) {
			angular.forEach(d.data, function (sched) {
			$scope.Scheduleload = {
				h_star :  $filter('date')(sched.h_star, "HH:mm"),
				h_end:  $filter('date')(sched.h_end, "HH:mm"),
				h_star1: $filter('date')(sched.h_star1, "HH:mm"),
				h_end1 :  $filter('date')(sched.h_end1, "HH:mm"),
				h_star2: $filter('date')(sched.h_star2, "HH:mm"),
				h_end2: $filter('date')(sched.h_end2, "HH:mm"),
				weekday:sched.weekday,
				tfhours:sched.tfhours,
			};
			$scope.addfortableSchedule.push($scope.Scheduleload);
		});
			if($scope.addfortableSchedule.length > 0)
				{				
					var Schedule = $scope.addfortableSchedule.filter(elem => elem.weekday === $scope.today.getDay());
					if(Schedule.length > 0)
						{
						console.log(Schedule)
							$scope.tfhours = Schedule[0].tfhours; 
							if(Schedule[0].h_end2 != null)
								{
									$scope.horafin = Schedule[0].h_end2;
								}
							else if (Schedule[0].h_end1 != null)
								{
									$scope.horafin = Schedule[0].h_end1;
								}
							else
								{
									$scope.horafin = Schedule[0].h_end;
								}
							$scope.horainicio = Schedule[0].h_star;
							$scope.showhorario = 3;
						}
					else
						{
							$scope.showhorario = 2;
						}
				}		
			
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
		
	};	
		
	// Toggle selection for a given fruit by name
	$scope.toggleSelection = function toggleSelection(Funcionality) {
		var idx = $scope.selection.indexOf(Funcionality);
		// Is currently selected
		if (idx > -1) {
			$scope.selection.splice(idx, 1);
		}
		// Is newly selected
		else {
			$scope.selection.push(Funcionality);
		}
	};

	$scope.selectedallFuncionality = function () {
		if($scope.selectAll==true)
			{
				$scope.selection.splice(0);
			}
		else
			{
				angular.forEach($scope.Funcionalidades, function (funcio) {
					$scope.selection.push(funcio);
				});
			}
		$scope.selectAll = !$scope.selectAll;		
	};

	$scope.asigfuncionality = function () {
		var totalpoint = 0;
		var funreservation = false;
		angular.forEach($scope.selection, function (selec) {			
			totalpoint = totalpoint + selec.pointFuncionality;
		})
		
		if (parseInt($scope.ptosuser) >= totalpoint) {
			angular.forEach($scope.selection, function (selec) {
				var dat = new Date();
				dat.setDate(dat.getDate() + selec.funcionality_days);
							
				if(selec.nameFuncionality == "Reservaciones")
					{
						funreservation = true;
					}
				var promisePost = BusinessCRUDService.buyfuncionality($scope.Business.idbusiness, selec.idfuncionality,dat);
				promisePost.then(function (d) {
					console.log(d.data);
					
				}, function (err) {
					console.log(err);
					$scope.errorMessage = "FUNCIONALITIES_ASSIGN";
					$scope.verMessageError = true;
				});

			});
				$scope.CerrarModal();
				$scope.selection = [];		
							
				toast({
	         	     duration  : 5000,
	         	     message   : $filter('translate')("FUNCIO_ACTIVATE"),
	         	     className : "alert-success",
	         	     position: "center",
	         	     dismissible: true,
	         	   });				
				 var recordsGet = RegisterUserCRUDService.lesspoint($window.sessionStorage.iduser, totalpoint);
                 recordsGet.then(function (d) {
                	 console.log(d.data)
                	 $window.sessionStorage.setItem('ptosUser', d.data);
                	 $rootScope.$broadcast('ptos', { d: $window.sessionStorage.ptosUser })
                 },
                 function (errorPl) {
                     $log.error( errorPl);
                 });
                 loadRecords();
 				if(funreservation)
 				{
 					$scope.AdicionarModalReservation();
 				}
			
			
		}
		else {
			$scope.errorMessage = "POINT_NO_ENOUGH";
			$scope.verMessageError = true;
		}
	};
	$scope.loadallcomentforbusines = function ()
	{
		var recordsGet1 = ComentCRUDService.getAllComentforBusiness(idbusiness);
					recordsGet1.then(function (d) {
						$scope.coments = d.data;					
					},
					function (errorPl) {
						console.log( errorPl)
					});
	}

	$scope.saveComent = function (Coment ) {
		console.log(Coment)
		var promisePost = BusinessCRUDService.savecoment(Coment,$window.sessionStorage.iduser);
				promisePost.then(function (d) {
					$scope.loadallcomentforbusines();
					$scope.CerrarModalComent();
				}, function (err) {
			console.log(err);
		});
	};

});

//controlador Business List

angular.module('app').controller("BusinessListController", function ($scope, $window, $filter, $location, BusinessCRUDService,ConfigurationCRUDService,RELATIVE_DIR) {

	$scope.Business = [];
	$scope.rutaimagen = RELATIVE_DIR+"/files/";
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	$scope.back = function () {
		$window.history.back(-1);	
	}
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Business, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
		$scope.groupToPages();
		
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
		}
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

	$scope.gonewbusiness = function () {
		$location.path('/business');
	}
	// paginado fin

	loadRecords($window.sessionStorage.iduser);

	//cargar todos los prestamos
	function loadRecords(iduser) {
 		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;

				var recordsGet = BusinessCRUDService.getallMyBusiness(iduser)
				recordsGet.then(function (d) {
					$scope.Business = d.data;
					$scope.search();
				},
					function (errorPl) {
						console.log("error" + errorPl)
					});
			},
			function (errorPl) {
				console.log("error" + errorPl)
			});

		
	}

	$scope.get = function (Business) {
//		$window.sessionStorage.setItem('idbusiness', Business.idbusiness);
		$location.path('/mybusiness/'+Business.idbusiness);
	}
	$scope.goclone = function (Business) {
//		$window.sessionStorage.setItem('idbusiness', Business.idbusiness);
		$location.path('/businessclone/'+Business.idbusiness);
	}
});

angular.module('app').controller("BusinessEditController", function ($scope, toast, $filter, $window, $location, BusinessCRUDService, ProvinceCRUDService, MunicipalityCRUDService,
		ConfigurationCRUDService,CategoryCRUDService,SubcategoryCRUDService, RELATIVE_DIR, $translate) {

	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idbusiness =urlParts[urlParts.length - 1];
	
	angular.extend($scope, {
		center:{
		      lat: 0,
		      lon: 0,
		      zoom: 12
        },
        defaults: {
            events: {
                map: [ 'singleclick']
            }
        },
        mouseclickposition: {},
        projection: 'EPSG:4326'
    });
    $scope.$on('openlayers.map.singleclick', function(event, data) {
        $scope.$apply(function() {
            if ($scope.projection === data.projection) {
                $scope.mouseclickposition = data.coord;
            } else {
                var p = ol.proj.transform([ data.coord[0], data.coord[1] ], data.projection, $scope.projection);
                $scope.mouseclickposition = {
                    lat: p[1],
                    lon: p[0],
                    projection: $scope.projection
                }
            }
            $scope.Business.latitude = $scope.mouseclickposition.lat;
            $scope.Business.longitude = $scope.mouseclickposition.lon;
            $scope.businessmark = {
            	lat: $scope.Business.latitude,
                lon: $scope.Business.longitude,
            }
        });
    });
    
	$scope.addSubCategorias = [];
	$scope.adicionarhorario = function () {
		if($scope.addfortableSchedule.length == 0)
		{
			angular.forEach($scope.diaseleccionados, function (day) {				
					$scope.Schedulefortable = {
							h_star : $scope.Business.h_star ,
							h_end: $scope.Business.h_end,
							h_star1: $scope.Business.h_star1,
							h_end1: $scope.Business.h_end1,
							h_star2: $scope.Business.h_star2,
							h_end2: $scope.Business.h_end2,
					weekday:day.id,
					tfhours:$scope.Business.tfhours,
				};
			$scope.addfortableSchedule.push($scope.Schedulefortable);
			});
			console.log($scope.addfortableSchedule)
			$scope.diaseleccionados = [];
			$scope.selectAll = false;
			$scope.Business.weekday = 0;
			$scope.horario1 = false;
			$scope.horario2 = false;
			$scope.horario3 = false;
			 $scope.Business.h_star="";
			 $scope.Business.h_end="";
			  $scope.Business.h_star1="";
			 $scope.Business.h_end1="";
			  $scope.Business.h_star2="";
			 $scope.Business.h_end2="";
			 $scope.Business.tfhours = false;
			 $scope.tfhours = false;
			 console.log($scope.Schedulefortable)
		}
		else
		{
			angular.forEach($scope.diaseleccionados, function (day) {				
				var Schedule = $scope.addfortableSchedule.filter(elem => elem.weekday === day.id);
			
				var idx = $scope.addfortableSchedule.indexOf(Schedule[0])
				if(idx > -1)
				{
					$scope.addfortableSchedule.splice(idx,1);
				}
				
				});

			angular.forEach($scope.diaseleccionados, function (day) {
					$scope.Schedulefortable = {
							h_star : $scope.Business.h_star ,
							h_end: $scope.Business.h_end,
							h_star1: $scope.Business.h_star1,
							h_end1: $scope.Business.h_end1,
							h_star2: $scope.Business.h_star2,
							h_end2: $scope.Business.h_end2,
					weekday:day.id,
					tfhours:$scope.Business.tfhours,
				};
			$scope.addfortableSchedule.push($scope.Schedulefortable);
			});
			$scope.diaseleccionados = [];
			$scope.selectAll = false;
			$scope.Business.weekday = 0;
			$scope.horario1 = false;
			$scope.horario2 = false;
			$scope.horario3 = false;
			 $scope.Business.h_star="";
			 $scope.Business.h_end="";
			  $scope.Business.h_star1="";
			 $scope.Business.h_end1="";
			  $scope.Business.h_star2="";
			 $scope.Business.h_end2="";
			 $scope.Business.tfhours = false;
			 $scope.tfhours = false
			 console.log($scope.Schedulefortable)
		}
				
	};
	$scope.deletehorario = function(schedule)
	{
		var idx = $scope.addfortableSchedule.indexOf(schedule)
		$scope.addfortableSchedule.splice(idx,1);
	}
	// Toggle selection for a given fruit by name
	$scope.daysselected = function daysselected(Day) {
		var idx = $scope.diaseleccionados.indexOf(Day);
		if (idx > -1) {
			$scope.diaseleccionados.splice(idx, 1);
		}
		else {
			$scope.diaseleccionados.push(Day);
		}
	};

	$scope.selectedalldays = function () {
		if($scope.selectAll==true)
		{
			$scope.diaseleccionados.splice(0);
		}
	else
		{
			angular.forEach($scope.dias, function (di) {
				$scope.diaseleccionados.push(di);
			});
		}
		$scope.selectAll = !$scope.selectAll;
	
	};
	$scope.toshowbottomaddschedule = function ()
	{	
		if ($scope.diaseleccionados.length != 0)
			{
				if($scope.Business.tfhours == true)
				{
					return false;
				}
			else
				{
					if($scope.Business.weekday == 1)
					{
						console.log($scope.Business.h_star == undefined)
						if($scope.Business.h_star != "" && $scope.Business.h_end  != "")	
							{
								return false;
							}
						return true;
					}
					if($scope.Business.weekday == 2)
					{
						if($scope.Business.h_star != "" && $scope.Business.h_end  != ""
								&& $scope.Business.h_star1 != "" && $scope.Business.h_end1 != "" )	
							{
								return false;
							}
						return true;
					}
					if($scope.Business.weekday == 3)
					{
						if($scope.Business.h_star != "" && $scope.Business.h_end  != ""
							&& $scope.Business.h_star1 != "" && $scope.Business.h_end1 != ""
								&& $scope.Business.h_star2 != "" && $scope.Business.h_end2 != "")	
							{
								return false;
							}
						return true
						;
					}
				}
				
			}
			return true;
	}
	
	$scope.tfhours = function () {	
		$scope.Business.weekday = 0
		$scope.selecttfhours = !$scope.selecttfhours;
		$scope.Business.tfhours = $scope.selecttfhours;
		
	};
	
	$('#file-input').change(function (e) {
		addImage(e);
	});

	function addImage(e) {
		var file = e.target.files[0],
			imageType = /image.*/;

		if (!file.type.match(imageType))
			return;

		var reader = new FileReader();
		reader.onload = fileOnload;
		reader.readAsDataURL(file);
	}

	function fileOnload(e) {
		var result = e.target.result;
		$('#imgSalida').attr("src", result);
	}
	
	$scope.clearmodalschedule = function()
	{
		$scope.selectAll = false;
		$scope.diaseleccionados = [];
		$scope.Business.weekday = 0;
		 $scope.Business.h_star="";
		 $scope.Business.h_end="";
		  $scope.Business.h_star1="";
		 $scope.Business.h_end1="";
		  $scope.Business.h_star2="";
		 $scope.Business.h_end2="";
		 $scope.selecttfhours = false;
		 $scope.Business.tfhours = false;
	}
	$scope.AdicionarModal = function () {
		$scope.clearmodalschedule();
		$scope.showModal = true;
	};
	$scope.CerrarModal = function () {		
		$scope.showModal = false;
	};

	$scope.deleteaddsubcategory = function (Subcategory) {
		$scope.addSubCategorias.splice($scope.addSubCategorias.indexOf(Subcategory),1);	
		console.log($scope.addSubCategorias)
		 $scope.changecategory();
	}
	$scope.addsubcategorybusiness = function (Business) {
		var Subcategoryforadd = $scope.Subcategorias.filter(elem => elem.idsubcategory === Business.Subcategory);	
		$scope.addSubCategorias.push(Subcategoryforadd[0]);
		$scope.Subcategorias.splice($scope.Subcategorias.indexOf(Subcategoryforadd[0]),1);
		if ($scope.Subcategorias.length === 0) {
			$scope.mostrarboton = true;
		}
		else {
			$scope.Business.Subcategory = $scope.Subcategorias[0].idsubcategory;
			$scope.mostrarboton = false;			
		}
	}
	$scope.changeprovince = function () {
		var recordsGet = MunicipalityCRUDService.getProvinceMunicipality($scope.Business.Province);
		recordsGet.then(function (d) {
			$scope.Municipios = d.data
			if (d.data.length != 0) {
				$scope.Business.municipality = d.data[0].idmunicipality;
			} else {
				$scope.Business.municipality = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	$scope.changecategory = function () {
		var recordsGet = SubcategoryCRUDService.getCategorySubcategory($scope.Business.Category);
		recordsGet.then(function (d) {
			$scope.Subcategorias = d.data
			angular.forEach($scope.addSubCategorias, function (value, key) {
				
				var Subcatego = $scope.Subcategorias.filter(elem => elem.idsubcategory === value.idsubcategory);
				if (Subcatego.length > 0) {
					var idx = $scope.Subcategorias.indexOf(Subcatego[0])
					if(idx>-1)
					{
						$scope.Subcategorias.splice(idx,1);
					}					
				}
			});
			if ($scope.Subcategorias.length != 0) {
				$scope.Business.Subcategory = $scope.Subcategorias[0].idsubcategory;

			} else {
				$scope.Business.Subcategory = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	
	//limpiar registro
	$scope.clear = function () {
		
		$scope.maxlen_namebusi = false;
		$scope.vererrormunicipality = false;
		$scope.maxlen_web_site = false;
		$scope.maxlen_licence_business = false;
		$scope.maxlen_phone_business = false;
		
		$scope.pathimage = RELATIVE_DIR+"/files/";
		$scope.addfortableSchedule = [];
		$scope.mySchedule = [];
		$scope.diaseleccionados = [];
		$scope.selectAll = false;
		$scope.addSchedule = [];
		$scope.selecttfhours = false;
		var dia = new Array(7);
		dia[0] = { id: 0, name: "Domingo" };
		dia[1] = { id: 1, name: "Lunes" };
		dia[2] = { id: 2, name: "Martes" };
		dia[3] = { id: 3, name: "Miércoles" };
		dia[4] = { id: 4, name: "Jueves" };
		dia[5] = { id: 5, name: "Viernes" };
		dia[6] = { id: 6, name: "Sábado" };
		$scope.dias = dia;
		$scope.newSchedule = {
			weekday:"",
			stime :"",
			etime :"",
			stime1:"",
			etime1:"",
			stime2:"",
			etime2:"",
		};
		$scope.selection = [];
		$scope.forcalification = false;
		$scope.clasification = true;
		$scope.ptosuser = $window.sessionStorage.ptosUser;
		$scope.userlog = $window.sessionStorage.iduser;
		$scope.Business = {
			idbusiness: 0,
			iduser: 0,
			name_business: "",
			desc_business: "",
			code_qr: "",
			dir_business: "",
			web_site: "",
			licence_business: "",
			phone_business: "",
			municipality: 0,
			Province: 0,
			Category: 0,
			Subcategory: 0,
			path_image: "",
			name_image: "",
			latitude: 0,
			longitude: 0,
			active: false,
			h_end: 0,
			h_star: 0,
			weekday: -1,
			closefor:"",
			tfhours: false,
		};
	}
	loadRecords();
	//cargar todos los prestamos
	function loadRecords() {
		$scope.clear();
		$scope.Closesfor = [
			{"idclosefor": 1, "name_closefor": "Permanentemente cerrado" },
    {"idclosefor": 2, "name_closefor": "Trasladado a nueva ubicación"},
    		{"idclosefor": 3, "name_closefor": "Temporalmente cerrado"}];	
		
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
		},
			function (errorPl) {
				console.log("error" + errorPl)
			});		
		$scope.showcalification = false;
		var recordsGet = BusinessCRUDService.getBusiness(idbusiness)
		recordsGet.then(function (d) {
			var busi = d.data;
			console.log(busi)
			if(busi.user.iduser != $scope.userlog)
				{
					toast({
		        	     duration  : 4000,
		        	     message   : $filter('translate')("BUSINESS_NOT_EDIT"),
		        	     className : "alert-danger",
		        	     position: "center",
		        	     dismissible: true,
		        	   });
				}
			$scope.Business = {
				idbusiness: busi.idbusiness,
				name_business: busi.name_business,
				desc_business: busi.desc_business,
				dir_business: busi.dir_business,
				web_site: busi.web_site,
				licence_business: busi.licence_business,
				phone_business: busi.phone_business,
				municipality: busi.municipality.idmunicipality,
				Province: busi.municipality.province.idprovince,
				name_image: busi.name_image,
				latitude: busi.latitude,
				longitude: busi.longitude,
				closefor: busi.closefor,
			};		
			
			$scope.pathimage = $scope.pathimage+busi.name_image;
			console.log($scope.pathimage)
			if (busi.latitude != null) {
				$scope.center.lat = parseFloat(busi.latitude);
				$scope.center.lon = parseFloat(busi.longitude);
			
				$scope.businessmark = {
		            	lat: parseFloat(busi.latitude),
		                lon: parseFloat(busi.longitude),
		            }
			}
			else
				{
				$scope.businessmark = {
		            	lat: 0,
		                lon: 0,
		            }
				}
			var recordsGet = ProvinceCRUDService.getAllProvince();
			recordsGet.then(function (d) {
				$scope.Provincias = d.data
				$scope.Business.Province = $scope.Business.Province;
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});

			var recordsGet = MunicipalityCRUDService.getProvinceMunicipality($scope.Business.Province);
			recordsGet.then(function (d) {
				$scope.Municipios = d.data
				$scope.Business.municipality = $scope.Business.municipality;
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
		var recordsGet = CategoryCRUDService.getAllCategories();
		recordsGet.then(function (d) {
			$scope.Categorias = d.data
			if (d.data.length != 0) {
				$scope.Business.Category = d.data[0].idcategory;
				var recordsGet = SubcategoryCRUDService.getCategorySubcategory($scope.Business.Category);
				recordsGet.then(function (d) {
					$scope.Subcategorias = d.data
					angular.forEach(busi.subcategory, function (subca) {
						$scope.addSubCategorias.push(subca);
						var Subcatego = $scope.Subcategorias.filter(elem => elem.idsubcategory === subca.idsubcategory);
						if (Subcatego.length != 0) {
							$scope.Subcategorias.splice($scope.Subcategorias.indexOf(Subcatego));
						}												
					});
					if (d.data.length != 0) {
						$scope.Business.Subcategory = d.data[0].idsubcategory;
					} else {
						$scope.Business.Subcategory = 0;
					}
				},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
						
			} else {
				$scope.Business.Category = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});	
		var recordsGet = BusinessCRUDService.getSchedules(busi.idbusiness);
			recordsGet.then(function (d) {
				angular.forEach(d.data, function (sched) {
				$scope.Scheduleload = {
					h_star :  $filter('date')(sched.h_star, "HH:mm"),
					h_end:  $filter('date')(sched.h_end, "HH:mm"),
					h_star1: $filter('date')(sched.h_star1, "HH:mm"),
					h_end1 :  $filter('date')(sched.h_end1, "HH:mm"),
					h_star2: $filter('date')(sched.h_star2, "HH:mm"),
					h_end2: $filter('date')(sched.h_end2, "HH:mm"),
					weekday:sched.weekday,
					tfhours:sched.tfhours,
				};
				$scope.addfortableSchedule.push($scope.Scheduleload);
			});
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
		},
			function (errorPl) {
				console.log("error" + errorPl)
			});				
	};

	$scope.save = function (Business) {
		var file = $scope.myFile;
		if(Business.name_business.length > 254)
		{
			$scope.maxlen_namebusi = true;
		}
		else if(Business.licence_business.length > 254)
		{
			$scope.maxlen_licence_business = true;
		}
		else if(Business.phone_business.length > 10)
		{
			$scope.maxlen_phone_business = true;
		}
		else if(Business.web_site.length > 254)
		{
			$scope.maxlen_web_site = true;
		}
		else if ($scope.addSubCategorias.length === 0) {
			$scope.verMessageSubcategory = true;
			$scope.MessageSubcategory = "SUBCATEGORY_REQUIRED";
			$scope.verMessageImage = false;
		}
		else if (file === undefined && Business.name_image.length == 0) {
			$scope.verMessageImage = true;
			$scope.MessageImage = "BUSINESS_IMAGE_REQUIRED";
			$scope.verMessageSubcategory = false;
		}
		else {
			$scope.verMessageImage = false;
			$scope.verMessageSubcategory = false;	
			if(Business.licence_business == undefined)
			{
				Business.licence_business = "";
			}
			if(Business.licence_business == undefined)
			{
				Business.licence_business = "";
			}
			if(Business.phone_business == undefined)
			{
				Business.phone_business = "";
			}
			if(Business.web_site == undefined)
			{
				Business.web_site = "";
			}
			if(Business.closefor == null || Business.closefor == undefined)
			{
				Business.closefor = "";
			}

			$scope.verMessageImage = false;
			$scope.verMessageSubcategory = false;
			var BusinessInser = {
				idbusiness: Business.idbusiness,
				iduser: $window.sessionStorage.iduser,
				name_business: Business.name_business,
				desc_business: Business.desc_business,
				code_qr: Business.code_qr,
				dir_business: Business.dir_business,
				web_site: Business.web_site,
				licence_business: Business.licence_business,
				phone_business: Business.phone_business,
				municipality: Business.municipality,
				Province: Business.Province,
				Category: Business.Category,
				Subcategory: Business.Subcategory,
				name_image: Business.name_business,
				active: Business.active,
				latitude: Business.latitude,
				longitude: Business.longitude,
				closefor: Business.closefor,
			};
			if (BusinessInser.latitude === undefined || BusinessInser.latitude === null) {
				BusinessInser.latitude = 0;
				BusinessInser.longitude = 0;
			}
				if(file != undefined)
				{
					var promisePost = BusinessCRUDService.withuploadFileToUrl(file, BusinessInser);
				promisePost.then(function (d) {
					$scope.newBusiness = d.data;
					var errorinser = false;
					var promisePost = BusinessCRUDService.deletesubcategoryforbusiness($scope.newBusiness.idbusiness);
					promisePost.then(function (d) {
						angular.forEach($scope.addSubCategorias, function (sub) {	
							$scope.insertbusinesssubcategory(sub.idsubcategory, $scope.newBusiness)
						});
					}
					, function (err) {
						errorinser = true;
						console.log(err);
					});

					var promisePost = BusinessCRUDService.deletescheduleforbusiness($scope.newBusiness.idbusiness);
					promisePost.then(function (d) {
						if(d.data)
						{							
							angular.forEach($scope.addfortableSchedule, function (sch) {		
								if(sch.h_star!=null && sch.h_star.length==5)
								{
									sch.h_star = new Date('1970-02-02 '+sch.h_star);
								}						
								if(sch.h_end!=null && sch.h_end.length==5)
								{
									sch.h_end = new Date('1970-02-02 '+sch.h_end);
									
								}
								 if(sch.h_star1!=null && sch.h_star1.length==5)
								{
									sch.h_star1 = new Date('1970-02-02 '+sch.h_star1);
								}
								if(sch.h_end1!=null && sch.h_end1.length==5)
								{
									sch.h_end1 = new Date('1970-02-02 '+sch.h_end1);
								}
								 if(sch.h_star2!=null && sch.h_star2.length==5)
								{
									sch.h_star2 = new Date('1970-02-02 '+sch.h_star2);
								}
								if(sch.h_end2!=null && sch.h_end2.length==5)
								{
									sch.h_end2 = new Date('1970-02-02 '+sch.h_end2);							
								}
								
							});
							angular.forEach($scope.addfortableSchedule, function (Schedu) {	
								var promisePost = BusinessCRUDService.saveSchedule($scope.newBusiness.idbusiness,Schedu);
								promisePost.then(function (d) {
								}, function (err) {
									console.log(err);
									$scope.errorMessage = "Error al salvar los horarios";
									$scope.verMessageError = true;
								});					
						});
						}
					}, function (err) {
						errorinser = true;
						console.log(err);
					});
					if(!errorinser) {				
					$location.path("/mybusiness/"+BusinessInser.idbusiness);
					toast({
                 	     duration  : 5000,
                 	     message   : $filter('translate')("BUSINESS_FOR_CHECK"),
                 	     className : "alert-success",
                 	     position: "center",
                 	     dismissible: true,
						});
					}
				}, function (err) {
					console.log(err);
					$scope.errorMessage = $translate('ERROR_DATA_SAVE');
					$scope.verMessageError = true;
				});
				
			}
			else{
				var promisePost = BusinessCRUDService.withoutuploadFileToUrl(BusinessInser);
				promisePost.then(function (d) {
					$scope.newBusiness = d.data;
							
					var promisePost = BusinessCRUDService.deletesubcategoryforbusiness($scope.newBusiness.idbusiness);
					promisePost.then(function (d) {
						angular.forEach($scope.addSubCategorias, function (sub) {	
							$scope.insertbusinesssubcategory(sub.idsubcategory, $scope.newBusiness)
						});
					}
					, function (err) {
						console.log(err);
					});

					console.log($scope.addfortableSchedule)
					var promisePost = BusinessCRUDService.deletescheduleforbusiness($scope.newBusiness.idbusiness);
					promisePost.then(function (d) {
						if(d.data)
						{
							angular.forEach($scope.addfortableSchedule, function (sch) {		
								if(sch.h_star!=null && sch.h_star.length==5)
								{
									sch.h_star = new Date('1970-02-02 '+sch.h_star);
								}						
								if(sch.h_end!=null && sch.h_end.length==5)
								{
									sch.h_end = new Date('1970-02-02 '+sch.h_end);
									
								}
								 if(sch.h_star1!=null && sch.h_star1.length==5)
								{
									sch.h_star1 = new Date('1970-02-02 '+sch.h_star1);
								}
								if(sch.h_end1!=null && sch.h_end1.length==5)
								{
									sch.h_end1 = new Date('1970-02-02 '+sch.h_end1);
								}
								 if(sch.h_star2!=null && sch.h_star2.length==5)
								{
									sch.h_star2 = new Date('1970-02-02 '+sch.h_star2);
								}
								if(sch.h_end2!=null && sch.h_end2.length==5)
								{
									sch.h_end2 = new Date('1970-02-02 '+sch.h_end2);							
								}
								
							});
							angular.forEach($scope.addfortableSchedule, function (Schedu) {	
								var promisePost = BusinessCRUDService.saveSchedule($scope.newBusiness.idbusiness,Schedu);
								promisePost.then(function (d) {
								}, function (err) {
									console.log(err);
									$scope.errorMessage = $filter('translate')("ERROR_DATA_SAVE");
									$scope.verMessageError = true;
								});					
						});						
						}						
					}
					, function (err) {
						console.log(err);
					});
					$location.path("/mybusiness/"+BusinessInser.idbusiness);
							toast({
							duration  : 5000,
							message   : $filter('translate')("BUSINESS_FOR_CHECK"),
							className : "alert-success",
							position: "center",
							dismissible: true,
						});
					
					
				}, function (err) {
					console.log(err);
					$scope.errorMessage = $filter('translate')("ERROR_DATA_SAVE");
					$scope.verMessageError = true;
				});
			}

		}

	};

	$scope.out = function (Business) {
		swal({
	  		  title: "",
	  		  text: $filter('translate')("WANT_OUT_BUSINES"),
	  		  type: "warning",
	  		  showCancelButton: true,
	  		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
	  		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
	  		  confirmButtonText: $filter('translate')("ACCEPT"),
	  		  cancelButtonText: $filter('translate')("CANCEL"),
	  		  closeOnConfirm: true,
	  		  closeOnCancel: true
	  		},
	  		function(isConfirm){
	  			if (isConfirm) {
	  				$window.history.back(-1);
	  			  }  		  
	  		});
		
	}
	$scope.saveallschedule = function (Schedu) {
		var promisePost = BusinessCRUDService.saveSchedule($scope.newBusiness.idbusiness,Schedu);
		promisePost.then(function (d) {
		}, function (err) {
			console.log(err);
			$scope.errorMessage = "ERROR_DATA_SAVE";
			$scope.verMessageError = true;
		});
	};

	$scope.insertbusinesssubcategory = function (idsubcategory, Business) {
		var promisePost = BusinessCRUDService.addSubcategory(idsubcategory, Business);
		promisePost.then(function (d) {
		}, function (err) {
			console.log(err);
			$scope.errorMessage = "ERROR_DATA_SAVE";
			$scope.verMessageError = true;
		});
	};
});

angular.module('app').controller("BusinessCloneController", function ($scope, toast, $filter, $window, $location, BusinessCRUDService, ProvinceCRUDService, MunicipalityCRUDService,
		ConfigurationCRUDService , CategoryCRUDService,SubcategoryCRUDService, RELATIVE_DIR) {

	var pathname = window.location.href;
	var urlParts = pathname.split('/');
	var idbusinessurl = parseInt(urlParts[urlParts.length - 1], 10);
	
	angular.extend($scope, {
		center:{
		      lat: 0,
		      lon: 0,
		      zoom: 12
        },
        defaults: {
            events: {
                map: [ 'singleclick']
            }
        },
        mouseclickposition: {},
        projection: 'EPSG:4326'
    });
    $scope.$on('openlayers.map.singleclick', function(event, data) {
        $scope.$apply(function() {
            if ($scope.projection === data.projection) {
                $scope.mouseclickposition = data.coord;
            } else {
                var p = ol.proj.transform([ data.coord[0], data.coord[1] ], data.projection, $scope.projection);
                $scope.mouseclickposition = {
                    lat: p[1],
                    lon: p[0],
                    projection: $scope.projection
                }
            }
            $scope.Business.latitude = $scope.mouseclickposition.lat;
            $scope.Business.longitude = $scope.mouseclickposition.lon;
            $scope.businessmark = {
            	lat: $scope.Business.latitude,
                lon: $scope.Business.longitude,
            }
        });
    });
    
	$scope.addSubCategorias = [];
	$scope.adicionarhorario = function () {
		if($scope.addfortableSchedule.length == 0)
		{
			angular.forEach($scope.diaseleccionados, function (day) {
					$scope.Schedulefortable = {
					h_star : $scope.Business.h_star,
					h_end: $scope.Business.h_end,
					h_star1: $scope.Business.h_star1,
					h_end1: $scope.Business.h_end1,
					h_star2: $scope.Business.h_star2,
					h_end2: $scope.Business.h_end2,
					weekday:day.id,
					tfhours:$scope.Business.tfhours,
				};
			$scope.addfortableSchedule.push($scope.Schedulefortable);
			});
			$scope.diaseleccionados = [];
			$scope.selectAll = false;
			$scope.Business.weekday = 0;
			$scope.horario1 = false;
			$scope.horario2 = false;
			$scope.horario3 = false;
			 $scope.Business.h_star="";
			 $scope.Business.h_end="";
			  $scope.Business.h_star1="";
			 $scope.Business.h_end1="";
			  $scope.Business.h_star2="";
			 $scope.Business.h_end2="";
			 $scope.selecttfhours = false;
			 $scope.Business.tfhours = false;
		}
		else
		{
			angular.forEach($scope.diaseleccionados, function (day) {				
				var Schedule = $scope.addfortableSchedule.filter(elem => elem.weekday === day.id);
				var idx = $scope.addfortableSchedule.indexOf(Schedule[0])
				if(idx > -1)
				{
					$scope.addfortableSchedule.splice(idx,1);
				}
				
				});

			angular.forEach($scope.diaseleccionados, function (day) {
					$scope.Schedulefortable = {
					h_star : $scope.Business.h_star,
					h_end: $scope.Business.h_end,
					h_star1: $scope.Business.h_star1,
					h_end1: $scope.Business.h_end1,
					h_star2: $scope.Business.h_star2,
					h_end2: $scope.Business.h_end2,
					weekday:day.id,
					tfhours:$scope.Business.tfhours,
				};
			$scope.addfortableSchedule.push($scope.Schedulefortable);
			});
			$scope.diaseleccionados = [];
			$scope.selectAll = false;
			$scope.Business.weekday = 0;
			$scope.horario1 = false;
			$scope.horario2 = false;
			$scope.horario3 = false;
			 $scope.Business.h_star="";
			 $scope.Business.h_end="";
			  $scope.Business.h_star1="";
			 $scope.Business.h_end1="";
			  $scope.Business.h_star2="";
			 $scope.Business.h_end2="";
			 $scope.selecttfhours = false;
			 $scope.Business.tfhours = false;
		}
				
	};
	$scope.deletehorario = function(schedule)
	{
		var idx = $scope.addfortableSchedule.indexOf(schedule)
		$scope.addfortableSchedule.splice(idx,1);
	}

	// Toggle selection for a given fruit by name
	$scope.daysselected = function daysselected(Day) {
		var idx = $scope.diaseleccionados.indexOf(Day);
		// Is currently selected
		if (idx > -1) {
			$scope.diaseleccionados.splice(idx, 1);
		}
		// Is newly selected
		else {
			$scope.diaseleccionados.push(Day);
		}
	};

	$scope.selectedalldays = function () {
		if($scope.selectAll==true)
		{
			$scope.selection.splice(0);
		}
	else
		{
			angular.forEach($scope.dias, function (funcio) {
				$scope.diaseleccionados.push(funcio);
			});
		}
	$scope.selectAll = !$scope.selectAll;
		
	};
	$scope.toshowbottomaddschedule = function ()
	{	
		if ($scope.diaseleccionados.length != 0)
			{
				if($scope.Business.tfhours == true)
				{
					return false;
				}
			else
				{
					if($scope.Business.weekday == 1)
					{
						
						if($scope.Business.h_star != "" && $scope.Business.h_end  != "")	
							{
								return false;
							}
						return true;
					}
					if($scope.Business.weekday == 2)
					{
						if($scope.Business.h_star != "" && $scope.Business.h_end  != ""
								&& $scope.Business.h_star1 != "" && $scope.Business.h_end1 != "" )	
							{
								return false;
							}
						return true;
					}
					if($scope.Business.weekday == 3)
					{
						if($scope.Business.h_star != "" && $scope.Business.h_end  != ""
								&& $scope.Business.h_star2 != "" && $scope.Business.h_end2 != "")	
							{
								return false;
							}
						return true
						;
					}
				}
				
			}
			return true;
	}
	$scope.tfhours = function () {	
		$scope.Business.weekday = 0
		$scope.selecttfhours = !$scope.selecttfhours;
		$scope.Business.tfhours = $scope.selecttfhours;
		
	};
	
	$('#file-input').change(function (e) {
		addImage(e);
	});

	function addImage(e) {
		var file = e.target.files[0],
			imageType = /image.*/;

		if (!file.type.match(imageType))
			return;

		var reader = new FileReader();
		reader.onload = fileOnload;
		reader.readAsDataURL(file);
	}

	function fileOnload(e) {
		var result = e.target.result;
		$('#imgSalida').attr("src", result);
	}

	$scope.AdicionarModal = function () {
		$scope.clearmodalschedule();
		$scope.showModal = true;
	};
	$scope.CerrarModal = function () {		
		$scope.showModal = false;
	};
	
	$scope.clearmodalschedule = function()
	{
		$scope.selectAll = false;
		$scope.diaseleccionados = [];
		$scope.Business.weekday = 0;
		 $scope.Business.h_star="";
		 $scope.Business.h_end="";
		  $scope.Business.h_star1="";
		 $scope.Business.h_end1="";
		  $scope.Business.h_star2="";
		 $scope.Business.h_end2="";
		 $scope.selecttfhours = false;
		 $scope.Business.tfhours = false;
	}
	$scope.deleteaddsubcategory = function (Subcategory) {
		$scope.addSubCategorias.splice($scope.addSubCategorias.indexOf(Subcategory),1);	
		 $scope.changecategory();
	}

	$scope.addsubcategorybusiness = function (Business) {		
		var Subcategoryforadd = $scope.Subcategorias.filter(elem => elem.idsubcategory === Business.Subcategory);
		
		$scope.addSubCategorias.push(Subcategoryforadd[0]);
		$scope.Subcategorias.splice($scope.Subcategorias.indexOf(Subcategoryforadd));
		if ($scope.Subcategorias.length === 0) {
			$scope.mostrarboton = true;
		}
		else {
			$scope.mostrarboton = false;			
		}
	}
	$scope.changeprovince = function () {
		var recordsGet = MunicipalityCRUDService.getProvinceMunicipality($scope.Business.Province);
		recordsGet.then(function (d) {
			$scope.Municipios = d.data
			console.log($scope.Municipios)
			if (d.data.length != 0) {
				$scope.Business.municipality = d.data[0].idmunicipality;
			} else {
				$scope.Business.municipality = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	$scope.changecategory = function () {
		var recordsGet = SubcategoryCRUDService.getCategorySubcategory($scope.Business.Category);
		recordsGet.then(function (d) {
			$scope.Subcategorias = d.data
			angular.forEach($scope.addSubCategorias, function (value, key) {
				
				var Subcatego = $scope.Subcategorias.filter(elem => elem.idsubcategory === value.idsubcategory);
				
				if (Subcatego.length > 0) {
					var idx = $scope.Subcategorias.indexOf(Subcatego[0])
					if(idx>-1)
					{
						$scope.Subcategorias.splice(idx,1);
					}				
				}
			});
			if (d.data.length != 0) {
				$scope.Business.Subcategory = d.data[0].idsubcategory;

			} else {
				$scope.Business.Subcategory = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}
	//limpiar registro
	$scope.clear = function () {
		
		$scope.maxlen_namebusi = false;
		$scope.vererrormunicipality = false;
		$scope.maxlen_web_site = false;
		$scope.maxlen_licence_business = false;
		$scope.maxlen_phone_business = false;
		
		$scope.pathimage = RELATIVE_DIR+"/files/";
		$scope.addfortableSchedule = [];
		$scope.mySchedule = [];
		$scope.diaseleccionados = [];
		$scope.addSchedule = [];
		$scope.selecttfhours = false;
		var dia = new Array(7);
		dia[0] = { id: 0, name: "Domingo" };
		dia[1] = { id: 1, name: "Lunes" };
		dia[2] = { id: 2, name: "Martes" };
		dia[3] = { id: 3, name: "Miércoles" };
		dia[4] = { id: 4, name: "Jueves" };
		dia[5] = { id: 5, name: "Viernes" };
		dia[6] = { id: 6, name: "Sábado" };
		$scope.dias = dia;

		$scope.newSchedule = {
			weekday:"",
			stime :"",
			etime :"",
			stime1:"",
			etime1:"",
			stime2:"",
			etime2:"",
		};
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
		},
			function (errorPl) {
				console.log("error" + errorPl)
			});
		$scope.selection = [];
		$scope.forcalification = false;
		$scope.clasification = true;
		$scope.ptosuser = $window.sessionStorage.ptosUser;
		$scope.userlog = $window.sessionStorage.iduser;
		$scope.markers = [];
		$scope.Business = {
			idbusiness: 0,
			iduser: 0,
			name_business: "",
			desc_business: "",
			code_qr: "",
			dir_business: "",
			web_site: "",
			licence_business: "",
			phone_business: "",
			municipality: 0,
			Province: 0,
			Category: 0,
			Subcategory: 0,
			path_image: "",
			name_image: "",
			latitude: 0,
			longitude: 0,
			active: false,
			h_end: 0,
			h_star: 0,
			weekday: -1,
			tfhours: false,
		};

	}

	loadRecords();

	//cargar todos los prestamos
	function loadRecords() {
		$scope.clear();
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
		},
			function (errorPl) {
				console.log("error" + errorPl)
			});	
		$scope.showcalification = false;
		var idbusiness = idbusinessurl;
		var recordsGet = BusinessCRUDService.getBusiness(idbusiness)
		recordsGet.then(function (d) {
			var busi = d.data;
			$scope.Business = {
				idbusiness: busi.idbusiness,
				name_business: busi.name_business,
				desc_business: busi.desc_business,
				dir_business: busi.dir_business,
				web_site: busi.web_site,
				licence_business: busi.licence_business,
				phone_business: busi.phone_business,
				municipality: busi.municipality.idmunicipality,
				Province: busi.municipality.province.idprovince,
				name_image: busi.name_image,
				latitude: busi.latitude,
				longitude: busi.longitude,
				tfhours: busi.tfhours,
			};
			if (busi.latitude != null) {
				$scope.center.lat = parseFloat(busi.latitude);
				$scope.center.lon = parseFloat(busi.longitude);
			
				$scope.businessmark = {
		            	lat: parseFloat(busi.latitude),
		                lon: parseFloat(busi.longitude),
		            }
			}
			else
			{
			$scope.businessmark = {
	            	lat: 0,
	                lon: 0,
	            }
			}

			var recordsGet = ProvinceCRUDService.getAllProvince();
			recordsGet.then(function (d) {
				$scope.Provincias = d.data
				$scope.Business.Province = $scope.Business.Province;
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});

			var recordsGet = MunicipalityCRUDService.getProvinceMunicipality($scope.Business.Province);
			recordsGet.then(function (d) {
				$scope.Municipios = d.data
				$scope.Business.municipality = $scope.Business.municipality;
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
		var recordsGet = CategoryCRUDService.getAllCategories();
		recordsGet.then(function (d) {
			$scope.Categorias = d.data
			if (d.data.length != 0) {
				$scope.Business.Category = d.data[0].idcategory;
				var recordsGet = SubcategoryCRUDService.getCategorySubcategory($scope.Business.Category);
				recordsGet.then(function (d) {
					$scope.Subcategorias = d.data
					angular.forEach(busi.subcategory, function (subca) {
						$scope.addSubCategorias.push(subca);
						var Subcatego = $scope.Subcategorias.filter(elem => elem.idsubcategory === subca.idsubcategory);
						if (Subcatego.length != 0) {
							$scope.Subcategorias.splice($scope.Subcategorias.indexOf(Subcatego));
						}												
					});
					if (d.data.length != 0) {
						$scope.Business.Subcategory = d.data[0].idsubcategory;

					} else {
						$scope.Business.Subcategory = 0;
					}
				},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
						
			} else {
				$scope.Business.Category = 0;
			}
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});			

			var recordsGet = BusinessCRUDService.getSchedules(busi.idbusiness);
			recordsGet.then(function (d) {
				angular.forEach(d.data, function (sched) {
				$scope.Scheduleload = {
					h_star :  $filter('date')(sched.h_star, "HH:mm"),
					h_end:  $filter('date')(sched.h_end, "HH:mm"),
					h_star1: $filter('date')(sched.h_star1, "HH:mm"),
					h_end1 :  $filter('date')(sched.h_end1, "HH:mm"),
					h_star2: $filter('date')(sched.h_star2, "HH:mm"),
					h_end2: $filter('date')(sched.h_end2, "HH:mm"),
					weekday:sched.weekday,
					tfhours:sched.tfhours,
				};
				$scope.addfortableSchedule.push($scope.Scheduleload);
			});
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});

		},
			function (errorPl) {
				console.log("error" + errorPl)
			});
				
	};

	$scope.save = function (Business) {
	
		var file = $scope.myFile;
		if(Business.name_business.length > 254)
		{
			$scope.maxlen_namebusi = true;
		}
		else if(Business.licence_business.length > 254)
		{
			$scope.maxlen_licence_business = true;
		}
		else if(Business.phone_business.length > 10)
		{
			$scope.maxlen_phone_business = true;
		}
		else if(Business.web_site.length > 254)
		{
			$scope.maxlen_web_site = true;
		}
		else if ($scope.addSubCategorias.length === 0) {
			$scope.verMessageSubcategory = true;
			$scope.MessageSubcategory = "SUBCATEGORY_REQUIRED";
			$scope.verMessageImage = false;
		}
		else if (file === undefined) {
			$scope.verMessageImage = true;
			$scope.MessageImage = "BUSINESS_IMAGE_REQUIRED";
			$scope.verMessageSubcategory = false;
		}
		else {
			$scope.verMessageImage = false;
			$scope.verMessageSubcategory = false;	
			if(Business.licence_business == undefined)
			{
				Business.licence_business = "";
			}
			if(Business.licence_business == undefined)
			{
				Business.licence_business = "";
			}
			if(Business.phone_business == undefined)
			{
				Business.phone_business = "";
			}
			if(Business.web_site == undefined)
			{
				Business.web_site = "";
			}
			var BusinessInser = {
				idbusiness: Business.idbusiness,
				iduser: $window.sessionStorage.iduser,
				name_business: Business.name_business,
				desc_business: Business.desc_business,
				code_qr: Business.code_qr,
				dir_business: Business.dir_business,
				web_site: Business.web_site,
				licence_business: Business.licence_business,
				phone_business: Business.phone_business,
				municipality: Business.municipality,
				Province: Business.Province,
				Category: Business.Category,
				Subcategory: Business.Subcategory,
				name_image: Business.name_business,
				active: Business.active,
				latitude: Business.latitude,
				longitude: Business.longitude,
			};
			if (BusinessInser.latitude === undefined) {
				BusinessInser.latitude = 0;
				BusinessInser.longitude = 0;
			}			
				var promisePost = BusinessCRUDService.uploadFileToUrl(file, BusinessInser);
				promisePost.then(function (d) {
					$scope.newBusiness = d.data;

					angular.forEach($scope.addSubCategorias, function (sub) {	
						$scope.insertbusinesssubcategory(sub.idsubcategory, $scope.newBusiness)
					});

					$scope.insertnotification($scope.newBusiness, BusinessInser.iduser);

					angular.forEach($scope.addfortableSchedule, function (sch) {		
						if(sch.h_star!=null && sch.h_star.length==5)
						{
							sch.h_star = new Date('1970-02-02 '+sch.h_star);
						}						
						if(sch.h_end!=null && sch.h_end.length==5)
						{
							sch.h_end = new Date('1970-02-02 '+sch.h_end);							
						}
						 if(sch.h_star1!=null && sch.h_star1.length==5)
						{
							sch.h_star1 = new Date('1970-02-02 '+sch.h_star1);
						}
						if(sch.h_end1!=null && sch.h_end1.length==5)
						{
							sch.h_end1 = new Date('1970-02-02 '+sch.h_end1);
						}
						 if(sch.h_star2!=null && sch.h_star2.length==5)
						{
							sch.h_star2 = new Date('1970-02-02 '+sch.h_star2);
						}
						if(sch.h_end2!=null && sch.h_end2.length==5)
						{
							sch.h_end2 = new Date('1970-02-02 '+sch.h_end2);							
						}
						
					});

					angular.forEach($scope.addfortableSchedule, function (Schedu) {	
							var promisePost = BusinessCRUDService.saveSchedule($scope.newBusiness.idbusiness,Schedu);
							promisePost.then(function (d) {
							}, function (err) {
								console.log(err);
								$scope.errorMessage = "ERROR_DATA_SAVE";
								$scope.verMessageError = true;
							});		
						
						});	
					
					if(!$scope.errorinser)
					{
						$window.sessionStorage.setItem('busninessnumber', parseInt($window.sessionStorage.busninessnumber + 1));				
						$location.path("/home");
						toast({
                 	     duration  : 5000,
                 	     message   : $filter('translate')("BUSINESS_FOR_CHECK"),
                 	     className : "alert-success",
                 	     position: "center",
                 	     dismissible: true,
                 	   });
					}
					
				}, function (err) {
					console.log(err);
					$scope.errorMessage = "ERROR_DATA_SAVE";
					$scope.verMessageError = true;
				});			
		}

	};
	

	$scope.out = function () {
		swal({
	  		  title: "",
	  		  text: $filter('translate')("WANT_OUT_BUSINES"),
	  		  type: "warning",
	  		  showCancelButton: true,
	  		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
	  		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
	  		  confirmButtonText: $filter('translate')("ACCEPT"),
	  		  cancelButtonText: $filter('translate')("CANCEL"),
	  		  closeOnConfirm: true,
	  		  closeOnCancel: true
	  		},
	  		function(isConfirm){
	  			if (isConfirm) {
	  				$window.history.back(-1);
	  			  }  		  
	  		});
	}
		$scope.insertnotification = function (Business, iduser) {
		var recordsGet = BusinessCRUDService.addNoti(Business, iduser);
		recordsGet.then(function (d) {
		}, function (err) {
			console.log(err);
		});
	};

	$scope.insertbusinesssubcategory = function (idsubcategory, Business) {
		var promisePost = BusinessCRUDService.addSubcategory(idsubcategory, Business);
		promisePost.then(function (d) {
		}, function (err) {
			console.log(err);
		});
	};
});

angular.module('app').controller("SearchingBusinessListController", function ($scope, $window, $filter, $location,NotificationCRUDService, CategoryCRUDService, ConfigurationCRUDService, BusinessCRUDService, RELATIVE_DIR ) {
	

	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;

		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Business, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
     $scope.logueado = function () {
        if ($window.sessionStorage.iduser != undefined) {
           return true;
		}
		else{
			return false;
		}
    };
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
	$scope.setPage = function (n) {
		console.log(n)
        $scope.currentPage = n;
    };

	// paginado fin
	$scope.Business = [];
	//limpiar registro
	$scope.clear = function () {
		$scope.markersbusi = [];
		$scope.allBusiness = [];
	$scope.rutaimagen = RELATIVE_DIR +"/files/";	
	$scope.Favorite= false;
	$scope.LastSearch= false;
	$scope.Islogged= false;
	if($window.sessionStorage.iduser != undefined)
		{
			$scope.Islogged= true;
		}
	
	$scope.Businessadd = {
			idbusiness: 0,
			iduser:0,
			name_business: "",
			desc_business: "",
			code_qr: "",
			dir_business: "",
			web_site: "",
			licence_business: "",
			phone_business: 0,
			municipality: 0,
			path_image: "",
			name_image: "",
			latitude: 0,
			longitude: 0,
			subcategory:[]
		};
	}

	loadRecords();
	function loadRecords(){
		$scope.clear();

		angular.extend($scope, {
			   center:{
			      lat: 0,
			      lon: 0,
			      zoom: 12
			    }
			  });
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
			$scope.itemsPerPage= d.data[0].recordbypage;
		
			$scope.center.lat = parseFloat(d.data[0].latitude);
			$scope.center.lon = parseFloat(d.data[0].longitude);
			var recordsGet1 = BusinessCRUDService.getallBusinessbyActive();
			recordsGet1.then(function (d) {	
				$scope.activebusi= d.data
						angular.forEach($scope.activebusi, function(bus) {
							var recordsGet = NotificationCRUDService.getSubcategoryBusiness(bus.idbusiness);
							recordsGet.then(function (d) {
								$scope.Businessadd = {
										idbusiness: bus.idbusiness,
										iduser: bus.user,
										name_business: bus.name_business,
										desc_business: bus.desc_business,
										code_qr: bus.code_qr,
										dir_business: bus.dir_business,
										web_site: bus.web_site,
										licence_business: bus.licence_business,
										phone_business: bus.phone_business,
										municipality: bus.municipality,
										path_image: bus.path_image,
										name_image: bus.name_image,
										latitude: bus.latitude,
										longitude: bus.longitude,
										subcategory:d.data
									};
								$scope.allBusiness.push($scope.Businessadd)
							},
								function (errorPl) {
			 	                console.log(errorPl);
			 	            });
						 });
					
				},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
				var allbusinessearching = JSON.parse($window.sessionStorage.searchlistbusiness);			
				angular.forEach(allbusinessearching, function (busi) {					
						$scope.Business.push(busi);
						});	
				$scope.search();	
				angular.forEach($scope.Business, function(bus) {
						if(bus.latitude != undefined || bus.longitude!= undefined)
						{
							$scope.markersbusi.push({
								lat: parseFloat(bus.latitude),
								lon: parseFloat(bus.longitude),
//								name: bus.name_business,
								label: {
//			                        message:  "<img src=\"examples/images/notredame.jpg\" />",
									message:  bus.name_business,
			                        show: false,
			                        showOnMouseOver: true
			                    },
			                    onClick: function (event, properties) {
//			                        console.log(event, properties);
			                    	$location.path('/mybusiness/'+bus.idbusiness);
			                      }
							});	
							}							
						});
				
				var recordsGet = CategoryCRUDService.getAllCategories();
				recordsGet.then(function (d) {
						$scope.Categorias = d.data

						if($window.sessionStorage.categorytosearch != "undefined")
						{
							$scope.Category= JSON.parse($window.sessionStorage.categorytosearch);
						}
										},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
					if($window.sessionStorage.texttosearch != "undefined")
					{
						$scope.Search= JSON.parse($window.sessionStorage.texttosearch);
					}
			
			},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
	}


	$scope.get = function (Business) {
//	window.sessionStorage.setItem('idbusiness', Business.idbusiness);
		$location.path('/mybusiness/'+Business.idbusiness);
	}
	$scope.gonewbusiness = function () {
		$location.path('/business');
	}
	$scope.changecategorySearch = function (listosearch) {
		$scope.Business = $filter('filter')(listosearch, function (item) {
									var isin = false;	
									angular.forEach(item.subcategory, function(sub) {
											if(sub.category.idcategory == $scope.Category)
											{
												isin = true;
												return true;
											}
										});	
									if(isin)
									{	
										return true;
									}
								return false;
								});		

		}
	$scope.changecategorybyBotons = function (btncategory) {
		 var Cat = $filter('filter')($scope.Categorias ,{name_category:btncategory});
		$scope.Category = Cat[0].idcategory;				
	}
	$scope.checkfavo = function () {
		if($scope.LastSearch == true)
			{
				$scope.LastSearch = !$scope.LastSearch;
			}
		$scope.Favorite = !$scope.Favorite;
	}
	$scope.checklastsearch = function () {
		if($scope.Favorite == true)
			{
				$scope.Favorite = !$scope.Favorite;
			}
		$scope.LastSearch = !$scope.LastSearch;
	}
	$scope.findbusi = function (listtoserach) {
		 if($scope.Category != undefined)
			{
				$scope.changecategorySearch(listtoserach);
				$scope.busitosearch = $scope.Business;
			}
			else
				{
					$scope.busitosearch = listtoserach;
				}
			$scope.filterBusiness = $filter('filter')($scope.busitosearch, function (item) {
									
										if(item.name_business.search($scope.Search) > -1)
										{
											return true;
										}
										if(item.desc_business.search($scope.Search) > -1)
										{
											return true;
										}
										if(item.dir_business.search($scope.Search) > -1)
										{
											return true;
										}									
									return false;
								});
			 if($scope.filterBusiness.length ==0)
			 {
			 $scope.filterBusiness = $scope.busitosearch;
			 }
			 $scope.markersbusi = [];
				angular.forEach($scope.filterBusiness, function(bus) {
								if(bus.latitude != undefined || bus.longitude!= undefined)
								{
									$scope.markersbusi.push({
										lat: parseFloat(bus.latitude),
										lon: parseFloat(bus.longitude),
//										name: bus.name_business,
										label: {
					                        message:  bus.name_business,
					                        show: false,
					                        showOnMouseOver: true
					                    },
					                    onClick: function (event, properties) {
//					                        console.log(event, properties);
					                    	$location.path('/mybusiness/'+bus.idbusiness);
					                      }
									});	
								}								
							});	
				$scope.Business = $scope.filterBusiness;
				$scope.search();
	 }
	$scope.Searching = function () {
				if($scope.LastSearch)
				{
					var recordsGet = BusinessCRUDService.lastsearch($window.sessionStorage.iduser);        
					recordsGet.then(function (d) {
						$scope.findbusi(d.data)
					},
					function (errorPl) {
							console.log( errorPl)
						});
				}
				else if($scope.Favorite)
				{
					var recordsGet = BusinessCRUDService.favo($window.sessionStorage.iduser);        
					recordsGet.then(function (d) {
						$scope.findbusi(d.data)
					},
					function (errorPl) {
							console.log( errorPl)
						});
				} 
				else
				{
					$scope.findbusi($scope.allBusiness)
				}
			}
	
});

angular.module('app').directive('backImg', function(){
    return function(scope, element, attrs){
        var url = attrs.backImg;
        element.css({
            'background-image': 'url(' + url +')',
			'background-size' : 'contain',
			'background-position': 'center center',
			'background-repeat': 'no-repeat',
			'height': '180px',
        });
    };
});

angular.module('app').controller("TopBusinessController", function ($scope, $window, $location, BusinessCRUDService, RELATIVE_DIR) {
	
		
	//limpiar registro
	$scope.clear = function () {
	$scope.Business = [];	
	$scope.rutaimagen = RELATIVE_DIR+"/files/";
	$scope.showcalification = false;
	$scope.calif = 
	{
		ptos:0,
	};
	$scope.businesscount = 0;
	$scope.ratings = [];	
	$scope.forreadonly = true;
	}
	loadRecords();
	function loadRecords(){		
		$scope.clear();
		var recordsGet = BusinessCRUDService.getTopBusiness()
				recordsGet.then(function (d) {
					$scope.businesscount = d.data.length;
					var forlength =3
					if(d.data.length < 3){
						forlength = d.data.length;	
					}	
					for( var i = 0; i < forlength ; i++)
					{				
						$scope.Business.push(d.data[i]);				
					}
				},
					function (errorPl) {
						console.log("error" + errorPl)
					});
		}	

	$scope.get = function (Business) {
//		$window.sessionStorage.setItem('idbusiness', Business.business.idbusiness);
		$location.path('/mybusiness/'+Business.business.idbusiness);
	}
	$scope.gonewbusiness = function () {
		$location.path('/business');
	}
	
});
angular.module('app').controller("BuyFuncionalityController", function ( $scope,toast,$rootScope, $timeout,$window, ConfigurationCRUDService, $filter, $location, BusinessCRUDService,
		FuncionalityCRUDService,RegisterUserCRUDService){
	
		var pathname = $window.location.href;	
		var urlParts = pathname.split('/');
		var iduser =urlParts[urlParts.length - 1];
		$scope.selection = [];
		$scope.selectionfuncionality = [];
		$scope.Funcionalitiestoo = [];
//		buscar
		$scope.searchcbusi = function () {    	
	    	if ( ($scope.search_name_business == undefined  || $scope.search_name_business =="") )
				{
	    		loadAllbusines();
				}
	    	else
			{
				$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
						{
						if(item.name_business != null)
							{
								if(item.name_business.toUpperCase().search($scope.search_name_business.toUpperCase()) > -1)
								{
									return true;
								}	
							}												
							return false;
							});
				$scope.Business = $scope.filterBusiness;
			}
		}
		$scope.searchdir = function () {    	
	    	if ( ($scope.search_dirbusines == undefined  || $scope.search_dirbusines =="") )
				{
	    		loadAllbusines();
				}
	    	else
			{
				$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
						{
						if(item.dir_business != null)
							{
								if(item.dir_business.toUpperCase().search($scope.search_dirbusines.toUpperCase()) > -1)
								{
									return true;
								}	
							}												
							return false;
							});
				$scope.Business = $scope.filterBusiness;
			}
		}
		$scope.searchfuncio = function () {    	
	    	if ( ($scope.search_funcio == undefined  || $scope.search_funcio =="") )
				{
	    		loadAllbusines();
				}
	    	else
			{    		
				$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
						{
						if(item.funcionality.length > 0)
							{
								var isin = false;
								angular.forEach(item.funcionality, function (funcio) {
									if(funcio.nameFuncionality.toUpperCase().search($scope.search_funcio.toUpperCase()) > -1)
									{
										isin = true;
										return ;
									}	
								});
								return isin;
							}												
							return false;
							});
				$scope.Business = $scope.filterBusiness;
			}
		}
		$scope.searchstate = function () {    	
	    	if ( ($scope.search_state == undefined  || $scope.search_state =="") )
				{
	    		loadAllbusines();
				}
	    	else
			{
				$scope.filterBusiness = $filter('filter')($scope.original, function (item) 
						{
						if(item.businessStates != null)
							{
								if(item.businessStates.name_state_business.toUpperCase().search($scope.search_state.toUpperCase()) > -1)
								{
									return true;
								}	
							}												
							return false;
							});
				$scope.Business = $scope.filterBusiness;
			}
		}		
		$scope.AdicionarModal = function () {
			$scope.showModal = true;
			$scope.verMessageError = false;			
		};
		$scope.CerrarModal = function () {
			$scope.showModal = false;
			$scope.selectAll = false;
			$scope.selectionfuncionality = [];	
			$scope.verMessageError = false;
			$scope.selection = [];
			$scope.selectAllBusiness = false;
		};
		
		$scope.clear = function () {		
		$scope.selectAll = false;	
		$scope.selectAllBusiness = false;
		$scope.verMessage = false;
		$scope.ptosuser = $window.sessionStorage.ptosUser;
		$scope.FuncionalityDateEnd = {
				idfuncionality: 0,
				descFuncionality: "",
				nameFuncionality: "",
				pointFuncionality: 0,
				point_plus: 0,
				point_less: 0,
				dateend: new Date(),

			};
		}	
		//cargar todos los negocios
		function loadAllbusines() {
			var recordsGet = BusinessCRUDService.getallBusinessbyUser(iduser);
			recordsGet.then(function (d) {
				$scope.Business = d.data
				$scope.original = $scope.Business;
				console.log($scope.Business)
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
		}
		
		loadAllfuncionalities();
		function loadAllfuncionalities() {
			if($window.sessionStorage.nameUser == undefined)
			{			 
				 $('#Login').modal('show');
				 $location.path('/home');
			}
			$scope.clear();
			loadAllbusines();
			var recordsGet = FuncionalityCRUDService.getAllFuncionalityNotSystem();
			recordsGet.then(function (d) {
				$scope.Funcionalities = d.data
				angular.forEach($scope.Funcionalities, function (funcio) {
					var dat = new Date();
					dat.setDate(dat.getDate() + funcio.funcionality_days);
					$scope.FuncionalityDateEnd = {
						idfuncionality: funcio.idfuncionality,
						descFuncionality: funcio.descFuncionality,
						nameFuncionality: funcio.nameFuncionality,
						pointFuncionality: funcio.pointFuncionality,
						point_plus: funcio.point_plus,
						point_less: funcio.point_less,
						dateend: dat,

					};
					//                    	console.log($scope.FuncionalityDateEnd);
					$scope.Funcionalitiestoo.push($scope.FuncionalityDateEnd);
				});
			},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
		}
			$scope.toggleSelectionFuncionality = function toggleSelectionFuncionality(Funcionality) {
			var idx = $scope.selectionfuncionality.indexOf(Funcionality);

			// Is currently selected
			if (idx > -1) {
				$scope.selectionfuncionality.splice(idx, 1);
			}
			// Is newly selected
			else {
				$scope.selectionfuncionality.push(Funcionality);
			}
		};
		$scope.selectedallFuncionalities = function () {
			if($scope.selectAll==true)
			{
				$scope.selectionfuncionality.splice(0);
			}
		else
			{
				angular.forEach($scope.Funcionalitiestoo, function (func) {
					$scope.selectionfuncionality.push(func);
				});
			}
		$scope.selectAll = !$scope.selectAll;
			
		};

		$scope.toggleSelection = function toggleSelection(Business) {
			var idx = $scope.selection.indexOf(Business);
			if (idx > -1) {
				$scope.selection.splice(idx, 1);
			}
			else {
				$scope.selection.push(Business);
			}
		};
		$scope.selectedallBusiness = function () {
				if($scope.selectAllBusiness==true)
					{
						$scope.selection.splice(0);
					}
				else
					{
						angular.forEach($scope.Business, function (busi) {
							$scope.selection.push(busi);
						});
					}
				$scope.selectAllBusiness = !$scope.selectAllBusiness;			
			};

		$scope.asigfuncionality = function () {
			
			var totalpoint = 0;
			var funreservation = false;
			angular.forEach($scope.selectionfuncionality, function (selec) {	
				totalpoint = totalpoint + selec.pointFuncionality;
			})
			totalpoint = totalpoint*$scope.selection.length
			if (parseInt($scope.ptosuser) >= totalpoint) {
				angular.forEach($scope.selection, function (selec) {
					angular.forEach($scope.selectionfuncionality, function (funcio) {
						var promisePost = BusinessCRUDService.buyfuncionality(selec.idbusiness, funcio.idfuncionality,$filter('date')(new Date($filter('date')(new Date(funcio.dateend), 'medium'))));					
						promisePost.then(function (d) {
						console.log(d.data);	
					}, function (err) {
						console.log(err);
						$scope.errorMessage = "FUNCIONALITIES_ASSIGN";
						$scope.verMessageError = true;
						});
					});
				});
					
					$scope.selection = [];					
					 var recordsGet = RegisterUserCRUDService.lesspoint($window.sessionStorage.iduser, totalpoint);
	                 recordsGet.then(function (d) {
	                	 console.log(d.data)
	                	 $window.sessionStorage.setItem('ptosUser', d.data);
	                	 $rootScope.$broadcast('ptos', { d: $window.sessionStorage.ptosUser })
	                 },
	                 function (errorPl) {
	                     $log.error( errorPl);
	                 });
					toast({
			   	     duration  : 5000,
			   	     message   : $filter('translate')("FUNC_ASIG"),
			   	     className : "alert-success",
			   	     position: "center",
			   	     dismissible: true,
			   	   });
					$scope.CerrarModal();
				
						
			}
			else {
				$scope.errorMessage = "POINT_NO_ENOUGH";
				$scope.verMessageError = true;
			}
			
					
				
		};
		$scope.back = function () {
			$window.history.back(-1);	
		}

	});


angular.module('app')
.directive('timepicker', [
      '$window', '$document', 'utils',
      'elemUtils', 'dateUtils', timepicker
    ])
    .directive('timeInput', ['$filter', 'dateUtils', timeInput])
    .directive('renderOnBlur', renderOnBlur)
    .directive('renderOnEnter', renderOnEnter);
  
  function timepicker ($window, $document, utils, elemUtils, dateUtils) {

      return {
        restrict: 'EA',
        scope: {
          'theTime': '=ngModel',
        },
        controller: ['$scope', 'utils', 'dateUtils', TimepickerController],
        replace: true,
        // Template at the bottom of the HTML section 
        templateUrl: 'timepicker.tpl.html',
        link: linker,
      };

      function TimepickerController ($scope, utils, dateUtils) {
        
        var self = this;
        
        $scope.choiceTimes = [];
        $scope.generateChoiceTimes = _generateChoiceTimes;
        $scope.selectPrevTime = _selectPrevTime;
        $scope.selectNextTime = _selectNextTime;
        $scope.isSelectedTime = _isSelectedTime;
        $scope.showingChoiceList = false;
        $scope.showChoiceList = _showChoiceList;
        $scope.hideChoiceList = _hideChoiceList;
        $scope.toggleChoiceList = _toggleChoiceList;
        $scope.setTime = _setTime;
        
        /// Event listeners

        $scope.$watch('theTime', function (newVal, oldVal) {
          if (newVal) { $scope.selectedTime = newVal; }
        });
        
        // Allow the linker to pass a reference to the
        // directive element.
        this.init = function (element, inputField, dropdown) {
          this.element = element;
          this.inputField = inputField;
          this.dropdown = dropdown;
        };
        
        /// Functions

        function _generateChoiceTimes (startingTime) {
          for(var i=0; i<48; i++) {
            // Create new time and normalize to Jan 1, 1970
            var newTime = dateUtils.dateAdd(
              startingTime, 'minute', 30*i
            );
            newTime.setYear(70);
            newTime.setMonth(1);
            newTime.setDate(1);
            // Add to list of choices
            $scope.choiceTimes.push(newTime);
          }
        }

        function _selectPrevTime () {

          // TODO: choiceTimes could be managed better with
          //       something like a circular linked list
          //       with next and prev methods on each node.

          // Don't change the selection if dropdown is not showing
          if (!$scope.showingChoiceList) { return; }

          // Get the next index value, based on the current selection.
          // If this is the first item, wrap around to the last item
          var selectedIndex = _getSelectedTimeIndex();

          var nextIndex = selectedIndex === 0 ? $scope.choiceTimes.length - 1
                                              : selectedIndex - 1;
          
          // Update the selected time
          $scope.selectedTime = $scope.choiceTimes[nextIndex];
          $scope.$apply();
          
        }
        
        function _selectNextTime () {

          // TODO: choiceTimes could be managed better with
          //       something like a circular linked list
          //       with next and prev methods on each node.

          // Don't change the selection if dropdown is not showing
          if (!$scope.showingChoiceList) { return; }

          // Get the next index value, based on the current selection.
          // If this is the first item, wrap around to the last item
          var selectedIndex = _getSelectedTimeIndex();
          var nextIndex = selectedIndex === $scope.choiceTimes.length - 1
                                              ? 0 : selectedIndex + 1;

          // Update the selected time
          $scope.selectedTime = $scope.choiceTimes[nextIndex];
          $scope.$apply();

        }
        
        /** 
         * Date objects cannot be directly compared. Instead,
         * coerce to a `Number` before making the comparison.
         **/
        function _isSelectedTime (time) {
          return Number(time) === Number($scope.selectedTime);
        }
        
        /**
         * Like the Array.indexOf() method: find the index of
         * the current time selection in the array or return `-1`.
         **/
        function _getSelectedTimeIndex () {
          for (var i = 0; i < $scope.choiceTimes.length; i++) {
            if (_isSelectedTime($scope.choiceTimes[i])) {
              return i;
            }
          }
          return -1;
        }
        
        function _showChoiceList (event) {
          if ($scope.showingChoiceList === true) { return; }
          $scope.showingChoiceList = true;
          $scope.$apply();
          $document.on('click', _hideChoiceList);
        }

        function _hideChoiceList (event) {
          if ($scope.showingChoiceList === false) { return; }
          if (event && (self.inputField[0].contains(event.target)
                        || self.dropdown[0].contains(event.target))) {
            return;
          }
          $scope.showingChoiceList = false;
          $scope.$apply();
          $document.off('click', _hideChoiceList);
        }

        function _toggleChoiceList (event) {
          if ($scope.showingChoiceList) { _hideChoiceList(event); }
          else { _showChoiceList(event); }
          $scope.$apply();
        }

        function _setTime(time) {
          $scope.theTime = time;
        }

      }

      function linker (scope, element, attrs, tpCtrl) {
        
        // initialize field value
        var inputField = element.find('input'),
            dropdown = angular.element(
                element[0].querySelector('.timepicker-dropdown')
            );

        // Pass element references to controller
        tpCtrl.init(element, inputField, dropdown);

        // Initialize scope variables
        scope.labelText = attrs.label || '';
        scope.theTime = attrs.defaultTime 
                          ? dateUtils.parseTimeStringToDate(attrs.defaultTime)
                          : new Date(1970, 1, 1, 0, 0, 0, 0);
        scope.selectedTime = scope.theTime;

        scope.generateChoiceTimes(scope.selectedTime);

        /// Event Listeners
        inputField.on('focus + click', function (event) {
          inputField[0].select();
          scope.showChoiceList();
          _updateDropdownPosition();
          scope.$apply();
          _updateDropdownScroll();
        });

        inputField.on('keydown', function (event) {
          switch(event.keyCode) {
            case 38: // Up arrow
              scope.selectPrevTime();
              _updateDropdownScroll();
              break;
            case 40: // Down arrow
              scope.selectNextTime();
              _updateDropdownScroll();
              break; 
            case 13: // Enter key
              scope.setTime(scope.selectedTime);
              scope.hideChoiceList();
              break;
            default: // Any other key
              // Any other should close the dropdown
              scope.hideChoiceList();
              _updateDropdownScroll();
              break;
          }
        });
        
        //
        dropdown.on('click', function (event) {
          inputField[0].focus();
          scope.$apply();
          scope.hideChoiceList();
        })

        // Hide the dropdown menu while resizing. Debounce for performance.
        $window.addEventListener('resize', utils.debounce(function () {
          dropdown.css({visibility: 'hidden'});
        }, 50, true));
        $window.addEventListener('resize', utils.debounce(function () {
          _updateDropdownPosition();
          dropdown.css({visibility: 'visible'});
        }, 50));
        
        /// Functions
        
        function _updateDropdownScroll () {
          var selectedTimeDiv = _getSelectedTimeDiv();
          if (selectedTimeDiv) {
            dropdown[0].scrollTop = selectedTimeDiv.offsetTop;
          }
        }
        
        /**
         * Get div corresponding to selected time.
         * ids are in the format of 'd' + timestamp
         */
        function _getSelectedTimeDiv () {
          return dropdown[0].querySelector(
            '#d' + String(Number(scope.selectedTime))
          );
        }

        function _updateDropdownPosition () {
          // Reposition the dropdown.
          // This needs to be called after the dropdown has been revealed, in
          // order for the width of the dropdown to be read properly.
          var dropdownPosition = elemUtils.below(
            inputField[0], dropdown[0]
          );
          dropdown.css('top', dropdownPosition.top + 'px');
          dropdown.css('left', dropdownPosition.left + 'px');
        }

      }
  }

  function timeInput ($filter, dateUtils) {

      return {
          restrict: 'A',
          require: 'ngModel',
          link: linker,
      };

      function linker (scope, element, attrs, ngModelCtrl) {

        var dateFormat = 'h:mm a';

        ngModelCtrl.$formatters = [function (modelValue) {
          return $filter('date')(modelValue, dateFormat);
        }];

        ngModelCtrl.$parsers.unshift(function (viewValue) {
          return dateUtils.parseTimeStringToDate(viewValue);
        });

      }

  }

  /**
   * Re-renders the ng-model attached to an input when the input
   * loses focus.
   **/
  function renderOnBlur () {

      return {
          restrict: 'A',
          require: 'ngModel',
          link: linker,
      };

      function linker (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function () {
              if (!ngModelCtrl.$modelValue) { return; }
              var viewValue = ngModelCtrl.$modelValue;
              for (var i = 0; i < ngModelCtrl.$formatters.length; i++) {
                  viewValue = ngModelCtrl.$formatters[i](viewValue);
              }
              ngModelCtrl.$viewValue = viewValue;
              ngModelCtrl.$render();
          });
      }

  }

  /**
   * Re-renders the ng-model attached to an input when the
   * user presses the `Enter` key while inside the input.
   **/
  function renderOnEnter () {

      return {
          restrict: 'A',
          require: 'ngModel',
          link: linker,
      };

      function linker (scope, element, attrs, ngModelCtrl) {
          element.on('keydown', function (event) {
            if(event.keyCode === 13) {
              event.preventDefault();
              event.stopPropagation();
              if (!ngModelCtrl.$modelValue) { return; }
              var viewValue = ngModelCtrl.$modelValue;
              for (var i = 0; i < ngModelCtrl.$formatters.length; i++) {
                  viewValue = ngModelCtrl.$formatters[i](viewValue);
              }
              ngModelCtrl.$viewValue = viewValue;
              ngModelCtrl.$render();              
            }
          });
      }

  }
  angular.module('app')
    .factory('utils', utils)
    .factory('elemUtils', elemUtils)
    .factory('dateUtils', dateUtils)
    .filter('dateToTimestamp', dateToTimestamp);
  
  function dateToTimestamp () {
    return function (date) {
      return date.valueOf();
    };
  }
  
  function utils () {
    
    var exports = {
      debounce: _debounce,
      objIndexFromId: _objIndexFromId,
    };
    
    return exports;

    function _objIndexFromId (source, idVal, idKey) {
      /* Retrieve index of object with key and value that match those provided */
      idKey = typeof idKey !== 'undefined' ? idKey : 'id';
      return source.map(function(x) {return x[idKey]; }).indexOf(idVal);
    }
    
    // Debounce function from David Walsh's blog
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function _debounce(func, wait, immediate) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        var later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }
    
  }
  
  /**
   * Library of useful DOM Element operations
   **/
  function elemUtils () {

    var exports = {
      // dimensions
      outerWidth: _outerWidth,
      outerHeight: _outerHeight,
      animationDuration: _animationDuration,
      // positioning
      above: _above,
      below: _below,
      centerOffset: _centerOffset,
    };

    return exports;

    /// Element dimension functions

    function _outerWidth (element) {
      /** Get the width of an element including margins */
      var elemCSS = window.getComputedStyle(element);
      return element.offsetWidth
        + (parseFloat(elemCSS.marginLeft, 10) || 0)
        + (parseFloat(elemCSS.marginRight, 10) || 0);
    }

    function _outerHeight (element) {
      /** Get the height of an element including margins */
      var elemCSS = window.getComputedStyle(element);
      return element.offsetHeight 
        + (parseFloat(elemCSS.marginTop, 10) || 0)
        + (parseFloat(elemCSS.marginBottom, 10) || 0);      
    }

    function _animationDuration (element) {
      /* Get the computed animation duration on the element */
      var elementCSS = window.getComputedStyle(element);
      return parseFloat(elementCSS['animation-duration'], 10);
    }

    /// Element positioning functions

    function _above (parent, child) {
      /* Get child position that centers it above parent */
      throw new Error('The `Position.above()` method is not yet implemented.');
    }

    function _below (parent, child) {
      /* Get child position that centers it below parent */

      var childLeft = _centerOffset(parent).left - (_outerWidth(child) / 2);
      var childTop = parent.offsetTop + parent.offsetHeight;

      // Align the child element to the left side of the screen
      // in edge cases where it would overflow the left side
      // or where the screen is skinnier than the child element.
      if (childLeft < 0 || window.innerWidth <= _outerWidth(child)) {
        childLeft = 0;
      } else if (childLeft + _outerWidth(child) > window.innerWidth) {
        // Ensure the popover doesn't overflow the right side of the screen.
        childLeft = window.innerWidth - _outerWidth(child);
      }

      return {
        top: childTop,
        left: childLeft,
      };

    }

    function _centerOffset (element) {
      /* Get the left offset (in pixels) of the center of an element */
      return {
        left: element.offsetLeft + element.offsetWidth / 2,
        top: element.offsetTop + element.offsetHeight / 2,
      };
    }

  }
  
  /**
  * This service provides some shortcut functions for
  * working with dates, especially dates in ISO format,
  * which is the date format Angular prefers when working
  * with date inputs.
  */
  function dateUtils () {

    return {
      dateAdd: _dateAdd,
      parseTimeStringToDate: _parseTimeStringToDate,
    };

    function _dateAdd(date, interval, units) {
      var ret = new Date(date); // don't change original date
      switch(interval.toLowerCase()) {
        case 'year'   :  ret.setFullYear(ret.getFullYear() + units);  break;
        case 'quarter':  ret.setMonth(ret.getMonth() + 3*units);  break;
        case 'month'  :  ret.setMonth(ret.getMonth() + units);  break;
        case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
        case 'day'    :  ret.setDate(ret.getDate() + units);  break;
        case 'hour'   :  ret.setTime(ret.getTime() + units*3600000);  break;
        case 'minute' :  ret.setTime(ret.getTime() + units*60000);  break;
        case 'second' :  ret.setTime(ret.getTime() + units*1000);  break;
        default       :  ret = undefined;  break;
      }
      return ret;
    }

    /**
     * Parses partial and complete time strings to a date object.
     * Makes time input a lot more user-friendly.
     *
     * For example, all of the following string inputs should be
     * parsed to a date object of today's date and time of 1pm.
     *
     *    var times = ['1:00 pm','1:00 p.m.','1:00 p','1:00pm',
     *                 '1:00p.m.','1:00p','1 pm','1 p.m.','1 p',
     *                 '1pm','1p.m.', '1p','13:00','13'];
     *
     * NOTE: This version is optimized for the en-US locale in
     *       either 12-hour or 24-hour format. It may not be
     *       suitable for all locales. Instaed of re-writing this
     *       function, it would make more sense to extend our
     *       capabilities with locale-specific functions.
     **/
    function _parseTimeStringToDate (timeString) {

      if (timeString == '') return null;

      var time = timeString.match(/(\d+)(:(\d\d))?\s*(p?)/i); 
      if (time == null) return null;

      var hours = parseInt(time[1],10);

      if (hours == 12 && !time[4]) { hours = 0; }
      else { hours += (hours < 12 && time[4])? 12 : 0; }

      var d = new Date(1970, 1, 1, 0, 0, 0);
      d.setHours(hours);
      d.setMinutes(parseInt(time[3],10) || 0);
      d.setSeconds(0, 0);
      return d;

    }
    
  }
    