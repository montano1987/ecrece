angular.module('app').controller("ComentUserController", function ($scope, $filter,$window,$location, BusinessCRUDService, ComentCRUDService, ConfigurationCRUDService) {   
	

	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Comentarios, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
    
    $scope.searchnuser = function () {    	
    	if ( ($scope.search_name_user == undefined  || $scope.search_name_user =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterComentarios = $filter('filter')($scope.original, function (item) 
					{
					if(item.iduser.nameUser != null)
						{
							if(item.iduser.nameUser.toUpperCase().search($scope.search_name_user.toUpperCase()) > -1)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Comentarios = $scope.filterComentarios;
			$scope.search();
		}
	}
    $scope.searchtitle = function () {    	
    	if ( ($scope.search_comenttitle == undefined  || $scope.search_comenttitle =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterComentarios = $filter('filter')($scope.original, function (item) 
					{
					if(item.title_coment != null)
						{
							if(item.title_coment.toUpperCase().search($scope.search_comenttitle.toUpperCase()) > -1)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Comentarios = $scope.filterComentarios;
			$scope.search();
		}
	}
    $scope.searchdescription = function () {    	
    	if ( ($scope.search_comentdescription == undefined  || $scope.search_comentdescription =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterComentarios = $filter('filter')($scope.original, function (item) 
					{
					if(item.desc_coment != null)
						{
							if(item.desc_coment.toUpperCase().search($scope.search_comentdescription.toUpperCase()) > -1)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Comentarios = $scope.filterComentarios;
			$scope.search();
		}
	}

    $scope.changetoradiobusiness = function (busi)
    {
    	
    	var recordsGet = ComentCRUDService.getAllComentforBusiness(busi.idbusiness);
        recordsGet.then(function (d) {	
				$scope.Comentarios = d.data
				$scope.original = $scope.Comentarios;
				$scope.search();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            }); 
        console.log($scope.Comentarios)
    }
    $scope.changetoradiouser = function ()
    {

    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
		{	
    		$scope.isadmin = true;				
    		var recordsGet = ComentCRUDService.getAllComent();
	        recordsGet.then(function (d) {	
					$scope.Comentarios = d.data
					$scope.original = $scope.Comentarios;
					$scope.search();
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
	            });
		}
    	else
    	{
    		var recordsGet = ComentCRUDService.getAllComentforUser($window.sessionStorage.iduser)
			recordsGet.then(function (d) {
				$scope.Comentarios = d.data
				$scope.original = $scope.Comentarios;
				$scope.search();	
			},
				function (errorPl) {
					console.log( errorPl)
				});
    		}			
    }
    
    $scope.callto = function (radiodata,busi) {
		if(radiodata == 1)
			{
			$scope.changetoradiouser();
			}
		if(radiodata == 2)
			{
			$scope.changetoradiobusiness(busi);
			}
	    }
    
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
  //limpiar registro
    $scope.clear = function () {
    	
    	$scope.user = JSON.parse($window.sessionStorage.userData);		
    	$scope.Rad = {
				toradio:1,
		}
    	$scope.Business = {
		}
        $scope.Coment = {
        		idcoment :0,
 	        	date_coment :new Date(),
 	        	desc_coment :"",
 	        	title_coment :"",
 	        	business :"",
 	        	iduser :"",
        };
    }

	$scope.nameuserFilter = $filter('filter')($scope.Comentarios, function (item){
		
			if(item.iduser.nameUser.search($scope.search_name_user) > -1)
			{
				return true;
			}
				return false;
		});

    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
    	$scope.clear();
    	var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
		    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
	    		{
		    		var recordsGet = ComentCRUDService.getAllComent();
			        recordsGet.then(function (d) {	
							$scope.Comentarios = d.data
							$scope.original = $scope.Comentarios;
							$scope.search();
			            },
			            function (errorPl) {
			                $log.error('Error ', errorPl);
			            });
	    		}
		    	else
		    	{
			    		var recordsGet = ComentCRUDService.getAllComentforUser($window.sessionStorage.iduser)
						recordsGet.then(function (d) {
							$scope.Comentarios = d.data
							$scope.original = $scope.Comentarios;
							$scope.search();
						},
							function (errorPl) {
								console.log( errorPl)
							});
		    		
		    		}
		    	},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
    	
    	if ($scope.user.role.desc_rol==="ROLE_ADMIN")
		{
			$scope.showbotomadd = false;
			var recordsGet = BusinessCRUDService.getallBusiness()
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];
				console.log($scope.Business.bus)
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
		else
		{
			
			var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];
				console.log($scope.Business.bus)
			
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}    	
    }   
    $scope.back = function () {
		$window.history.back(-1);	
	}
});

angular.module('app').service('ComentCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {

this.getAllComentforUser = function getAllComentforUser(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/coment/user/'+iduser
        });
}

this.getAllComentforBusiness = function getAllComentforBusiness(idbusiness){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/coment/business/'+ idbusiness
        });
}
this.getAllComent = function getAllComent(){
    return $http({
      method: 'GET',
      url: RELATIVE_DIR+'/api/coment/all'
    });
}
this.deletecoment = function deletecoment(coment){
    return $http({
      method: 'POST',
      url: RELATIVE_DIR+'/api/delete/coment/',
      data: coment
    });
}

}]);
//comentarios fin

