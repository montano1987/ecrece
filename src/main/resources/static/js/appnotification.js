//Notification Controller
angular.module('app').controller("NotificationController", function ($scope,$sce,$rootScope,toast,$timeout, $location, $filter, $window, NotificationCRUDService,PointPackageCRUDService,BusinessCRUDService, ConfigurationCRUDService,RELATIVE_DIR) {   
	
    var locdir = window.location;
	 var pathNamedir = locdir.pathname.substring(0, locdir.pathname.lastIndexOf('/') + 1);
	 var dir = locdir.href.substring(0, locdir.href.length - ((locdir.pathname + locdir.search + locdir.hash).length - pathNamedir.length));
 	
    angular.extend($scope, {
		   center:{
		      lat: 0,
		      lon: 0,
		      zoom: 12
		    }
		  });   
  
    //modal adicionar
    $scope.AdicionarModal = function (notification) {
    	if(notification.notificationsType.desc_notification_type==="Solicitud de Puntos" 
						    		&& notification.notification_state.nameNotificationstate==="Pendiente" 
						    		&& notification.idpointpackage!=null
						    		&& notification.idpayment_form!=null)
    		{    	        	
    	        	$scope.PakageRequest.formpayment = notification.idpayment_form.name_payment_form;
    	        	$scope.PakageRequest.idpointpackage = notification.idpointpackage;
    	        	$scope.union = $scope.PakageRequest.idpointpackage.point_value+ " | "+ $scope.PakageRequest.idpointpackage.cost
    	        	$scope.notitosend = notification;
    	        	$scope.showModalPackageRequest = true;  
    		}
    	if((notification.notificationsType.desc_notification_type==="Creación de Negocio" || notification.notificationsType.desc_notification_type==="Edición de Negocio")&&
    				notification.idbusiness_fk!=null &&
    				notification.notification_state.nameNotificationstate==="Pendiente")
    		{
    		 var promiseGetSingle = BusinessCRUDService.getBusiness(notification.idbusiness_fk.idbusiness);
 	        promiseGetSingle.then(function (d) {
 	        	console.log(d.data)
	 	        	$scope.Business.idbusiness = d.data.idbusiness;
	 	        	$scope.Business.name_business = d.data.name_business;
	 	        	
	 	        	$scope.Business.municipality = d.data.municipality.name_municipality;
	 	        	$scope.Business.dir_business = d.data.dir_business;
	 	        	$scope.Business.desc_business = d.data.desc_business;
	 	        	$scope.Business.web_site = d.data.web_site;
	 	        	$scope.Business.licence_business= d.data.licence_business;
	 	        	$scope.Business.phone_business = d.data.phone_business;
	 	        	$scope.Business.idnotification = notification.idnotification;
	 	        	$scope.Business.name_image = RELATIVE_DIR +"/files/" +d.data.name_image;
	 	        
	 	        	if(d.data.latitude != null)
	 	        		{
	 	        		$scope.markersbusi.push({
							lat: parseFloat(d.data.latitude),
							lon: parseFloat(d.data.longitude),
							label: {
//		                        message:  "<img src=\"examples/images/notredame.jpg\" />",
								message:  d.data.name_business,
		                        show: false,
		                        showOnMouseOver: true
		                    }
						});	
	 	        		$scope.center.lat = parseFloat(d.data.latitude);
						$scope.center.lon = parseFloat(d.data.longitude);
	 	        		}
	 	        	else
	 	        		{
	 	        		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
	 					recordsGet.then(function (d) {
	 						$scope.center.lat = parseFloat(d.data[0].latitude);
							$scope.center.lon = parseFloat(d.data[0].longitude);
	 					},
						function (errorPl) {
							console.log("error" + errorPl)
						});
	 	        		}
	 	        	
	 	        	 var promiseGetSingle = NotificationCRUDService.getSubcategoryBusiness(d.data.idbusiness);
	 	 	        promiseGetSingle.then(function (d) {
	 	 	        	console.log(d.data)
	 	 	        	$scope.addSubCategorias = d.data;
	 	 	        },
	 	            function (errorPl) {
	 	                console.log(errorPl);
	 	            });

	 	        	$scope.showModalBusiness = true;
	 	        	$timeout(function () {
	 	   			$scope.showmap = true;
	 	   			}, 500);
 	            },
 	            function (errorPl) {
 	                $scope.errorMessage = "BUSINESS_NFOUND";
 	                console.log(errorPl);
 	                $scope.verMessageError = true;
 	            });
    		}
    };   
    

	$scope.aceptPakageRequest = function(noti)
	{
		var promiseGetSingle = NotificationCRUDService.acceptrequestpoint(noti.idnotification);
        promiseGetSingle.then(function (d) {
        	console.log(d.data);
        	$rootScope.$broadcast('ptos', { d: d.data })
        	 loadRecords($window.sessionStorage.iduser );
        	 $scope.CerrarModal();
        },        
        function (errorPl) {
            $scope.errorMessage = "REQUEST_ERR_PROCESING";
            console.log(errorPl);
            $scope.verMessageError = true;
        });
	}
	$scope.rejectrequestpoint = function(noti)
	{
		var promiseGetSingle = NotificationCRUDService.rejectrequestpoint(noti.idnotification);
        promiseGetSingle.then(function (d) {
        	 loadRecords($window.sessionStorage.iduser );
        	 $scope.CerrarModal();        	 
        },        
        function (errorPl) {
            $scope.errorMessage = "REQUEST_ERR_PROCESING";
            console.log(errorPl);
            $scope.verMessageError = true;
        });
	}
	
$scope.BusinessPen = function (Business,Pendiente) {
	dir=dir+'#!/mybusiness/'+Business.idbusiness;
	$scope.ModDir = {
			dir:dir,
			idmoddir:0,
	};
    	
    	var recordsPost = NotificationCRUDService.putPend(Business,Pendiente);
    	recordsPost.then(function (d) {	
    		$scope.CerrarModal();
	    		toast({
	       	     duration  : 5000,
	       	     message   : $filter('translate')("BUSINESS_PEND"),
	       	     className : "alert-success",
	       	     position: "center",
	       	     dismissible: true,
	       	   });	
	    		
	    		var promiseGetSingle = NotificationCRUDService.sendmailputPend(Business,Pendiente,$scope.ModDir );
                promiseGetSingle.then(function (d) {
                	
                },        
                    function (errorPl) {
                    console.log(errorPl);
                });
                loadRecords($window.sessionStorage.iduser );
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
    	
    }

	$scope.aceptBusiness = function(Business)
	{
		dir=dir+'#!/mybusiness/'+Business.idbusiness;
		$scope.ModDir = {
				dir:dir,
				idmoddir:0,
		};
		var promiseGetSingle = NotificationCRUDService.acceptBusiness(Business.idnotification);
        promiseGetSingle.then(function (d) {
        	 loadRecords($window.sessionStorage.iduser );
        	 $scope.CerrarModal();
        		toast({
            	     duration  : 5000,
            	     message   : $filter('translate')("BUSINESS_ACCEPT"),
            	     className : "alert-success",
            	     position: "center",
            	     dismissible: true,
            	   });
        		var promiseGetSingle = NotificationCRUDService.sendmailacceptBusiness(Business.idnotification, $scope.ModDir );
                promiseGetSingle.then(function (d) {
                	
                },        
                    function (errorPl) {
                    console.log(errorPl);
                });
        },        
        function (errorPl) {
            $scope.errorMessage = "BUSINESS_ERR_PROCESING";
            console.log(errorPl);
            $scope.verMessageError = true;
        });
	}
    
    $scope.CerrarModal = function () {
    	$scope.clear();
        $scope.showModalPackageRequest= false;
		$scope.showModalBusiness = false;
		$scope.showModalPromotion = false;
		
	};
	$scope.ShowPromotion = function (Promo) {
		$scope.showModalPromotion = true;
		$scope.Promotion = {
			idpromotion: Promo.idpromotion,
			daysPromotion:Promo.daysPromotion,
			codePromotion:Promo.codePromotion, 
			date_start_promotion: new Date (Promo.date_start_promotion),
			date_end_promotion:new Date (Promo.date_end_promotion),  
			desc_promotion:Promo.desc_promotion,         
        };
	};

	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Notificaciones, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
   $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };   
//    fin paginado
//    busqueda
    $scope.searchonsubject = function () {    	
    	if ( ($scope.search_subject_notification_form == undefined  || $scope.search_subject_notification_form =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterNotificaciones = $filter('filter')($scope.original, function (item) 
					{
					if(item.subject_notification != null)
						{
							if(item.subject_notification.toUpperCase().search($scope.search_subject_notification_form.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Notificaciones = $scope.filterNotificaciones;
			$scope.search();
		}
	}
    $scope.searchonuser = function () {    	
    	if ( ($scope.search_user_notification == undefined  || $scope.search_user_notification =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterNotificaciones = $filter('filter')($scope.original, function (item) 
					{					
					if(item.usernotification != null)
						{
							if(item.usernotification.nameUser.toUpperCase().search($scope.search_user_notification.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Notificaciones = $scope.filterNotificaciones;
			$scope.search();
		}
	}
    $scope.searchontype = function () {    	
    	if ( ($scope.search_type_notification == undefined  || $scope.search_type_notification =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterNotificaciones = $filter('filter')($scope.original, function (item) 
					{
					if(item.notificationsType.desc_notification_type != null)
						{
							if(item.notificationsType.desc_notification_type.toUpperCase().search($scope.search_type_notification.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Notificaciones = $scope.filterNotificaciones;
			$scope.search();
		}
	}
    $scope.searchondate = function () {    	
    	if ( ($scope.search_date_notification_form == undefined  || $scope.search_date_notification_form =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
    		$scope.filterNotificaciones = $filter('filter')($scope.original, function (item) 
					{
					if(item.create_date != null)
						{
							if(angular.equals($filter('date')(item.create_date, "yyyy-MM-dd"), $filter('date')($scope.search_date_notification_form, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Notificaciones = $scope.filterNotificaciones;
			$scope.search();
		}
	}
    $scope.searchonstate = function () {    	
    	if ( ($scope.search_state_notification == undefined  || $scope.search_state_notification =="") )
			{
    		$scope.callto($scope.Rad.toradio, $scope.Business.bus)
			}
    	else
		{
			$scope.filterNotificaciones = $filter('filter')($scope.original, function (item) 
					{
					if(item.notification_state.nameNotificationstate != null)
						{
							if(item.notification_state.nameNotificationstate.toUpperCase().search($scope.search_state_notification.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Notificaciones = $scope.filterNotificaciones;
			$scope.search();
		}
	}
    
    
    $scope.changetoradiobusiness = function (business)
    {
    	var recordsGet = NotificationCRUDService.getAllNotificationbyBusiness(business.idbusiness);
        recordsGet.then(function (d) {	
				$scope.Notificaciones = d.data
				$scope.original = $scope.Notificaciones;
				$scope.search();								
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});
    }
    $scope.changetoradiouser = function ()
    {
    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
		{	
    		var recordsGet = NotificationCRUDService.getAllNotification();
	        recordsGet.then(function (d) {	
					$scope.Notificaciones = d.data;
					$scope.original = $scope.Notificaciones;
					$scope.search();								
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
				});
		}
    	else
    	{
    		var recordsGet = NotificationCRUDService.getAllNotificationbyUser($window.sessionStorage.iduser);
	        recordsGet.then(function (d) {	
					$scope.Notificaciones = d.data;
					$scope.original = $scope.Notificaciones;
					$scope.search();								
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
				});
    		}			
    }
    
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
  //limpiar registro
    $scope.clear = function () {
    	$scope.showModalPackageRequest = false;
    	$scope.showModalBusiness = false;
    	$scope.showModalPromotion = false;
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
        $scope.addSubCategorias = new Array();
        $scope.markers = new Array();
    	$scope.showmap = false;
    	$scope.showbtnadd = false;
    	$scope.showModalNotiClient = false;	
    	$scope.Rad = {
				toradio:1,
		}
    	$scope.Pendiente = {
    			descripcion:"",
		}
    	$scope.imgchek = RELATIVE_DIR+"/img/check.svg";
//    	$scope.rutaimagen = RELATIVE_DIR +"/files/";
    	$scope.markersbusi = [];
    	$scope.user = JSON.parse($window.sessionStorage.userData);
		$scope.Promotion = {
			idpromotion: 0,
			daysPromotion:0,
			codePromotion:"", 
			date_start_promotion: "",
			date_end_promotion:"",  
			desc_promotion:"",         
        };
        $scope.PakageRequest = {
        		idnotification: 0,
        		pointcost: 0,
        		formpayment: 0,
        };
        $scope.union;
        $scope.Business = {
        		idbusiness :0,
 	        	name_business :"",
 	        	municipality :0,
 	        	dir_business :"",
 	        	desc_business :"",
 	        	web_site :"",
 	        	licence_business:"",
 	        	phone_business :"",
 	        	name_image:"",
 	        	idnotification:0,
        };
    }
    
    $scope.showbottom = function (noti) {
    	if(noti.notification_state.nameNotificationstate == "Pendiente" )
    		{
    		return true;
    		}
    	else {
			return false;
		}
	}
    $scope.showbotomnoticlient= function(){
    	
    }
    $scope.clearNotiClient = function(){
    	$scope.NotiClient = {
    			subject_notification :"",
    			desc_notification :"",
    			tonitification: 1,
        };
    }
    $scope.openNotiClient = function () {
    	$scope.clearNotiClient();
    	$scope.showModalNotiClient = true;		
	}
    $scope.closeNotiClient = function () {    	
    	$scope.showModalNotiClient = false;		
	}
    $scope.noticlient = function (NotiClient) {    	
    	console.log(NotiClient)
    	var recordsGet = NotificationCRUDService.sendnoticlient(NotiClient, $window.sessionStorage.iduser);
        recordsGet.then(function (d) {	
				console.log(d.data)
				$scope.closeNotiClient();
				toast({
             	     duration  : 5000,
             	     message   :  $filter('translate')("NOTI_SEND_SUCC"),
             	     className : "alert-success",
             	     position: "center",
             	     dismissible: true,
             	   });
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
			});	
	}
	
	$scope.goDetail = function (Notification) {
		$location.path("/notificationdetail/"+Notification.idnotification);  
		
	}	
	 $scope.callto = function (radiodata,business) {
			if(radiodata == 1)
				{
					$scope.changetoradiouser();
				}
			if(radiodata == 2)
				{
					$scope.changetoradiobusiness(business);
				}
		    }
	
    loadRecords($window.sessionStorage.iduser );
    //cargar todos los prestamos
    function loadRecords(iduser) {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
		$scope.clear();		
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
		    	if($scope.user.role.desc_rol == "ROLE_ADMIN")		    		
	    		{
		    		$scope.showbtnadd = true;
		    		var recordsGet = NotificationCRUDService.getAllNotificationAdmin($scope.user.iduser);
			        recordsGet.then(function (d) {	
							$scope.Notificaciones = d.data;
							$scope.original = $scope.Notificaciones;
							$scope.search();								
			            },
			            function (errorPl) {
			                $log.error('Error ', errorPl);
						});
	    		}
		    	else
		    	{			    		
			    		var recordsGet = NotificationCRUDService.getAllNotificationbyUser($window.sessionStorage.iduser);
				        recordsGet.then(function (d) {	
								$scope.Notificaciones = d.data
								$scope.original = $scope.Notificaciones;
								$scope.search();
								if($scope.original.length > 0)
									{
										$scope.showbtnadd = true;
									}
				            },
				            function (errorPl) {
				                $log.error('Error ', errorPl);
							});		    		
		    		}
		    	},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
    	
    	if ($scope.user.role.desc_rol==="ROLE_ADMIN")
		{
			$scope.showbotomadd = false;
			var recordsGet = BusinessCRUDService.getallBusiness()
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];				
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
		else
		{
			var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business.bus = d.data[0];			
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
    	
    }
    
    //guardar prestamos
    $scope.save = function (Notification) {
    var NotificationInser = {  
    idnotification:Notification.idnotification,
    subject_notification: Notification.subject_notification,
            };
        //si IsNewRecord == 1 adiciono si no edito
        if ($scope.IsNewRecord === 1) {
            var promisePost = NotificationCRUDService.addNotification(NotificationInser);
            promisePost.then(function (d) {
                loadRecords();
                $scope.Notification.idnotification = d.data.idnotification;
                $scope.Message = "SUCCESSFULY_SAVE";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                  
                    setTimeout(function () {
                    $scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        } else { //Else Edit the record
            var promisePut = NotificationCRUDService.updateNotification($scope.Notification.idnotification,$scope.Notification.subject_notification);
            promisePut.then(function (d) {
                loadRecords();
                $scope.Message = "SUCCESSFULY_EDIT";
                $scope.verMessage = true;
                $scope.showModal = false;
                (function next() {                  
                    setTimeout(function () {
                    $scope.verMessage = false;
                        $scope.$digest();
                    }, 3000);
                           
            })();
            }, function (err) {
                $scope.errorMessage = "ERROR_DATA_EDIT";
                console.log(err);
                $scope.verMessageError = true;
            });
        }
    };
    $scope.back = function () {
		$window.history.back(-1);	
	}
    
 
});

angular.module('app').service('NotificationCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {

this.getAllNotificationUser = function getAllNotificationUser(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/Notification/foruser/'+iduser
        });
}
this.getNotification = function getNotification(idnotification){
    return $http({
      method: 'GET',
      url: RELATIVE_DIR+'/api/Notification/'+idnotification
    });
}
this.sendnoticlient = function sendnoticlient(NotiClient,iduser){
    return $http({
      method: 'POST',
      url: RELATIVE_DIR+'/api/Notification/noticlient/user/'+iduser,
      data: NotiClient
    });
}

this.acceptBusiness = function acceptBusiness(idnotification){
    return $http({
      method: 'GET',
      url: RELATIVE_DIR+'/api/aceptBusiness/'+idnotification+'/business'
    });
}
this.sendmailacceptBusiness = function sendmailacceptBusiness(idnotification,ModDir){
    return $http({
      method: 'POST',
      url: RELATIVE_DIR+'/api/sendmail/aceptBusiness/'+idnotification,     
      data:ModDir
    });
}
this.promotionforall = function promotionforall(Promotion){
    return $http({
      method: 'POST',
	  url: RELATIVE_DIR+'/api/Notification/promotionforall',
	  data: Promotion
    });
}
this.getSubcategoryBusiness = function getSubcategoryBusiness(idbusiness){
    return $http({
      method: 'GET',
      url: RELATIVE_DIR+'/api/businessSubcategory/business/'+idbusiness
    });
}
   
    this.addNotification = function addNotification(Notification){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/Notification',
          data: Notification
        });
    }
    this.acceptrequestpoint = function acceptrequestpoint(idnotification){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/Notification/pointrequest/'+idnotification
        });
    }
    this.rejectrequestpoint = function rejectrequestpoint(idnotification){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/Notification/pointrequest/reject/'+idnotification
        });
    }

    this.deleteNotification = function deleteNotification(idnotification){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/Notification/'+idnotification
        })
    }

    this.updateNotification = function updateNotification(idnotification,subject_notification){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/Notification/'+idnotification,
          data: {idnotification:idnotification, subject_notification: subject_notification}
        })
    }

    this.getAllNotificationbyUser = function getAllNotificationbyUser(iduser){   
    	  return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/notification/user/'+iduser
        });
    }
    this.getAllNotificationAdmin = function getAllNotificationAdmin(iduser){   
  	  return $http({
        method: 'GET',
        url: RELATIVE_DIR+'/api/notification/admin/all/'+iduser
      });
  }
    this.getAllNotificationbyBusiness = function getAllNotificationbyBusiness(idbusiness){   
    	  return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/notification/business/'+idbusiness
        });
    }
    this.putPend = function putPend(Business,Pendiente){   
  	  return $http({
        method: 'POST',
        url: RELATIVE_DIR+'/api/notification/business/'+Business.idbusiness+'/pend/'+Pendiente.descripcion+'/idnotification/'+Business.idnotification
      });
  }
    this.sendmailputPend = function sendmailputPend(Business,Pendiente,ModDir){   
    	  return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/notification/sendmailpend/business/'+Business.idbusiness+'/desc/'+Pendiente.descripcion,
          data:ModDir
        });
    }
    this.putnotiread = function putnotiread(idnotification){   
    	  return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/notification/read/'+idnotification
        });
    }    

}]);
//Notification fin

//reservation controller
angular.module('app').controller("NotificationDetailController", function ($scope,$rootScope,$window,$location, NotificationCRUDService ) {
		
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idnotification =urlParts[urlParts.length - 1];
	$scope.showbotom = false;
	
	loadRecords(idnotification);
	function loadRecords(idnotification){
		if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
	      var recordsGet = NotificationCRUDService.getNotification(idnotification);        
	        recordsGet.then(function (d) {
	        $scope.Notification = d.data
	        	if($scope.Notification.read == false)
	        		{
		        		 var recordsGet = NotificationCRUDService.putnotiread($scope.Notification.idnotification);        
		     	        recordsGet.then(function (d) {
		     	        	 $rootScope.$broadcast('loadnoti', { d: 'load' })
			     	       },
			   	        function (errorPl) {
			   	            $log.error('Error ', errorPl);
			   	        });	     	        
	        		}
	        	console.log($scope.Notification)
	        	if(($scope.Notification.urltodetail != null && $scope.Notification.subject_notification == "Reservación") 
	        			|| ($scope.Notification.urltodetail != null && $scope.Notification.subject_notification == "Nueva oferta") )
					{
	        			$scope.showbotom = true;
					}
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	  }
	
	$scope.gohome = function()
	{
		 $window.sessionStorage.setItem('idnotification', "");
		$location.path("/home");
	}
	
	$scope.goback = function ()
	{
		$window.history.back(-1);
	}
	$scope.goto = function ()
	{
		var urlPartstwo= $scope.Notification.urltodetail.split('/');
		var id =urlPartstwo[urlPartstwo.length - 1];
		var location = urlPartstwo[urlPartstwo.length - 2]
		$location.path("/"+location+"/"+id);
	}
	
	
	
		
});
