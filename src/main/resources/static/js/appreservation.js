//inicio Promotion
angular.module('app').controller("ReservationController", function ($scope, $filter, $window,$location,BusinessCRUDService, ReservationCRUDService, ConfigurationCRUDService) {
	
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Reservations, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
	
    $scope.searchdesc = function () {    	
    	if ( ($scope.search_desc == undefined  || $scope.search_desc =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
//	    	var tofilter;
//	    	if($scope.original.length != $scope.Reservations.length)
//	    		{
//	    			tofilter = $scope.Reservations ;
//	    		}    		
//	    	else
//	    		{
//	    			tofilter = $scope.original;
//	    		}
			$scope.filterReservations = $filter('filter')($scope.original, function (item) 
					{
					if(item.description != null)
						{
							if(item.description.toUpperCase().search($scope.search_desc.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Reservations = $scope.filterReservations;
			$scope.search();
		}
	}
    
    $scope.searchdateend = function () {   
    	if ( ($scope.search_dateend == undefined  || $scope.search_dateend =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterReservations = $filter('filter')($scope.original, function (item) 
					{
					if(item.dateend != null)
						{
							if(angular.equals(item.dateend, $filter('date')($scope.search_dateend, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Reservations = $scope.filterReservations;
			$scope.search();
		}
	}
    $scope.searchdate = function () {   
    	if ( ($scope.search_date == undefined  || $scope.search_date =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{	    	
			$scope.filterReservations = $filter('filter')($scope.original, function (item) 
					{
					if(item.date != null)
						{
							if(angular.equals(item.date, $filter('date')($scope.search_date, "yyyy-MM-dd")))
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Reservations = $scope.filterReservations;
			$scope.search();
		}
	}

    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
	$scope.callto = function (radiodata) {
		if(radiodata == 1)
			{
			$scope.changetoradiouser();
			}
		if(radiodata == 2)
			{
			$scope.changetoradiobusiness();
			}
	    }
	//limpiar registro
    $scope.clear = function () {
	$scope.user = JSON.parse($window.sessionStorage.userData);
		$scope.Rad = {
				toradio:1,
		}
    }
    $scope.IsNewRecord = 1;
    $scope.changetoradiobusiness = function ()
    {
		var recordsGet = ReservationCRUDService.getAllReservationbybusinesss($scope.Business.idbusiness);
			recordsGet.then(function (d) {
					$scope.Reservations = d.data;	
					$scope.original = $scope.Reservations;
					console.log($scope.original)
					$scope.search();
				},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});		
    }
    $scope.changetoradiouser = function ()
    {
    	var recordsGet = ReservationCRUDService.getAllReservationbyuser($window.sessionStorage.iduser);
		recordsGet.then(function (d) {
				$scope.Reservations = d.data;
				$scope.original = $scope.Reservations;
				$scope.search();
			},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});			
    }
    
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
    	$scope.clear();    	
		var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
    		var recordsGet = ReservationCRUDService.getAllReservationbyuser($window.sessionStorage.iduser);
				recordsGet.then(function (d) {
						$scope.Reservations = d.data;
						$scope.original = $scope.Reservations;
						$scope.search();
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});		    	
		},
		function (errorPl) {
			$log.error('Error ', errorPl);
		});
		if ($scope.user.role.desc_rol==="ROLE_ADMIN")
		{
			var recordsGet = BusinessCRUDService.getallBusiness()
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business = d.data[0];				
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
		else
		{
			var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
			recordsGet.then(function (d) {
				$scope.BusinessList = d.data;
				$scope.Business = d.data[0];		
			},
				function (errorPl) {
					console.log( errorPl)
				});
		}
    }
    
    $scope.goDetail = function (idreservation) {
		$location.path("/detailreservation/"+idreservation); 
	}
    $scope.back = function () {
    	$window.history.back(-1);	
	}
    
});

angular.module('app').service('ReservationCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {	
        
    this.getAll = function getAll(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/reservation'
        });
	}
    
    this.getAllReservationbyuser = function getAllReservationbyuser(iduser){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/reservation/'+iduser
          });
    }
    this.getAllReservationbybusinesss = function getAllReservationbybusinesss(idbusiness){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/reservation/business/'+idbusiness
          });
    }  
    

}]);

//reservation controller
angular.module('app').controller("ReservationDetailController", function ($scope,$filter, $window,$location, BusinessCRUDService, toast) {
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idreservation =urlParts[urlParts.length - 1];
	$scope.Reservation;
	$scope.showbotom = false;
	loadRecords();
	function loadRecords(){
		if($window.sessionStorage.iduser != undefined)
			{
			  var recordsGet = BusinessCRUDService.detailreservation(idreservation);        
		        recordsGet.then(function (d) {
		        	$scope.Reservation = d.data;
		        	console.log($scope.Reservation)
		        	if($scope.Reservation.business.user.iduser == $window.sessionStorage.iduser && $scope.Reservation.accept == false && $scope.Reservation.reject == false)
						{
		        			$scope.showbotom = true;
						}				
		        },
		        function (errorPl) {
		            $log.error('Error ', errorPl);
		        });
			}
		else
		{
			$window.sessionStorage.setItem('toredirect', '/detailreservation/'+idreservation);
			$location.path("/home");
			$('#Login').modal('show');
		}
	    
	  }
	$scope.gohome = function()
	{
		$location.path("/home");
	}
	
	$scope.acceptreservation = function()
	{
		   var recordsPost = BusinessCRUDService.acceptreservation($scope.Reservation.idreservation);        
		   recordsPost.then(function (d) {
	        		console.log(d.data)
	        		toast({
	                	     duration  : 4000,
	                	     message   : $filter('translate')("RESERVATION_ACCEPT"),
	                	     className : "alert-success",
	                	     position: "center",
	                	     dismissible: true,
	                	   });
	        		$location.path("/reservations");
	        		var recordsPost1 = BusinessCRUDService.sendmailacceptreservation(d.data.idreservation);        
	     		   recordsPost1.then(function (d) {
	     			   console.log("send email")	     			  
	     	        },
	     	        function (errorPl) {
	     	            $log.error('Error ', errorPl);
	     	        });
	        		
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	}
	$scope.rejectreservation = function()
	{		
		   var recordsPost = BusinessCRUDService.sendnotificationrejectreservation($scope.Reservation.idreservation);        
		   recordsPost.then(function (d) {
			   $location.path("/reservations");   
			  toast({
         	     duration  : 4000,
         	     message   : $filter('translate')("RESERVATION_REJECT"),
         	     className : "alert-danger",
         	     position: "center",
         	     dismissible: true,
         	   });
			  var recordsPost1 = BusinessCRUDService.sendmailrejectreservation($scope.Reservation.idreservation);        
			   recordsPost1.then(function (d) {
				   console.log("mail send")
		        },
		        function (errorPl) {
		            $log.error('Error ', errorPl);
		        });
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	}
	$scope.back = function () {
		$window.history.back(-1);	
	}
});