// app.js

var app = angular.module('app', ['openlayers-directive','ngRoute','ui.router','pascalprecht.translate','ngMessages','angularjsToast','ngSanitize' ,'ngAnimate','ui.bootstrap','moment-picker','ui.calendar']);

app.constant("api","");
app.constant("Pendiente","Pendiente");
app.constant("Aceptado","Aceptado");
app.constant("Creado","Creado");
var direccion = "";
getAbsolutePath();
function getAbsolutePath() {
    var direccionrelativa = window.location.pathname;
    direccion = direccionrelativa.split('/').join('');
	//direccionrelativa.repalce('/','');
	console.log(direccion);
	return direccionrelativa;
}
app.constant('RELATIVE_DIR', direccion);

app.config(['$translateProvider',"$httpProvider",'RELATIVE_DIR', function ($translateProvider,$httpProvider, RELATIVE_DIR) {
	$httpProvider.interceptors.push('TokenInterceptor');
	$translateProvider.useStaticFilesLoader({
	    prefix: RELATIVE_DIR+'/i18n/local-',
	    suffix: '.json'
	  })
	$translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.preferredLanguage('es');
	$translateProvider.useMissingTranslationHandlerLog();
	
	}]);

app.run(['$rootScope','$transitions', function($rootScope, $transitions) {
	  $rootScope.lang = 'es';  
	  $transitions.onSuccess({}, function () {
	        document.body.scrollTop = document.documentElement.scrollTop = 0;
	    })
	}])
	
//	app.run.$inject = ['$rootScope', '$location', '$http', '$window'];
//    function run($rootScope, $location, $http, $window) {
//    	 $rootScope.lang = 'es';
//        
//    }
    app.factory('TokenInterceptor', ['$q', '$window','$rootScope','$location', '$filter' , function ($q, $window,$rootScope, $location, $filter, toast) {
    
    	return {
            request: function (config) {
            		if(config.url.includes("/api/users/getpoint/")|| config.url.includes("/api/Notification/foruser/") || 
            				config.url.includes("/api/promotions") || config.url.includes("/api/offers/active") || config.url.includes("/api/configuration")
            				|| config.url.includes("/language/es") || config.url.includes("api/Notification/foruser/"))
            		{
            			true; 			
            		}  
            		else
					{
	    				var element = document.getElementById("page-loading");
	          	  		element.style.display = "block";
					}
            	  
            	console.log(config);
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers['Authorization'] = $window.sessionStorage.token;
				}
				// // else {
				// // 	if(config.url != "/html/business/business-list-search.html")
				// // 	{
				// // 		$window.sessionStorage.clear();
				// // 	}                	
                // // }
                
                return config;
            },
            response: function (response) {
            	 var element = document.getElementById("page-loading");
            	 element.style.display = "none";
            	 
                return response || $q.when(response);
            },

            'responseError': function(rejection) {
            	 var element = document.getElementById("page-loading");
            	 element.style.display = "none";

                // If token expired, or token required
                if(rejection.status === 403 ) {
					$window.sessionStorage.clear(); 
					$rootScope.$broadcast('language', { d: "ES" })
					$location.path("/home"); 
										           	
                	toast({
               	     duration  : 5000,
               	     message   : $filter('translate')("SESSION_EXPIRED"),
               	     className : "alert-danger",
               	     position: "center",
               	     dismissible: true,
               	   });
                }
                return $q.reject(rejection);
            }
        };
    }]);
    
  //controlador login
    app.controller("LoginController", function ($scope,  $filter, toast,$rootScope, $location, $window,$http, LoginCRUDService, BusinessCRUDService,RolCRUDService,RegisterUserCRUDService,RELATIVE_DIR) {
		
		$scope.maxlength=8;
    	$scope.showModal = false;  
	    $scope.verMessageError = false;
		$scope.errorMessage = "";
		$scope.Sexs = [{
    		"idsex": 0, "namesex": "Masculino" },
    {"idsex": 1, "namesex": "Femenino"}];
//	    $scope.UserLogin = {
//        		username1:"",
//        		password1:"",
//    	
//    	};
		$scope.today = new Date();
		
		$scope.roleadmin= function()
		{
			if($window.sessionStorage.length>1)
			{
			if($window.sessionStorage.userData !=undefined)
					{
						$scope.user = JSON.parse($window.sessionStorage.userData);
						if ($scope.user.role.desc_rol==="ROLE_ADMIN")
							{
								return true;
							}
							else
							{
								return false;
							}
					}				
				else {
					return false;
					}
			}			
		}
		 $scope.noselected = function()
		{
			if( $scope.RegisterUser.rol!= 0 )
				{
					return false;
				}
			else
				{
					$scope.Messageerror = "ROL_ISREQUIRED";
					return true;
				}		
		}
		$scope.autentication = true;
		
		 $scope.$on('showforauthenticate', function(event, option) {
	    		$scope.autentication = option.d;
	    		});
		 
		$scope.changelogin = function () {
			$scope.autentication = !$scope.autentication;
			$scope.verMessageError = false;
			$scope.UserLogin.username1 = "";
			$scope.UserLogin.password1 = "";
	    };
	    
		
	    $scope.CerrarModal = function () {	        
			$scope.showModal = false;
			$scope.clear();
	    };
	    $scope.goforgotpass= function()
        {
	    	$scope.CerrarModal();    
        	$location.path("/forgotpass"); 
        } 
	    $scope.openterm= function()
        {
	    	$window.open($scope.pathtermandcond, '_blank');
        } 
	    //limpiar registro
	    $scope.clear = function () {
	    	$scope.autentication = true;
			$scope.unequalpass = false;
			$scope.showunequal = false;
			$scope.userempty = false;
			$scope.passempty = false;
			$scope.pathtermandcond = RELATIVE_DIR +'/img/TerminosyCondiciones.pdf'
			$scope.dateempty = false;
			$scope.passuserempty = false;
			$scope.reppassempty = false;
			$scope.mailuserempty = false;
			$scope.nameUserempty = false;
			$scope.C = new Date();
			
	        $scope.UserLogin = {
	        		username1:"",
	        		password1:"",        	
			};	       
	        $scope.exituser = false;
	        var loc = window.location;
		    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		    var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
		  		
		    var forbirthday = new Date();
		    forbirthday.setDate(forbirthday.getDate() - 1);
		    $scope.RegisterUser = {
	        		iduser: 0,
	        		nameUser: "",
	        		mail: "",
	                sex: 0,
	                date_bith: forbirthday,
	                phoneUser:"",	                
					password:"",
					password2:"",               
	                name:"",
	                surnameUser: "",
	                surname2_user:"",
	                accept:false,
	                rol: 1,
	                language:$window.sessionStorage.language,
	                dir:dir+'#!/activationuser/',
//	                ver los campos que tengo q cargar y enviar por detraspg
	        };
	        $scope.verMessage = false;
	        $scope.verMessageError = false;
	        $scope.errorMessage = "";
		}
		
		 loadRecords(); 
		function loadRecords() { 
				$scope.clear();
				var recordsGet = RolCRUDService.getAllRol();
				recordsGet.then(function (d) {
					$scope.Roles=d.data;
					var rol1 = $scope.Roles.filter(item => item.desc_rol === "ROLE_USER");
					$scope.RegisterUser.rol = rol1[0].idrol;
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			}
        
    	$scope.equalpass = function () {
	       if($scope.RegisterUser.password != $scope.RegisterUser.password2)
					{
						$scope.unequalpass = true;
					} 
					$scope.showunequal = false;
	    };
        //guardar 
	    
        $scope.loguearse = function (UserLogin) {
        	if(UserLogin.username1.length>0 && UserLogin.password1.length>0)
        		{
        		var promisePost = LoginCRUDService.login(UserLogin);
                promisePost.then(function (d) {
                	if (d.data && (d.data.iduser != null)) {                        	                        
                        $window.sessionStorage.setItem('token', d.data.token);
                        $window.sessionStorage.setItem('iduser', d.data.iduser);
                        $window.sessionStorage.setItem('nameUser', d.data.nameUser);
                        $window.sessionStorage.setItem('ptosUser', d.data.ptosUser);
                        $window.sessionStorage.setItem('userData', JSON.stringify(d.data));
//                        $http.defaults.headers.common['Authorization'] = d.data.token;
						var userlang = JSON.parse($window.sessionStorage.userData);
					
						 $rootScope.$broadcast('ptos', { d: $window.sessionStorage.ptosUser })
                       
                        $rootScope.$broadcast('language', { d: userlang.language })
                        
						// $scope.user = JSON.parse($window.sessionStorage.userData);
						
						$rootScope.$broadcast('nameUser', { a: $window.sessionStorage.nameUser })						
                        
                        var promisePost = BusinessCRUDService.getallBusinessbyUsertoLogin(d.data.iduser);
    	                promisePost.then(function (b) {
    	                	$window.sessionStorage.setItem('busninessnumber', b.data.length);
    	                	$rootScope.$broadcast('busninessnumber', { a: $window.sessionStorage.busninessnumber })
    	                	if($window.sessionStorage.toredirect != undefined)
    	                		{
    	                			$location.path($window.sessionStorage.toredirect);
	    	                		
    	                		}    	                	
    	                	else if (b.data.length>1)    	                		
    	                		{	
    	                		$location.path('/businessUser');
    	                		}
    	                	else if (b.data.length ===1)
    	                		{	
	    	                		$window.sessionStorage.setItem('idbusiness', b.data[0].idbusiness);
	    	                		$location.path('/mybusiness/'+b.data[0].idbusiness);
    	                		}
    	                	else{
    	                		$location.path('/home');
    	                	}
    	                	
	                		$('.navbar-collapse').removeClass('show');
	                		$('.hamb .nav-icon, #menu-fix').removeClass('o');	
    	                	
    	                },
    	        		function (err) {
    	                    console.log(err);
    	                    $scope.errorMessage = "ERROR_DATA";
    	                    $scope.verMessageError = true;
    	                });
    	                $scope.CerrarModal();
                    } 
                	else {
                    	 
                    	$scope.errorMessage = "ERROR_DATA";
                        $scope.verMessageError = true;
                    }
                },
        		function (err) {
                    console.log(err);
                    $scope.errorMessage = "ERROR_DATA";
                    $scope.verMessageError = true;
                });
        		}
        	else{
        		$scope.userempty = true;
    			$scope.passempty = true;
        	}        	
		};   
		
		 $scope.save = function (RegisterUser) {
			 console.log(RegisterUser)
			//si IsNewRecord == 1 adiciono si no edito      	
				if(angular.equals($scope.exituser, false))
				{					
				var promisePost = RegisterUserCRUDService.addRegisterUser(RegisterUser);
					promisePost.then(function (d) {						
						$scope.Message = "SUCCESSFULL_REGISTRE";
						$scope.verMessage = true; 
						$scope.CerrarModal();
						$location.path("/home");
						toast({
		              	     duration  : 5000,
		              	     message   :  $filter('translate')("CHECK_EMAIL_NEW_USER"),
		              	     className : "alert-success",
		              	     position: "center",
		              	     dismissible: true,
		              	   });
						var promisePost = RegisterUserCRUDService.sendmailregistration(d.data);
						promisePost.then(function (d) {	
							console.log("send good")
						}, function (err) {
							console.log(err);
						});
					}, function (err) {
						console.log(err);
						$scope.errorMessage = "ERROR_DATA_SAVE";
						$scope.verMessageError = true;
					});    
				}
		};
		$scope.lengthpass = function () {
			if($scope.RegisterUser.password.length < 6)
				{
					$scope.passminlength = true;
				}
			else
				{
				 $scope.passminlength = false;
				}
		}
		 //existe nameUser
			$scope.existnameuser = function (RegisterUser) {
				$scope.equalpass();
				$scope.lengthpass();
				console.log(RegisterUser)
				if(RegisterUser.nameUser.length>0 && RegisterUser.mail.length>0 && RegisterUser.password.length>0 && RegisterUser.password2.length>0)
        		{
					if($scope.unequalpass == false && $scope.passminlength == false)
					{
						var promiseGetSingle = RegisterUserCRUDService.existNameUser(RegisterUser);
						promiseGetSingle.then(function (d) {
							$scope.existNameUserMessage = d.data;
							$scope.exituser= d.data;
							$scope.save(RegisterUser);
						}, function (err) {
							console.log(err);
							$scope.errorMessage = "ERROR_USER_NAME";
							$scope.verMessageError = true;
						});
					}
					else{
						$scope.showunequal = true;
						$scope.unequalpass = false;	
					}
        		}
				else
					{
					$scope.dateempty = true;
					$scope.passuserempty = true;
					$scope.reppassempty = true;
					$scope.mailuserempty = true;
					$scope.nameUserempty = true;
						
					}
								    		
			}
			$scope.islogin = function()
			{
				if($window.sessionStorage.iduser == undefined )
					{return true;}
				else
					{return false;}
			}

	
    });
    app.service('LoginCRUDService',['$http','$window','$location','RELATIVE_DIR', function ($http,$window,$location,RELATIVE_DIR) {
       	
        this.login = function login(UserLogin){
            return $http({
              method: 'POST',
              url: RELATIVE_DIR+'/api/user/and/pass',
              data: UserLogin
            });
        }
        this.getUser = function getUser(iduser){
            return $http({
              method: 'GET',
              url: RELATIVE_DIR+'/api/users/'+iduser
            });
        }
        
        this.saveprofile = function saveprofile(User){
            return $http({
              method: 'POST',
              url: RELATIVE_DIR+'/api/user/profile/'+User.iduser,
              data: User
            });
        }
        
        this.actualizarptos = function actualizarptos(iduser,ptos){
            return $http({
              method: 'POST',
              url: RELATIVE_DIR+'/api/user/'+iduser+'/ptos/'+ptos
            });
        }
        this.changeRol = function changeRol(iduser,idnewrol){
            return $http({
              method: 'POST',
              url: RELATIVE_DIR+'/api/user/'+iduser+'/idnewrol/'+idnewrol
            });
        }        
    }]);
    //fin login
    
    
    app.controller("navbarController", function ($scope,$interval, $window, $location,$translate, $timeout,$rootScope , NavbarCRUDService, PromotionCRUDService, RELATIVE_DIR, 
    		RegisterUserCRUDService, PromotionCRUDService, OfferCRUDService, FuncionalityCRUDService,BusinessCRUDService,NotificationCRUDService ) {
    	    	
    	
    	//limpiar registro
        $scope.clear = function () {
            $scope.Language = {
            		name_languaje: "",
            };     
            $scope.DetailPromo;
            $scope.forshowpopuo=false;
            $scope.toshowpopup= false;
        }        
        $interval(function () {
        	$scope.loadPromo(); 
        	$scope.loadOffer();        
   		}, 60000);
        
        $interval(function () {
        	if($window.sessionStorage.iduser != undefined)
    		{
        		$scope.loadNoti($window.sessionStorage.iduser);
    		}        
   		}, 60000);
        
        $scope.myonlybusiness = function () {	        
			$location.path('/mybusiness/'+$window.sessionStorage.idbusiness); 
	    };
	    $scope.navegateto = function (ruta) {	        
			$location.path(ruta);
			$('.navbar-collapse').removeClass('show');
			$('.hamb .nav-icon, #menu-fix').removeClass('o');
	    };
        $scope.invite = function()
    	{
    		$('#Invite').modal('show'); // abrir
    	}
        $scope.buypoint = function()
    	{
        	$location.path('/buypoints');
    	}
        $scope.buyfuncionality = function()
    	{
        	
        	$location.path('/buyfuncionality/'+$window.sessionStorage.iduser);
    	}
        $scope.goDetailNoti = function (Notification) {

//    		$window.sessionStorage.setItem('idnotification', Notification.idnotification);
    		$location.path("/notificationdetail/"+Notification.idnotification );  
    		
    	}
        $scope.gopromotion= function(Promo)
        {
//        	$window.sessionStorage.setItem('Promo', JSON.stringify(Promo));
        	$location.path('/promotiondetail/'+Promo.idpromotion);   
        	$('.navbar-collapse').removeClass('show');
			$('.hamb .nav-icon, #menu-fix').removeClass('o');
        } 
       
        $scope.showpopuo = function()
        {
        	 if($window.sessionStorage.userData != undefined)
        		 {
	        		 var recordsGet = RegisterUserCRUDService.updatepoint($window.sessionStorage.iduser);
	                 recordsGet.then(function (d) {
	                	 $window.sessionStorage.setItem('ptosUser', d.data);
	                	 $rootScope.ptosUser = $window.sessionStorage.ptosUser;
	                	 $scope.toshowpopup = !$scope.toshowpopup;
	                 },
	                 function (errorPl) {
	                     $log.error( errorPl);
	                 }); 
        		 }
        	 
        }
		 $scope.$on('loadnoti', function(event, option) {	
			 if($window.sessionStorage.iduser != undefined)
				 {
				 	$scope.loadNoti($window.sessionStorage.iduser)
				 }			 
		});
        $scope.loadNoti = function (iduser) {        	
	        		var recordsGet = NotificationCRUDService.getAllNotificationUser(iduser);
	                recordsGet.then(function (d) {
	    				$scope.ListNoti = d.data;
	    				
	                },
	                function (errorPl) {
	                    $log.error( errorPl);
	                });        		
        }
        
        $scope.loadPromo = function () {
        	var recordsGet = PromotionCRUDService.getAllPromotions();
            recordsGet.then(function (d) {
				$scope.ListPromo = d.data;
            },
            function (errorPl) {
                $log.error( errorPl);
            });        	
        }
        
      $scope.gooffer= function(Offer)
      {
//      	$window.sessionStorage.setItem('Offer', JSON.stringify(Offer));
      	$location.path('/offerdetail/'+Offer.idoffer);
      	$('.navbar-collapse').removeClass('show');
		$('.hamb .nav-icon, #menu-fix').removeClass('o');
      	
      }      
	    $scope.loadOffer = function () {
	    	var recordsGet = OfferCRUDService.getAllOffersActive();
	        recordsGet.then(function (d) {
				$scope.ListOffer = d.data;
	        },
	        function (errorPl) {
	            $log.error( errorPl);
	        });        	
	    }        
        if($window.sessionStorage === undefined)
        	{
        	$rootScope.nameUser = "";
            $rootScope.numberbusiness = 0;
        	}
        else{
			$rootScope.ptosUser = $window.sessionStorage.ptosUser;
        	$rootScope.nameUser = $window.sessionStorage.nameUser;
			$rootScope.numberbusiness = $window.sessionStorage.busninessnumber;
        }
       
        $scope.loadfuncionalitiesformodal = function()
        {
        	var recordsGet = FuncionalityCRUDService.getAllFuncionality();
            recordsGet.then(function (d) {
            	$scope.funcionalities=d.data;
            	angular.forEach($scope.funcionalities, function(fun) {
                	if(fun.pointFuncionality <=  $window.sessionStorage.ptosUser && fun.sistema == false)
                		{
                			$scope.funcionalitiestoshow.push(fun);
                		}
                });
            	if($scope.funcionalitiestoshow.length == 0)
            		{
            		angular.forEach($scope.funcionalities, function(fun) {
                    	if(fun.sistema == true)
                    		{
                    			$scope.funcionalitiestoshow.push(fun);
                    		}
                    });
            		}
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
        }    
        
        loadRecords();
        //cargar todos los prestamos
        function loadRecords() {
        	
        	$scope.clear();        	 
        	$scope.loadPromo();
        	$scope.loadOffer();
        	$scope.funcionalities = [];
        	$scope.funcionalitiestoshow = [];
        	$scope.loadfuncionalitiesformodal();
        	if($window.sessionStorage.iduser != undefined)
    		{
        		$scope.loadNoti($window.sessionStorage.iduser);
    		}        	
        	$scope.coment = false;
        	$scope.reservation = false;
        	$scope.promo = false;
        	$scope.offer = false;
        	$scope.calification = false;
            var recordsGet = NavbarCRUDService.getAllLanguages();
            recordsGet.then(function (d) {
				$scope.Lenguajes = d.data;				
            	if($window.sessionStorage.userData !=undefined)
            		{
            		var user = JSON.parse($window.sessionStorage.userData);
	            		if(user.language.length!=0)
	            		{
	            		var primerleng = $scope.Lenguajes.filter(item => item.name_languaje.toLowerCase() === user.language);
	                    $scope.Language.name_languaje = primerleng[0].name_languaje;
	                    $scope.changeLanguage($scope.Language.name_languaje.toUpperCase());
	                    $window.sessionStorage.setItem('language', $scope.Language.name_languaje.toLowerCase());	                    	                    
	            		}
            		}
            	else
            		{
            		var primerleng = $scope.Lenguajes.filter(item => item.name_languaje === "ES");
                    $scope.Language.name_languaje = primerleng[0].name_languaje;
                    $scope.changeLanguage($scope.Language.name_languaje);
                    $window.sessionStorage.setItem('language', $scope.Language.name_languaje.toLowerCase());
            		}                
                },
                function (errorPl) {
                    $log.error('Error ', errorPl);
                });
        }
        
        $scope.$on('language', function(event, option) {
        	
    		$scope.changeLanguage(option.d.toUpperCase());
    		loadRecords();
    		 $scope.Language.name_languaje = option.d.toUpperCase();
    		});
        
    	$scope.changeLanguage = function (key) {  		
    		$window.sessionStorage.setItem('language', key.toLowerCase());
    	        $rootScope.lang = key.toLowerCase();
    	        $translate.use(key.toLowerCase());
    	        if($window.sessionStorage.userData !=undefined)
    	        	{
	    	        	var recordsGet = RegisterUserCRUDService.changelanguageuser(key.toLowerCase(),$window.sessionStorage.iduser);
	    	            recordsGet.then(function (d) {
	    	            	 $window.sessionStorage.setItem('userData', JSON.stringify(d.data));
	    	                },
	    	                function (errorPl) {
	    	                    $log.error('Error ', errorPl);
	    	                });
    	        	}
    	    };
	    $scope.variosnegocios= function () {  		
	    	if(parseInt($rootScope.numberbusiness) > 1)
	    		{
	    			return true;
	    		}
	    	else {
	    			return false;
	    	}
    	    };
        	    
    	    $scope.unnegocios= function () {  		
    	    	if(parseInt($rootScope.numberbusiness) == 1)
    	    		{
    	    			return true;
    	    		}
    	    	else {
    	    			return false;
    	    	}
        	    };
    	
    	 $scope.roleadmin= function()
    	{
    		if($window.sessionStorage.length>1)
    		{
				if($window.sessionStorage.userData !=undefined)
				{
					$scope.user = JSON.parse($window.sessionStorage.userData);
					if ($scope.user.role.desc_rol==="ROLE_ADMIN")
						{
							return true;
						}
					else
						{
							return false; 
						}
				}
    		
    		else {
    			return false;
    			}
    		}
    		
		}    	
				
    	 $scope.$on('busninessnumber', function(event, opt) {    	  		
    		 $rootScope.numberbusiness = opt.a;
    	    		});
    	 
    	$scope.$on('nameUser', function(event, opt) {    	  		
   		$rootScope.nameUser = opt.a;
   		$scope.logueado();
			});
			$scope.$on('ptos', function(event, opt) {    	  		
   		$rootScope.ptosUser = opt.d;
    		});
    	$scope.logout = function () {
    		var promisePost = NavbarCRUDService.destroytoken($window.sessionStorage.iduser);
            promisePost.then(function (d) {
            }, function (err) {
                console.log(err);
            });
    		$rootScope.nameUser = "";
    		$rootScope.$broadcast('checksearch', { d: "not" })
    		$scope.logueado();    		
    		$window.sessionStorage.clear();
    		$location.path('/home'); 
    			$('.navbar-collapse').removeClass('show');
    			$('.hamb .nav-icon, #menu-fix').removeClass('o');	
    		
    	};
    	$scope.logueado = function (){
    		if($window.sessionStorage.nameUser)
    			{
    				return true;    				
    			}
    		else
    			{
    				return false;  
    			}
    	}
    });
    
    app.service('NavbarCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
     	
        this.getAllLanguages = function getAllLanguages(){        	
            return $http({
              method: 'GET',
              url: RELATIVE_DIR+'/api/languages'
            });
        }
        this.destroytoken = function destroytoken(iduser){        	
            return $http({
              method: 'GET',
              url: RELATIVE_DIR+'/api/user/iduser/'+iduser
            });
        }

    }]);
   

// las rutas
app.config(function($stateProvider, $urlRouterProvider,RELATIVE_DIR) {
	
    $urlRouterProvider.otherwise('home');
    	
    $stateProvider
    
    .state('home', {
        url: '/home',
        views: {

            // the main template will be placed here (relatively named)
            '': { templateUrl: RELATIVE_DIR+'/html/home/head-home.html' },

            // the child views will be defined here (absolutely named)
            'sessiontwo@home': { 
            	templateUrl: RELATIVE_DIR+'/html/home/sessiontwo-home.html' },

            // for column two, we'll define a separate controller 
            'sessionthree@home': { 
                templateUrl: RELATIVE_DIR+'/html/home/sessionthree-home.html'},
            'sessionfour@home': { 
                templateUrl: RELATIVE_DIR+'/html/home/sessionfour-home.html'
                    }
                }
            })
            .state('business', {
        url: '/business',
        templateUrl: RELATIVE_DIR+'/html/business/business.html',
        controller: 'BusinessController'
        })
        .state('mybusiness/{idbusiness}', {
        url: '/mybusiness/{idbusiness}',
        templateUrl: RELATIVE_DIR+'/html/business/business-det.html',
        controller: 'BusinessDetailController'
		})
		 .state('searchingbusiness', {
        url: '/searchingbusiness',
        templateUrl: RELATIVE_DIR+'/html/business/business-list-search.html',
        controller: 'SearchingBusinessListController'
		})
		.state('businessedit/{idbusiness}', {
        url: '/businessedit/{idbusiness}',
        templateUrl: RELATIVE_DIR+'/html/business/business-edit.html',
        controller: 'BusinessEditController'
		})
		.state('businessclone/{idbusiness}', {
        url: '/businessclone/{idbusiness}',
        templateUrl: RELATIVE_DIR+'/html/business/business-clone.html',
        controller: 'BusinessCloneController'
        })
        .state('userprofile', {
        url: '/userprofile',
        templateUrl: RELATIVE_DIR+'/html/modificationuser/modificationuser.html',
        controller: 'UserProfileController'
        })
        .state('listuser', {
        url: '/listuser',
        templateUrl: RELATIVE_DIR+'/html/modificationuser/listuser.html',
        controller: 'ListUserController'
        })
        .state('configuration', {
        url: '/configuration',
        templateUrl: RELATIVE_DIR+'/html/nomencladores/configuration/configuration-list.html',
        controller: 'ConfigurationController'
        })
        .state('home/{usercod}', {
        url: '/home/{usercod}',
        templateUrl: RELATIVE_DIR+'/html/static-pages/invitado.html',
        controller: 'InvitedController'
        })
        .state('/mybusiness/{idbusiness}/qr', {
        url: '/mybusiness/{idbusiness}/qr',
        templateUrl: RELATIVE_DIR+'/html/static-pages/visitqr.html',
        controller: 'VisitedQRController'
        })
        .state('category', {
            url: '/category',
            templateUrl: RELATIVE_DIR+'/html/nomencladores/category/category-list.html',
            controller: 'CateController'
        })
        .state('ratings', {
            url: '/ratings',
            templateUrl: RELATIVE_DIR+'/html/clasification/clasification-list.html',
            controller: 'CalificationController'
        })
        .state('jobs', {
            url: '/jobs',
            templateUrl: RELATIVE_DIR+'/html/jobs/jobs-list.html',
            controller: 'JobController'
        })
        .state('jobsoffers', {
            url: '/jobsoffers',
            templateUrl: RELATIVE_DIR+'/html/jobs/jobsoffers-list.html',
            controller: 'JobOffersController'
        })
        .state('pointpackage', {
            url: '/pointpackage',
            templateUrl: RELATIVE_DIR+'/html/nomencladores/pointpackage/pointpackage-list.html',
            controller: 'PointPackageController'
        })
	    .state('country', {
	        url: '/country',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/country/country-list.html',
	        controller: 'CountryController'
        })
        .state('schedulejoboffer', {
	        url: '/schedulejoboffer',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/schedulejoboffer/schedulejoboffer-list.html',
	        controller: 'ScheduleJobOfferController'
        })	
        .state('schoollevel', {
	        url: '/schoollevel',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/schoollevel/schoollevel-list.html',
	        controller: 'SchoollevelController'
        })	
        .state('contracttype', {
	        url: '/contracttype',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/contracttype/contracttype-list.html',
	        controller: 'ContractTypeController'
        })
        .state('professionalexperience', {
	        url: '/professionalexperience',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/professionalexperience/professionalexperience-list.html',
	        controller: 'ProfessionalExperienceController'
        })
        .state('buypoints', {
	        url: '/buypoints',
	        templateUrl: RELATIVE_DIR+'/html/modificationuser/buypoints.html',
	        controller: 'BuyPointsControllers'
        })
        .state('provinces', {
	        url: '/provinces',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/province/province-list.html',
	        controller: 'ProvinceController'
	    })	 
	    .state('municipality', {
            url: '/municipality',
            templateUrl: RELATIVE_DIR+'/html/nomencladores/municipality/municipality-list.html',
            controller: 'MunicipalityController'
        })  
         .state('notificationstate', {
            url: '/notificationstate',
            templateUrl: RELATIVE_DIR+'/html/nomencladores/notificationstate/notificationstate-list.html',
            controller: 'NotificationStateController'
        }) 
        .state('workusers', {
            url: '/workusers',
            templateUrl: RELATIVE_DIR+'/html/users/workusers.html',
            controller: 'WorkUsersController'
        }) 
        .state('forgotpass', {
            url: '/forgotpass',
            templateUrl: RELATIVE_DIR+'/html/modificationuser/forgopassword.html',
            controller: 'ForgotPassController'
        }) 
        .state('funcionalities', {
            url: '/funcionalities',
            templateUrl: RELATIVE_DIR+'/html/nomencladores/funcionality/funcionality-list.html',
            controller: 'FuncionalityController'
        })  
        .state('notificationtype', {
            url: '/notificationtype',
            templateUrl: RELATIVE_DIR+'/html/nomencladores/notificationtype/notificationtype-list.html',
            controller: 'TypeNotificationController'
        })      
        .state('usersStates', {
	        url: '/usersStates',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/usersStates/userstates-list.html',
	        controller: 'UserStatesController'
        })
         .state('statePackage', {
	        url: '/statePackage',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/statepackage/statepackage-list.html',
	        controller: 'StatePackageController'
        })
        .state('roles', {
	        url: '/roles',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/rol/rol-list.html',
	        controller: 'RolController'
        })
        .state('registeruser', {
	        url: '/registeruser',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/registeruser/registeruser.html',
	        controller: 'RegisterUserController'
        })
        .state('activationuser/{codigo}', {
	        url: '/activationuser/{codigo}',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/registeruser/activationcode.html',
	        controller: 'ActivationCodeController'
        })
         .state('changepassword/{token}', {
	        url: '/changepassword/{token}',
	        templateUrl: RELATIVE_DIR+'/html/modificationuser/changepass.html',
	        controller: 'ChangePassController'
        })
        .state('businessFuncionality', {
	        url: '/businessFuncionality',
	        templateUrl: RELATIVE_DIR+'/html/business/business-list.html',
	        controller: 'ActivateFuncionalityController'
        })        
         .state('promotions', {
        url: '/promotions',
        templateUrl: RELATIVE_DIR+'/html/promotions/promotions-list.html',
        controller: 'PromotionController'
		 })

		 .state('offers', {
        url: '/offers',
        templateUrl: RELATIVE_DIR+'/html/offers/offer-list.html',
        controller: 'OfferController'
		 })
 		.state('notifications', {
        url: '/notifications',
        templateUrl: RELATIVE_DIR+'/html/notifications/notifications-list.html',
        controller: 'NotificationController'
		 })
		 .state('reservations', {
        url: '/reservations',
        templateUrl: RELATIVE_DIR+'/html/reservation/reservation-list.html',
        controller: 'ReservationController'
		 })
		  .state('coments', {
        url: '/coments',
        templateUrl: RELATIVE_DIR+'/html/coments/coment-list.html',
        controller: 'ComentUserController'
		 })
        .state('paymentForms', {
	        url: '/paymentForms',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/paymentForms/paymentforms-list.html',
	        controller: 'PaymentFormsController'
        })
        .state('subcategories', {
	        url: '/subcategories',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/subcategory/subcategory-list.html',
	        controller: 'SubcategoryController'
	    })
	    .state('businessUser', {
        url: '/businessUser',
        templateUrl: RELATIVE_DIR+'/html/business/business-list-user.html',
        controller: 'BusinessListController'     	
        })
        .state('listpromobybusiness/{idbusiness}', {
        url: '/listpromobybusiness/{idbusiness}',
        templateUrl: RELATIVE_DIR+'/html/promotions/promotions-business-list.html',
        controller: 'PromotionListBusinessController'     	
        })
        .state('listofferbybusiness/{idbusiness}', {
        url: '/listofferbybusiness/{idbusiness}',
        templateUrl: RELATIVE_DIR+'/html/offers/offer-business-list.html',
        controller: 'OfferListBusinessController'     	
        })
        .state('detailreservation/{idreservacion}', {
        url: '/detailreservation/{idreservacion}',
        templateUrl: RELATIVE_DIR+'/html/reservation/reservation-detail.html',
        controller: 'ReservationDetailController'     	
        })
         .state('promotiondetail/{idpromotion}', {
        url: '/promotiondetail/{idpromotion}',
        templateUrl: RELATIVE_DIR+'/html/promotions/promotion-detail.html',
        controller: 'PromotionDetailController'     	
        })
         .state('jobdetail/{idjob}', {
        url: '/jobdetail/{idjob}',
        templateUrl: RELATIVE_DIR+'/html/jobs/job-detail.html',
        controller: 'JobDetailController'     	
        })
        .state('offerdetail/{idoffer}', {
        url: '/offerdetail/{idoffer}',
        templateUrl: RELATIVE_DIR+'/html/offers/offer-detail.html',
        controller: 'OfferDetailController'     	
        })
        .state('notificationdetail/{idnotification}', {
        url: '/notificationdetail/{idnotification}',
        templateUrl: RELATIVE_DIR+'/html/notifications/notifications-detail.html',
        controller: 'NotificationDetailController'     	
        })
        .state('invitefriend', {
        url: '/invitefriend',
        templateUrl: RELATIVE_DIR+'/html/static-pages/invitefriend.html',
        controller: 'NotificationDetailController'     	
        })
	    .state('businessStates', {
	        url: '/businessStates',
	        templateUrl: RELATIVE_DIR+'/html/nomencladores/businessStates/businesstates-list.html',
	        controller: 'BusinessStatesController'
        })
        .state('buyfuncionality/{iduser}', {
	        url: '/buyfuncionality/{iduser}',
	        templateUrl: RELATIVE_DIR+'/html/funcionalities/buyfuncionality.html',
	        controller: 'BuyFuncionalityController'
        });
    

});

/*inicio de prueba*/

app.directive('modal', function () {
    return {
        template:
            '<div class="modal fade" ng-transclude></div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });
            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });
            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

/*fin de prueba*/


app.service('RegisterUserCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {
		
	this.sendmailregistration = function sendmailregistration(User){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/sendmailregistration',
          data:User
        });
    }
	
	this.getRegisterUser = function getRegisterUser(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/users/'+iduser
        });
	}
   
    this.addRegisterUser = function addRegisterUser(RegisterUser){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/rol/'+RegisterUser.rol,
          data: RegisterUser
        });
    }
    
    this.addasigpointuser = function addasigpointuser(iduser,idpackage){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/asigpointuser/'+iduser+"/package/"+idpackage
        });
    }
    this.existNameUser = function existNameUser(RegisterUser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/users/exist/'+RegisterUser.nameUser+'/email/'+RegisterUser.mail
        }); 
    }	
    this.updateRegisterUser = function updateRegisterUser(iduser,
    		name_user,email, Sex,date_bith, phone_user,password, name, surnameUser,surname2_user){
        return $http({
          method: 'PUT',
          url: RELATIVE_DIR+'/api/users/'+iduser+'/state',
          data: {iduser: iduser,
      		name_user: name_user,
            email: email,
            Sex: Sex,
            date_bith: date_bith,
            phone_user:phone_user,
            password:password,               
            name:name,
            surnameUser: surnameUser,
            surname2_user:surname2_user  }
        })
    }
	
    this.getAllRegisterUser = function getAllRegisterUser(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/users'
        });
    }
    
    this.changeUserState = function changeUserState(iduser,activateuser){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/'+iduser+'/changeuserstate/'+activateuser
        });
    }    
    this.sendmailforgotpass = function sendmailforgotpass(email){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/forgotpass/'+ email
        });
    }
    this.sendmailwithtoken = function sendmailwithtoken(iduser,ModDir){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/'+iduser+'/sendmailwithtoken',
          data: ModDir
        });
    }
    
    this.getuserbytoken = function getuserbytoken(token){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/users/token/'+ token
        });
    }
    this.newpass = function newpass(newpass, User){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/newpass/'+ newpass,
          data: User
        });
    }
    this.changelanguageuser = function changelanguageuser(language, iduser){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/users/'+ iduser+'/language/'+language
        });
    }
    this.updatepoint = function updatepoint(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/users/getpoint/'+iduser
        });
    }
        this.lesspoint = function lesspoint(iduser,point){
            return $http({
              method: 'GET',
              url: RELATIVE_DIR+'/api/users/lesspoint/'+iduser+'/'+point
            });
    }
    

}]);
// Registrar Usuario fin


//Activation Code inicio****************************
app.controller("ActivationCodeController", function ($scope, $rootScope, $location, toast, ActivationCodeCRUDService ) {
	
	var pathname = window.location.href;
	var urlParts = pathname.split('/');
	var codevalidation =urlParts[urlParts.length - 1];	
	$scope.message = false;
    $scope.user;
    $scope.todisabled = true;
    console.log(codevalidation);   
   
    //guardar prestamos
	loadregistration(codevalidation);
    function loadregistration (codevalidation) {
         var promisePost = ActivationCodeCRUDService.addActivationCode(codevalidation);
            promisePost.then(function (d) {
            	$scope.user = d.data;  
            	console.log($scope.user)
            	if($scope.user.iduser == null)
            		{
        				$scope.todisabled = true;
        				$scope.message = true;
            		}
            	else{
            		$scope.todisabled = false;
            	}
            }, function (err) {
                console.log(err);
            });
          };   
          $scope.finishregistration = function()  
          {
        	  var promisePost = ActivationCodeCRUDService.activateuser($scope.user);
              promisePost.then(function (d) {
            	  $location.path('/home');
            	  $('#Login').modal('show');
              }, function (err) {
                  console.log(err);
              });
          }
          $scope.gohome = function () {
      		$location.path('/home');
      	}
});
app.service('ActivationCodeCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {	
   
    this.addActivationCode = function addActivationCode(codevalidation){
    	console.log(codevalidation)
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/activationcode/'+codevalidation
        });
    }
    this.activateuser = function activateuser(user){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/activeuser',
          data:user
        });
    }
	
}]);
// Activation Code Usuario fin


//Buy Points inicio****************************
app.controller("BuyPointsControllers", function ($scope,$window,$filter, $location, toast,FuncionalityCRUDService, BuyPointsCRUDService,PointPackageCRUDService,PaymentFormsCRUDService) {


    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    $scope.funcionalitytosystem = [];
    
    //modal adicionar
    $scope.showmodalpackage = function () {
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
        $scope.showModal = false;
    };
//    loadRecords();
    $scope.BuyPoint= {
    		id_package:0,
    		idpayment_form:0,
    		};
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{			 
			 $('#Login').modal('show');
			 $location.path('/home');
		}
    	var recordsGet = PointPackageCRUDService.getAllPointPackage();
        recordsGet.then(function (d) {
                $scope.PaquetePuntos = d.data;
                if(d.data.length!=0)
                {$scope.BuyPoint.id_package=d.data[0].id_package}
                else
                {$scope.BuyPoint.id_package=0}
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
            
            var recordsGet = PaymentFormsCRUDService.getAllPaymentForms();
            	recordsGet.then(function (d) {
                $scope.FormasPago = d.data;
                if(d.data.length!=0)
                {$scope.BuyPoint.idpayment_form=d.data[0].idpayment_form}
                else
                {$scope.BuyPoint.idpayment_form=0}
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
    	var recordsGet = FuncionalityCRUDService.getAllFuncionalitySystem();
		recordsGet.then(function (d) {
			$scope.funcionalitytosystem = d.data
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
		var recordsGet = FuncionalityCRUDService.getAllFuncionalityNotSystem();
		recordsGet.then(function (d) {
			$scope.funcionality = d.data
			console.log($scope.funcionality)
		},
			function (errorPl) {
				$log.error('Error ', errorPl);
			});
        
    }
    
    $scope.out = function () {
    	$window.history.back(-1);	
    }
    //guardar prestamos
    $scope.save = function (BuyPoint) {

            var promisePost = BuyPointsCRUDService.addBuyPoints(BuyPoint,$window.sessionStorage.iduser);
            promisePost.then(function (d) {
            	toast({
              	     duration  : 5000,
              	     message   :  $filter('translate')("REQUEST_PROCESS"),
              	     className : "alert-success",
              	     position: "center",
              	     dismissible: true,
              	   });
            		$scope.CerrarModal();         	  
             	                  
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "ERROR_DATA_SAVE";
                $scope.verMessageError = true;
            });
        
    };

});
app.service('BuyPointsCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {

	
    this.addBuyPoints = function addBuyPoints(BuyPoint,iduser){
        return $http({
            method: 'POST',
            url: RELATIVE_DIR+'/api/buypoint/'+iduser+'/pointpackage/'+BuyPoint.id_package+'/payform/'+BuyPoint.idpayment_form           
        });
    }

}]);


// Buy Points fin
//Inicio WorkUsers
app.controller("WorkUsersController", function ($scope,$window, $location, toast, $filter, WorkUsersCRUDService,RegisterUserCRUDService,PointPackageCRUDService,PaymentFormsCRUDService) {
	
    
    $scope.showModal = false;
    $scope.verMessage = false;
    $scope.verMessageError = false;
    $scope.errorMessage = "";
    $scope.selection = [];

    // Toggle selection for a given fruit by name
    $scope.toggleSelection = function toggleSelection(User) {
      var idx = $scope.selection.indexOf(User);

      // Is currently selected
      if (idx > -1) {
        $scope.selection.splice(idx, 1);
      }

      // Is newly selected
      else {
        $scope.selection.push(User);
      }
      
    };
    $scope.selectedall = function() {
    	if($scope.selectAll==true)
		{
			$scope.selection.splice(0);
		}
	else
		{
			angular.forEach($scope.Usuarios, function (us) {
				$scope.selection.push(us);
			});
		}
    	$scope.selectAll = !$scope.selectAll;
      };
    
    //modal adicionar
    $scope.AdicionarModal = function () {
    	var recordsGet = PointPackageCRUDService.getAllPointPackage();
        recordsGet.then(function (d) {
                $scope.PaquetePuntos = d.data
                if(d.data.length!=0)
                {$scope.WorkUsers.id_package=d.data[0].id_package}
                else
                {$scope.WorkUsers.id_package=0}
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
            
        $scope.showModal = true;
    };
    $scope.CerrarModal = function () {
    	$scope.clear();
        $scope.showModal = false;
    };
    $scope.habilitarBoton = function (){
    	if($scope.selection.length!=0)
    		{
    			return false;
    		}
    	else
    		{
    			return true;
    		}
    }
    //limpiar registro
    $scope.clear = function () {
    	 $scope.selectAll = false;
    	 $scope.selection = [];
        $scope.IsNewRecord = 1;
        $scope.PaquetePuntos =[];
        $scope.WorkUsers = {
        		id_package: 0,
        		date_end: new Date(),
        };
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	
    	if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
        $scope.clear();
        var recordsGet = RegisterUserCRUDService.getAllRegisterUser();
        recordsGet.then(function (d) {
                $scope.Usuarios = d.data
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });
    }
    $scope.AsingPoint = function (WorkUsers) {    
    	var allfine = false;
    	angular.forEach($scope.selection, function(selec) {
    		var promisePost = WorkUsersCRUDService.asingPoint(selec,WorkUsers);
            promisePost.then(function (d) {
            	loadRecords();
            	$scope.CerrarModal();
            	allfine=true;
	            	toast({
	           	     duration  : 4000,
	           	     message   : $filter('translate')("POINT_ASSING"),
	           	     className : "alert-success",
	           	     position: "center",
	           	     dismissible: true,
	           	   });
            }, function (err) {
                console.log(err);
                $scope.errorMessage = "PACKAGE_ERR_ASSIGN";
                $scope.verMessageError = true;
            });
        }); 
    	if(allfine)
    		{
    		toast({
         	     duration  : 5000,
         	     message   : $filter('translate')("ASSIG_COMPLETE"),
         	     className : "alert-success",
         	     position: "center",
         	     dismissible: true,
         	   });
    		}    	
    };
    $scope.back = function () {
    	$window.history.back(-1);	
	}
    
});


app.service('WorkUsersCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {

	 this.asingPoint = function asingPoint(selec,WorkUsers){
        return $http({
            method: 'POST',
            url: RELATIVE_DIR+'/api/users/'+selec.iduser+'/package/'+WorkUsers.id_package
        });
    }

}]);
//fin paquete puntos
app.controller("SearchListController", function ($scope,$location, $http, $window,$filter,RELATIVE_DIR,olData, ConfigurationCRUDService, CategoryCRUDService,MunicipalityCRUDService,BusinessCRUDService,NotificationCRUDService) {
	
	$scope.clear = function () {
		$scope.showmap = false;
		$scope.Business = [];
		$scope.allbusinessbd = [];
		$scope.markersbusi = [];
		$scope.point = [];
		$scope.latini= 0;
		$scope.lonini= 0;	
		$scope.Favorite= false;
		$scope.LastSearch= false;
		$scope.Islogged= false;
		if($window.sessionStorage.iduser != undefined)
			{
				$scope.Islogged= true;
			}
		$scope.$on('checksearch', function(event, option) {	
			$scope.Islogged= false;		 
		});
	}
	$scope.openMap = function () {
		$scope.showmap = !$scope.showmap;
		if($scope.showmap==true){
			$('.form-header').addClass('map-opened');
		}
		else
			{
				$('.form-header').removeClass('map-opened');
			}
	}
	$scope.checkfavo = function () {
		if($scope.LastSearch == true)
			{
				$scope.LastSearch = !$scope.LastSearch;
			}
		$scope.Favorite = !$scope.Favorite;
	}
	$scope.checklastsearch = function () {
		if($scope.Favorite == true)
			{
				$scope.Favorite = !$scope.Favorite;
			}
		$scope.LastSearch = !$scope.LastSearch;
	}
	 loadall();	 
	 function loadall() {
		$scope.clear();

		angular.extend($scope, {
			   center:{
			      lat: 0,
			      lon: 0,
			      zoom: 12
			    },
			    layer: {}
			  });
		
		$scope.Businessadd = {
				idbusiness: 0,
				iduser: 0,
				name_business: "",
				desc_business: "",
				code_qr: "",
				dir_business: "",
				web_site: "",
				licence_business: "",
				phone_business: "",
				municipality: 0,
				path_image: "",
				name_image: "",
				latitude: 0,
				longitude: 0,
				subcategory:[]
			};
		    var recordsGetMunicipalities = MunicipalityCRUDService.getAllMunicipality();
		    recordsGetMunicipalities.then(function (d) {
					$scope.Municipalities = d.data					
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
	            });
		    var recordsGet = CategoryCRUDService.getAllCategories();
	        recordsGet.then(function (d) {
					$scope.Categorias = d.data
						            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
	            });
	      
	        var recordsGet = BusinessCRUDService.getallBusinessbyActive();
	        recordsGet.then(function (d) {	
				$scope.templist = d.data;
				angular.forEach($scope.templist, function(bus) {
					var recordsGet = NotificationCRUDService.getSubcategoryBusiness(bus.idbusiness);
					recordsGet.then(function (d) {
						$scope.Businessadd = {
								idbusiness: bus.idbusiness,
								iduser: bus.user,
								name_business: bus.name_business,
								desc_business: bus.desc_business,
								code_qr: bus.code_qr,
								dir_business: bus.dir_business,
								web_site: bus.web_site,
								licence_business: bus.licence_business,
								phone_business: bus.phone_business,
								municipality: bus.municipality,
								path_image: bus.path_image,
								name_image: bus.name_image,
								latitude: bus.latitude,
								longitude: bus.longitude,
								subcategory:d.data
							};
						$scope.Business.push($scope.Businessadd);
						$scope.allbusinessbd.push($scope.Businessadd);
					},
						function (errorPl) {
	 	                console.log(errorPl);
	 	            });
					if(bus.latitude != undefined || bus.longitude!= undefined)
					{
						$scope.markersbusi.push({
							lat: parseFloat(bus.latitude),
							lon: parseFloat(bus.longitude),
//							name: bus.name_business,
							label: {
		                        message:  bus.name_business,
		                        show: false,
		                        showOnMouseOver: true
		                    },
		                    onClick: function (event, properties) {
//		                        console.log(event, properties);
		                    	$location.path('/mybusiness/'+bus.idbusiness);
		                      }
						});						

					}					
					});			
				
					
					var recordsGet = ConfigurationCRUDService.getAllConfiguration()
				recordsGet.then(function (d) {
				    
					$scope.center.lat = parseFloat(d.data[0].latitude);
					$scope.center.lon = parseFloat(d.data[0].longitude);
					},
					function (errorPl) {
						console.log("error" + errorPl)
					});
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
				});	   	       
	    } 
     
	 $scope.changecategorySearch = function (listosearch) {
		$scope.Business = $filter('filter')(listosearch, function (item) {
									var isin = false;	
									angular.forEach(item.subcategory, function(sub) {
											if(sub.category.idcategory == $scope.Category)
											{
												isin = true;
												return true;
											}
										});	
									if(isin)
									{	
										return true;
									}
								return false;
								});		

		}	 
		 $scope.changecategorybyBotons = function (btncategory) {
			 var Cat = $filter('filter')($scope.Categorias ,{name_category:btncategory});
			$scope.Category = Cat[0].idcategory;				
		}		
		 $scope.findbusinessonmap = function (listtosearch) {
			 if($scope.Category != undefined)
				{
					$scope.changecategorySearch(listtosearch);
					$scope.busitosearch = $scope.Business;
				}
			 else
				{
					$scope.busitosearch = listtosearch;
				}
				 $scope.filterBusiness = $filter('filter')($scope.busitosearch, function (item) {
						
						if(item.name_business.search($scope.Search) > -1)
						{
							return true;
						}
						if(item.desc_business.search($scope.Search) > -1)
						{
							return true;
						}
						if(item.dir_business.search($scope.Search) > -1)
						{
							return true;
						}									
					return false;
				});
				 if($scope.filterBusiness.length ==0)
					 {
					 $scope.filterBusiness = $scope.busitosearch;
					 }
				  	$scope.markersbusi = [];
						angular.forEach($scope.filterBusiness, function(bus) {
										if(bus.latitude != undefined || bus.longitude!= undefined)
										{
											$scope.markersbusi.push({
												lat: parseFloat(bus.latitude),
												lon: parseFloat(bus.longitude),
//												name: bus.name_business,
												label: {
							                        message:  bus.name_business,
							                        show: false,
							                        showOnMouseOver: true
							                    },
							                    onClick: function (event, properties) {
//							                        console.log(event, properties);
							                    	$location.path('/mybusiness/'+bus.idbusiness);
							                      }
											});	
										}								
									});					
		 		}
		 
		 $scope.findbusi = function (listtoserach) {
			 if($scope.Category != undefined)
				{
					$scope.changecategorySearch(listtoserach);
					$scope.busitosearch = $scope.Business;
				}
				else
					{
						$scope.busitosearch = listtoserach;
					}
				$scope.filterBusiness = $filter('filter')($scope.busitosearch, function (item) {
										
											if(item.name_business.search($scope.Search) > -1)
											{
												return true;
											}
											if(item.desc_business.search($scope.Search) > -1)
											{
												return true;
											}
											if(item.dir_business.search($scope.Search) > -1)
											{
												return true;
											}									
										return false;
									});
				 if($scope.filterBusiness.length ==0)
				 {
				 $scope.filterBusiness = $scope.busitosearch;
				 }				
				if($window.sessionStorage.userData !=undefined && ($scope.Search != "" && $scope.Search != undefined) && $scope.Category != undefined)
				{
					$scope.searchbusines = {
						idCategory: $scope.Category,
						idMunicipality:undefined,
						idUser : $window.sessionStorage.iduser,
						desc_search:$scope.Search,
						date_search:new Date()

					};
					var recordsGet = BusinessCRUDService.savesearch($scope.searchbusines);        
						recordsGet.then(function (d) {	
						},
						function (errorPl) {
								console.log( errorPl)
							});
				}
				$window.sessionStorage.setItem('texttosearch', JSON.stringify($scope.Search));
				$window.sessionStorage.setItem('categorytosearch', JSON.stringify($scope.Category));
				 $window.sessionStorage.setItem('searchlistbusiness', JSON.stringify($scope.filterBusiness));
				
				 $location.path('searchingbusiness');
		 }
		$scope.Searching = function () {
			if($scope.LastSearch && $scope.showmap){
				var recordsGet = BusinessCRUDService.lastsearch($window.sessionStorage.iduser);        
				recordsGet.then(function (d) {
					$scope.findbusinessonmap(d.data)
				},
				function (errorPl) {
						console.log( errorPl)
					});
			}
			else if($scope.LastSearch)
				{
					var recordsGet = BusinessCRUDService.lastsearch($window.sessionStorage.iduser);        
					recordsGet.then(function (d) {
						$scope.findbusi(d.data)
					},
					function (errorPl) {
							console.log( errorPl)
						});
				}
			else if($scope.Favorite && $scope.showmap){
				var recordsGet = BusinessCRUDService.favo($window.sessionStorage.iduser);        
				recordsGet.then(function (d) {
					$scope.findbusinessonmap(d.data)
				},
				function (errorPl) {
						console.log( errorPl)
					});
			}
			else if($scope.Favorite)
				{
					var recordsGet = BusinessCRUDService.favo($window.sessionStorage.iduser);        
					recordsGet.then(function (d) {
						$scope.findbusi(d.data)
					},
					function (errorPl) {
							console.log( errorPl)
						});
				} 
			else if ($scope.showmap && (!$scope.Favorite && !$scope.LastSearch)){
				$scope.findbusinessonmap($scope.allbusinessbd);
			}
			else
				{
					$scope.findbusi($scope.allbusinessbd)
				}
			

			}
		
  
});


//controlador User Profile

app.controller("UserProfileController", function ($scope,toast,$filter, $window, $rootScope, $location, LoginCRUDService, VisitsService ) {	
	
	  $scope.Sexs = [{
  		"idsex": 0, "namesex": "Masculino" },
  {"idsex": 1, "namesex": "Femenino"}];	  
	  
	$scope.clear = function() {
        $scope.IsNewRecord = 1;
        $scope.User = {
        		iduser: 0,
        		nameUser: "",
        		mail: "",
                sex: 0,
                date_bith: new Date(),
                phoneUser:"",	              
                name:"",
                surnameUser: "",
                surname2_user:"",
                language:$window.sessionStorage.language,
                receivepromotions: true,
                receiveoffer: true,                
        };
        $scope.Changepass = {
        		oldpassword:"",
        		newpassword: "",
        		repeatpassword:"",
        };
        $scope.showModal = false;
        $scope.verMessageErrorPass = false;
    }
  loadRecords(); 
  function loadRecords() { 
    	$scope.clear();
        var recordsGet = LoginCRUDService.getUser($window.sessionStorage.iduser);        
        recordsGet.then(function (d) {
        	console.log(d.data)
        	var dat = new Date(d.data.date_bith);
        	 $scope.User = {
             		iduser: d.data.iduser,
             		nameUser: d.data.nameUser,
             		mail: d.data.mail,
                     sex: d.data.sex,
                     date_bith: dat,
                     phoneUser:d.data.phoneUser,	               
                     name:d.data.name,
                     surnameUser: d.data.surnameUser,
					 surname2_user:d.data.surname2_user,
					 language:d.data.language,
					 receivepromotions: d.data.receivepromotions,
		                receiveoffer: d.data.receiveoffer,		              
             };  
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });       
    }
  
  $scope.showchangepass = function ()
  {
	  $scope.showModal = true;
	  $scope.verMessageErrorPass = false;
  };
  $scope.tochangepass = function (changepass)
  {	
	 
	 if(changepass.newpassword != changepass.repeatpassword)
		{
		 	$scope.verMessageErrorPass = true;
		} 
	 else
		 {
			 var recordsGet = VisitsService.changepass(changepass, $scope.User.iduser);        
		        recordsGet.then(function (d) {
		        	if(d.data = true)
		        		{
		        			$scope.CerrarModal();
		        			toast({
			               	     duration  : 4000,
			               	     message   : $filter('translate')("PASS_CHANGE"),
			               	     className : "alert-success",
			               	     position: "center",
			               	     dismissible: true,
			               	   });
		        		}
		        	else
		        		{
		        			$scope.verMessageErrorPass = true;
		        		}
		        },
		        function (errorPl) {
		            $log.error('Error ', errorPl);
		        });
		 }
  };
  $scope.CerrarModal = function ()
  {
	  $scope.showModal = false;
	  $scope.verMessageErrorPass = false;
	  $scope.LimpiarModal();
  };
  $scope.LimpiarModal = function ()
  {
	  $scope.verMessageErrorPass = false;
	  $scope.Changepass = {
      		oldpassword:"",
      		newpassword: "",
      		repeatpassword:"",
      };
  };
  
  $scope.save = function(user) {
	  if(user.phoneUser > 10)
		{
			$scope.phonemaxlength = true;
		}
	  else if(user.name > 50)
		  {
		  	$scope.namemaxlength = true;
		  }
	  else if(user.mail > 50)
	  {
	  	$scope.mailmaxlength = true;
	  }
	  else if(user.surnameUser > 50)
	  {
	  	$scope.surnamemaxlength = true;
	  }
	  else if(user.surname2_user > 50)
	  {
	  	$scope.surnametwomaxlength = true;
	  }
      var recordsGet = LoginCRUDService.saveprofile(user);        
        recordsGet.then(function (d) {
        	$scope.clear();
        	$location.path('/home');
        },
        function (errorPl) {
            $log.error('Error ', errorPl);
        });
  }  
  $scope.Out = function() {
	  	$window.history.back(-1);
	  }
});

app.controller("SesionTwo", function ($scope,$location,$filter,$rootScope, toast, $window, RELATIVE_DIR,VisitsService ) {
	$scope.PROMOCIONES = RELATIVE_DIR+"/img/PROMOCIONES1.png";
	$scope.NEGOCIO = RELATIVE_DIR+"/img/NEGOCIO1.png";
	$scope.PUNTOS = RELATIVE_DIR+"/img/PUNTOS1.png"; 
	$scope.toredirect = "";
	
	$scope.navegate = function(toredirect)
	{	
		if(toredirect == 'business' && $window.sessionStorage.nameUser == undefined)
		{
			 $window.sessionStorage.setItem('toredirect', '/business');
			$('#Login').modal('show'); // abrir
		}
		else if(toredirect == 'business' && $window.sessionStorage.nameUser != undefined)
		{
			$location.path('/business');
		}
		else if(toredirect == 'buypoints' && $window.sessionStorage.nameUser == undefined)
			{
			 	$window.sessionStorage.setItem('toredirect', '/buypoints');
			 	$('#Login').modal('show'); // abrir
			}
		else if(toredirect == 'buypoints' && $window.sessionStorage.nameUser != undefined)
					{
						$location.path('/buypoints');
					}
	}
	
	$scope.showpath = function ()
	{
//		$("#ShowRoute").on("shown.bs.modal", function () { 
//			$rootScope.$broadcast('forshowmap', { d: "si" })
//			
//		}).modal('show');
	    	
//		var request = new XMLHttpRequest();
//
//		request.open('GET', 'https://api.openrouteservice.org/v2/directions/foot-walking?api_key=5b3ce3597851110001cf62480fda1d6b3f02408caad37df842f606dd&start=8.681495,49.41461&end=8.687872,49.420318');
//
//		request.setRequestHeader('Accept', 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8');
//
//		request.onreadystatechange = function () {
//		  if (this.readyState === 4) {
//		    console.log('Status:', this.status);
//		    console.log('Headers:', this.getAllResponseHeaders());
////		    console.log('Body:', this.responseText);
//		    $scope.info = JSON.parse(this.responseText);
//		    console.log($scope.info.features[0].geometry.coordinates);
//		  }
//		};
//
//		request.send();
//		
//		$location.path('/rutamapa');
//		var recordsPost = VisitsService.showpath();
//		recordsPost.then(function (d) {
//			console.log(d.data)
//			console.log(d.data)
//			$scope.myimag = d.data;
//			$scope.blob = new Blob([$scope.myimag], {type: "application/png"}); 
//			$scope.dataurl = URL.createObjectURL($scope.blob);
//            },
//            function (errorPl) {
//                console.log(errorPl);
//            });
//		$scope.headline = $filter('translate')("BUSINESS");
		
	}
//	
	
  
});

app.service('VisitsService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {  
    
    this.savevisits = function savevisits(iduser,idbusiness){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/visits/iduser/'+iduser+'/business/'+idbusiness
        });
    }
    this.showpath = function showpath(){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/image/path'
        });
    }
    this.changepass = function changepass(changepass, iduser){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/user/oldpass/'+ changepass.oldpassword +"/newpass/"+changepass.newpassword+"/iduser/"+iduser
        });
    }
}]);

//list user
app.controller("ListUserController", function ($scope,$filter,$window,$location, $rootScope,UserStatesCRUDService, ConfigurationCRUDService, 
		RegisterUserCRUDService,RELATIVE_DIR, RolCRUDService, LoginCRUDService) {
	
	
		// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Users, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
		$scope.groupToPages();
		
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
    	$scope.pagedItems = [];       
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
		}
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
 // paginado fin
    $scope.searchnameuser = function () {	
    	if ( ($scope.search_name_user == undefined  || $scope.search_name_user =="") )
			{
				loadRecords();
			}
    	else
		{
		$scope.filterUsers = $filter('filter')($scope.original, function (item) {
						if(item.nameUser.toUpperCase().search($scope.search_name_user.toUpperCase()) > -1)
						{
							return true;
						}						
						return false;
					});
					$scope.Users = $scope.filterUsers;
					$scope.search();
		}
	}
    $scope.searchname = function () {	
    	if ( ($scope.search_name == undefined  || $scope.search_name =="") )
			{
				loadRecords();
			}
    	else
		{
    	var tofilter;
    	if($scope.original.length != $scope.Users.length)
    		{
    			tofilter = $scope.Users;
    		}    		
    	else
    		{
    			tofilter = $scope.original;
    		}
		$scope.filterUsers = $filter('filter')(tofilter, function (item) {
				if(item.name != null)
					{
						if(item.name.toUpperCase().search($scope.search_name.toUpperCase()) > -1)
						{
							return true;
						}	
					}											
						return false;
					});
					$scope.Users = $scope.filterUsers;
					$scope.search();
		}
	}
    $scope.searchsurName = function () {	
    	if ( ($scope.search_surName == undefined  || $scope.search_surName =="") )
			{
				loadRecords();
			}
    	else
		{
    	var tofilter;
    	if($scope.original.length != $scope.Users.length)
    		{
    			tofilter = $scope.Users;
    		}    		
    	else
    		{
    			tofilter = $scope.original;
    		}
			$scope.filterUsers = $filter('filter')(tofilter, function (item) {
				if(item.surnameUser != null && item.surnameUser.length>0)
				{
						if(item.surnameUser.toUpperCase().search($scope.search_surName.toUpperCase()) > -1)
					{
						return true;
					}
				}							
				return false;
					});
				$scope.Users = $scope.filterUsers;
				$scope.search();
		}
	}
    $scope.searchemail = function () {	
    	if ( ($scope.search_mail == undefined  || $scope.search_mail =="") )
			{
				loadRecords();
			}
    	else
		{
    	var tofilter;
    	if($scope.original.length != $scope.Users.length)
    		{
    			tofilter = $scope.Users;
    		}    		
    	else
    		{
    			tofilter = $scope.original;
    		}
		$scope.filterUsers = $filter('filter')(tofilter, function (item) {
			if(item.mail != null)
				{
					if(item.mail.toUpperCase().search($scope.search_mail.toUpperCase()) > -1)
					{
						return true;
					}
				}							
				return false;
					});
					$scope.Users = $scope.filterUsers;
					$scope.search();
		}
	}
    $scope.searchuserstate = function () {	
    	if ( ($scope.search_userstate == undefined  || $scope.search_userstate =="") )
			{
				loadRecords();
			}
    	else
		{
    	var tofilter;
    	if($scope.original.length != $scope.Users.length)
    		{
    			tofilter = $scope.Users;
    		}    		
    	else
    		{
    			tofilter = $scope.original;
    		}
		$scope.filterUsers = $filter('filter')(tofilter, function (item) {
			if(item.userState.name_user_state != null)
				{
					if(item.userState.name_user_state.toUpperCase().search($scope.search_userstate.toUpperCase()) > -1)
					{
						return true;
					}
				}							
				return false;
					});
					$scope.Users = $scope.filterUsers;
					$scope.search();
		}
	}
    $scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
	
    $scope.showforauthicate= function()
    {
 		$rootScope.$broadcast('showforauthenticate', { d: false });
    }
	$scope.showdetailuser = function(user)
	{	
		console.log(user)
		$scope.User = user;
		$scope.User.sex = parseInt(user.sex);
		$scope.User.newrol = $scope.User.role.idrol;
		$scope.todate = $scope.User.date_bith;
		if($scope.User.userState.iduser_state == 1)
			{
				$scope.User.activateuser = true;
			}
		else
			{
				$scope.User.activateuser = false;
			}
		$scope.showModalUser = true;
	}
	$scope.closedetailuser = function()
	{
		$scope.showModalUser = false;
	}
	
	$scope.clear = function()
	{
		$scope.showModalUser = false;
		
	}
	$scope.modifuser = function(User)
	{
		console.log(User.role.idrol);
		console.log(User.newrol)
		if(User.role.idrol != User.newrol)
			{
			var recordsPost = LoginCRUDService.changeRol(User.iduser, User.newrol);
			recordsPost.then(function (d) {					
				loadRecords();
				$scope.closedetailuser();
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
	            });
			}
		var recordsPost = RegisterUserCRUDService.changeUserState(User.iduser, User.activateuser);
		recordsPost.then(function (d) {	
			
			loadRecords();
			$scope.closedetailuser();
            },
            function (errorPl) {
                $log.error('Error ', errorPl);
            });	
	}
	
	$scope.Sexs = [{
		"idsex": 0, "namesex": "Masculino" },
{"idsex": 1, "namesex": "Femenino"}];
	
	 loadRecords();
	    //cargar todos los prestamos
	    function loadRecords() {
	    	if($window.sessionStorage.nameUser == undefined)
			{			 
				 $('#Login').modal('show');
				 $location.path('/home');
			}
	    	$scope.clear();
	    	var recordsGet = ConfigurationCRUDService.getAllConfiguration()
			recordsGet.then(function (d) {
				if(d.data[0].recordbypage===null)
					$scope.itemsPerPage = 0;
				else
					 $scope.itemsPerPage = d.data[0].recordbypage;
				var recordsGet = RegisterUserCRUDService.getAllRegisterUser();
		        recordsGet.then(function (d) {	
						$scope.Users = d.data;
						$scope.original = $scope.Users;
						$scope.search();
						console.log($scope.Users)
		            },
		            function (errorPl) {
		                $log.error('Error ', errorPl);
		            });	 
			});	    	
			
	        var recordsGet = UserStatesCRUDService.getAllUserStates();
	        recordsGet.then(function (d) {
					$scope.UserStatess = d.data;
	            },
	            function (errorPl) {
	                $log.error('Error ', errorPl);
				});
	        
	        var recordsGet = RolCRUDService.getAllRol();
			recordsGet.then(function (d) {
				$scope.Roles=d.data;				
				},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});
			};
	     
});
app.controller("ForgotPassController", function ($scope,$filter, $location, RegisterUserCRUDService, toast) {
	
	$scope.clear = function()
	{
		$scope.mailuserempty = false;
		$scope.ForgotPass = {
				mail:"",
		};
		$scope.ModDir = {
				dir:"",
				idmoddir:0,
		};
		$scope.showmessage = false;
		 $scope.User;
	}	
	
	$scope.sendmailforgot = function(ForgotPass)
	{
		if(ForgotPass.mail.length > 0)
			{
			var loc = window.location;
		    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		    var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
		    $scope.ModDir = {
					dir:dir+'#!/changepassword/',
					idmoddir:0,
			};
		   
			  var recordsGet = RegisterUserCRUDService.sendmailforgotpass(ForgotPass.mail);
				recordsGet.then(function (d) {
					console.log(d.data)	
					$scope.User = d.data;
					if($scope.User.iduser == null)
						{
							$scope.showmessage = true;
						}
					else
						{
						$scope.sendmailwithtoken($scope.User.iduser,$scope.ModDir);						
							toast({
			               	     duration  : 4000,
			               	     message   :$filter('translate')("PASS_CHANGE_CHECK"),
			               	     className : "alert-success",
			               	     position: "center",
			               	     dismissible: true,
			               	   });
							$location.path("/home");  
						}
					
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
			}
		else
			{
			$scope.mailuserempty = true;
			}
	}
	
	$scope.sendmailwithtoken = function (iduser,ModDir)
	{
		var recordsGet = RegisterUserCRUDService.sendmailwithtoken(iduser,ModDir);
		recordsGet.then(function (d) {
			console.log("send mail")
		},
		function (errorPl) {
			$log.error('Error ', errorPl);
		})
	}
		 
	$scope.goback = function()
	{
		$location.path("/home");
	}
});

app.controller("ChangePassController", function ($scope,$location, RegisterUserCRUDService, toast) {
		var pathname = window.location.href;
		var urlParts = pathname.split('/');
		var token =urlParts[urlParts.length - 1];	
		$scope.User;
		$scope.showmessage = false;
		
	    loaduser(token);
	    function loaduser (token) {
	    	$scope.passminlength = false;
	         var promiseGet = RegisterUserCRUDService.getuserbytoken(token);
	         	promiseGet.then(function (d) {	         		
	         		$scope.User = d.data;
	         		console.log($scope.User) ;
	         		if(d.data == undefined || d.data == null || d.data == '')
	         			{
	         				$scope.showmessage = true;
	         			}
	            }, function (err) {
	                console.log(err);
	            });
	          };
	          
          $scope.goback = function()
      	{
      		$location.path("/home");
      	}
	      $scope.newpass = function ()
	      {
	    	  if($scope.newpassword.length >6)
	    		  {
	    		  var promiseGet = RegisterUserCRUDService.newpass($scope.newpassword,$scope.User);
		         	promiseGet.then(function (d) {	         		
		         		if(d.data!= undefined)
		         			{
		         				$location.path("/home");
		         				$('#Login').modal('show');
		         			}
		            }, function (err) {
		                console.log(err);
		            });
		         	
	    		  }
	    	  else
	    		  {
	    		  	$scope.passminlength = true;
	    		  }
	    		  
	    		  
	    	  
	      }
	      $scope.goforgotpass= function()
	        {   
	        	$location.path("/forgotpass"); 
	        }
});

app.controller("InviteController", function ($scope,$filter, $location,toast, $window, RELATIVE_DIR,InviteService ) {
	 var loc = window.location;
	var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    var dir = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	
	$scope.LimpiarModal = function () {
        $scope.clear();      
    };  
	
	$scope.clear = function ()
	{
		$scope.mailuserempty = false;
		
			$scope.Invite = {
					iduser:$window.sessionStorage.iduser,
					mail: "",
					desc_invite: "",
		            urltoinvite:dir+"#!/home",
		        };	
	};
	load();
	function load () {
		 $scope.clear();
	 };
	  
	 $scope.CerrarModal = function () {
	        $scope.showModal = false;
	        $scope.clear();
		};		
		
	$scope.save = function(Invite)
	{	$scope.Invite.iduser = $window.sessionStorage.iduser;
		if ($scope.Invite.iduser != undefined)
			{
			if($scope.Invite.mail.length>0)
				{
					console.log("entre")
					$scope.CerrarModal();
	         		toast({
	               	     duration  : 4000,
	               	     message   : $filter('translate')("INVITED_SEND"),
	               	     className : "alert-success",
	               	     position: "center",
	               	     dismissible: true,
	               	   });
					var promiseGet = InviteService.sendinvitation(Invite);
		         	promiseGet.then(function (d) {	
		         		console.log("todo bien.....")
		         		
		            }, function (err) {
		                console.log(err);
		            });
				}
			else
				{
				$scope.mailuserempty = true;
				}
			}		
		
	};
});
app.controller("VisitedQRController", function ($scope,$location, $window, RELATIVE_DIR, VisitedQRService) {
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idbusiness =urlParts[urlParts.length - 2];
	
	load();
	function load () {
		$location.path("mybusiness/"+idbusiness);  
		var promiseGet = VisitedQRService.visitedqr(idbusiness);
     	promiseGet.then(function (d) {	
     		console.log("qrfooooooooooooooooooooo")
        }, function (err) {
            console.log(err);
        });
	 };
});

app.service('VisitedQRService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {  
    
    this.visitedqr = function visitedqr(idbusiness){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/visitqr/business/'+idbusiness+'/qr'
        });
    };
}]);
app.service('InviteService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {  
    
    this.sendinvitation = function sendinvitation(Invite){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/user/invite',
          data:Invite
        });
    };
    this.sumpointuser = function sumpointuser(usercode){
        return $http({
          method: 'POST',
          url: RELATIVE_DIR+'/api/user/invited/'+usercode
        });
    };
}]);

app.controller("InvitedController", function ($scope,$location, $window, RELATIVE_DIR, InviteService) {
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var usercode =urlParts[urlParts.length - 1];
	
	load();
	function load () {
		$location.path("/home");  
		var promiseGet = InviteService.sumpointuser(usercode);
     	promiseGet.then(function (d) {	
     		console.log("se le sumo")
        }, function (err) {
            console.log(err);
        });
	 };
});

app.controller("RouteController", function ($scope,$location,toast,$timeout,$filter, $window, RELATIVE_DIR, InviteService) {
	
	$scope.$on('forshowmap', function(event, option) {	
		 loadRecord();			 
	});
	function loadRecord () {
		$scope.Business = JSON.parse($window.sessionStorage.BusinessRoute);
		$scope.shownamebusi = false;
		$timeout(function () {
			$scope.shownamebusi = true;
			}, 300);
		
		if($scope.Business.latitude != null)
			{
			var container = L.DomUtil.get('mapid');
		      if(container != null){
		        container._leaflet_id = null;
		      }
		     
			      $scope.map = L.map('mapid').
		 		 setView([$scope.Business.latitude, $scope.Business.longitude], 
		 		 14);
		 	      $scope.map.scrollWheelZoom.disable();
		
			 var layer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			 		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			 	}).addTo($scope.map); 
			 
				layer.on('loading', function (event) {
			 		  console.log('start loading tiles');
			 		 var element = document.getElementById("idspinner-loadingmap");
	       	  			element.style.display = "block";	       	  	
			 		});
			 	layer.on("load",function() {
			 		console.log("all visible tiles have been loaded") 			 		
			 		var element = document.getElementById("idspinner-loadingmap");
	            	 element.style.display = "none";	            	
			 		});
			 	layer.on("tileunload ",function() {
			 		console.log("tiles unloadddd")			 		
			 		var element = document.getElementById("idspinner-loadingmap");
	            	 element.style.display = "none";	            	 
			 		});
			 
			 	$scope.destiny = L.marker([$scope.Business.latitude, $scope.Business.longitude], {icon: L.icon.glyph({ prefix: '', cssClass:'sans-serif', glyph: 'B' }) }).addTo($scope.map)
			 						.bindPopup("<b>"+$scope.Business.name_business+"</b>").openPopup();
			 	
			 	
			 	$scope.origin;			 	
			 	$scope.nomoreclik = false;
				function onMapClick(e) {
			 		if(!$scope.nomoreclik)
			 			{
			 			if($scope.origin != undefined )
			 			{
			 				$scope.map.removeLayer($scope.origin); // remove 
			 			}		 		
			 				$scope.origin =	L.marker([e.latlng.lat, e.latlng.lng], {icon: L.icon.glyph({ prefix: '', cssClass:'sans-serif', glyph: 'A' }) }).addTo($scope.map)
			 			}
			 		console.log($scope.origin)
				}
				console.log($scope.origin)
			 	$scope.map.on('click', onMapClick);
			 	
			}		
		}	
 
	$scope.calroute = function()
	{
		console.log($scope.origin)
		if(angular.isDefined($scope.origin))
			{
				$scope.nomoreclik = true;
					$scope.map.removeLayer($scope.destiny);
					$scope.map.removeLayer($scope.origin);

					$scope.control = L.Routing.control({
					 	waypoints: [					 		
					 		L.latLng($scope.origin._latlng.lat, $scope.origin._latlng.lng),
					 		L.latLng($scope.Business.latitude, $scope.Business.longitude)
					 	],
					 	 language: 'es', 
					 	 routeWhileDragging: true,
					     reverseWaypoints: false,
					     showAlternatives: false,
					     altLineOptions: {
					         styles: [
					             {color: 'black', opacity: 0.15, weight: 9},
					             {color: 'white', opacity: 0.8, weight: 6},
					             {color: 'blue', opacity: 0.5, weight: 2}
				         ]
				     },
				     createMarker: function(i, wp) {
							return L.marker(wp.latLng, {
								draggable: true,
								icon: L.icon.glyph({ glyph: String.fromCharCode(65 + i) })
							});
						}
				 })
				 .on('routingstart' ,function() {			 		
				 		var element = document.getElementById("idspinner-loadingmap");
		            	 element.style.display = "block";	            	
				 		})
				 .on('routesfound routingerror',function() {			 		
				 		var element = document.getElementById("idspinner-loadingmap");
		            	 element.style.display = "none";
		            	 $scope.nomoreclik = true;
		            	 if ($('.leaflet-routing-container').hasClass('.leaflet-routing-container-hide')){
			           		 $(this).removeClass('maxheigforroute'); 
			               }else{        
			               }
//			                $('.leaflet-routing-alt').hide();
				 		})
				 .addTo($scope.map);
					
					L.Routing.errorControl($scope.control, {
			            header: 'Routing error',
			            formatMessage(error) {
			                if (error.status < 0) {
			                	toast({
			                 	     duration  : 5000,
			                 	     message   : $filter('translate')("ERROR_CALCULATE_ROUTE"),
			                 	     className : "alert-danger",
			                 	     position: "center",
			                 	     dismissible: true,
			                 	   });
			                } else {
			                	toast({
			                 	     duration  : 5000,
			                 	     message   : $filter('translate')("ERROR_CALCULATE_ROUTE"),
			                 	     className : "alert-danger",
			                 	     position: "center",
			                 	     dismissible: true,
			                 	   });
			                }
			                $('.leaflet-routing-error').hide();
			                
			            }
			        }).addTo($scope.map);
				/* L.Routing.errorControl($scope.control).addTo($scope.map);
				 $('.leaflet-routing-container').addClass('leaflet-routing-container-hide');*/
			}
		else
			{
			toast({
          	     duration  : 5000,
          	     message   : $filter('translate')("SELECT_ORIGIN"),
          	     className : "alert-danger",
          	     position: "center",
          	     dismissible: true,
          	   });
			}
		
	}
	
	$scope.CerrarModalRoute = function ()
	{		
		if ($scope.control != null) {
			$scope.map.removeControl($scope.control);
			$scope.control = null;
        }
		$window.sessionStorage.setItem('BusinessRoute', "");
		$("#ShowRoute").on("shown.bs.modal", function () { 

		}).modal('hide');
	}
	
});


