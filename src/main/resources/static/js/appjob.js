//inicio Promotion
angular.module('app').controller("JobController", function ($scope, $filter,toast, $location, $window, JobCRUDService, ConfigurationCRUDService, BusinessCRUDService,SchoollevelCRUDService,ContractTypeCRUDService,ProfessionalExperienceCRUDService, ScheduleJobOfferCRUDService ) {
	
    $scope.today = new Date();

    //modal adicionar
    $scope.AdicionarModal = function () {
    	$scope.clearfield();   
    		$scope.showModalJob = true;	};
    $scope.CerrarModal = function () {
        $scope.showModalJob = false;
	};
	
    $scope.clearfield = function () {   
    	
    	if($scope.Rad.toradio == 2)
    		{
	    		var recordsGet = BusinessCRUDService.getfuncionalitiesbybusiness($scope.Business.idbusiness)
				recordsGet.then(function (d) {
					$scope.funcionalityselected = d.data;	
						$scope.daysfuncionality =  $filter('filter')($scope.funcionalityselected, function (item) 
							{
								if(item.idfuncionality.nameFuncionality == "Empleo")
								{
									return true;
								}													
								return false;
								});
					$scope.dayshow = new Date ($scope.daysfuncionality[0].date_end);
					$scope.dayblock = new Date ($scope.daysfuncionality[0].date_end);			
					$scope.dayblock.setDate($scope.dayblock.getDate()+1);
					
					$scope.Job = {
		    				titlejob:"",
		    				descjob:"",
		    				experiencejob:"",
		    				dateend: $scope.dayshow,
		    				phonejob:"",
		    				salaryjob: 0,
		    				business: $scope.Business,
		    				schedulejoboffer: null,
		    				sex: null,
		    				maxage:0,
		    				minage:0,
		    				schoollevel:null,
		    				contracttype:null,
		    				otherrequirement: "",
		    				professionalexperience: null
		    		};
				},
					function (errorPl) {
						console.log( errorPl)
					});
	    		
    		}  
    	
    	
	};
	$scope.savejob = function (Job){
		console.log(Job)			
			var promisePost = BusinessCRUDService.savejob(Job);
			promisePost.then(function (d) {  
				$scope.CerrarModal();
				$scope.callto($scope.Rad.toradio)
				toast({
		    	     duration  : 5000,
		    	     message   : $filter('translate')("JOB_OFFER_INSERT"),
		    	     className : "alert-success",
		    	     position: "center",
		    	     dismissible: true,
		    	   });
				
	          }, function (err) {
	              console.log(err);
	          });
		}
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Jobs, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin
    
//    busqueda
    $scope.searchtitle = function () {    	
    	if ( ($scope.search_jobtitle == undefined  || $scope.search_jobtitle =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.titlejob != null)
						{
							if(item.titlejob.toUpperCase().search($scope.search_jobtitle.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.searchcontract = function () {    	
    	if ( ($scope.search_contract == undefined  || $scope.search_contract =="") )
			{
    			$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.contracttype != null)
						{
							if(item.contracttype.desc_contracttype.toUpperCase().search($scope.search_contract.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.searchschedulejob = function () {    	
    	if ( ($scope.search_schedulejob == undefined  || $scope.search_schedulejob =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.contracttype != null)
						{
							if(item.schedulejoboffer.desc_schedu.toUpperCase().toUpperCase().search($scope.search_schedulejob.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.searchsalary = function () {   
    	if ( ($scope.search_salarymodel == undefined  || $scope.search_salarymodel =="") )
			{
    		$scope.callto($scope.Rad.toradio)
			}
    	else
		{	    	
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.salaryjob != null)
						{
							if(item.salaryjob == $scope.search_salarymodel)
							{
								return true;
							}	
						}												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
 
	$scope.callto = function (radiodata) {
		if(radiodata == 1)
			{
			$scope.changetoradiouser();
			}
		if(radiodata == 2)
			{
			$scope.changetoradiobusiness();
			}
	    };
	    $scope.showcheck = function () {
	    	$scope.showbotomadd= true;	    		
	    }
	 $scope.changetoradiobusiness = function ()
	    {		 
				var recordsGet = JobCRUDService.getAllJobsbyBusiness($scope.Business.idbusiness);
				recordsGet.then(function (d) {
						$scope.Jobs = d.data;
						$scope.original = $scope.Jobs;
						$scope.search();
						console.log($scope.Jobs)
						var recordsGet = BusinessCRUDService.getFuncionalitybyBusiness($scope.Business.idbusiness);
						recordsGet.then(function (b) {
							var funcio = $scope.filterUsers = $filter('filter')(b.data, function (item) {
										if(item.idfuncionality == 9)
										{
											return true;
										}						
										return false;
									});
							if(funcio.length > 0)
								{
									$scope.showbotomadd= false;
								}
							else
								{
									$scope.showbotomadd= true;
								}
						},
								function (errorPl) {
							$log.error('Error ', errorPl);
						});
						
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});
	    }
	    $scope.changetoradiouser = function ()
	    {
	    	if ($scope.user.role.desc_rol==="ROLE_ADMIN")
			{
				var recordsGet = JobCRUDService.getAllJobbyUserAdmin();
				recordsGet.then(function (d) {
						$scope.Jobs = d.data;
						$scope.original = $scope.Jobs;
						$scope.search();
						console.log($scope.Jobs)
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});	
			}
		else
			{
				var recordsGet = JobCRUDService.getAllJobsbyUser($window.sessionStorage.iduser);
				recordsGet.then(function (d) {
						$scope.Jobs = d.data;
						$scope.original = $scope.Jobs;
						$scope.search();
						console.log($scope.Jobs)
					},
					function (errorPl) {
						$log.error('Error ', errorPl);
					});	 
			}	
	    }
	    $scope.showbuttondelete = function (job) {
	    	
	    	if(job.business.user.iduser == $window.sessionStorage.iduser)
	    		{
	    			return true;
	    		}
	    	else
	    		{
	    			return false;
	    		}
	    		
	    }
		$scope.DeleteJob = function (idjob) {
			swal({
	    		  title: "",
	    		  text: $filter('translate')("WANT_DELETE"),
	    		  type: "error",
	    		  showCancelButton: true,
	    		  confirmButtonClass: "btn-a-ecrece btn-withoutshadow",
	    		  cancelButtonClass: "btn-a-ecrece btn-withoutshadow",
	    		  confirmButtonText: $filter('translate')("DELETE"),
	    		  cancelButtonText: $filter('translate')("CANCEL"),
	    		  closeOnConfirm: true,
	    		  closeOnCancel: true
	    		},
	    		function(isConfirm){
	    			if (isConfirm) {
	    			    var promiseDelete = JobCRUDService.deleteJob(idjob);
	    	            promiseDelete.then(function (d) {  
	    	            	loadRecords();           	
	    	                $scope.Message = "SUCCESSFULY_DELETE";
	    	                $scope.verMessage = true;
	    	                (function next() {                   
	    	                        setTimeout(function () {
	    	                        	$scope.verMessage = false;
	    	                            $scope.$digest();
	    	                        }, 3000);
	    	                               
	    	                })();                
	    	            }, function (err) {
	    	                $scope.errorMessage = "CATEGORY_ERR_DELETE";
	    	                console.log(err);
	    	                $scope.verMessageError = true;
	    	            });
	    			  }
	    		  
	    		});
	    }
	
	//limpiar registro
    $scope.clear = function () {
    	$scope.isadmin = false;
		$scope.IsNewRecord = 1;
		$scope.lesszero = false;
		$scope.showbotomadd= true;
		
		$scope.Business = {
				idbusiness: 0,
		}
		
		$scope.user = JSON.parse($window.sessionStorage.userData);
		$scope.Rad = {
				toradio:1,
		}
		
		$scope.Job = {
				titlejob:"",
				descjob:"",
				experiencejob:"",
				dateend: $scope.today,
				phonejob:"",
				salaryjob: 0,
				business: "",
				schedulejoboffer: null,
				sex: null,
				maxage:0,
				minage:0,
				schoollevel:null,
				contracttype:null,
				otherrequirement: "",
				professionalexperience: null
		};
        $scope.verMessage = false;
        $scope.verMessageError = false;
        $scope.errorMessage = "";
    }
    $scope.IsNewRecord = 1;
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	if($window.sessionStorage.nameUser == undefined)
		{
			 $('#Login').modal('show');
			 $location.path('/home');
		}	
    	$scope.clear();
    	 $scope.Sexs = [{
    	  		"idsex": 0, "namesex": "Masculino" },
    	  {"idsex": 1, "namesex": "Femenino"}];	  
    	var recordsGet = ScheduleJobOfferCRUDService.getAllScheduleJobOffer()
		recordsGet.then(function (b) {
			$scope.ScheduleJobOffer = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	
    	var recordsGet = SchoollevelCRUDService.getAllSchoollevel()
		recordsGet.then(function (b) {
			$scope.Schoollevel = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	var recordsGet = ContractTypeCRUDService.getAllContractType()
		recordsGet.then(function (b) {
			$scope.ContractType = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	var recordsGet = ProfessionalExperienceCRUDService.getAllProfessionalExperience()
		recordsGet.then(function (b) {
			$scope.ProfessionalExperience = b.data;
		},
		function (errorPl) {
			console.log( errorPl);
		});
    	
    	var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
			if ($scope.user.role.desc_rol==="ROLE_ADMIN")
			{
				
				var recordsGet = BusinessCRUDService.getallBusiness()
				recordsGet.then(function (d) {
					$scope.BusinessList = d.data;
					$scope.Business = d.data[0];
					$scope.callto($scope.Rad.toradio);
				},
					function (errorPl) {
						console.log( errorPl)
					});
			}
			else
			{	
				var recordsGet = BusinessCRUDService.getallBusinessbyUser($window.sessionStorage.iduser)
				recordsGet.then(function (d) {
					$scope.BusinessList = d.data;
					$scope.Business = d.data[0];
					$scope.callto($scope.Rad.toradio);
				},
					function (errorPl) {
						console.log( errorPl)
					});
			}
			   	
		},
		function (errorPl) {
			$log.error('Error ', errorPl);
		});
		
    }
    
    $scope.goDetail = function (idjob) {
		$location.path("jobdetail/"+idjob); 
	}
    $scope.back = function () {
    	$window.history.back(-1);	
	}
});

angular.module('app').service('JobCRUDService',['$http','RELATIVE_DIR', function ($http,RELATIVE_DIR) {	
  
    
    this.getAllJobsbyBusiness = function getAllJobsbyBusiness(idbusiness){
    	return $http({
            method: 'GET',
            url: RELATIVE_DIR+'/api/job/business/'+idbusiness
          });
    }
    this.getAllJobsbyUser = function getAllJobsbyUser(iduser){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/job/user/'+iduser
        });
    }
    this.getAllJobbyUserAdmin = function getAllJobbyUserAdmin(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/job'
        });
    }
    this.getJob = function getJob(idjob){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/job/'+idjob
        });
    }
    this.getAllJobsOffer = function getAllJobsOffer(){
        return $http({
          method: 'GET',
          url: RELATIVE_DIR+'/api/job/offer'
        });
    }
    this.deleteJob = function deleteJob(idjob){
        return $http({
          method: 'DELETE',
          url: RELATIVE_DIR+'/api/job/'+idjob
        });
    }
   

}]);
//fin Promotion
angular.module('app').controller("JobDetailController", function ($scope,$window,RELATIVE_DIR,$location,JobCRUDService) {
		
	var pathname = $window.location.href;	
	var urlParts = pathname.split('/');
	var idjob =urlParts[urlParts.length - 1];
	
	loadRecords();
	function loadRecords(){
	      var recordsGet = JobCRUDService.getJob(idjob);        
	        recordsGet.then(function (d) {
	        	$scope.JobDetail = d.data;
	        	console.log($scope.JobDetail)
	        },
	        function (errorPl) {
	            $log.error('Error ', errorPl);
	        });
	  };	
	
	$scope.out = function () {
		$location.path('/home');
	}
	$scope.back = function () {
		$window.history.back(-1);	
	}
	$scope.gobusiness = function (idbusiness) {
		$location.path('/mybusiness/'+idbusiness);
	}
	
});
angular.module('app').controller("JobOffersController", function ($scope, $filter,toast, $location, $window, JobCRUDService, ConfigurationCRUDService, BusinessCRUDService,SchoollevelCRUDService,ContractTypeCRUDService,ProfessionalExperienceCRUDService, ScheduleJobOfferCRUDService) {
	
	// paginado inicio
	var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
	};
	
	// init the filtered items
    $scope.search = function () {
        $scope.filteredItems = $filter('filter')($scope.Jobs, function (item) {
            for(var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        $scope.currentPage = 0;
        $scope.page=$scope.currentPage;
        // now group by pages
        $scope.groupToPages();
	};
	
	// calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];
        
        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
	};
	
	$scope.range = function (size,start, end) {
        var ret = [];        
                      
        if (size < end) {
            end = size;
            start = size-size;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }        
                 
        return ret;
    };
    
    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
			$scope.currentPage++;
			$scope.page=$scope.currentPage;
        }
	};
	
	 $scope.lastPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {			
			$scope.currentPage=$scope.pagedItems.length - 1;
			$scope.page=$scope.currentPage;
        }
	};
	 $scope.firstPage = function () {
         if ($scope.currentPage > 0) {			
			$scope.currentPage=0;
			$scope.page=$scope.currentPage;
        }
    };
    
    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };
	// paginado fin

//  busqueda
    $scope.searchtitle = function () {    	
    	if ( ($scope.search_jobtitle == undefined  || $scope.search_jobtitle =="") )
			{
	    		$scope.Jobs = $scope.original;
				$scope.search();
			}
    	else
		{
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.titlejob != null)
						{
							if(item.titlejob.toUpperCase().search($scope.search_jobtitle.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.searchcontract = function () {    	
    	if ( ($scope.search_contract == undefined  || $scope.search_contract =="") )
			{
	    		$scope.Jobs = $scope.original;
				$scope.search();
			}
    	else
		{
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.contracttype != null)
						{
							if(item.contracttype.desc_contracttype.toUpperCase().search($scope.search_contract.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.searchschedulejob = function () {    	
    	if ( ($scope.search_schedulejob == undefined  || $scope.search_schedulejob =="") )
			{
	    		$scope.Jobs = $scope.original;
				$scope.search();
			}
    	else
		{
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.contracttype != null)
						{
							if(item.schedulejoboffer.desc_schedu.toUpperCase().toUpperCase().search($scope.search_schedulejob.toUpperCase()) > -1)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.searchsalary = function () {   
    	if ( ($scope.search_salarymodel == undefined  || $scope.search_salarymodel =="") )
			{
	    		$scope.Jobs = $scope.original;
				$scope.search();
			}
    	else
		{	    	
			$scope.filterJobs = $filter('filter')($scope.original, function (item) 
					{
					if(item.salaryjob != null)
						{
							if(item.salaryjob == $scope.search_salarymodel)
							{
								return true;
							}	
						}
												
						return false;
						});
			$scope.Jobs = $scope.filterJobs;
			$scope.search();
		}
	}
    $scope.gap = 1;    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 0;
    $scope.pagedItems = [];
	$scope.currentPage = 0;
 
    loadRecords();
    //cargar todos los prestamos
    function loadRecords() {
    	
    	var recordsGet = ConfigurationCRUDService.getAllConfiguration()
		recordsGet.then(function (d) {
			$scope.itemsPerPage = d.data[0].recordbypage;
			var recordsGet = JobCRUDService.getAllJobsOffer();
			recordsGet.then(function (d) {
					$scope.Jobs = d.data;
					$scope.original = $scope.Jobs;
					$scope.search();
					console.log($scope.Jobs)
				},
				function (errorPl) {
					$log.error('Error ', errorPl);
				});	 
			   	
		},
		function (errorPl) {
			$log.error('Error ', errorPl);
		});
		
    }
    
    $scope.goDetail = function (idjob) {
		$location.path("jobdetail/"+idjob); 
	}
    $scope.back = function () {
    	$window.history.back(-1);	
	}
	
});

