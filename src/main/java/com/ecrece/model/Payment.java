package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the payments database table.
 * 
 */
@Entity
@Table(name="payments")
public class Payment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idpayment;

	//bi-directional many-to-one association to PaymentForm
	@ManyToOne
	@JoinColumn(name="idform_payment")
	private PaymentForms paymentForm;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="iduser")
	private User user;
	
	private Date payment_date;
	
	private String state;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIdpayment() {
		return this.idpayment;
	}

	public void setIdpayment(Integer idpayment) {
		this.idpayment = idpayment;
	}

	public PaymentForms getPaymentForm() {
		return this.paymentForm;
	}

	public void setPaymentForm(PaymentForms paymentForm) {
		this.paymentForm = paymentForm;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	

}