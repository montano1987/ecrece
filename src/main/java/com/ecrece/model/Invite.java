package com.ecrece.model;

public class Invite {
	
    private String mail;	

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	private String desc_invite;
    
    private String urltoinvite;
    
    private Integer iduser;
    
    public Integer getIduser() {
		return iduser;
	}

	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}


	public String getDesc_invite() {
		return desc_invite;
	}

	public void setDesc_invite(String desc_invite) {
		this.desc_invite = desc_invite;
	}

	public String getUrltoinvite() {
		return urltoinvite;
	}

	public void setUrltoinvite(String urltoinvite) {
		this.urltoinvite = urltoinvite;
	}
    
}
