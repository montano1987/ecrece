package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the offers database table.
 * 
 */
@Entity
@Table(name="offers")
public class Offer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idoffer;

	private Boolean activeOffer;
	
	private String descOffer;
	
	private String nameOffer;
	
	private Date date_start;
	
	private Date date_end;
	
	private String urldir;
	
	@ManyToOne
	@JoinColumn(name = "idbusiness")	
	private Business business;
	
	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	//bi-directional many-to-one association to Request
	@OneToMany(mappedBy="offer")
	private List<Request> requests;
	
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Date getDate_end() {
		return date_end;
	}

	public void setDate_end(Date date_end) {
		this.date_end = date_end;
	}
	
	public Integer getIdoffer() {
		return this.idoffer;
	}

	public void setIdoffer(Integer idoffer) {
		this.idoffer = idoffer;
	}

	public Boolean getActiveOffer() {
		return this.activeOffer;
	}

	public void setActiveOffer(Boolean activeOffer) {
		this.activeOffer = activeOffer;
	}

		public List<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}
		
	public Date getDate_start() {
		return date_start;
	}

	public void setDate_start(Date date_start) {
		this.date_start = date_start;
	}
	
	

	public String getDescOffer() {
		return descOffer;
	}

	public void setDescOffer(String descOffer) {
		this.descOffer = descOffer;
	}

	public String getNameOffer() {
		return nameOffer;
	}

	public void setNameOffer(String nameOffer) {
		this.nameOffer = nameOffer;
	}

	public Request addRequest(Request request) {
		getRequests().add(request);
		request.setOffer(this);
		return request;
	}

	public Request removeRequest(Request request) {
		getRequests().remove(request);
		request.setOffer(null);
		return request;
	}
	public String getUrldir() {
		return urldir;
	}

	public void setUrldir(String urldir) {
		this.urldir = urldir;
	}

}