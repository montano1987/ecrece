package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the offers database table.
 * 
 */
@Entity
@Table(name="reservationcharacteristics")
public class ReservationCharacteristics implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idreservationcharacteristics;

	private Boolean date;
	
	private Boolean dateend;
	
	private Boolean night;
	
	private Boolean personsamount;
	
	private Boolean description;
	
	private Boolean schedule;
	
	private Boolean domicile;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Boolean getDomicile() {
		return domicile;
	}

	public void setDomicile(Boolean domicile) {
		this.domicile = domicile;
	}

	public Boolean getSchedule() {
		return schedule;
	}

	public void setSchedule(Boolean schedule) {
		this.schedule = schedule;
	}

	public Boolean getScheduleend() {
		return scheduleend;
	}

	public void setScheduleend(Boolean scheduleend) {
		this.scheduleend = scheduleend;
	}

	public Boolean getArticle() {
		return article;
	}

	public void setArticle(Boolean article) {
		this.article = article;
	}

	private Boolean scheduleend;
	
	private Boolean article;
	
	@ManyToOne
	@JoinColumn(name = "idbusiness")	
	private Business business;	

	public Integer getIdreservationcharacteristics() {
		return idreservationcharacteristics;
	}

	public void setIdreservationcharacteristics(Integer idreservationcharacteristics) {
		this.idreservationcharacteristics = idreservationcharacteristics;
	}

	public Boolean getDate() {
		return date;
	}

	public void setDate(Boolean date) {
		this.date = date;
	}

	public Boolean getDateend() {
		return dateend;
	}

	public void setDateend(Boolean dateend) {
		this.dateend = dateend;
	}

	public Boolean getNight() {
		return night;
	}

	public void setNight(Boolean night) {
		this.night = night;
	}

	public Boolean getPersonsamount() {
		return personsamount;
	}

	public void setPersonsamount(Boolean personsamount) {
		this.personsamount = personsamount;
	}

	public Boolean getDescription() {
		return description;
	}

	public void setDescription(Boolean description) {
		this.description = description;
	}

	
	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}
}