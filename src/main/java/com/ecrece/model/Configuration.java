package com.ecrece.model;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "configurations")
public class Configuration {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_configuration;	

    private String latitude;
    
	private String longitude;
    
	private Integer ptos_star;
	
	private Integer daysfuncionality;
	
	private Integer recordbypage;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getRecordbypage() {
		return recordbypage;
	}
	public void setRecordbypage(Integer recordbypage) {
		this.recordbypage = recordbypage;
	}
	public Integer getPtos_star() {
		return ptos_star;
	}
	public void setPtos_star(Integer ptos_star) {
		this.ptos_star = ptos_star;
	}
	public Integer getId_configuration() {
		return id_configuration;
	}
	public void setId_configuration(Integer id_configuration) {
		this.id_configuration = id_configuration;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public Integer getDaysfuncionality() {
		return daysfuncionality;
	}
	public void setDaysfuncionality(Integer daysfuncionality) {
		this.daysfuncionality = daysfuncionality;
	}
	
	

	
}
