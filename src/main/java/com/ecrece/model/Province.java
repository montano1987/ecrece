package com.ecrece.model;



import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "provinces")
public class Province  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idprovince;	
	
	
    private String name_province;


	@ManyToOne
	@JoinColumn(name = "idcountry")	
	private Country country;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIdprovince() {
		return idprovince;
	}


	public void setIdprovince(Integer idprovince) {
		this.idprovince = idprovince;
	}


	public String getName_province() {
		return name_province;
	}


	public void setName_province(String name_province) {
		this.name_province = name_province;
	}


	public Country getCountry() {
		return country;
	}


	public void setCountry(Country country) {
		this.country = country;
	}
	
	
	

	
}
