package com.ecrece.model;

import javax.persistence.*;

public class ModReport {	
	
	@Id
    private Integer idmodreport;	
	
	public Integer getIdmodreport() {
		return idmodreport;
	}

	public void setIdmodreport(Integer idmodreport) {
		this.idmodreport = idmodreport;
	}

	public String getDesc_report() {
		return desc_report;
	}

	public void setDesc_report(String desc_report) {
		this.desc_report = desc_report;
	}

	private String desc_report;

	
}