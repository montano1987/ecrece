package com.ecrece.model;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "business_states")
public class BusinessStates  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idstate_business;	

    private String name_state_business;
    
    private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIdstate_business() {
		return idstate_business;
	}

	public void setIdstate_business(Integer idstate_business) {
		this.idstate_business = idstate_business;
	}

	public String getName_state_business() {
		return name_state_business;
	}

	public void setName_state_business(String name_state_business) {
		this.name_state_business = name_state_business;
	}

	


	
	



	
	
}
