package com.ecrece.model;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notifications_types")
public class NotificationType  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idnotifications_type;	

    private String desc_notification_type;
    
    private String name_notification_type;
    
    private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	public String getName_notification_type() {
		return name_notification_type;
	}

	public void setName_notification_type(String name_notification_type) {
		this.name_notification_type = name_notification_type;
	}

	public Integer getIdnotifications_type() {
		return idnotifications_type;
	}

	public void setIdnotifications_type(Integer idnotifications_type) {
		this.idnotifications_type = idnotifications_type;
	}

	public String getDesc_notification_type() {
		return desc_notification_type;
	}

	public void setDesc_notification_type(String desc_notification_type) {
		this.desc_notification_type = desc_notification_type;
	}

}
