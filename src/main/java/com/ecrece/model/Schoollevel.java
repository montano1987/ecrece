package com.ecrece.model;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the coments database table.
 * 
 */
@Entity
@Table(name="schoollevel")
public class Schoollevel implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idschoollevel;
	
	public Integer getIdschoollevel() {
		return idschoollevel;
	}

	public void setIdschoollevel(Integer idschoollevel) {
		this.idschoollevel = idschoollevel;
	}

	public String getDesc_schoollevel() {
		return desc_schoollevel;
	}

	public void setDesc_schoollevel(String desc_schoollevel) {
		this.desc_schoollevel = desc_schoollevel;
	}

	private String desc_schoollevel;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
}