package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the funcionalities database table.
 * 
 */
@Entity
@Table(name="funcionalities")
public class Funcionality implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idfuncionality;

	private String descFuncionality;
	
	@Column(name="name_funcionality")
	private String nameFuncionality;
	
	@Column(name="point_funcionality")
	private Integer pointFuncionality;
	
	private Integer point_plus;
	
	private Integer point_less;
	
	private Integer funcionality_days;
	
	private Boolean sistema;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Boolean getSistema() {
		return sistema;
	}

	public void setSistema(Boolean sistema) {
		this.sistema = sistema;
	}

	public Integer getFuncionality_days() {
		return funcionality_days;
	}

	public void setFuncionality_days(Integer funcionality_days) {
		this.funcionality_days = funcionality_days;
	}

	public Integer getPoint_plus() {
		return point_plus;
	}

	public void setPoint_plus(Integer point_plus) {
		this.point_plus = point_plus;
	}

	public Integer getPoint_less() {
		return point_less;
	}

	public void setPoint_less(Integer point_less) {
		this.point_less = point_less;
	}

	public Integer getPointFuncionality() {
		return pointFuncionality;
	}

	public void setPointFuncionality(Integer pointFuncionality) {
		this.pointFuncionality = pointFuncionality;
	}

	public String getNameFuncionality() {
		return nameFuncionality;
	}

	public void setNameFuncionality(String nameFuncionality) {
		this.nameFuncionality = nameFuncionality;
	}

	public Funcionality() {
	}

	public Integer getIdfuncionality() {
		return this.idfuncionality;
	}

	public void setIdfuncionality(Integer idfuncionality) {
		this.idfuncionality = idfuncionality;
	}

	public String getDescFuncionality() {
		return this.descFuncionality;
	}

	public void setDescFuncionality(String descFuncionality) {
		this.descFuncionality = descFuncionality;
	}


}