package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the funcionalities database table.
 * 
 */
@Entity
@Table(name="func_user_business")
public class FuncionalityUserBusiness implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idfunc_business;
	
	private Date date_start;
	
	private Date date_end;	

	private Boolean active;
	
	@ManyToOne
	@JoinColumn(name = "idfuncionality")
	private Funcionality idfuncionality;
	
	@ManyToOne
	@JoinColumn(name = "idbusiness")	
	private Business idbusiness;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}


	public Integer getIdfunc_business() {
		return idfunc_business;
	}

	public void setIdfunc_business(Integer idfunc_business) {
		this.idfunc_business = idfunc_business;
	}

	public Date getDate_start() {
		return date_start;
	}

	public void setDate_start(Date date_start) {
		this.date_start = date_start;
	}

	public Date getDate_end() {
		return date_end;
	}

	public void setDate_end(Date date_end) {
		this.date_end = date_end;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Funcionality getIdfuncionality() {
		return idfuncionality;
	}

	public void setIdfuncionality(Funcionality idfuncionality) {
		this.idfuncionality = idfuncionality;
	}

	public Business getIdbusiness() {
		return idbusiness;
	}

	public void setIdbusiness(Business idbusiness) {
		this.idbusiness = idbusiness;
	}


}