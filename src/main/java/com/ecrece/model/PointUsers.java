package com.ecrece.model;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "point_users")
public class PointUsers  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idpointuser;	

    private Integer id_package;
    
	private Integer points;
    
    private Integer id_state_package;
    
    private Integer iduser;
    
    private Date date_end_point;

    private Date create_date;
    
    private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
    
	public Integer getIdpointuser() {
		return idpointuser;
	}

	public void setIdpointuser(Integer idpointuser) {
		this.idpointuser = idpointuser;
	}

	public Integer getId_package() {
		return id_package;
	}

	public void setId_package(Integer id_package) {
		this.id_package = id_package;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Integer getId_state_package() {
		return id_state_package;
	}

	public void setId_state_package(Integer id_state_package) {
		this.id_state_package = id_state_package;
	}

	public Integer getIduser() {
		return iduser;
	}

	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	public Date getDate_end_point() {
		return date_end_point;
	}

	public void setDate_end_point(Date date_end_point) {
		this.date_end_point = date_end_point;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	

	
}
