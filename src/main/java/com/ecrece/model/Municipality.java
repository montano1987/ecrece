package com.ecrece.model;



import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "municipalities")
public class Municipality  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idmunicipality;	
	
	
    private String name_municipality;


	@ManyToOne
	@JoinColumn(name = "idprovince")	
	private Province province;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	
	public Integer getIdmunicipality() {
		return idmunicipality;
	}

	public void setIdmunicipality(Integer idmunicipality) {
		this.idmunicipality = idmunicipality;
	}

	public String getName_municipality() {
		return name_municipality;
	}

	public void setName_municipality(String name_municipality) {
		this.name_municipality = name_municipality;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	
}
