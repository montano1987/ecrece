package com.ecrece.model;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the coments database table.
 * 
 */
@Entity
@Table(name="schedulejoboffer")
public class ScheduleJobOffer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idschedulejoboffer;
	
	private String desc_schedu;
	
	public String getDesc_schedu() {
		return desc_schedu;
	}

	public void setDesc_schedu(String desc_schedu) {
		this.desc_schedu = desc_schedu;
	}

	public Integer getIdschedulejoboffer() {
		return idschedulejoboffer;
	}

	public void setIdschedulejoboffer(Integer idschedulejoboffer) {
		this.idschedulejoboffer = idschedulejoboffer;
	}
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
}