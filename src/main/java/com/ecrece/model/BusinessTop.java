package com.ecrece.model;
import javax.persistence.ManyToOne;

public class BusinessTop {
	
	private Double avg;
	
	private Integer countcalif;
	
	//bi-directional many-to-one association to Role
	@ManyToOne
	private Business business;
		
	public Double getAvg() {
		return avg;
	}

	public void setAvg(Double avg) {
		this.avg = avg;
	}

	public Integer getCountcalif() {
		return countcalif;
	}

	public void setCountcalif(Integer countcalif) {
		this.countcalif = countcalif;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

}
