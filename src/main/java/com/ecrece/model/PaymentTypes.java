package com.ecrece.model;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipos_pago")
public class PaymentTypes  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idtipo_pago;

	private String nom_tipo_pago;	
    
    private String desc_tipo_pago;
    
    private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

    public Integer getIdtipo_pago() {
		return idtipo_pago;
	}

	public void setIdtipo_pago(Integer idtipo_pago) {
		this.idtipo_pago = idtipo_pago;
	}

	public String getNom_tipo_pago() {
		return nom_tipo_pago;
	}

	public void setNom_tipo_pago(String nom_tipo_pago) {
		this.nom_tipo_pago = nom_tipo_pago;
	}

	public String getDesc_tipo_pago() {
		return desc_tipo_pago;
	}

	public void setDesc_tipo_pago(String desc_tipo_pago) {
		this.desc_tipo_pago = desc_tipo_pago;
	}	


	
	
}
