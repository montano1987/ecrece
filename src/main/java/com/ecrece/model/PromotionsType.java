package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the promotions_types database table.
 * 
 */
@Entity
@Table(name="promotions_types")
public class PromotionsType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idpromotionType;

	private String namePromotionType;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

//	//bi-directional many-to-one association to Promotion
//	@OneToMany(mappedBy="promotionsType")
//	private List<Promotion> promotions;


	public Integer getIdpromotionType() {
		return this.idpromotionType;
	}

	public void setIdpromotionType(Integer idpromotionType) {
		this.idpromotionType = idpromotionType;
	}

	public String getNamePromotionType() {
		return this.namePromotionType;
	}

	public void setNamePromotionType(String namePromotionType) {
		this.namePromotionType = namePromotionType;
	}

}