package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;



/**
 * The persistent class for the business database table.
 * 
 */
@Entity
@Table(name = "business_subcategorys")
public class BusinessSubcategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idbusinesssubcategory;
	
	private Integer idbusiness;
	
	@ManyToOne
	@JoinColumn(name = "idsubcategory")	
	private Subcategory idsubcategory;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Subcategory getIdsubcategory() {
		return idsubcategory;
	}

	public void setIdsubcategory(Subcategory idsubcategory) {
		this.idsubcategory = idsubcategory;
	}

	public Integer getIdbusiness() {
		return idbusiness;
	}

	public void setIdbusiness(Integer idbusiness) {
		this.idbusiness = idbusiness;
	}


	public Integer getIdbusinesssubcategory() {
		return idbusinesssubcategory;
	}

	public void setIdbusinesssubcategory(Integer idbusinesssubcategory) {
		this.idbusinesssubcategory = idbusinesssubcategory;
	}

	
}