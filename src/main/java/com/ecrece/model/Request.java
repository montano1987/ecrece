package com.ecrece.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the requests database table.
 * 
 */
@Entity
@Table(name="requests")
public class Request implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idrequest;

	private Timestamp date_request;
	
private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Timestamp getDate_request() {
		return date_request;
	}

	public void setDate_request(Timestamp date_request) {
		this.date_request = date_request;
	}

	public Integer getDesc_request() {
		return desc_request;
	}

	public void setDesc_request(Integer desc_request) {
		this.desc_request = desc_request;
	}

	private Integer desc_request;

	//bi-directional many-to-one association to Offer
	@ManyToOne
	@JoinColumn(name="idoffer_fk")
	private Offer offer;

	public Integer getIdrequest() {
		return this.idrequest;
	}

	public void setIdrequest(Integer idrequest) {
		this.idrequest = idrequest;
	}

	public Offer getOffer() {
		return this.offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

}