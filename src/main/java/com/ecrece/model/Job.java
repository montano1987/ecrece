package com.ecrece.model;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the coments database table.
 * 
 */
@Entity
@Table(name="jobs")
public class Job implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idjob;
		
	public Integer getIdjob() {
		return idjob;
	}

	public void setIdjob(Integer idjob) {
		this.idjob = idjob;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public float getSalaryjob() {
		return salaryjob;
	}

	public void setSalaryjob(float salaryjob) {
		this.salaryjob = salaryjob;
	}

	public String getSchedulejob() {
		return schedulejob;
	}

	public void setSchedulejob(String schedulejob) {
		this.schedulejob = schedulejob;
	}

	public String getPhonejob() {
		return phonejob;
	}

	public void setPhonejob(String phonejob) {
		this.phonejob = phonejob;
	}

	public Date getDateend() {
		return dateend;
	}

	public void setDateend(Date dateend) {
		this.dateend = dateend;
	}


	public String getDescjob() {
		return descjob;
	}

	public void setDescjob(String descjob) {
		this.descjob = descjob;
	}

	public String getTitlejob() {
		return titlejob;
	}

	public void setTitlejob(String titlejob) {
		this.titlejob = titlejob;
	}

	@ManyToOne
	@JoinColumn(name="idbusiness_fk")
	private Business business;	
	
	@ManyToOne
	@JoinColumn(name="idschoollevel_fk")
	private Schoollevel schoollevel;
	
	public Schoollevel getSchoollevel() {
		return schoollevel;
	}

	public void setSchoollevel(Schoollevel schoollevel) {
		this.schoollevel = schoollevel;
	}
	
	@ManyToOne
	@JoinColumn(name="idcontracttype_fk")
	private ContractType contracttype;

	public ContractType getContracttype() {
		return contracttype;
	}

	public void setContracttype(ContractType contracttype) {
		this.contracttype = contracttype;
	}
	
	private String otherrequirement;

	public String getOtherrequirement() {
		return otherrequirement;
	}

	public void setOtherrequirement(String otherrequirement) {
		this.otherrequirement = otherrequirement;
	}
	
	@ManyToOne
	@JoinColumn(name="idprofessionalexperience_fk")
	private ProfessionalExperience professionalExperience;
	
	public ProfessionalExperience getProfessionalExperience() {
		return professionalExperience;
	}

	public void setProfessionalExperience(ProfessionalExperience professionalExperience) {
		this.professionalExperience = professionalExperience;
	}

	private float salaryjob;
	
	private String schedulejob;
	
	private String phonejob;
	
	private Date dateend;
	
	private Integer maxage;
	
	private Integer minage;
	
	public Integer getMinage() {
		return minage;
	}

	public void setMinage(Integer minage) {
		this.minage = minage;
	}

	public Integer getMaxage() {
		return maxage;
	}

	public void setMaxage(Integer maxage) {
		this.maxage = maxage;
	}

	private Integer sex;
	
	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	private ScheduleJobOffer schedulejoboffer;
	
	public ScheduleJobOffer getSchedulejoboffer() {
		return schedulejoboffer;
	}

	public void setSchedulejoboffer(ScheduleJobOffer schedulejoboffer) {
		this.schedulejoboffer = schedulejoboffer;
	}

	private String descjob;
	
	private String titlejob;
	
    private Boolean is_deleted;
    
    public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}

	private Long last_update;
    
    private Timestamp create_at;
}