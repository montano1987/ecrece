package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the notificaciones database table.
 * 
 */
@Entity
@Table(name="notifications")
public class Notification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idnotification;

	private String subject_notification;

	private String cc_notification;

	private String desc_notification;

	private String for_notification;
	
	private Date create_date;	
	
	private String urltodetail;
	
	private boolean read;

	@ManyToOne
	@JoinColumn(name="notification_state")
	private NotificationState notification_state;
	
	@ManyToOne
	@JoinColumn(name="idpointpackage")
	private PointPackage idpointpackage;

	@ManyToOne
	@JoinColumn(name="idbusiness_to")
	private Business idbusiness_to;
	
	@ManyToOne
	@JoinColumn(name="iduser_to")
	private User iduser_to;
	
	@ManyToOne
	@JoinColumn(name="usernotification")
	private User usernotification;
	
	@ManyToOne
	@JoinColumn(name="idbusiness_fk")
	private Business idbusiness_fk;

	//bi-directional many-to-one association to NotificationsType
	@ManyToOne
	@JoinColumn(name="type_notifications_fk")
	private NotificationType notificationsType;
	
	@ManyToOne
	@JoinColumn(name="idpointuser")
	private PointUsers idpointuser;
	
	@ManyToOne
	@JoinColumn(name="idpayment_form")
	private PaymentForms idpayment_form;
	
	@ManyToOne
	@JoinColumn(name="idpromotion")
	private Promotion Promotion;
	
	@ManyToOne
	@JoinColumn(name="idreservation")
	private Reservation Reservation;
	
	@ManyToOne
	@JoinColumn(name="idoffer")
	private Offer offer;	
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public String getUrltodetail() {
		return urltodetail;
	}
	
	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void setUrltodetail(String urltodetail) {
		this.urltodetail = urltodetail;	}

	
	public Reservation getReservation() {
		return Reservation;
	}


	public void setReservation(Reservation reservation) {
		Reservation = reservation;
	}

	public Offer getOffer() {
		return offer;
	}


	public void setOffer(Offer offer) {
		this.offer = offer;
	}


	public Promotion getPromotion() {
		return Promotion;
	}


	public void setPromotion(Promotion promotion) {
		Promotion = promotion;
	}


	public Integer getIdnotification() {
		return idnotification;
	}


	public void setIdnotification(Integer idnotification) {
		this.idnotification = idnotification;
	}


	public String getCc_notification() {
		return cc_notification;
	}

	public PointPackage getIdpointpackage() {
		return idpointpackage;
	}


	public void setIdpointpackage(PointPackage idpointpackage) {
		this.idpointpackage = idpointpackage;
	}

	
	public void setCc_notification(String cc_notification) {
		this.cc_notification = cc_notification;
	}


	public String getDesc_notification() {
		return desc_notification;
	}


	public void setDesc_notification(String desc_notification) {
		this.desc_notification = desc_notification;
	}


	public String getFor_notification() {
		return for_notification;
	}


	public void setFor_notification(String for_notification) {
		this.for_notification = for_notification;
	}
	
	public String getSubject_notification() {
		return subject_notification;
	}


	public void setSubject_notification(String subject_notification) {
		this.subject_notification = subject_notification;
	}
	
	public NotificationType getNotificationsType() {
		return this.notificationsType;
	}

	public void setNotificationsType(NotificationType notificationsType) {
		this.notificationsType = notificationsType;
	}


	public Date getCreate_date() {
		return create_date;
	}


	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public NotificationState getNotification_state() {
		return notification_state;
	}


	public void setNotification_state(NotificationState notification_state) {
		this.notification_state = notification_state;
	}
	
	

	public Business getIdbusiness_to() {
		return idbusiness_to;
	}


	public void setIdbusiness_to(Business idbusiness_to) {
		this.idbusiness_to = idbusiness_to;
	}


	public User getIduser_to() {
		return iduser_to;
	}


	public void setIduser_to(User iduser_to) {
		this.iduser_to = iduser_to;
	}


	public User getUsernotification() {
		return usernotification;
	}


	public void setUsernotification(User usernotification) {
		this.usernotification = usernotification;
	}


	public Business getIdbusiness_fk() {
		return idbusiness_fk;
	}


	public void setIdbusiness_fk(Business idbusiness_fk) {
		this.idbusiness_fk = idbusiness_fk;
	}


	public PointUsers getIdpointuser() {
		return idpointuser;
	}


	public void setIdpointuser(PointUsers idpointuser) {
		this.idpointuser = idpointuser;
	}


	public PaymentForms getIdpayment_form() {
		return idpayment_form;
	}


	public void setIdpayment_form(PaymentForms idpayment_form) {
		this.idpayment_form = idpayment_form;
	}





}