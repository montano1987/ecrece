package com.ecrece.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Timestamp;


/**
 * The persistent class for the coments database table.
 * 
 */
@Entity
@Table(name="replycoment")
public class ReplyComent implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idcreply;	

	private Timestamp date_replycoment;

	private String desc_replycoment;

	private String title_replycoment;

	//bi-directional many-to-one association to Business
	@ManyToOne
	@JoinColumn(name="idbusiness_fk")
	private Business business;
	
	@ManyToOne
	@JoinColumn(name="replyforiduser_fk")
	private User replyforiduser;
	
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Integer getIdcreply() {
		return idcreply;
	}

	public void setIdcreply(Integer idcreply) {
		this.idcreply = idcreply;
	}

	public User getReplyforiduser() {
		return replyforiduser;
	}

	public void setReplyforiduser(User replyforiduser) {
		this.replyforiduser = replyforiduser;
	}

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idcoment_fk")
	private Coment coment;

	public Coment getComent() {
		return coment;
	}

	public void setComent(Coment coment) {
		this.coment = coment;
	}

	public Business getBusiness() {
		return this.business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public Timestamp getDate_replycoment() {
		return date_replycoment;
	}

	public void setDate_replycoment(Timestamp date_replycoment) {
		this.date_replycoment = date_replycoment;
	}

	public String getDesc_replycoment() {
		return desc_replycoment;
	}

	public void setDesc_replycoment(String desc_replycoment) {
		this.desc_replycoment = desc_replycoment;
	}

	public String getTitle_replycoment() {
		return title_replycoment;
	}

	public void setTitle_replycoment(String title_replycoment) {
		this.title_replycoment = title_replycoment;
	}


}