package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.*;




/**
 * The persistent class for the business database table.
 * 
 */
@Entity
@Table(name = "business")
public class Business implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idbusiness;
	
	private String desc_business;
	
	private String code_qr;

	private String dir_business;
	
	private String web_site;
	
	private String licence_business;
	
	@ManyToOne
	@JoinColumn(name = "iduser_fk")	
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "idstate_business_fk")	
	private BusinessStates businessStates;
	
	private String latitude;

	private String longitude;
	
	private String name_business;
	
	private String phone_business;
	
	private String name_image;
	
	private Date crate_date;
	
	private String closefor;
	

	@ManyToOne
	@JoinColumn(name = "idmunicipality")	
	private Municipality municipality;
		
//	@OneToMany
//	 @JoinTable(name = "func_user_business", joinColumns = { @JoinColumn(name ="idbusiness") }, inverseJoinColumns = { @JoinColumn(name = "idfuncionality")})
//	private List<Funcionality> funcionality;

	@OneToMany
//	@JoinTable(name = "business_subcategorys", joinColumns = { @JoinColumn(name = "idbusiness") }, inverseJoinColumns = { @JoinColumn(name = "idsubcategory") })
	@JoinColumn
	private List<Subcategory> subcategory;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	
	public List<Subcategory> getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(List<Subcategory> subcategory) {
		this.subcategory = subcategory;
	}
//
//
//	public List<Funcionality> getFuncionality() {
//		return funcionality;
//	}
//
//	public void setFuncionality(List<Funcionality> funcionality) {
//		this.funcionality = funcionality;
//	}
	public Integer getIdbusiness() {
		return idbusiness;
	}

	public void setIdbusiness(Integer idbusiness) {
		this.idbusiness = idbusiness;
	}

	public String getDesc_business() {
		return desc_business;
	}

	public void setDesc_business(String desc_business) {
		this.desc_business = desc_business;
	}

	public String getCode_qr() {
		return code_qr;
	}

	public void setCode_qr(String code_qr) {
		this.code_qr = code_qr;
	}

	public String getDir_business() {
		return dir_business;
	}

	public void setDir_business(String dir_business) {
		this.dir_business = dir_business;
	}

	public String getWeb_site() {
		return web_site;
	}

	public void setWeb_site(String web_site) {
		this.web_site = web_site;
	}

	public String getLicence_business() {
		return licence_business;
	}

	public void setLicence_business(String licence_business) {
		this.licence_business = licence_business;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BusinessStates getBusinessStates() {
		return businessStates;
	}

	public void setBusinessStates(BusinessStates businessStates) {
		this.businessStates = businessStates;
	}


	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getName_business() {
		return name_business;
	}

	public void setName_business(String name_business) {
		this.name_business = name_business;
	}

	public String getPhone_business() {
		return phone_business;
	}

	public void setPhone_business(String phone_business) {
		this.phone_business = phone_business;
	}

	public Municipality getMunicipality() {
		return municipality;
	}

	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}

	public String getName_image() {
		return name_image;
	}

	public void setName_image(String name_image) {
		this.name_image = name_image;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public Date getCrate_date() {
		return crate_date;
	}

	public void setCrate_date(Date crate_date) {
		this.crate_date = crate_date;
	}
	public String getClosefor() {
		return closefor;
	}

	public void setClosefor(String closefor) {
		this.closefor = closefor;
	}
}