package com.ecrece.model;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the coments database table.
 * 
 */
@Entity
@Table(name="coments")
public class Coment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idcoment;

	private Timestamp date_coment;

	private String desc_coment;

	private String title_coment;

	//bi-directional many-to-one association to Business
	@ManyToOne
	@JoinColumn(name="idbusiness_fk")
	private Business business;
	
	@ManyToOne
	@JoinColumn(name="iduser_fk")
	private User iduser;
	
	
	@OneToMany(mappedBy = "coment")
    private List<ReplyComent> replyComent;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public List<ReplyComent> getReplyComent() {
		return replyComent;
	}

	public void setReplyComent(List<ReplyComent> replyComent) {
		this.replyComent = replyComent;
	}

	public User getIduser() {
		return iduser;
	}

	public void setIduser(User iduser) {
		this.iduser = iduser;
	}

	public Timestamp getDate_coment() {
		return date_coment;
	}

	public void setDate_coment(Timestamp date_coment) {
		this.date_coment = date_coment;
	}

	public String getDesc_coment() {
		return desc_coment;
	}

	public void setDesc_coment(String desc_coment) {
		this.desc_coment = desc_coment;
	}

	public String getTitle_coment() {
		return title_coment;
	}

	public void setTitle_coment(String title_coment) {
		this.title_coment = title_coment;
	}
	public Integer getIdcoment() {
		return this.idcoment;
	}

	public void setIdcoment(Integer idcoment) {
		this.idcoment = idcoment;
	}

	public Business getBusiness() {
		return this.business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

}