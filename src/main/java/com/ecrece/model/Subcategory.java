package com.ecrece.model;



import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "subcategorys")
public class Subcategory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idsubcategory;	
	
	
    private String name_subcategory;


  //bi-directional many-to-one association to Category
  	@ManyToOne
	@JoinColumn(name = "idcategory_fk")	
	private Category category;
  	
  	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIdsubcategory() {
		return idsubcategory;
	}


	public void setIdsubcategory(Integer idsubcategory) {
		this.idsubcategory = idsubcategory;
	}


	public String getName_subcategory() {
		return name_subcategory;
	}


	public void setName_subcategory(String name_subcategory) {
		this.name_subcategory = name_subcategory;
	}


	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}
	
	
	

	
}
