package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;



/**
 * The persistent class for the business database table.
 * 
 */
@Entity
@Table(name = "schedule_business")
public class ScheduleBusiness implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idschedule;
	
	@ManyToOne
	@JoinColumn(name = "idbusiness")	
	private Business business;
	
	private Integer weekday;
	
	private Timestamp h_star;	

	private Timestamp h_end;
	
	private Timestamp h_star1;	


	private Timestamp h_end1;
	
	private Timestamp h_star2;	

	private Timestamp h_end2;
	
	private Boolean tfhours;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Boolean getTfhours() {
		return tfhours;
	}

	public void setTfhours(Boolean tfhours) {
		this.tfhours = tfhours;
	}
	
	public Integer getIdschedule() {
		return idschedule;
	}

	public void setIdschedule(Integer idschedule) {
		this.idschedule = idschedule;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public Integer getWeekday() {
		return weekday;
	}

	public void setWeekday(Integer weekday) {
		this.weekday = weekday;
	}

	public Timestamp getH_star() {
		return h_star;
	}

	public void setH_star(Timestamp h_star) {
		this.h_star = h_star;
	}

	public Timestamp getH_end() {
		return h_end;
	}

	public void setH_end(Timestamp h_end) {
		this.h_end = h_end;
	}
	public Timestamp getH_star1() {
		return h_star1;
	}

	public void setH_star1(Timestamp h_star1) {
		this.h_star1 = h_star1;
	}

	public Timestamp getH_end1() {
		return h_end1;
	}

	public void setH_end1(Timestamp h_end1) {
		this.h_end1 = h_end1;
	}

	public Timestamp getH_star2() {
		return h_star2;
	}

	public void setH_star2(Timestamp h_star2) {
		this.h_star2 = h_star2;
	}

	public Timestamp getH_end2() {
		return h_end2;
	}

	public void setH_end2(Timestamp h_end2) {
		this.h_end2 = h_end2;
	}

}