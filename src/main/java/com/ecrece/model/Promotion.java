package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the promotions database table.
 * 
 */
@Entity
@Table(name="promotions")
public class Promotion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idpromotion;

	private Date date_end_promotion;

	private Date date_start_promotion;

	private String desc_promotion;
	
	private Integer daysPromotion;
	
	private Boolean activePromotion;
	
	private String codePromotion;
	
	private String title_promotion;
	
	private String urldir;
	
	@ManyToOne
	@JoinColumn(name = "idbusiness")	
	private Business business;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public Date getDate_end_promotion() {
		return date_end_promotion;
	}

	public void setDate_end_promotion(Date date_end_promotion) {
		this.date_end_promotion = date_end_promotion;
	}

	public Date getDate_start_promotion() {
		return date_start_promotion;
	}

	public void setDate_start_promotion(Date date_start_promotion) {
		this.date_start_promotion = date_start_promotion;
	}

	public String getDesc_promotion() {
		return desc_promotion;
	}

	public void setDesc_promotion(String desc_promotion) {
		this.desc_promotion = desc_promotion;
	}
	public Integer getIdpromotion() {
		return idpromotion;
	}

	public void setIdpromotion(Integer idpromotion) {
		this.idpromotion = idpromotion;
	}

	
	public Integer getDaysPromotion() {
		return daysPromotion;
	}

	public void setDaysPromotion(Integer daysPromotion) {
		this.daysPromotion = daysPromotion;
	}	

//	public PromotionsType getPromotionsType() {
//		return this.promotionsType;
//	}
//
//	public void setPromotionsType(PromotionsType promotionsType) {
//		this.promotionsType = promotionsType;
//	}

	public Boolean getActivePromotion() {
		return activePromotion;
	}

	public void setActivePromotion(Boolean activePromotion) {
		this.activePromotion = activePromotion;
	}
	
	public String getCodePromotion() {
		return codePromotion;
	}

	public void setCodePromotion(String codePromotion) {
		this.codePromotion = codePromotion;
	}
	
	public String getTitle_promotion() {
		return title_promotion;
	}

	public void setTitle_promotion(String title_promotion) {
		this.title_promotion = title_promotion;
	}
	public String getUrldir() {
		return urldir;
	}

	public void setUrldir(String urldir) {
		this.urldir = urldir;
	}
		
	
}