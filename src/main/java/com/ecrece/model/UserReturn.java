package com.ecrece.model;
import javax.persistence.ManyToOne;

public class UserReturn {
	
	private Integer iduser;	

	private String name;
	
	private String nameUser;

	private Integer ptosUser;
	
	private String token;
	
	private String language;
	
	

	//bi-directional many-to-one association to Role
	@ManyToOne
	private Role role;

	//bi-directional many-to-one association to UserState
	@ManyToOne
	private UsersStates userState;

	public Integer getIduser() {
		return iduser;
	}

	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public Integer getPtosUser() {
		return ptosUser;
	}

	public void setPtosUser(Integer ptosUser) {
		this.ptosUser = ptosUser;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public UsersStates getUserState() {
		return userState;
	}

	public void setUserState(UsersStates userState) {
		this.userState = userState;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}	
	
}
