package com.ecrece.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer iduser;
	
	private Boolean active;

	private String code;
	
	private Date date_bith;

	private Date dateCreateUser;

	private String mail;

	private String name;
	
	@Column(unique=true)
	private String nameUser;

	private String password;

	private String phoneUser;

	private Integer ptosUser;

	private String sex;
	

	private String surnameUser;
	
	private String surname2_user;
	
	private String token;
	
	private String language;
	
	private Boolean receivepromotions;
	
	private Boolean receiveoffer;
	
	private String dir;
	
	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="idrol")
	private Role role;

	//bi-directional many-to-one association to UserState
	@ManyToOne
	@JoinColumn(name="idstate_fk")
	private UsersStates userState;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIduser() {
		return iduser;
	}

	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	public Date getDateCreateUser() {
		return dateCreateUser;
	}

	public void setDateCreateUser(Date dateCreateUser) {
		this.dateCreateUser = dateCreateUser;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneUser() {
		return phoneUser;
	}

	public void setPhoneUser(String phoneUser) {
		this.phoneUser = phoneUser;
	}

	public Integer getPtosUser() {
		return ptosUser;
	}

	public void setPtosUser(Integer ptosUser) {
		this.ptosUser = ptosUser;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	
	public String getSurnameUser() {
		return surnameUser;
	}

	public void setSurnameUser(String surnameUser) {
		this.surnameUser = surnameUser;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public UsersStates getUserState() {
		return userState;
	}

	public void setUserState(UsersStates userState) {
		this.userState = userState;
	}

	public String getSurname2_user() {
		return surname2_user;
	}

	public void setSurname2_user(String surname2_user) {
		this.surname2_user = surname2_user;
	}

	public Date getDate_bith() {
		return date_bith;
	}

	public void setDate_bith(Date date_bith) {
		this.date_bith = date_bith;
	}
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public Boolean getReceivepromotions() {
		return receivepromotions;
	}

	public void setReceivepromotions(Boolean receivepromotions) {
		this.receivepromotions = receivepromotions;
	}

	public Boolean getReceiveoffer() {
		return receiveoffer;
	}

	public void setReceiveoffer(Boolean receiveoffer) {
		this.receiveoffer = receiveoffer;
	}
	


}
