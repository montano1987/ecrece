package com.ecrece.model;

import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the notification_state database table.
 * 
 */
@Entity
@Table(name="notification_state")
public class NotificationState  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idnotificationstate;

	private String descNotificationstate;

	private String nameNotificationstate;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	public NotificationState() {
	}

	public Integer getIdnotificationstate() {
		return this.idnotificationstate;
	}

	public void setIdnotificationstate(Integer idnotificationstate) {
		this.idnotificationstate = idnotificationstate;
	}

	public String getDescNotificationstate() {
		return this.descNotificationstate;
	}

	public void setDescNotificationstate(String descNotificationstate) {
		this.descNotificationstate = descNotificationstate;
	}

	public String getNameNotificationstate() {
		return this.nameNotificationstate;
	}

	public void setNameNotificationstate(String nameNotificationstate) {
		this.nameNotificationstate = nameNotificationstate;
	}

}