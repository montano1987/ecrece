package com.ecrece.model;

import java.io.Serializable;

public class ClientNotification implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String subject_notification;
	
	private String desc_notification;

	private Integer tonitification;
	
	public Integer getTonitification() {
		return tonitification;
	}

	public void setTonitification(Integer tonitification) {
		this.tonitification = tonitification;
	}

	public String getSubject_notification() {
		return subject_notification;
	}

	public void setSubject_notification(String subject_notification) {
		this.subject_notification = subject_notification;
	}

	public String getDesc_notification() {
		return desc_notification;
	}

	public void setDesc_notification(String desc_notification) {
		this.desc_notification = desc_notification;
	}
	

}