package com.ecrece.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the version_movil database table.
 * 
 */
@Entity
@Table(name="version_movil")

public class VersionMovil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="\"idAplication\"")
	private Integer idAplication;

	@Column(name="num_aplication")
	private String numAplication;

	public VersionMovil() {
	}

	public Integer getIdAplication() {
		return this.idAplication;
	}

	public void setIdAplication(Integer idAplication) {
		this.idAplication = idAplication;
	}

	public String getNumAplication() {
		return this.numAplication;
	}

	public void setNumAplication(String numAplication) {
		this.numAplication = numAplication;
	}

}