package com.ecrece.model;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user_state")
public class UsersStates  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer iduser_state;	

    private String name_user_state;
    
//  //bi-directional many-to-one association to User
//  	@OneToMany(mappedBy="userState")
//  	private List<User> users;
    
    private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIduser_state() {
		return iduser_state;
	}

	public void setIduser_state(Integer iduser_state) {
		this.iduser_state = iduser_state;
	}

	public String getName_user_state() {
		return name_user_state;
	}

	public void setName_user_state(String name_user_state) {
		this.name_user_state = name_user_state;
	}
	
//	public List<User> getUsers() {
//		return this.users;
//	}
//
//	public void setUsers(List<User> users) {
//		this.users = users;
//	}
//
//	public User addUser(User user) {
//		getUsers().add(user);
//		user.setUserState(this);
//
//		return user;
//	}
//
//	public User removeUser(User user) {
//		getUsers().remove(user);
//		user.setUserState(null);
//
//		return user;
//	}

	
	
}
