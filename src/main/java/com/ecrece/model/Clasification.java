package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the clasifications database table.
 * 
 */
@Entity
@Table(name="clasifications")
public class Clasification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_clasification;

	private Integer ptosclasification;
	
	private Date createDate;

	//bi-directional many-to-one association to Business
	@ManyToOne
	@JoinColumn(name="idbusiness")
	private Business business;
	
	@ManyToOne
	@JoinColumn(name="iduser")
	private User user;

	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	public Integer getId_clasification() {
		return id_clasification;
	}

	public void setId_clasification(Integer id_clasification) {
		this.id_clasification = id_clasification;
	}

	public Integer getPtosclasification() {
		return ptosclasification;
	}

	public void setPtosclasification(Integer ptosclasification) {
		this.ptosclasification = ptosclasification;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Business getBusiness() {
		return this.business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}