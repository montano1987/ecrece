package com.ecrece.model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the reservations database table.
 * 
 */
@Entity
@Table(name="reservations")
public class Reservation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idreservation;

	private Date date;

	private Date dateend;

	private Integer night;

	private Integer personsamount;

	private String description;

	private Timestamp schedule;

	private Timestamp scheduleend;
	
	private String article;
	
	private String urldir;
	
	private Boolean accept;
	
	private Boolean reject;
	
	private Boolean domicile;
	
	private Boolean pend;
	
	private String dir_reservation;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Boolean getPend() {
		return pend;
	}

	public void setPend(Boolean pend) {
		this.pend = pend;
	}
	public String getDir_reservation() {
		return dir_reservation;
	}

	public void setDir_reservation(String dir_reservation) {
		this.dir_reservation = dir_reservation;
	}

	public Boolean getDomicile() {
		return domicile;
	}

	public void setDomicile(Boolean domicile) {
		this.domicile = domicile;
	}

	public Boolean getImmediate() {
		return immediate;
	}

	public void setImmediate(Boolean immediate) {
		this.immediate = immediate;
	}

	private Boolean immediate;
	
	public Boolean getReject() {
		return reject;
	}

	public void setReject(Boolean reject) {
		this.reject = reject;
	}

	//bi-directional many-to-one association to Business
	@ManyToOne
	@JoinColumn(name="idbusiness")
	private Business business;

	@ManyToOne
	@JoinColumn(name="iduser_fk")
	private User usertoreservation;
	
	private Date createdate;
	
	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Boolean getAccept() {
		return accept;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept;
	}

	public String getUrldir() {
		return urldir;
	}

	public void setUrldir(String urldir) {
		this.urldir = urldir;
	}
	
	public User getUsertoreservation() {
		return usertoreservation;
	}

	public void setUsertoreservation(User usertoreservation) {
		this.usertoreservation = usertoreservation;
	}

	public Integer getIdreservation() {
		return idreservation;
	}

	public void setIdreservation(Integer idreservation) {
		this.idreservation = idreservation;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDateend() {
		return dateend;
	}

	public void setDateend(Date dateend) {
		this.dateend = dateend;
	}

	public Integer getNight() {
		return night;
	}

	public void setNight(Integer night) {
		this.night = night;
	}

	public Integer getPersonsamount() {
		return personsamount;
	}

	public void setPersonsamount(Integer personsamount) {
		this.personsamount = personsamount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getSchedule() {
		return schedule;
	}

	public void setSchedule(Timestamp schedule) {
		this.schedule = schedule;
	}

	public Timestamp getScheduleend() {
		return scheduleend;
	}

	public void setScheduleend(Timestamp scheduleend) {
		this.scheduleend = scheduleend;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public Business getBusiness() {
		return business;
	}
	
	public void setBusiness(Business business) {
		this.business = business;
	}
}