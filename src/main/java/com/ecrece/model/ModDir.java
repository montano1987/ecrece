package com.ecrece.model;

import javax.persistence.*;


/**
 * The persistent class for the coments database table.
 * 
 */
@Entity
public class ModDir {	
	
	@Id
    private Integer idmoddir;	
	
	public Integer getIdmoddir() {
		return idmoddir;
	}

	public void setIdmoddir(Integer idmoddir) {
		this.idmoddir = idmoddir;
	}

	private String dir;

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}
	
}