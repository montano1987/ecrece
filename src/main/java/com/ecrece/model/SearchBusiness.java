package com.ecrece.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "searchBusiness")
public class SearchBusiness implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idSearch;	
	
	private Integer idCategory;
	
	private Integer idMunicipality;
	
	private Integer idUser;
	
	private String desc_search;
	
	private Date date_search;
	
	private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public Date getDate_search() {
		return date_search;
	}
	public void setDate_search(Date date_search) {
		this.date_search = date_search;
	}
	public String getDesc_search() {
		return desc_search;
	}
	public void setDesc_search(String desc_search) {
		this.desc_search = desc_search;
	}
	public Integer getIdCategory() {
		return idCategory;
	}
	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}
	public Integer getIdMunicipality() {
		return idMunicipality;
	}
	public void setIdMunicipality(Integer idMunicipality) {
		this.idMunicipality = idMunicipality;
	}
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	public Integer getIdSearch() {
		return idSearch;
	}
	public void setIdSearch(Integer idSearch) {
		this.idSearch = idSearch;
	}	
	

}
