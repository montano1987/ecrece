package com.ecrece.model;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "payment_forms")
public class PaymentForms  {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idpayment_form;	

    private String name_payment_form;
    
    private String desc_payment_form;
    
    private Boolean is_deleted;
    
    private Long last_update;
    
    private Timestamp create_at;
    
    public Long getLast_update() {
		return last_update;
	}

	public void setLast_update(Long last_update) {
		this.last_update = last_update;
	}

	public Timestamp getCreate_at() {
		return create_at;
	}

	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	
	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Integer getIdpayment_form() {
		return idpayment_form;
	}

	public void setIdpayment_form(Integer idpayment_form) {
		this.idpayment_form = idpayment_form;
	}

	public String getName_payment_form() {
		return name_payment_form;
	}

	public void setName_payment_form(String name_payment_form) {
		this.name_payment_form = name_payment_form;
	}

	public String getDesc_payment_form() {
		return desc_payment_form;
	}

	public void setDesc_payment_form(String desc_payment_form) {
		this.desc_payment_form = desc_payment_form;
	}

	

	



	
	
}
