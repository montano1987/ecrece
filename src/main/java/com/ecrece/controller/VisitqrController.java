package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.Visitedqr;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.VisitqrRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class VisitqrController {	
	    @Autowired
	    private VisitqrRepository visitqrRepository;   	    
	    
	    @Autowired
	    private BusinessRepository businessRepository;
	
	    @GetMapping("/visitqr")
	    public  List<Visitedqr> getVisitqrs() {
	    	List<Visitedqr> vis = visitqrRepository.findAll().stream()
					.filter(visit -> visit.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return vis;	    
	    }
	    
	    @PostMapping("/visitqr/business/{idbusiness}/qr")
	    public Visitedqr createVisits(@PathVariable(value = "idbusiness") Integer idbusiness) {
	    	Visitedqr visi = new Visitedqr();
	    	visi.setBusiness(businessRepository.findById(idbusiness).get());
	    	visi.setCreate_date(new Date());   
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	visi.setCreate_at(tstamp);
	    	visi.setLast_update(tstamp.getTime());
	    	visi.setIs_deleted(false);
	        return visitqrRepository.save(visi);
	    }

}
