package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Category;
import com.ecrece.repository.CategoryRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class CategoryController {	

	    @Autowired
	    private CategoryRepository categoryRepository;
	    
	
	        
	    @GetMapping("/categories")
	    public  List<Category> getCategories() {
	        List<Category> cate = categoryRepository.findAll().stream()
					.filter(country -> country.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return cate;	
	    }

	    @GetMapping("/categories/{idcategory}")
	    public Category getCategory(@PathVariable(value = "idcategory") Integer idcategory) {
	        return categoryRepository.findById(idcategory)
	                .orElseThrow(() -> new ResourceNotFoundException("Category"));
	    }
	    
	    @GetMapping("/categories/findall/{descategory}")
	    public List<Category> findall(@PathVariable(value = "descategory") String descategory) {
	        return categoryRepository.findforAll(descategory);
	    }
	    
	    @PostMapping("/categories")
	    public Category createCategory(@Valid @RequestBody Category category1) {
	    	Category cate = category1;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	cate.setCreate_at(tstamp);
	    	System.out.print(tstamp);
	    	cate.setLast_update(tstamp.getTime());
	    	cate.setIs_deleted(false);
	        return categoryRepository.save(cate);
	    }

	    @PutMapping("/categories/{idcategory}")
	    public Category updateCategory(@PathVariable (value = "idcategory") Integer idcategory,
	                                   @Valid @RequestBody Category categoryRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return categoryRepository.findById(idcategory)
	                .map(category -> {
	                	category.setName_category(categoryRequest.getName_category());
	                	category.setLast_update(tstamp.getTime());
	                    return categoryRepository.save(category);
	                }).orElseThrow(() -> new ResourceNotFoundException("Category not found with id " + idcategory));
	    }


	    @DeleteMapping("/categories/{idcategory}")
	    public ResponseEntity<?> deleteCategory(@PathVariable (value = "idcategory") Integer idcategory) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return categoryRepository.findById(idcategory)
	                .map(category -> {
	                	category.setLast_update(tstamp.getTime());
	                	category.setIs_deleted(true);
	                	categoryRepository.save(category);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Category not found with id " + idcategory));
	    }

}
