package com.ecrece.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Language;
import com.ecrece.repository.LanguageRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class LanguageController {	

	    @Autowired
	    private LanguageRepository languageRepository;
	    
	
	        
	    @GetMapping("/languages")
	    public  List<Language> getLanguages() {
	    	List<Language> lang = languageRepository.findAll().stream()
					.filter(funcio -> funcio.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return lang;
	    }

	    @GetMapping("/languages/{idlanguaje}")
	    public Language getLanguage(@PathVariable(value = "idlanguaje") Integer idlanguaje) {
	        return languageRepository.findById(idlanguaje)
	                .orElseThrow(() -> new ResourceNotFoundException("Languages"));
	    }
	    
}
