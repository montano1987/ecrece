package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Client;
import com.ecrece.model.Coment;
import com.ecrece.model.Funcionality;
import com.ecrece.model.Notification;
import com.ecrece.model.NotificationState;
import com.ecrece.model.NotificationType;
import com.ecrece.model.ReplyComent;
import com.ecrece.model.User;
import com.ecrece.repository.ClientReposity;
import com.ecrece.repository.ComentReposity;
import com.ecrece.repository.FuncionalityRepository;
import com.ecrece.repository.NotificationRepository;
import com.ecrece.repository.NotificationStatesRepository;
import com.ecrece.repository.NotificationTypeRepository;
import com.ecrece.repository.ReplyComentReposity;
import com.ecrece.repository.UserRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ComentController {	

	    @Autowired
	    private ComentReposity comentRepository;
	    @Autowired
	    private UserRepository userRepository;
	    
	    @Autowired
	    private NotificationTypeRepository notificationtypeRepository;
	    
	    @Autowired
	    private NotificationStatesRepository notificationStatesRepository;
	    
	    @Autowired
	    private NotificationRepository notificationRepository;
	    
	    @Autowired
		private FuncionalityRepository funcionalityRepository;
	    
	    @Autowired
	    private ClientReposity clientReposity;
	        
	    @Autowired
	    private ReplyComentReposity  replyComentReposity;
	    
	    @GetMapping("/coment/all")
	    public  List<Coment> getComents() {
	    	List<Coment> con = comentRepository.findAll().stream()
					.filter(country -> country.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return con;	
	    }
	    @GetMapping("/coment/user/{iduser}")
	    public  List<Coment> getComentsbyUser(@PathVariable(value = "iduser") Integer iduser) {
	        return comentRepository.findbyUser(iduser);
	    }
	    
	    @GetMapping("/coment/business/{idbusiness}")
	    public List<Coment> getComentsbyBusiness(@PathVariable(value = "idbusiness") Integer idbusiness) { 
	           
	     return  comentRepository.findbyBusiness(idbusiness);
	   
	    }
	    

	    @GetMapping("/coment/{idcoment}")
	    public Coment getComent(@PathVariable(value = "idcoment") Integer idcoment) {
	        return comentRepository.findById(idcoment)
	                .orElseThrow(() -> new ResourceNotFoundException("Coment"));
	    }
	    @PostMapping("/delete/coment/")
	    public void deleteComent(@Valid @RequestBody Coment coment) {
	    	List<ReplyComent> replys = replyComentReposity.findbyComent(coment.getIdcoment());
	    	if(replys.size() > 0)
	    	{
	    		for (ReplyComent replyComent : replys) {
	    			Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    			replyComent.setLast_update(tstamp.getTime());
	    			replyComent.setIs_deleted(true);
	    			replyComentReposity.save(replyComent);
				}
	    	}
	    	Coment comenttodelete = comentRepository.findById(coment.getIdcoment()).get();
	    	Timestamp tstamp1 = new Timestamp(System.currentTimeMillis());
	    	comenttodelete.setLast_update(tstamp1.getTime());
	    	comenttodelete.setIs_deleted(true);
	    	comentRepository.save(comenttodelete);
	    }
	    
	    @PostMapping("/coment/user/{iduser}")
	    public Coment createComment(@PathVariable(value = "iduser") Integer iduser,
	    							@Valid @RequestBody Coment coment) {
	    	User us = userRepository.findById(iduser).get();
	    	
	    	Coment newcomment = coment;
	    	newcomment.setIduser(us);
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	newcomment.setCreate_at(tstamp);
	    	System.out.print(tstamp);
	    	newcomment.setLast_update(tstamp.getTime());
	    	newcomment.setIs_deleted(false);
	    	
	    	Notification notification = new Notification();
	    	notification.setRead(false);
			notification.setFor_notification(coment.getBusiness().getName_business()
					+ " del Usuario " + coment.getIduser().getNameUser());
			notification.setSubject_notification(
					"Nuevo Comentario ");
			notification.setCreate_date(new Date());
			notification.setIdpointuser(null);
			notification.setIdpayment_form(null);
			notification.setDesc_notification("Se comentó en el negocio: "+coment.getBusiness().getName_business());
			List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
			Optional<NotificationType> notitype = notificationtypes.stream()
					.filter(p -> p.getDesc_notification_type().equals("Informativa")).findFirst();
			if (notitype.isPresent()) {
				notification.setNotificationsType(notitype.get());
			} else {
				NotificationType notitype2 = new NotificationType();
				notitype2.setDesc_notification_type("Informativa");
				notificationtypeRepository.save(notitype2);
				notification.setNotificationsType(notitype2);

			}
			List<NotificationState> notificationstate = notificationStatesRepository.findAll();
			Optional<NotificationState> notistate1 = notificationstate.stream()
					.filter(p -> p.getNameNotificationstate().equals("Informativo")).findFirst();
			if (notistate1.isPresent()) {
				notification.setNotification_state(notistate1.get());
			} else {
				NotificationState notistate2 = new NotificationState();
				notistate2.setDescNotificationstate("En estado Informativo");
				notistate2.setNameNotificationstate("Informativo");
				notificationStatesRepository.save(notistate2);
				notification.setNotification_state(notistate2);

			}
			notification.setIdbusiness_to(coment.getBusiness());
			notification.setIduser_to(coment.getBusiness().getUser());
			notificationRepository.save(notification);
			
			Funcionality fun = funcionalityRepository.findById(5).get();
			User userbus = coment.getBusiness().getUser();			
			userbus.setPtosUser(userbus.getPtosUser() + fun.getPoint_plus());
			userRepository.save(us);
			
			 Client cli = clientReposity.findbyBusiness(coment.getBusiness().getIdbusiness(),iduser);
	    	 if(cli == null)
	    	 {
	    		 Client newclient = new Client();
	    		 newclient.setBusiness(coment.getBusiness());
	    		 newclient.setIduser(us);
	    		 clientReposity.save(newclient);
	    	 }
	    	 
	        return comentRepository.save(newcomment);
	    }
}
