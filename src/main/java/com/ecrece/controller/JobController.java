package com.ecrece.controller;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Job;
import com.ecrece.repository.JobRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class JobController {	
	   
	    @Autowired
	    private JobRepository jobRepository;	   
		        
	   
		@GetMapping("/job")
	    public  List<Job> getJobs() {
			List<Job> jobs = jobRepository.findAll().stream()
					.filter(job -> job.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return jobs;	    
			}
		@GetMapping("/job/user/{iduser}")
	    public  List<Job> getJobsbyUser(@PathVariable(value = "iduser") Integer iduser) {
			Date today = new Date(System.currentTimeMillis());
			List<Job> jobs = jobRepository.findAll().stream()
					.filter(job -> job.getIs_deleted().equals(false) && job.getBusiness().getUser().getIduser().equals(iduser) && job.getDateend().compareTo(today)== 1 )
					.collect(Collectors.toList());
			return jobs;	    
			}
		@GetMapping("/job/offer")
	    public  List<Job> getJobOffer() {
			Date today = new Date(System.currentTimeMillis());
			List<Job> jobs = jobRepository.findAll().stream()
					.filter(job -> job.getIs_deleted().equals(false) && job.getDateend().compareTo(today)== 1 )
					.collect(Collectors.toList());
			return jobs;	    
			}
		@GetMapping("/job/business/{idbusiness}")
	    public  List<Job> getJobsbyBusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
			Date today = new Date(System.currentTimeMillis());
			List<Job> jobs = jobRepository.findAll().stream()
					.filter(job -> job.getIs_deleted().equals(false) && job.getBusiness().getIdbusiness().equals(idbusiness) && job.getDateend().compareTo(today)== 1)
					.collect(Collectors.toList());
			return jobs;	    
			}

	    @GetMapping("/job/{idjob}")
	    public Job getJob(@PathVariable(value = "idjob") Integer idjob) {
	        return jobRepository.findById(idjob)
	                .orElseThrow(() -> new ResourceNotFoundException("Job"));
	    }
    
	    @PostMapping("/job")
	    public Job createJob(@Valid @RequestBody Job job) {
	    	Job newjob = job;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	newjob.setCreate_at(tstamp);
	    	newjob.setLast_update(tstamp.getTime());
	    	newjob.setIs_deleted(false);
	        return jobRepository.save(newjob);
	    }

	    @PutMapping("/job/{idjob}")
	    public Job updateJob(@PathVariable Integer idjob,
	                                   @Valid @RequestBody Job jobRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return jobRepository.findById(idjob)
	                .map(job -> {
	                	job.setBusiness(jobRequest.getBusiness());
	                	job.setDateend(jobRequest.getDateend());
	                	job.setDescjob(jobRequest.getDescjob());
	                	job.setSex(job.getSex());
	                	job.setMaxage(job.getMaxage());
	                	job.setMinage(job.getMinage());
	                	job.setPhonejob(jobRequest.getPhonejob());
	                	job.setSalaryjob(jobRequest.getSalaryjob());
	                	job.setSchedulejob(jobRequest.getSchedulejob());
	                	job.setTitlejob(jobRequest.getTitlejob());
	                	job.setSchedulejob(jobRequest.getSchedulejob());
	                	job.setSchoollevel(jobRequest.getSchoollevel());
	                	job.setContracttype(jobRequest.getContracttype());
	                	job.setOtherrequirement(jobRequest.getOtherrequirement());
	                	job.setProfessionalExperience(jobRequest.getProfessionalExperience());
	                	job.setLast_update(tstamp.getTime());	                	
	                    return jobRepository.save(job);
	                }).orElseThrow(() -> new ResourceNotFoundException("Job not found with id " + idjob));
	    }


	    @DeleteMapping("/job/{idjob}")
	    public ResponseEntity<?> deleteJob(@PathVariable Integer idjob) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	return jobRepository.findById(idjob)
	                .map(jobitem -> {
	                	jobitem.setLast_update(tstamp.getTime());
	                	jobitem.setIs_deleted(true);
	                	jobRepository.save(jobitem);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Job not found with id " + idjob));
	    }

}
