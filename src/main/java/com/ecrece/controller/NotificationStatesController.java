package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.NotificationState;
import com.ecrece.repository.NotificationStatesRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class NotificationStatesController {	

	    @Autowired
	    private NotificationStatesRepository notificationStatesRepository;
	    
	        
	    @GetMapping("/notificationStates")
	    public  List<NotificationState> getNotificationStates() {
	    	List<NotificationState> notistate = notificationStatesRepository.findAll().stream()
					.filter(nstate -> nstate.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return notistate;	
	    }

	    @GetMapping("/notificationStates/{idnotificationstate}")
	    public NotificationState getNotificationStates(@PathVariable(value = "idnotificationstate") Integer idnotificationstate) {
	        return notificationStatesRepository.findById(idnotificationstate)
	                .orElseThrow(() -> new ResourceNotFoundException("Notification States not found"));
	    }
	    
	    @PostMapping("/notificationStates")
	    public NotificationState createNotificationState(@Valid @RequestBody NotificationState notificationState) {
	    	NotificationState nstate = notificationState;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	nstate.setCreate_at(tstamp);
	    	nstate.setLast_update(tstamp.getTime());
	    	nstate.setIs_deleted(false);
	        return notificationStatesRepository.save(nstate);
	    }

	    @PutMapping("/notificationStates/{idnotificationstate}")
	    public NotificationState updateNotificationState(@PathVariable Integer idnotificationstate,
	                                   @Valid @RequestBody NotificationState notificationStateRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return notificationStatesRepository.findById(idnotificationstate)
	                .map(notificationState -> {
	                	notificationState.setNameNotificationstate(notificationStateRequest.getNameNotificationstate());
	                	notificationState.setDescNotificationstate(notificationStateRequest.getDescNotificationstate());
	                	notificationState.setLast_update(tstamp.getTime());
	                    return notificationStatesRepository.save(notificationState);
	                }).orElseThrow(() -> new ResourceNotFoundException("NotificationState not found with id " + idnotificationstate));
	    }

	    @DeleteMapping("/notificationStates/{iduser_state}")
	    public ResponseEntity<?> deleteNotificationState(@PathVariable Integer iduser_state) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return notificationStatesRepository.findById(iduser_state)
	                .map(NotificationState -> {
	                	NotificationState.setLast_update(tstamp.getTime());
	                	NotificationState.setIs_deleted(true);
	                	notificationStatesRepository.save(NotificationState);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("NotificationState not found with id " + iduser_state));
	    }

}
