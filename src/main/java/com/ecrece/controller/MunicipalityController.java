package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Municipality;
import com.ecrece.model.Province;
import com.ecrece.repository.MunicipalityRepository;
import com.ecrece.repository.ProvinceRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class MunicipalityController {
	
		private Province province;
		

	    @Autowired
	    private MunicipalityRepository municipalityRepository;
	    
	    @Autowired
	    private ProvinceRepository provinceRepository;
	    
	    @PersistenceContext
	    private EntityManager em;
	        
	    @GetMapping("/municipality")
	    public  List<Municipality> geMunicipality() {
	    	List<Municipality> muni = municipalityRepository.findAll().stream()
					.filter(mun -> mun.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return muni;
	    }

	    @GetMapping("/municipality/{idmunicipality}")
	    public Municipality getMunicipality(@PathVariable(value = "idmunicipality") Integer idmunicipality) {
	        return municipalityRepository.findById(idmunicipality)
	                .orElseThrow(() -> new ResourceNotFoundException("Municipality not found"+ idmunicipality));
	    }
	    
	    @GetMapping("/municipality/province/{idprovince}")
	    public List<Municipality> getListMunicipality(@PathVariable(value = "idprovince") Integer idprovince) {
	        return municipalityRepository.findByProvince(idprovince);
	    }
	    
	    
	    @PostMapping("provinces/{idprovince}/municipality")
	    public Municipality createMunicipality(@PathVariable (value = "idprovince") Integer idprovince,
	                                 @Valid @RequestBody Municipality municipal) {
	        Municipality municipality = municipal;
        	municipality.setProvince(provinceRepository.findById(idprovince).get());
	        Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        municipality.setCreate_at(tstamp);
	        municipality.setLast_update(tstamp.getTime());
	        municipality.setIs_deleted(false);
	        
	        return municipalityRepository.save(municipality);
	    }

	    @PutMapping("provinces/{idprovince}/municipality/{idmunicipality}")
	    public Municipality updateMunicipality(@PathVariable (value = "idprovince") Integer idprovince,
	                                 @PathVariable (value = "idmunicipality") Integer idmunicipality,
	                                 @Valid @RequestBody Municipality MunicipalityRequest) {
	    	
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        if(!provinceRepository.existsById(idprovince)) {
	            throw new ResourceNotFoundException("Province "+"id"+idprovince);
	        }
	                
	        return municipalityRepository.findById(idmunicipality).map(municipality -> {
	        	municipality.setName_municipality(MunicipalityRequest.getName_municipality());
//	        	obteniedo la province inicio
	        	provinceRepository.findById(idprovince).map(province -> {
	 	            this.province=province;
	 	            return province;
	 	        }).orElseThrow(() -> new ResourceNotFoundException("Province "+"id"+idprovince));
//	        	fin
	        	municipality.setProvince(province);
	        	municipality.setLast_update(tstamp.getTime());
	            return municipalityRepository.save(municipality);
	        }).orElseThrow(() -> new ResourceNotFoundException("Municipality" + "id"+ idmunicipality));
	    }


	    @DeleteMapping("/municipality/{idmunicipality}")
	    public ResponseEntity<?> deleteSubcategoria(@PathVariable(value = "idmunicipality") Integer idmunicipality) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return municipalityRepository.findById(idmunicipality)
	                .map(municipa -> {
	                	municipa.setLast_update(tstamp.getTime());
	                	municipa.setIs_deleted(true);
	                	municipalityRepository.save(municipa);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Municiplity not found with id " + municipalityRepository));
	    }
}
