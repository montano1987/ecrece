package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.UsersStates;
import com.ecrece.repository.UsersStatesRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class UsersStatesController {	

	    @Autowired
	    private UsersStatesRepository usersStatesRepository;
	    
	
	        
	    @GetMapping("/usersStates")
	    public  List<UsersStates> getUsersStates() {
	    	List<UsersStates> ustate = usersStatesRepository.findAll().stream()
					.filter(userstate -> userstate.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return ustate;	    
	    }

	    @GetMapping("/usersStates/{iduser_state}")
	    public UsersStates getUsersStates(@PathVariable(value = "iduser_state") Integer iduser_state) {
	        return usersStatesRepository.findById(iduser_state)
	                .orElseThrow(() -> new ResourceNotFoundException("UsersStates not found"));
	    }
	    
	    @PostMapping("/usersStates")
	    public UsersStates createUsersStates(@Valid @RequestBody UsersStates usersStates) {
	    	UsersStates ustate = usersStates;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	ustate.setCreate_at(tstamp);
	    	ustate.setLast_update(tstamp.getTime());
	    	ustate.setIs_deleted(false);
	        return usersStatesRepository.save(ustate);
	    }

	    @PutMapping("/usersStates/{iduser_state}")
	    public UsersStates updateUsersStates(@PathVariable Integer iduser_state,
	                                   @Valid @RequestBody UsersStates usersStatesRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return usersStatesRepository.findById(iduser_state)
	                .map(usersStates -> {
	                	usersStates.setName_user_state(usersStatesRequest.getName_user_state());
	                	usersStates.setLast_update(tstamp.getTime());
	                    return usersStatesRepository.save(usersStates);
	                }).orElseThrow(() -> new ResourceNotFoundException("UsersStates not found with id " + iduser_state));
	    }


	    @DeleteMapping("/usersStates/{iduser_state}")
	    public ResponseEntity<?> deleteUsersStates(@PathVariable Integer iduser_state) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return usersStatesRepository.findById(iduser_state)
	                .map(usersStates -> {
	                	usersStates.setLast_update(tstamp.getTime());
	                	usersStates.setIs_deleted(true);
	                	usersStatesRepository.save(usersStates);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("UsersStates not found with id " + iduser_state));
	    }

}
