package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Business;
import com.ecrece.model.Client;
import com.ecrece.model.ClientNotification;
import com.ecrece.model.ModDir;
import com.ecrece.model.Notification;
import com.ecrece.model.NotificationState;
import com.ecrece.model.NotificationType;
import com.ecrece.model.Offer;
import com.ecrece.model.Promotion;
import com.ecrece.model.User;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.ClientReposity;
import com.ecrece.repository.NotificationRepository;
import com.ecrece.repository.NotificationStatesRepository;
import com.ecrece.repository.NotificationTypeRepository;
import com.ecrece.repository.UserRepository;
import com.ecrece.util.SendMail;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class NotificationController {	

	    @Autowired
	    private NotificationRepository notificationRepository;	    
	
	    @Autowired
	    private NotificationTypeRepository notificationtypeRepository;	
	    
	    @Autowired
	    private NotificationStatesRepository notificationStatesRepository;	
	    
	    @Autowired
	    private UserRepository userRepository;	
	   
	    @Autowired
	    private BusinessRepository businessRepository;
	    
	    @Autowired
	    private ClientReposity clientReposity;
	    
	    @GetMapping("/notification/user/{iduser}")
	    public  List<Notification> getNotificationbyuser(@PathVariable(value = "iduser") Integer iduser) {
	    	
	    	User user = userRepository.findById(iduser).get();	    	
	    	if (user.getRole().getDesc_rol().equals("ROLE_ADMIN")) {
	    		 List<Notification> usernotification  = notificationRepository.findallformoderator(iduser);
	    		 return usernotification;
			}
	    	else
	    	{
	    		List<Notification> usernotification = notificationRepository.findallforuser(iduser);
	    		return usernotification;
	    	}
	    	
	    }
	    
	    @GetMapping("/Notification/pointrequest/{idnotification}")
	    public Integer ConfirmarSolicitudPuntos(@PathVariable(value = "idnotification") Integer idnotification) throws Exception {
	      
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	Notification oldnoti =  notificationRepository.findById(idnotification).get();
	      
	      if( oldnoti.getIdpointpackage()!=null) {
	    		        	
	        	 List<NotificationState> notificationstateold = notificationStatesRepository.findAll();
	 		  	  Optional<NotificationState> notistateold = notificationstateold.stream()
	 		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
	 		            .findFirst();
	 		  	  	if (notistateold.isPresent()) {
	 		  	  	oldnoti.setNotification_state(notistateold.get());
	 		  	  	oldnoti.setLast_update(tstamp.getTime());
	 		  	  	notificationRepository.save(oldnoti);
	 				}
	 		  	  	else
	 		  	  	{
	 		  	  		NotificationState notistate2 = new NotificationState();
	 		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
	 		  	  	    notistate2.setNameNotificationstate("Aceptada");	 		  	  	    
	 		  	  		notificationStatesRepository.save(notistate2);
	 		  	  		oldnoti.setNotification_state(notistate2);
	 		  	  		oldnoti.setLast_update(tstamp.getTime());
	 		  	  		notificationRepository.save(oldnoti);
	 		  	  		
	 		  	  	}
	 		  	  	
	 		  	  	User us = oldnoti.getUsernotification();
	 		  	  	us.setPtosUser(us.getPtosUser()+oldnoti.getIdpointpackage().getPoint_value());
	 		  	  	us.setLast_update(tstamp.getTime());
	 		  	  	userRepository.save(us);
	 		  	  	Integer ptos = us.getPtosUser();
	 		  	  	
//	 		  	  	nueva notificacion
	        	Notification newnotif = new Notification();
	        	newnotif.setRead(false);
	        	newnotif.setFor_notification(oldnoti.getUsernotification().getNameUser());
	        	newnotif.setSubject_notification("Solicitud de puntos aceptada");
	        	newnotif.setCreate_date(new Date());
	        	newnotif.setDesc_notification("La solicitud de paquete de "+oldnoti.getIdpointpackage().getPoint_value()+" puntos del usuario " +oldnoti.getUsernotification().getNameUser()+" ha sido procesada. Presione el botón para actualizar su puntuación.");
	 		  	  List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
	 		  	  Optional<NotificationType> notitype = notificationtypes.stream()
	   	            .filter(p -> p.getDesc_notification_type().equals("Confirmación"))
	   	            .findFirst();
	 		  	  	if (notitype.isPresent()) {
	 		  	  	newnotif.setNotificationsType(notitype.get());
	 				}
	 		  	  	else
	 		  	  	{
	 		  	  		NotificationType notitype2 = new NotificationType();
	 		  	  		notitype2.setDesc_notification_type("Confirmación");
	 		  	  		notificationtypeRepository.save(notitype2);
	 		  	  		newnotif.setNotificationsType(notitype2);
	 		  	  	}
	 		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
	 		  	  Optional<NotificationState> notistate = notificationstate.stream()
	 		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
	 		            .findFirst();
	 		  	  	if (notistate.isPresent()) {
	 		  	  	newnotif.setNotification_state(notistate.get());
	 				}
	 		  	  	else
	 		  	  	{
	 		  	  		NotificationState notistate2 = new NotificationState();
	 		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
	 		  	  	    notistate2.setNameNotificationstate("Aceptada");
	 		  	  		notificationStatesRepository.save(notistate2);
	 		  	  		newnotif.setNotification_state(notistate2);
	 		  	  		
	 		  	  	}	
	 		  	newnotif.setUsernotification(null);
	 		  	newnotif.setIduser_to(oldnoti.getUsernotification());
	 		  	newnotif.setCreate_at(tstamp);
	 		  	newnotif.setLast_update(tstamp.getTime());
	 		  	newnotif.setIs_deleted(false);
	 		  	notificationRepository.save(newnotif);
	 		  	return ptos;
	        }
	      return oldnoti.getUsernotification().getPtosUser();
	      
	    }
	    
	    @GetMapping("/Notification/pointrequest/reject/{idnotification}")
	    public void RechazarSolicitudPuntos(@PathVariable(value = "idnotification") Integer idnotification) throws Exception {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	Notification oldnoti =  notificationRepository.findById(idnotification).get();
	      
	      if( oldnoti.getIdpointpackage()!=null) {
	    		        	
	        	 List<NotificationState> notificationstateold = notificationStatesRepository.findAll();
	 		  	  Optional<NotificationState> notistateold = notificationstateold.stream()
	 		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
	 		            .findFirst();
	 		  	  	if (notistateold.isPresent()) {
	 		  	  	oldnoti.setNotification_state(notistateold.get());
	 		  	  	oldnoti.setLast_update(tstamp.getTime());
	 		  	  	notificationRepository.save(oldnoti);
	 				}
	 		  	  	else
	 		  	  	{
	 		  	  		NotificationState notistate2 = new NotificationState();
	 		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
	 		  	  	    notistate2.setNameNotificationstate("Aceptada");
	 		  	  		notificationStatesRepository.save(notistate2);
	 		  	  		oldnoti.setNotification_state(notistate2);
	 		  	  		oldnoti.setLast_update(tstamp.getTime());
	 		  	  		notificationRepository.save(oldnoti);
	 		  	  		
	 		  	  	}
	 		  	  	
//	 		  	  	nueva notificacion
	        	Notification newnotif = new Notification();
	        	newnotif.setRead(false);
	        	newnotif.setFor_notification(oldnoti.getUsernotification().getNameUser());
	        	newnotif.setSubject_notification("Solicitud de puntos rechaza");
	        	newnotif.setCreate_date(new Date());
	        	newnotif.setDesc_notification("La solicitud de paquete de "+oldnoti.getIdpointpackage().getPoint_value()+" puntos del usuario " +oldnoti.getUsernotification().getNameUser()+" ha sido rechazada. Disculpe las molestias ocacionadas.");
	 		  	  List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
	 		  	  Optional<NotificationType> notitype = notificationtypes.stream()
	   	            .filter(p -> p.getDesc_notification_type().equals("Confirmación"))
	   	            .findFirst();
	 		  	  	if (notitype.isPresent()) {
	 		  	  	newnotif.setNotificationsType(notitype.get());
	 				}
	 		  	  	else
	 		  	  	{
	 		  	  		NotificationType notitype2 = new NotificationType();
	 		  	  		notitype2.setDesc_notification_type("Confirmación");
	 		  	  		notificationtypeRepository.save(notitype2);
	 		  	  		newnotif.setNotificationsType(notitype2);
	 		  	  	}
	 		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
	 		  	  Optional<NotificationState> notistate = notificationstate.stream()
	 		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
	 		            .findFirst();
	 		  	  	if (notistate.isPresent()) {
	 		  	  	newnotif.setNotification_state(notistate.get());
	 				}
	 		  	  	else
	 		  	  	{
	 		  	  		NotificationState notistate2 = new NotificationState();
	 		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
	 		  	  	    notistate2.setNameNotificationstate("Aceptada");
	 		  	  		notificationStatesRepository.save(notistate2);
	 		  	  		newnotif.setNotification_state(notistate2);
	 		  	  		
	 		  	  	}	
	 		  	newnotif.setUsernotification(null);
	 		  	newnotif.setIduser_to(oldnoti.getUsernotification());
	 		  	newnotif.setCreate_at(tstamp);
	 		  	newnotif.setLast_update(tstamp.getTime());
	 		  	newnotif.setIs_deleted(false);
	 		  	notificationRepository.save(newnotif);
	        }
	      
	    }
	    
	    
	    @GetMapping("/Notification/{idnotification_form}")
	    public Notification getNotification(@PathVariable(value = "idnotification_form") Integer idnotification_form) {
	        return notificationRepository.findById(idnotification_form)
	                .orElseThrow(() -> new ResourceNotFoundException("Notification not found"));
	    }
	    @GetMapping("/Notification/foruser/{iduser}")
	    public List<Notification> getNotificationUser(@PathVariable(value = "iduser") Integer iduser) {
	       User us = userRepository.findById(iduser).get();
	       List<Notification> listnoti = new ArrayList<Notification>();
	       if(us.getRole().getIdrol() == 3)
	       {
	    	 listnoti = notificationRepository.getallnotificationbyuseradmin(iduser);  
	       }
	       else
	       {
	    	   listnoti = notificationRepository.getallnotificationbyuser(iduser);   
	       }
	    	
	    	return listnoti;
	    }
	    @PostMapping("/notification/read/{idnotification}")
	    public void putNotificationRead(@PathVariable(value = "idnotification") Integer idnotification) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	       Notification noti = notificationRepository.findById(idnotification).get();
	       noti.setRead(true);
	       noti.setLast_update(tstamp.getTime());
	       notificationRepository.save(noti);
	    }
	    @PostMapping("/Notification/noticlient/user/{iduser}")
	    public void sendnoticlient(@PathVariable(value = "iduser") Integer iduser, @Valid @RequestBody ClientNotification clientNotification) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	List<Client> clients = new ArrayList<>();
	    	User usernotification= userRepository.findById(iduser).get();
	    	if(clientNotification.getTonitification() == 1) {
	    		List<Business> listbusi = businessRepository.findnBusinessbyUser(iduser);
		    	for (Business business : listbusi) {
		    		List<Client> tempclient = new ArrayList<>();
		    		tempclient = clientReposity.findClientbyBusiness(business.getIdbusiness());
		    		for (Client clie : tempclient) {
		    			clients.add(clie);
					}
				}
		    	
		    	for (Client client : clients) {
		// 		  	  	nueva notificacion
		        	Notification newnotif = new Notification();
		        	newnotif.setRead(false);
		        	newnotif.setSubject_notification(clientNotification.getSubject_notification());
		        	newnotif.setCreate_date(new Date());
		        	newnotif.setDesc_notification(clientNotification.getDesc_notification());
		 		  	  List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		 		  	  Optional<NotificationType> notitype = notificationtypes.stream()
		   	            .filter(p -> p.getDesc_notification_type().equals("Informativa"))
		   	            .findFirst();
		 		  	  	if (notitype.isPresent()) {
		 		  	  	newnotif.setNotificationsType(notitype.get());
		 				}
		 		  	  	else
		 		  	  	{
		 		  	  		NotificationType notitype2 = new NotificationType();
		 		  	  		notitype2.setDesc_notification_type("Informativa");
		 		  	  		notificationtypeRepository.save(notitype2);
		 		  	  		newnotif.setNotificationsType(notitype2);
		 		  	  	}
		 		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		 		  	  Optional<NotificationState> notistate = notificationstate.stream()
		 		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
		 		            .findFirst();
		 		  	  	if (notistate.isPresent()) {
		 		  	  	newnotif.setNotification_state(notistate.get());
		 				}
		 		  	  	else
		 		  	  	{
		 		  	  		NotificationState notistate2 = new NotificationState();
		 		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
		 		  	  	    notistate2.setNameNotificationstate("Aceptada");
		 		  	  		notificationStatesRepository.save(notistate2);
		 		  	  		newnotif.setNotification_state(notistate2);	 		  	  		
		 		  	  	}	
		 		  	newnotif.setUsernotification(usernotification);
		 		  	newnotif.setIduser_to(client.getIduser());
		 		  	newnotif.setCreate_at(tstamp);
		 		  	newnotif.setLast_update(tstamp.getTime());
		 		  	newnotif.setIs_deleted(false);		 		  	
		 		  	notificationRepository.save(newnotif);
				}
	    	}
	    	else
	    	{
	    		List<User> userconnet = userRepository.getUserConnet();
	    		for (User user : userconnet) {
	    			// 		  	  	nueva notificacion
	    			        	Notification newnotif = new Notification();
	    			        	newnotif.setRead(false);
	    			        	newnotif.setSubject_notification(clientNotification.getSubject_notification());
	    			        	newnotif.setCreate_date(new Date());
	    			        	newnotif.setDesc_notification(clientNotification.getDesc_notification());
	    			 		  	  List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
	    			 		  	  Optional<NotificationType> notitype = notificationtypes.stream()
	    			   	            .filter(p -> p.getDesc_notification_type().equals("Informativa"))
	    			   	            .findFirst();
	    			 		  	  	if (notitype.isPresent()) {
	    			 		  	  	newnotif.setNotificationsType(notitype.get());
	    			 				}
	    			 		  	  	else
	    			 		  	  	{
	    			 		  	  		NotificationType notitype2 = new NotificationType();
	    			 		  	  		notitype2.setDesc_notification_type("Informativa");
	    			 		  	  		notificationtypeRepository.save(notitype2);
	    			 		  	  		newnotif.setNotificationsType(notitype2);
	    			 		  	  	}
	    			 		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
	    			 		  	  Optional<NotificationState> notistate = notificationstate.stream()
	    			 		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
	    			 		            .findFirst();
	    			 		  	  	if (notistate.isPresent()) {
	    			 		  	  	newnotif.setNotification_state(notistate.get());
	    			 				}
	    			 		  	  	else
	    			 		  	  	{
	    			 		  	  		NotificationState notistate2 = new NotificationState();
	    			 		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
	    			 		  	  	    notistate2.setNameNotificationstate("Aceptada");
	    			 		  	  		notificationStatesRepository.save(notistate2);
	    			 		  	  		newnotif.setNotification_state(notistate2);	 		  	  		
	    			 		  	  	}	
	    			 		  	newnotif.setUsernotification(usernotification);
	    			 		  	newnotif.setIduser_to(user);
	    			 		  	newnotif.setCreate_at(tstamp);
	    			 		  	newnotif.setLast_update(tstamp.getTime());
	    			 		  	newnotif.setIs_deleted(false);	
	    			 		  	notificationRepository.save(newnotif);
	    					}
	    	}
	    	
	    	
	    	
	    }
	    
	    @GetMapping("/notification/admin/all/{iduser}")
	    public List<Notification> getallNotification(@PathVariable(value = "iduser") Integer iduser) {
	        return notificationRepository.findallorder(iduser);
	    }
	    @GetMapping("/notification/business/{idbusiness}")
	    public List<Notification> getallNotificationbyBusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	        return notificationRepository.findallforbusiness(idbusiness);
	    }
	    
	    @PostMapping("/Notification/offerforall")
	    public void offerforall(@Valid @RequestBody Offer offer) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        Notification notification = new  Notification();
	        notification.setRead(false);
	        notification.setCreate_date(new Date());
	        notification.setDesc_notification(offer.getDescOffer());
	        notification.setFor_notification("Nueva Oferta");
	        notification.setSubject_notification("Nueva Oferta");
	        
	        List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		  	  Optional<NotificationType> notitype = notificationtypes.stream()
 	            .filter(p -> p.getDesc_notification_type().equals("Oferta"))
 	            .findFirst();
		  	  	if (notitype.isPresent()) {
		  	  	notification.setNotificationsType(notitype.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationType notitype2 = new NotificationType();
		  	  		notitype2.setDesc_notification_type("Oferta");
		  	  		notificationtypeRepository.save(notitype2);
		  	  		notification.setNotificationsType(notitype2);
		  	  	}
		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		  	  Optional<NotificationState> notistate = notificationstate.stream()
		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
		            .findFirst();
		  	  	if (notistate.isPresent()) {
		  	  	notification.setNotification_state(notistate.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationState notistate2 = new NotificationState();
		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
		  	  	    notistate2.setNameNotificationstate("Aceptada");
		  	  		notificationStatesRepository.save(notistate2);
		  	  		notification.setNotification_state(notistate2);
		  	  		
		  	  	}
		  	  	notification.setOffer(offer);		  	
		  	  notification.setCreate_at(tstamp);
		  	  notification.setLast_update(tstamp.getTime());
		  	  notification.setIs_deleted(false);
	        notificationRepository.save(notification);
	    }
	    
	    @PostMapping("/Notification/promotionforall")
	    public void promoforall(@Valid @RequestBody Promotion Promotion) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        Notification notification = new  Notification();
	        notification.setRead(false);
	        notification.setCreate_date(new Date());
	        notification.setDesc_notification(Promotion.getDesc_promotion());
	        notification.setFor_notification("Nueva Promoción");
	        notification.setSubject_notification("Nueva Promoción");
	        
	        List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		  	  Optional<NotificationType> notitype = notificationtypes.stream()
 	            .filter(p -> p.getDesc_notification_type().equals("Promoción"))
 	            .findFirst();
		  	  	if (notitype.isPresent()) {
		  	  	notification.setNotificationsType(notitype.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationType notitype2 = new NotificationType();
		  	  		notitype2.setDesc_notification_type("Promoción");
		  	  		notificationtypeRepository.save(notitype2);
		  	  		notification.setNotificationsType(notitype2);
		  	  	}
		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		  	  Optional<NotificationState> notistate = notificationstate.stream()
		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
		            .findFirst();
		  	  	if (notistate.isPresent()) {
		  	  	notification.setNotification_state(notistate.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationState notistate2 = new NotificationState();
		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
		  	  	    notistate2.setNameNotificationstate("Aceptada");
		  	  		notificationStatesRepository.save(notistate2);
		  	  		notification.setNotification_state(notistate2);
		  	  		
		  	  	}
		  	  	notification.setPromotion(Promotion);
		  	  notification.setCreate_at(tstamp);
		  	  notification.setLast_update(tstamp.getTime());
		  	  notification.setIs_deleted(false);
	        notificationRepository.save(notification);
	    }
	    
	    @PostMapping("/notification/sendmailpend/business/{idbusiness}/desc/{desc}")
	    public void sendmailputpend( @PathVariable(value = "idbusiness")Integer idbusiness,
	    		@PathVariable(value = "desc")String desc,@Valid @RequestBody ModDir modDir)
	    {	    	
	      Business busi = businessRepository.findById(idbusiness).get();
	  	  
	  	  String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
	    	  		"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
	    	  		" <head> \r\n" + 
	    	  		"  <meta charset=\"UTF-8\"> \r\n" + 
	    	  		"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
	    	  		"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
	    	  		"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
	    	  		"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
	    	  		"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
	    	  		"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
	    	  		"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
	    	  		"  <!--[if !mso]><!-- --> \r\n" + 
	    	  		"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
	    	  		"  <!--<![endif]--> \r\n" + 
	    	  		"  <style type=\"text/css\">\r\n" + 
	    	  		"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
	    	  		"#outlook a {\r\n" + 
	    	  		"	padding:0;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".ExternalClass {\r\n" + 
	    	  		"	width:100%;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".ExternalClass,\r\n" + 
	    	  		".ExternalClass p,\r\n" + 
	    	  		".ExternalClass span,\r\n" + 
	    	  		".ExternalClass font,\r\n" + 
	    	  		".ExternalClass td,\r\n" + 
	    	  		".ExternalClass div {\r\n" + 
	    	  		"	line-height:100%;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-button {\r\n" + 
	    	  		"	mso-style-priority:100!important;\r\n" + 
	    	  		"	text-decoration:none!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		"a[x-apple-data-detectors] {\r\n" + 
	    	  		"	color:inherit!important;\r\n" + 
	    	  		"	text-decoration:none!important;\r\n" + 
	    	  		"	font-size:inherit!important;\r\n" + 
	    	  		"	font-family:inherit!important;\r\n" + 
	    	  		"	font-weight:inherit!important;\r\n" + 
	    	  		"	line-height:inherit!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-desk-hidden {\r\n" + 
	    	  		"	display:none;\r\n" + 
	    	  		"	float:left;\r\n" + 
	    	  		"	overflow:hidden;\r\n" + 
	    	  		"	width:0;\r\n" + 
	    	  		"	max-height:0;\r\n" + 
	    	  		"	line-height:0;\r\n" + 
	    	  		"	mso-hide:all;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-button-border:hover {\r\n" + 
	    	  		"	border-style:solid solid solid solid!important;\r\n" + 
	    	  		"	background:#d6a700!important;\r\n" + 
	    	  		"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-button-border:hover a.es-button {\r\n" + 
	    	  		"	background:#d6a700!important;\r\n" + 
	    	  		"	border-color:#d6a700!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		"</style> \r\n" + 
	    	  		" </head> \r\n" + 
	    	  		" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
	    	  		"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
	    	  		"   <!--[if gte mso 9]>\r\n" + 
	    	  		"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
	    	  		"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
	    	  		"			</v:background>\r\n" + 
	    	  		"		<![endif]--> \r\n" + 
	    	  		"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    	  		"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
	    	  		"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
	    	  		"           <div> \r\n" + 
	    	  		"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    	  		"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
	    	  		"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    	  		"                      </tr> \r\n" + 
	    	  		"                    </table></td> \r\n" + 
	    	  		"                  </tr> \r\n" + 
	    	  		"                </table></td> \r\n" + 
	    	  		"              </tr> \r\n" + 
	    	  		"            </table> \r\n" + 
	    	  		"           </div> \r\n" + 
	    	  		"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
	    	  		"         </tr> \r\n" + 
	    	  		"       </table> \r\n" + 
	    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
	    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, su negocio está en estado pendiente, debido a: "+desc+"  .Disculpe por las molestias ocacionadas.</p></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		
"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"><a href=\""+modDir.getDir()+"\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FFC80A;border-width:10px 20px 10px 20px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;\">Ver Negocio</a></span></td> \r\n" + 
"                     </tr> \r\n" + 
"                   </table></td> \r\n" + 
"                 </tr> \r\n" + 
"               </table></td> \r\n" + 
"             </tr> \r\n" + 
"           </table></td> \r\n" + 
"         </tr> \r\n" + 
	    	  		
	    	  		
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"></span></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"           </table></td> \r\n" + 
	    	  		"         </tr> \r\n" + 
	    	  		"       </table> \r\n" + 
	    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
	    	  		"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
	    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table> \r\n" + 
	    	  		"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
	    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  	
	    	  		"                       </td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table> \r\n" + 
	    	  		"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
	    	  		"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table> \r\n" + 
	    	  		"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"           </table></td> \r\n" + 
	    	  		"         </tr> \r\n" + 
	    	  		"       </table></td> \r\n" + 
	    	  		"     </tr> \r\n" + 
	    	  		"   </table> \r\n" + 
	    	  		"  </div>  \r\n" + 
	    	  		" </body>\r\n" + 
	    	  		"</html>";
	  	  SendMail email = new SendMail();	    
	  	  String subject = "Solicitud del negocio en estado pendiente.";	      
//	  	  String body = "Hola el negocio "+reser.getBusiness().getName_business()+" ha rechazado su solicitud de reservación. Disculpe por las molestias ocacionadas.";
	  	  String[] to = {busi.getUser().getMail()};
	  	  email.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);	
	    }
	    @PostMapping("/notification/business/{idbusiness}/pend/{desc}/idnotification/{idnotification}")
	    public void businesspend(@PathVariable(value = "idbusiness") Integer idbusiness,
	    		@PathVariable(value = "desc") String desc,
	    		@PathVariable(value = "idnotification") Integer idnotification) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        Business busi = businessRepository.findById(idbusiness).get();
	        Notification oldnotiNotification = notificationRepository.findById(idnotification).get();
	        
	        List<NotificationState> notificationstate1 = notificationStatesRepository.findAll();
		  	  Optional<NotificationState> notistate1 = notificationstate1.stream()
		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
		            .findFirst();
		  	  	if (notistate1.isPresent()) {
		  	  	oldnotiNotification.setNotification_state(notistate1.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationState notistate2 = new NotificationState();
		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
		  	  	    notistate2.setNameNotificationstate("Aceptada");
		  	  		notificationStatesRepository.save(notistate2);
		  	  		oldnotiNotification.setNotification_state(notistate2);		  	  		
		  	  	}
		  	  oldnotiNotification.setLast_update(tstamp.getTime());
		  	  notificationRepository.save(oldnotiNotification);
	        
	    	Notification notification = new  Notification();
	    	notification.setRead(false);
	        notification.setCreate_date(new Date());
	        notification.setDesc_notification("Motivo: "+desc);
	        notification.setFor_notification("Negocio Pendiente");
	        notification.setSubject_notification("Negocio:"+ busi.getName_business()+ " en estado pendiente.");
	        
	        List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		  	  Optional<NotificationType> notitype = notificationtypes.stream()
 	            .filter(p -> p.getDesc_notification_type().equals("Informativa"))
 	            .findFirst();
		  	  	if (notitype.isPresent()) {
		  	  	notification.setNotificationsType(notitype.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationType notitype2 = new NotificationType();
		  	  		notitype2.setDesc_notification_type("Informativa");
		  	  		notificationtypeRepository.save(notitype2);
		  	  		notification.setNotificationsType(notitype2);
		  	  	}
		  	 List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		  	  Optional<NotificationState> notistate = notificationstate.stream()
		            .filter(p -> p.getNameNotificationstate().equals("Aceptada"))
		            .findFirst();
		  	  	if (notistate.isPresent()) {
		  	  	notification.setNotification_state(notistate.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationState notistate2 = new NotificationState();
		  	  	    notistate2.setDescNotificationstate("En estado Aceptada");
		  	  	    notistate2.setNameNotificationstate("Aceptada");
		  	  		notificationStatesRepository.save(notistate2);
		  	  		notification.setNotification_state(notistate2);
		  	  		
		  	  	}
		  	  	notification.setIduser_to(busi.getUser());
		  	  	notification.setCreate_at(tstamp);
			  	notification.setLast_update(tstamp.getTime());
			  	notification.setIs_deleted(false);
	        notificationRepository.save(notification);
	    }
	    @PostMapping("/Notification")
	    public Notification createNotification(@Valid @RequestBody Notification notification) {
	    	Notification noti = notification;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	noti.setCreate_at(tstamp);
	    	noti.setLast_update(tstamp.getTime());
	    	noti.setIs_deleted(false);
	        return notificationRepository.save(noti);
	    }

	    @PutMapping("/Notification/{idnotification_form}")
	    public Notification updateNotification(@PathVariable Integer idnotification_form,
	                                   @Valid @RequestBody Notification NotificationRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return notificationRepository.findById(idnotification_form)
	                .map(tipo_notificacion -> {
	                	tipo_notificacion.setDesc_notification(NotificationRequest.getDesc_notification());
	                	tipo_notificacion.setLast_update(tstamp.getTime());
	                    return notificationRepository.save(tipo_notificacion);
	                }).orElseThrow(() -> new ResourceNotFoundException("notificacion not found with id " + idnotification_form));
	    }


	    @DeleteMapping("/Notification/{idnotification_form}")
	    public ResponseEntity<?> deletetypeNotification(@PathVariable Integer idnotification_form) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return notificationRepository.findById(idnotification_form)
	                .map(tipo_notificacion -> {
	                	tipo_notificacion.setLast_update(tstamp.getTime());
	                	tipo_notificacion.setIs_deleted(true);
	                	notificationRepository.save(tipo_notificacion);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("notificacion not found with id " + idnotification_form));
	    }

}
