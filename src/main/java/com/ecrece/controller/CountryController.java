package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Country;
import com.ecrece.repository.CountryRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class CountryController {	
	   
	    @Autowired
	    private CountryRepository countryRepository;	   
		        
	   
		@GetMapping("/countries")
	    public  List<Country> getCountries() {
			List<Country> con = countryRepository.findAll().stream()
					.filter(country -> country.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return con;	    
			}

	    @GetMapping("/countries/{idcountry}")
	    public Country getCountry(@PathVariable(value = "idcountry") Integer idcountry) {
	        return countryRepository.findById(idcountry)
	                .orElseThrow(() -> new ResourceNotFoundException("Country"));
	    }
	    @GetMapping("/countries/findall/{descountry}")
	    public List<Country> findforall(@PathVariable(value = "descountry") String descountry) {
	        return countryRepository.findforall(descountry);
	    }
	    
	    @PostMapping("/countries")
	    public Country createCountry(@Valid @RequestBody Country country) {
	    	Country cou = country;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	cou.setCreate_at(tstamp);
	    	cou.setLast_update(tstamp.getTime());
	    	cou.setIs_deleted(false);
	        return countryRepository.save(country);
	    }

	    @PutMapping("/countries/{idcountry}")
	    public Country updateCountry(@PathVariable Integer idcountry,
	                                   @Valid @RequestBody Country countryRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return countryRepository.findById(idcountry)
	                .map(country -> {
	                	country.setName_country(countryRequest.getName_country());
	                	country.setLast_update(tstamp.getTime());
	                    return countryRepository.save(country);
	                }).orElseThrow(() -> new ResourceNotFoundException("Country not found with id " + idcountry));
	    }


	    @DeleteMapping("/countries/{idcountry}")
	    public ResponseEntity<?> deleteCountry(@PathVariable Integer idcountry) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return countryRepository.findById(idcountry)
	                .map(country -> {
	                	country.setLast_update(tstamp.getTime());
	                	country.setIs_deleted(true);
	                	countryRepository.save(country);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Country not found with id " + idcountry));
	    }

}
