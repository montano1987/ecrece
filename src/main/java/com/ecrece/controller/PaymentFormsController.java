package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.PaymentForms;
import com.ecrece.repository.PaymentFormsRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class PaymentFormsController {	

	    @Autowired
	    private PaymentFormsRepository paymentFormsRepository;
	    
	
	        
	    @GetMapping("/paymentForms")
	    public  List<PaymentForms> getPaymentForms() {
	    	List<PaymentForms> pform = paymentFormsRepository.findAll().stream()
					.filter(paymentf -> paymentf.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return pform;	 
	    }

	    @GetMapping("/paymentForms/{idpayment_form}")
	    public PaymentForms getPaymentForms(@PathVariable(value = "idpayment_form") Integer idpayment_form) {
	        return paymentFormsRepository.findById(idpayment_form)
	                .orElseThrow(() -> new ResourceNotFoundException("PaymentForms"));
	    }
	    
	    @PostMapping("/paymentForms")
	    public PaymentForms createPaymentForms(@Valid @RequestBody PaymentForms paymentForms) {
	    	PaymentForms pform = paymentForms;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	pform.setCreate_at(tstamp);
	    	pform.setLast_update(tstamp.getTime());
	    	pform.setIs_deleted(false);
	        return paymentFormsRepository.save(pform);
	    }

	    @PutMapping("/paymentForms/{idpayment_form}")
	    public PaymentForms updatPaymentForms(@PathVariable Integer idpayment_form,
	                                   @Valid @RequestBody PaymentForms paymentFormsRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return paymentFormsRepository.findById(idpayment_form)
	                .map(paymentForms -> {
	                	paymentForms.setName_payment_form(paymentFormsRequest.getName_payment_form());
	                	paymentForms.setDesc_payment_form(paymentFormsRequest.getDesc_payment_form());
	                	paymentForms.setLast_update(tstamp.getTime());
	                    return paymentFormsRepository.save(paymentForms);
	                }).orElseThrow(() -> new ResourceNotFoundException("paymentForms not found with id " + idpayment_form));
	    }


	    @DeleteMapping("/paymentForms/{idpayment_form}")
	    public ResponseEntity<?> deletPaymentForms(@PathVariable Integer idpayment_form) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return paymentFormsRepository.findById(idpayment_form)
	                .map(paymentForms -> {
	                	paymentForms.setLast_update(tstamp.getTime());
	                	paymentForms.setIs_deleted(true);
	                	paymentFormsRepository.save(paymentForms);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("paymentForms not found with id " + idpayment_form));
	    }

}
