package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Promotion;
import com.ecrece.repository.PromotionRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class PromotionController {	

	    @Autowired
	    private PromotionRepository promotionRepository;	    
	
	   // @Autowired
	    //private promotionTypeRepository promotiontypeRepository;	
	  
	    @GetMapping("/promotionsall")
	    public  List<Promotion> getAll() {
	    	List<Promotion> prom = promotionRepository.findAll().stream()
					.filter(promo -> promo.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return prom;	
	    }
	    @GetMapping("/promotions")
	    public  List<Promotion> getPromotions() {
	        return promotionRepository.findPromotionActive();
	    }
	    @GetMapping("/promotions/user/{iduser}")
	    public  List<Promotion> getPromotionsbuUser(@PathVariable(value = "iduser") Integer iduser) {
	    	List<Promotion> promo = promotionRepository.findPromotionbyUser(iduser);
	        return promo;
	    }
	    @GetMapping("/promotions/useradmin")
	    public  List<Promotion> getPromotionsbuUserAdmin() {
	    	List<Promotion> promo = promotionRepository.findPromotionbyUserAdmin();
	        return promo;
	    }
	    @GetMapping("/promotion/business/{idbusiness}")
	    public  List<Promotion> getoffersbybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	        return promotionRepository.findAllbybusiness(idbusiness);
	    }
	    
	    @GetMapping("/promotion/details/{idpromotion}")
	    public Promotion getpromotion(@PathVariable(value = "idpromotion") Integer idpromotion) {
	        return promotionRepository.findById(idpromotion)
	                .orElseThrow(() -> new ResourceNotFoundException("Promotion"));
	    }
	   
	    
	    @GetMapping("/promotion/{idpromotion_form}")
	    public Promotion getPromotion(@PathVariable(value = "idpromotion_form") Integer idpromotion_form) {
	        return promotionRepository.findById(idpromotion_form)
	                .orElseThrow(() -> new ResourceNotFoundException("Promotion not found"));
	    }
	    @GetMapping("/promotions/business/{idbusiness}")
	    public List<Promotion> getlistbybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	        return promotionRepository.findpromotionbybusiness(idbusiness);
	    }
	    	    
	    @PostMapping("/promotion")
	    public Promotion createPromotion(@Valid @RequestBody Promotion promotion) {
	    	Promotion promo = promotion;	    	
	    	if(promotion.getBusiness()==null)
	    	{ 		
		    	promo.setBusiness(null);
	    	}
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	promo.setCreate_at(tstamp);
	    	promo.setLast_update(tstamp.getTime());
	    	promo.setIs_deleted(false);
	        return promotionRepository.save(promo);
	    }
	  
	    @PutMapping("/promotion/{idpromotion}")
	    public Promotion updatePromotion(@PathVariable (value = "idpromotion") Integer idpromotion,
	                                   @Valid @RequestBody Promotion PromotionRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return promotionRepository.findById(idpromotion)
	                .map(promo -> {
	                	promo.setCodePromotion(PromotionRequest.getCodePromotion());
	                	promo.setDate_end_promotion(PromotionRequest.getDate_end_promotion());
	                	promo.setDate_start_promotion(PromotionRequest.getDate_start_promotion());
	                	promo.setDaysPromotion(PromotionRequest.getDaysPromotion());
	                	promo.setDesc_promotion(PromotionRequest.getDesc_promotion());
	                	promo.setTitle_promotion(PromotionRequest.getTitle_promotion());
	                	promo.setLast_update(tstamp.getTime());
	                    return promotionRepository.save(promo);
	                }).orElseThrow(() -> new ResourceNotFoundException("Promotion not found with id " + idpromotion));
	    }


	    @DeleteMapping("/promotion/{idpromotion}")
	    public ResponseEntity<?> deletePromotion(@PathVariable (value = "idpromotion") Integer idpromotion) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return promotionRepository.findById(idpromotion)
	                .map(promo -> {
	                	promo.setLast_update(tstamp.getTime());
	                	promo.setIs_deleted(true);
	                	promotionRepository.save(promo);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Promo not found with id " + idpromotion));
	    }

}
