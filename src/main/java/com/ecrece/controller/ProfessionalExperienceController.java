package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.ProfessionalExperience;
import com.ecrece.repository.ProfessionalExperienceRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ProfessionalExperienceController {	
	   
	    @Autowired
	    private ProfessionalExperienceRepository professionalExperienceRepository;	   
		        
	   
		@GetMapping("professionalexperience")
	    public  List<ProfessionalExperience> getProfessionalExperience() {
			List<ProfessionalExperience> ProfessionalExperiences = professionalExperienceRepository.findAll().stream()
					.filter(professionalExperience -> professionalExperience.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return ProfessionalExperiences;	    
			}
	
	    @GetMapping("/professionalexperience/{idprofessionalexperience}")
	    public ProfessionalExperience getProfessionalExperience(@PathVariable(value = "idprofessionalexperience") Integer idprofessionalexperience) {
	        return professionalExperienceRepository.findById(idprofessionalexperience)
	                .orElseThrow(() -> new ResourceNotFoundException("ProfessionalExperience"));
	    }
    
	    @PostMapping("/professionalexperience")
	    public ProfessionalExperience createProfessionalExperience(@Valid @RequestBody ProfessionalExperience professionalExperience) {
	    	ProfessionalExperience newProfessionalExperience = professionalExperience;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	newProfessionalExperience.setCreate_at(tstamp);
	    	newProfessionalExperience.setLast_update(tstamp.getTime());
	    	newProfessionalExperience.setIs_deleted(false);
	        return professionalExperienceRepository.save(newProfessionalExperience);
	    }

	    @PutMapping("/professionalexperience/{idprofessionalexperience}")
	    public ProfessionalExperience updateProfessionalExperience(@PathVariable(value = "idprofessionalexperience") Integer idprofessionalexperience,
	                                   @Valid @RequestBody ProfessionalExperience professionalExperience) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return professionalExperienceRepository.findById(idprofessionalexperience)
	                .map(professionalexpe -> {
	                	professionalexpe.setDesc_professionalexperience(professionalExperience.getDesc_professionalexperience());
	                	professionalexpe.setLast_update(tstamp.getTime());
	                    return professionalExperienceRepository.save(professionalexpe);
	                }).orElseThrow(() -> new ResourceNotFoundException("ProfessionalExperience not found with id " + idprofessionalexperience));
	    }
	    
	    @DeleteMapping("/professionalexperience/{idprofessionalexperience}")
	    public ResponseEntity<?> deleteJob(@PathVariable(value = "idprofessionalexperience") Integer idprofessionalexperience) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	return professionalExperienceRepository.findById(idprofessionalexperience)
	                .map(professionalExperience -> {
	                	professionalExperience.setLast_update(tstamp.getTime());
	                	professionalExperience.setIs_deleted(true);
	                	professionalExperienceRepository.save(professionalExperience);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("ProfessionalExperience not found with id " + idprofessionalexperience));
	    }

}
