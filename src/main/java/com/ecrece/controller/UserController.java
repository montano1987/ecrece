package com.ecrece.controller;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Configuration;
import com.ecrece.model.Funcionality;
import com.ecrece.model.Invite;
import com.ecrece.model.ModDir;
import com.ecrece.model.Notification;
import com.ecrece.model.NotificationState;
import com.ecrece.model.NotificationType;
import com.ecrece.model.Payment;
import com.ecrece.model.PaymentForms;
import com.ecrece.model.PointPackage;
import com.ecrece.model.PointUsers;
import com.ecrece.model.Role;
import com.ecrece.model.User;
import com.ecrece.model.UserLogin;
import com.ecrece.model.UserReturn;
import com.ecrece.model.UsersStates;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.ConfigurationRepository;
import com.ecrece.repository.FuncionalityRepository;
import com.ecrece.repository.NotificationRepository;
import com.ecrece.repository.NotificationStatesRepository;
import com.ecrece.repository.NotificationTypeRepository;
import com.ecrece.repository.PaymentFormsRepository;
import com.ecrece.repository.PaymentRepository;
import com.ecrece.repository.PointPackageRepository;
import com.ecrece.repository.RolesRepository;
import com.ecrece.repository.UserRepository;
import com.ecrece.repository.UsersStatesRepository;
import com.ecrece.util.SendMail;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class UserController {
	
		private User user;
		
		@Autowired
	    private UserRepository userRepository;
	    
	    @Autowired
	    private NotificationRepository notificationRepository;
	    
	    @Autowired
	    private NotificationTypeRepository notificationtypeRepository;
	    
	    @Autowired
	    private PointPackageRepository pointPackageRepository;
	    
	    @Autowired
	    private PaymentFormsRepository paymentFormsRepository;
	    
	    @Autowired
	    private PaymentRepository paymentRepository;
	    @Autowired
	    private BusinessRepository businessRepository;
	    @Autowired
	    private NotificationStatesRepository notificationStatesRepository;
	    @Autowired
	    private RolesRepository rolesRepository;
	    
	    @Autowired
	    private ConfigurationRepository configurationRepository;
	    
	    @Autowired
	    private UsersStatesRepository usersStatesRepository;
	    
	    @Autowired
	    private FuncionalityRepository funcionalityRepository;
	    
	    @GetMapping("/users")
	    public  List<User> getUser() {
	    	List<User> us = userRepository.findAll().stream()
					.filter(use -> use.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return us;	  
	    }
	    
	    @PostMapping("/users/{iduser}/changeuserstate/{activateuser}")
	    public  void changeuserstateUser(@PathVariable(value = "iduser") Integer iduser,
	    									@PathVariable(value = "activateuser") Boolean activateuser) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	User us = userRepository.findById(iduser).get();
	    	if(activateuser)
	    	{	    		
	    		us.setUserState(usersStatesRepository.findById(1).get());
	    		us.setLast_update(tstamp.getTime());
	    		userRepository.save(us);
	    	}
	    	else
	    	{
	    		us.setUserState(usersStatesRepository.findById(2).get());
	    		us.setLast_update(tstamp.getTime());
	    		userRepository.save(us);
	    	}	    	
	    }
	    
	    @PostMapping("/users/forgotpass/{email}")
	    public User sendmailforgotpass(@PathVariable(value = "email") String email) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	  List<User> users = userRepository.findAll();
		  	  Optional<User> userto = users.stream()
	            .filter(p -> p.getMail().equals(email) && p.getUserState().getIduser_state() == 1)
	            .findFirst();
			  	if (userto.isPresent()) {
			  		User us = userto.get();
			  		String token = getJWTToken(us.getNameUser(),us);
			  		us.setToken(token);	
			  		us.setLast_update(tstamp.getTime());
			  		return userRepository.save(us);
					}
			  	else
			  	{
			  		User us = new User();
			  		return us;
			  	}	    	
	    }
	    
	    @PostMapping("/users/{iduser}/sendmailwithtoken")
	    public void sendmailwithtoken(@PathVariable(value = "iduser") Integer iduser, @Valid @RequestBody ModDir modDir) {	    	
	    	  User us = userRepository.findById(iduser).get();
	    	  String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
	    	  		"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
	    	  		" <head> \r\n" + 
	    	  		"  <meta charset=\"UTF-8\"> \r\n" + 
	    	  		"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
	    	  		"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
	    	  		"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
	    	  		"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
	    	  		"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
	    	  		"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
	    	  		"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
	    	  		"  <!--[if !mso]><!-- --> \r\n" + 
	    	  		"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
	    	  		"  <!--<![endif]--> \r\n" + 
	    	  		"  <style type=\"text/css\">\r\n" + 
	    	  		"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
	    	  		"#outlook a {\r\n" + 
	    	  		"	padding:0;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".ExternalClass {\r\n" + 
	    	  		"	width:100%;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".ExternalClass,\r\n" + 
	    	  		".ExternalClass p,\r\n" + 
	    	  		".ExternalClass span,\r\n" + 
	    	  		".ExternalClass font,\r\n" + 
	    	  		".ExternalClass td,\r\n" + 
	    	  		".ExternalClass div {\r\n" + 
	    	  		"	line-height:100%;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-button {\r\n" + 
	    	  		"	mso-style-priority:100!important;\r\n" + 
	    	  		"	text-decoration:none!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		"a[x-apple-data-detectors] {\r\n" + 
	    	  		"	color:inherit!important;\r\n" + 
	    	  		"	text-decoration:none!important;\r\n" + 
	    	  		"	font-size:inherit!important;\r\n" + 
	    	  		"	font-family:inherit!important;\r\n" + 
	    	  		"	font-weight:inherit!important;\r\n" + 
	    	  		"	line-height:inherit!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-desk-hidden {\r\n" + 
	    	  		"	display:none;\r\n" + 
	    	  		"	float:left;\r\n" + 
	    	  		"	overflow:hidden;\r\n" + 
	    	  		"	width:0;\r\n" + 
	    	  		"	max-height:0;\r\n" + 
	    	  		"	line-height:0;\r\n" + 
	    	  		"	mso-hide:all;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-button-border:hover {\r\n" + 
	    	  		"	border-style:solid solid solid solid!important;\r\n" + 
	    	  		"	background:#d6a700!important;\r\n" + 
	    	  		"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		".es-button-border:hover a.es-button {\r\n" + 
	    	  		"	background:#d6a700!important;\r\n" + 
	    	  		"	border-color:#d6a700!important;\r\n" + 
	    	  		"}\r\n" + 
	    	  		"</style> \r\n" + 
	    	  		" </head> \r\n" + 
	    	  		" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
	    	  		"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
	    	  		"   <!--[if gte mso 9]>\r\n" + 
	    	  		"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
	    	  		"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
	    	  		"			</v:background>\r\n" + 
	    	  		"		<![endif]--> \r\n" + 
	    	  		"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    	  		"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
	    	  		"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
	    	  		"           <div> \r\n" + 
	    	  		"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    	  		"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
	    	  		"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    	  		"                      </tr> \r\n" + 
	    	  		"                    </table></td> \r\n" + 
	    	  		"                  </tr> \r\n" + 
	    	  		"                </table></td> \r\n" + 
	    	  		"              </tr> \r\n" + 
	    	  		"            </table> \r\n" + 
	    	  		"           </div> \r\n" + 
	    	  		"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
	    	  		"         </tr> \r\n" + 
	    	  		"       </table> \r\n" + 
	    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
	    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, "+us.getNameUser()+" haga click en el botón para cambiar la contraseña.</p></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"><a href=\""+modDir.getDir()+us.getToken()+"\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FFC80A;border-width:10px 20px 10px 20px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;\">Cambiar Contraseña</a></span></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"           </table></td> \r\n" + 
	    	  		"         </tr> \r\n" + 
	    	  		"       </table> \r\n" + 
	    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
	    	  		"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
	    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table> \r\n" + 
	    	  		"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
	    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		 
	    	  		"                       </td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table> \r\n" + 
	    	  		"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
	    	  		"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table> \r\n" + 
	    	  		"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
	    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    	  		"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    	  		"                     </tr> \r\n" + 
	    	  		"                   </table></td> \r\n" + 
	    	  		"                 </tr> \r\n" + 
	    	  		"               </table></td> \r\n" + 
	    	  		"             </tr> \r\n" + 
	    	  		"           </table></td> \r\n" + 
	    	  		"         </tr> \r\n" + 
	    	  		"       </table></td> \r\n" + 
	    	  		"     </tr> \r\n" + 
	    	  		"   </table> \r\n" + 
	    	  		"  </div>  \r\n" + 
	    	  		" </body>\r\n" + 
	    	  		"</html>";
	    	  SendMail mail = new SendMail();	    
		      String subject = "Nueva contraseña en Ecrece";		      
//		      String body = "Hola "+us.getNameUser() +" haga click en esta dirección para crear una nueva contraseña en Ecrece "+modDir.getDir()+us.getToken();
		      String[] to = {us.getMail()};
		      mail.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);
	    	  
	    }
	    
	    @PostMapping("/users/newpass/{newpass}")
	    public User newpass(@PathVariable(value = "newpass") String newpass, @Valid @RequestBody User user) {	    	
	    	User us = user;
	    	us.setPassword(DigestUtils.md5DigestAsHex(newpass.getBytes()));	 
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	us.setLast_update(tstamp.getTime());
	    	return userRepository.save(us);
	    }
	    
	    @GetMapping("/users/{iduser}")
	    public User getUser(@PathVariable(value = "iduser") Integer iduser) {
	        return userRepository.findById(iduser)
	                .orElseThrow(() -> new ResourceNotFoundException("User not found"+ iduser));
	    }
	    @PostMapping("/user/{iduser}/idnewrol/{idnewrol}")
	    public void changerol(@PathVariable(value = "iduser") Integer iduser,
	    		@PathVariable(value = "idnewrol") Integer idnewrol) {
	    	User us = userRepository.findById(iduser).get();
	    	us.setRole(rolesRepository.findById(idnewrol).get());
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	us.setLast_update(tstamp.getTime());
	    	userRepository.save(us);
	    }
	    
	    @PostMapping("/user/profile/{iduser}")
	    public User saveProfile(@PathVariable(value = "iduser") Integer iduser,
	    					@Valid @RequestBody User userrequest) {
	       User old = userRepository.findById(iduser)
	                .orElseThrow(() -> new ResourceNotFoundException("User not found"+ iduser));
	     
	       old.setName(userrequest.getName());
	       old.setSex(userrequest.getSex());
	       old.setDate_bith(userrequest.getDate_bith());
	       old.setPhoneUser(userrequest.getPhoneUser());
	       old.setSurnameUser(userrequest.getSurnameUser());
	       old.setSurname2_user(userrequest.getSurname2_user());
	       old.setReceiveoffer(userrequest.getReceiveoffer());
	       old.setReceivepromotions(userrequest.getReceivepromotions());
	       Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	       old.setLast_update(tstamp.getTime());
	       return userRepository.save(old);
	    }
	    
	    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    public String generateCode(int length) {
	        Random random = new Random();
	        StringBuilder builder = new StringBuilder(length);

	        for (int i = 0; i < length; i++) {
	            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
	        }

	        return builder.toString();
	    }
	    
	    @PostMapping("/users/rol/{idrol}")
	    public User createUser(@PathVariable(value = "idrol") Integer idrol,
	    					@Valid @RequestBody User user) {
	        this.user = user;	        
	        this.user.setCode(generateCode(6));
	        this.user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
	        this.user.setDateCreateUser(new Date());
	        this.user.setReceiveoffer(true);
	        this.user.setReceivepromotions(true);
	        
	        List<UsersStates> userstates = usersStatesRepository.findAll();
		  	  Optional<UsersStates> usert = userstates.stream()
	            .filter(p -> p.getName_user_state().equals("Pendiente"))
	            .findFirst();
		  	  	if (usert.isPresent()) {
		  	  	this.user.setUserState(usert.get());
				}
	        
	        List<Configuration> conf =  configurationRepository.findAll();
	        this.user.setPtosUser(conf.get(0).getPtos_star());
	        
	        List<Role> roles = rolesRepository.findAll();
		  	  Optional<Role> rol = roles.stream()
	            .filter(p -> p.getIdrol().equals(idrol))
	            .findFirst();
		  	  	if (rol.isPresent()) {
		  	  	this.user.setRole(rol.get());
				}
		  	  	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		  	  this.user.setCreate_at(tstamp);
		  	this.user.setLast_update(tstamp.getTime());
		  	this.user.setIs_deleted(false);
	        User newuser =	userRepository.save(this.user);       
	      	
	     NotificationGeneration(newuser, "Moderador", "Creación Usuario", "Se ha creado el usuario "+newuser.getNameUser(), "Informativa", null, null, null, null, "Informativo", null, null, null);
		    return newuser;
	    }
	    @PostMapping("/users/{iduser}/language/{language}")
	    public User changelanguage(@PathVariable(value = "iduser") Integer iduser, @PathVariable(value = "language") String language ) {
	    	
	    	User us = userRepository.findById(iduser).get();
	    	us.setLanguage(language);
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	us.setLast_update(tstamp.getTime());
	    	return userRepository.save(us);
	    }
	    @PostMapping("/users/sendmailregistration")
	    public void sendmailregistration( @Valid @RequestBody User user) throws IOException, NotFoundException, ChecksumException, FormatException {
	    	ApplicationHome home = new ApplicationHome(this.getClass()); 
//	    	String ruta = home.getDir().toString().replace("com\\ecrece\\controller","")+File.separator+"static"+File.separator+"img"+File.separator+"ecrecelogo.jpg";
//	    	InputStream qrInputStream = new FileInputStream(ruta);
//	        BufferedImage qrBufferedImage = ImageIO.read(qrInputStream);
//
//	        LuminanceSource source = new BufferedImageLuminanceSource(qrBufferedImage);
//	        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//	        Reader reader = new MultiFormatReader();
//	        Result stringBarCode = reader.decode(bitmap);
//
//	         stringBarCode.getText();
	    	
	         
	         File file = new File(home.getDir().toString().replace("com\\ecrece\\controller","")+File.separator+"static"+File.separator+"img"+File.separator+"ecrecelogo.jpg");

		        Path path = Paths.get(file.getAbsolutePath());
		        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
		        
		        byte[] encoded = Base64.getEncoder().encode(resource.getByteArray());
		        
	    		String tobody = "<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
	    				" <head> \r\n" + 
	    				"  <meta charset=\"UTF-8\"> \r\n" + 
	    				"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
	    				"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
	    				"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
	    				"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
	    				"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" +  
	    				"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
	    				"  <!--<![endif]--> \r\n" + 
	    				"  <style type=\"text/css\">\r\n" + 
	    				"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
	    				"#outlook a {\r\n" + 
	    				"	padding:0;\r\n" + 
	    				"}\r\n" + 
	    				".ExternalClass {\r\n" + 
	    				"	width:100%;\r\n" + 
	    				"}\r\n" + 
	    				".ExternalClass,\r\n" + 
	    				".ExternalClass p,\r\n" + 
	    				".ExternalClass span,\r\n" + 
	    				".ExternalClass font,\r\n" + 
	    				".ExternalClass td,\r\n" + 
	    				".ExternalClass div {\r\n" + 
	    				"	line-height:100%;\r\n" + 
	    				"}\r\n" + 
	    				".es-button {\r\n" + 
	    				"	mso-style-priority:100!important;\r\n" + 
	    				"	text-decoration:none!important;\r\n" + 
	    				"}\r\n" + 
	    				"a[x-apple-data-detectors] {\r\n" + 
	    				"	color:inherit!important;\r\n" + 
	    				"	text-decoration:none!important;\r\n" + 
	    				"	font-size:inherit!important;\r\n" + 
	    				"	font-family:inherit!important;\r\n" + 
	    				"	font-weight:inherit!important;\r\n" + 
	    				"	line-height:inherit!important;\r\n" + 
	    				"}\r\n" + 
	    				".es-desk-hidden {\r\n" + 
	    				"	display:none;\r\n" + 
	    				"	float:left;\r\n" + 
	    				"	overflow:hidden;\r\n" + 
	    				"	width:0;\r\n" + 
	    				"	max-height:0;\r\n" + 
	    				"	line-height:0;\r\n" + 
	    				"	mso-hide:all;\r\n" + 
	    				"}\r\n" + 
	    				".es-button-border:hover {\r\n" + 
	    				"	border-style:solid solid solid solid!important;\r\n" + 
	    				"	background:#d6a700!important;\r\n" + 
	    				"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
	    				"}\r\n" + 
	    				".es-button-border:hover a.es-button {\r\n" + 
	    				"	background:#d6a700!important;\r\n" + 
	    				"	border-color:#d6a700!important;\r\n" + 
	    				"}\r\n" + 
	    				"</style> \r\n" + 
	    				" </head> \r\n" + 
	    				" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
	    				"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
	    				"   <!--[if gte mso 9]>\r\n" + 
	    				"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
	    				"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
	    				"			</v:background>\r\n" + 
	    				"		<![endif]--> \r\n" + 
	    				"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    				"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    				"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
	    				"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src="+encoded+" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
	    				"           <div> \r\n" + 
	    				"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    				"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
	    				"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    				"                      </tr> \r\n" + 
	    				"                    </table></td> \r\n" + 
	    				"                  </tr> \r\n" + 
	    				"                </table></td> \r\n" + 
	    				"              </tr> \r\n" + 
	    				"            </table> \r\n" + 
	    				"           </div> \r\n" + 
	    				"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
	    				"         </tr> \r\n" + 
	    				"       </table> \r\n" + 
	    				"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
	    				"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
	    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"center\" style=\"padding:0;Margin:0;\"><h1 style=\\\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\\\">eCrece</h1></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table></td> \r\n" + 
	    				"             </tr> \r\n" + 
	    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">"+user.getNameUser()+" está a un paso de activar su cuenta de usuario en el sistema Ecrece. * ATENCION * Este enlace estará disponible durante 12 horas luego de las cuales no podrás utilizarlo. Te sugerimos activar antes que concluya el plazo.</p><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Solo le falta precionar el botón.</p></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table></td> \r\n" + 
	    				"             </tr> \r\n" + 
	    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"><a href="+user.getDir()+user.getCode()+" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FFC80A;border-width:10px 20px 10px 20px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;\">Activar Usuario</a></span></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table></td> \r\n" + 
	    				"             </tr> \r\n" + 
	    				"           </table></td> \r\n" + 
	    				"         </tr> \r\n" + 
	    				"       </table> \r\n" + 
	    				"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
	    				"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
	    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
	    				"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
	    				"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table> \r\n" + 
	    				"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
	    				"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				 
	    				"                       </td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table> \r\n" + 
	    				"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
	    				"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table> \r\n" + 
	    				"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
	    				"             </tr> \r\n" + 
	    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
	    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	    				"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
	    				"                     </tr> \r\n" + 
	    				"                   </table></td> \r\n" + 
	    				"                 </tr> \r\n" + 
	    				"               </table></td> \r\n" + 
	    				"             </tr> \r\n" + 
	    				"           </table></td> \r\n" + 
	    				"         </tr> \r\n" + 
	    				"       </table></td> \r\n" + 
	    				"     </tr> \r\n" + 
	    				"   </table> \r\n" + 
	    				"  </div>  \r\n" + 
	    				" </body>\r\n" + 
	    				"</html>";
	    	SendMail email = new SendMail();	    
		      String subject = "Registro en Ecrece";		      
//		      String body = " Hola "+user.getNameUser()+" haga click en esta dirección para confirmar su registro en Ecrece "+user.getDir()+user.getCode();
		      String body = tobody;
		      String[] to = {user.getMail()};
		      email.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, body);		   
	    }
	     
	    public void NotificationGeneration(User usernotification, String para, String asunto, String descripcion, 
	    		String tiponotificacion, Integer iduserto, Integer idbusinesto, Integer idbusinesnotificatio, 
	    		PointUsers idpointuser, String notifistate, Integer idpaymentform, PointPackage idpointpackage, User userto)
	    {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	Notification notification = new Notification();
	    	notification.setRead(false);
	  	    notification.setFor_notification(para);
	  	    notification.setSubject_notification(asunto);
	  	    notification.setCreate_date(new Date());
	  	    notification.setDesc_notification(descripcion);
		  	  List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		  	  Optional<NotificationType> notitype = notificationtypes.stream()
  	            .filter(p -> p.getDesc_notification_type().equals(tiponotificacion))
  	            .findFirst();
		  	  	if (notitype.isPresent()) {
		  	  		notification.setNotificationsType(notitype.get());
				}
		  	  	else
		  	  	{
		  	  		NotificationType notitype2 = new NotificationType();
		  	  		notitype2.setDesc_notification_type(tiponotificacion);
		  	  		notificationtypeRepository.save(notitype2);
		  	  		notification.setNotificationsType(notitype2);
		  	  	}
		  	  	
		  	  List<NotificationState> notificationstate = notificationStatesRepository.findAll();
 		  	  Optional<NotificationState> notistate = notificationstate.stream()
 		            .filter(p -> p.getNameNotificationstate().equals(notifistate))
 		            .findFirst();
 		  	  	if (notistate.isPresent()) {
 		  	  	notification.setNotification_state(notistate.get());
 				}
 		  	  	else
 		  	  	{
 		  	  		NotificationState notistate2 = new NotificationState();
 		  	  	    notistate2.setDescNotificationstate("En estado "+notifistate);
 		  	  	    notistate2.setNameNotificationstate(notifistate);
 		  	  		notificationStatesRepository.save(notistate2);
 		  	  	notification.setNotification_state(notistate2);
 		  	  		
 		  	  	}
		  	  	
		  	  	notification.setIdpointuser(idpointuser);
		  	  	
		  	  	if(idpointpackage!=null)
		  	  	{
		  	  	notification.setIdpointpackage(idpointpackage);
		  	  	}
		  	  	else {
		  	  	notification.setIdpointpackage(null);
		  	  	}
		  	  	if(usernotification!=null)
		  	  	{
		  	  	notification.setUsernotification(usernotification);
		  	  	}
		  	  	else {
		  	  	notification.setUsernotification(null);
		  	  	}
		  	  	if (iduserto!=null) {
		  	  	notification.setIduser_to(userRepository.findById(iduserto).get());
				} else {
					notification.setIduser_to(null);
				}
		  	  	if (idbusinesto!=null) {
		  	  	notification.setIdbusiness_to(businessRepository.findById(idbusinesto).get());
				} else {
					notification.setIdbusiness_to(null);
				}
		  	  	if (idbusinesnotificatio!=null) {
		  	  	notification.setIdbusiness_fk(businessRepository.findById(idbusinesnotificatio).get());
				} else {
					notification.setIdbusiness_fk(null);
				}
		  	  if (idpaymentform!=null) {
		  	  	notification.setIdpayment_form(paymentFormsRepository.findById(idpaymentform).get());
				} else {
					notification.setIdpayment_form(null);
				}
		  	if (userto!=null) {
		  	  	notification.setIduser_to(userto);
				} else {
					notification.setIduser_to(userto);
				}
		  	notification.setCreate_at(tstamp);
		  	notification.setLast_update(tstamp.getTime());
		  	notification.setIs_deleted(false);
	  	    
	  	notificationRepository.save(notification);
	    }
	    
	    @PostMapping("/activationcode/{activationcode}")
	    public User Activationcode(@PathVariable(value = "activationcode") String activationcode) {
	    	User us = userRepository.finduserforactivation(activationcode);    	   
	  	   if(us != null)
	  	   {
	  		   return us;
	  	   }
	  	   else {
	  		   return new User();
	  	   } 
	    }
	    @PostMapping("/activeuser")
	    public User ActivateUser(@Valid @RequestBody User userrequest) {
	    	
	    	
    	  User us = userrequest;
    	  List<UsersStates> userstates = usersStatesRepository.findAll();
	  	  Optional<UsersStates> usert = userstates.stream()
            .filter(p -> p.getName_user_state().equals("Activo"))
            .findFirst();
	  	  	if (usert.isPresent()) {
	  	  	us.setUserState(usert.get());
			}
    	  us.setActive(true);
    	  Timestamp tstamp = new Timestamp(System.currentTimeMillis());
    	  us.setLast_update(tstamp.getTime());
    	 return userRepository.save(us);
	    }
	    @GetMapping("/users/exist/{nameUser}/email/{mail}")
	    public Boolean ExistUser(@PathVariable(value = "nameUser") String nameUser,
	    						@PathVariable(value = "mail") String mail) {
	   
	    	boolean exit= false;	    	    		
	    List<User> users = userRepository.findAll();
	    Optional<User> profilo = users.stream()
	            .filter(p -> p.getNameUser().equals(nameUser) || p.getMail().equals(mail))
	            .findFirst();
	        if (profilo.isPresent())
	        	{
	        		return exit = true;
	        	}
	        else
	        	{
	        	
	        	 return exit;
	        	}
	    }
	    
	    @PostMapping("/asigpointuser/{iduser}/package/{idpackage}")
	    public void asigpointuser( @PathVariable (value = "iduser") Integer iduser,
	    					@PathVariable (value = "idpackage") Integer idpackage) {	    	
	    	
	    	PointPackage pointpak= pointPackageRepository.findById(idpackage).get();
	    	User user = userRepository.findById(iduser).get();
	    	user.setPtosUser(user.getPtosUser()+pointpak.getPoint_value());
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	user.setLast_update(tstamp.getTime());
	    	userRepository.save(user);
	    }
	    
	    
	    
	    
	    @PutMapping("/users/{iduser}")
	    public User updateUser( @PathVariable (value = "iduser") Integer iduser,
	                                 @Valid @RequestBody User UserRequest) {
	    	
	    
	        return userRepository.findById(iduser).map(user -> {
	        	user.setDate_bith(UserRequest.getDate_bith());
	        	user.setMail(UserRequest.getMail());
	        	user.setName(UserRequest.getName());
	        	user.setNameUser(UserRequest.getNameUser());
	        	user.setPhoneUser(UserRequest.getPhoneUser());
	        	user.setSex(UserRequest.getSex());
	        	user.setSurname2_user(UserRequest.getSurname2_user());
	        	user.setSurnameUser(UserRequest.getSurnameUser());
	        	user.setPassword(DigestUtils.md5DigestAsHex(UserRequest.getPassword().getBytes()));
	        	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        	user.setLast_update(tstamp.getTime());
	            return userRepository.save(user);
	        }).orElseThrow(() -> new ResourceNotFoundException("User" + "id"+ iduser));
	    }


	    @PutMapping("users/{iduser}/state")
	    public User cambiarestado(@PathVariable (value = "iduser") Integer iduser,
	                                 @Valid @RequestBody User UserRequest) { 
	                
	        return userRepository.findById(iduser).map(user -> {
	        	user.setActive(UserRequest.getActive());	        	
	        	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        	user.setLast_update(tstamp.getTime());
	            return userRepository.save(user);
	        }).orElseThrow(() -> new ResourceNotFoundException("User" + "id"+ iduser));
	    }
	    
	    @PostMapping("users/{iduser}/package/{id_package}")
	    public void asignarpuntosmoderador(@PathVariable (value = "iduser") Integer iduser,
					    		@PathVariable (value = "id_package") Integer id_package) {   	    	   
	        
	    	PointPackage poinpack = pointPackageRepository.findById(id_package).get();	       
	        
	    	User user = userRepository.findById(iduser).get();
	    	user.setPtosUser(user.getPtosUser()+poinpack.getPoint_value());
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	user.setLast_update(tstamp.getTime());
	    	userRepository.save(user);
	    	
	       NotificationGeneration(null,"Usuario "+user.getNameUser(),"Asiganación de Puntos","Se le adicionaron "+poinpack.getPoint_value(),
	    		   				"Confirmación",iduser,null,null, null, "Aceptada",null,null,user );
	    }
	    

		@GetMapping("/users/token/{token}")
		public User getuserbytoken(@PathVariable(value = "token") String token) {
			User us = userRepository.getuserbytoken(token);
			return us;
		}

	    @PostMapping("buypoint/{iduser}/pointpackage/{id_package}/payform/{idpayment_form}")
	    public void solicitarpuntosusuario(@PathVariable (value = "iduser") Integer iduser,
					    		@PathVariable (value = "id_package") Integer id_package,
					    		@PathVariable (value = "idpayment_form") Integer idpayment_form) {   
	    	        
	        PointPackage poinpack = pointPackageRepository.findById(id_package).get();
	       
	        if(poinpack.getId_package()!=0 )
	        {
	           User us =  userRepository.findById(iduser).get();
	 	       PaymentForms pay = paymentFormsRepository.findById(idpayment_form).get();
	 	       
	 	       Payment payme = new Payment();
	 	       payme.setState("Pendiente");
	 	       payme.setPayment_date(new Date());
	 	       payme.setPaymentForm(pay);
	 	       payme.setUser(us);
	 	      Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		 	     payme.setCreate_at(tstamp);
		 	     payme.setLast_update(tstamp.getTime());
		 	     payme.setIs_deleted(false);
	 	       paymentRepository.save(payme);
	 	       
	 	       NotificationGeneration(us,"Moderador","Solicitud de Puntos del usuario "+us.getNameUser(),
	 	    		   						"Se solicita la activación del paquete de puntos "+poinpack.getPoint_value()+" con costo de " +poinpack.getCost()+" la forma de pago será "+pay.getName_payment_form(),
	 	    		   						"Solicitud de Puntos",null,null,null,null,"Pendiente",idpayment_form,poinpack,null);

	        }
    }
	    @PostMapping("/user/and/pass")
		public UserReturn login(@Valid @RequestBody UserLogin userLogin) {
	    	UserReturn userreturn = new UserReturn();
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	System.out.print(tstamp.getTime());
	    	User user = new User();
	    	if (ExistUser(userLogin.getUsername1(),"")) {	    		
				if(userRepository.findByNameUser(userLogin.getUsername1()).getNameUser().length()!=0 && DigestUtils.md5DigestAsHex(userLogin.getPassword1().getBytes()).equals(userRepository.findByNameUser(userLogin.getUsername1()).getPassword()) && userRepository.findByNameUser(userLogin.getUsername1()).getUserState().getIduser_state() == 1)
				{
					user = userRepository.findByNameUser(userLogin.getUsername1());
					String token = getJWTToken(userLogin.getUsername1(),user);
					user.setToken(token);
					
					user.setLast_update(tstamp.getTime());
					userRepository.save(user);
					userreturn.setIduser(user.getIduser());
					userreturn.setLanguage(user.getLanguage());
					userreturn.setName(user.getName());
					userreturn.setNameUser(user.getNameUser());
					userreturn.setPtosUser(user.getPtosUser());
					userreturn.setRole(user.getRole());
					userreturn.setToken(user.getToken());
					userreturn.setUserState(user.getUserState());
					return userreturn;
				}
				else
				{
					return userreturn;
				}
			} else {
				return userreturn;
			}			
		}
	    @GetMapping("/user/iduser/{iduser}")
		public void logout(@PathVariable (value = "iduser") Integer iduser) {
	    	User user = userRepository.findById(iduser).get();
	    	user.setToken(null);
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	user.setLast_update(tstamp.getTime());
	    	userRepository.save(user);
		}
	    
	    @PostMapping("/user/oldpass/{olpass}/newpass/{newpass}/iduser/{iduser}")
		public boolean changepass(@PathVariable (value = "olpass") String olpass,
	    		@PathVariable (value = "newpass") String newpass,
	    		@PathVariable (value = "iduser") Integer iduser) {
	    	User us = userRepository.findById(iduser).get();
	    	String pass = DigestUtils.md5DigestAsHex(olpass.getBytes());
	    	if(us.getPassword().equals(pass))
	    	{
	    		String newpassinsert = DigestUtils.md5DigestAsHex(newpass.getBytes());
	    		us.setPassword(newpassinsert);
	    		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		    	user.setLast_update(tstamp.getTime());
	    		userRepository.save(us);
	    		return true;
    		}
	    	return false;
	    		    			
		}
	    @PostMapping("/user/invite")
		public void invite(@Valid @RequestBody Invite invite) {
	    	
	    	User us = userRepository.findById(invite.getIduser()).get();
	    	String nameuser = us.getNameUser();
	        
    		String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
    				"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
    				" <head> \r\n" + 
    				"  <meta charset=\"UTF-8\"> \r\n" + 
    				"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
    				"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
    				"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
    				"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
    				"  <title>Copia de Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
    				"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
    				"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
    				"  <!--[if !mso]><!-- --> \r\n" + 
    				"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
    				"  <!--<![endif]--> \r\n" + 
    				"  <style type=\"text/css\">\r\n" + 
    				"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
    				"#outlook a {\r\n" + 
    				"	padding:0;\r\n" + 
    				"}\r\n" + 
    				".ExternalClass {\r\n" + 
    				"	width:100%;\r\n" + 
    				"}\r\n" + 
    				".ExternalClass,\r\n" + 
    				".ExternalClass p,\r\n" + 
    				".ExternalClass span,\r\n" + 
    				".ExternalClass font,\r\n" + 
    				".ExternalClass td,\r\n" + 
    				".ExternalClass div {\r\n" + 
    				"	line-height:100%;\r\n" + 
    				"}\r\n" + 
    				".es-button {\r\n" + 
    				"	mso-style-priority:100!important;\r\n" + 
    				"	text-decoration:none!important;\r\n" + 
    				"}\r\n" + 
    				"a[x-apple-data-detectors] {\r\n" + 
    				"	color:inherit!important;\r\n" + 
    				"	text-decoration:none!important;\r\n" + 
    				"	font-size:inherit!important;\r\n" + 
    				"	font-family:inherit!important;\r\n" + 
    				"	font-weight:inherit!important;\r\n" + 
    				"	line-height:inherit!important;\r\n" + 
    				"}\r\n" + 
    				".es-desk-hidden {\r\n" + 
    				"	display:none;\r\n" + 
    				"	float:left;\r\n" + 
    				"	overflow:hidden;\r\n" + 
    				"	width:0;\r\n" + 
    				"	max-height:0;\r\n" + 
    				"	line-height:0;\r\n" + 
    				"	mso-hide:all;\r\n" + 
    				"}\r\n" + 
    				".es-button-border:hover {\r\n" + 
    				"	border-style:solid solid solid solid!important;\r\n" + 
    				"	background:#d6a700!important;\r\n" + 
    				"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
    				"}\r\n" + 
    				".es-button-border:hover a.es-button {\r\n" + 
    				"	background:#d6a700!important;\r\n" + 
    				"	border-color:#d6a700!important;\r\n" + 
    				"}\r\n" + 
    				"</style> \r\n" + 
    				" </head> \r\n" + 
    				" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
    				"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
    				"   <!--[if gte mso 9]>\r\n" + 
    				"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
    				"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
    				"			</v:background>\r\n" + 
    				"		<![endif]--> \r\n" + 
    				"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    				"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    				"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
    				"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
    				"           <div> \r\n" + 
    				"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    				"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
    				"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    				"                      </tr> \r\n" + 
    				"                    </table></td> \r\n" + 
    				"                  </tr> \r\n" + 
    				"                </table></td> \r\n" + 
    				"              </tr> \r\n" + 
    				"            </table> \r\n" + 
    				"           </div> \r\n" + 
    				"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
    				"         </tr> \r\n" + 
    				"       </table> \r\n" + 
    				"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
    				"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table></td> \r\n" + 
    				"             </tr> \r\n" + 
    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, el usuario "+nameuser+" te invita a visitar el sistema eCrece. \n eCrece es un sistema que facilita a los propietarios de los pequeños y medianos negocios, la interacción con clientes y la gestión de su información.\r\n" + 
    						"Los propietarios de negocios pueden interactuar con sus clientes o posibles clientes utilizando notificaciones, ofertas, promociones, comentarios y solicitudes de servicios o productos, que pueden potenciar el crecimiento del negocio.\r\n" + 
    						"Los usuarios de la aplicación pueden hacer búsquedas de negocios que puedan satisfacer sus necesidades de servicios y productos.</p><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Descripción del usuario: "+invite.getDesc_invite()+"</p></td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table></td> \r\n" + 
    				"             </tr> \r\n" + 
    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"><a href=\""+invite.getUrltoinvite()+"/"+us.getCode()+"\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FFC80A;border-width:10px 20px 10px 20px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;\">Visitar</a></span></td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table></td> \r\n" + 
    				"             </tr> \r\n" + 
    				"           </table></td> \r\n" + 
    				"         </tr> \r\n" + 
    				"       </table> \r\n" + 
    				"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    				"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
    				"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
    				"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table> \r\n" + 
    				"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
    				"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				 
    				"                      </td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table> \r\n" + 
    				"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
    				"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table> \r\n" + 
    				"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
    				"             </tr> \r\n" + 
    				"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
    				"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    				"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    				"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    				"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    				"                     </tr> \r\n" + 
    				"                   </table></td> \r\n" + 
    				"                 </tr> \r\n" + 
    				"               </table></td> \r\n" + 
    				"             </tr> \r\n" + 
    				"           </table></td> \r\n" + 
    				"         </tr> \r\n" + 
    				"       </table></td> \r\n" + 
    				"     </tr> \r\n" + 
    				"   </table> \r\n" + 
    				"  </div>  \r\n" + 
    				" </body>\r\n" + 
    				"</html>";
	    	  SendMail mail = new SendMail();	    
		      String subject = "Hola, un amigo te invita a visitar Ecrece";		      
//		      String body = "Hola el usuario "+us.getNameUser() +" de Ecrece te invita a visitar nuestro sitio \n"+ 
//		    		  		invite.getUrltoinvite()+"/"+us.getCode()+" \n "+invite.getDesc_invite();
		      String[] to = {invite.getMail()};
		      mail.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);	    			
		}
	    @PostMapping("/user/invited/{usercode}")
	  		public void inviteduser(@PathVariable (value = "usercode") String usercode) {
	  	    	
	  	    	User us = userRepository.getuserbycode(usercode);	  	    	
	  	    	if(us != null)
	  	    	{
	  	    	 Funcionality fun = funcionalityRepository.findById(8).get();
	  	    	 us.setPtosUser(us.getPtosUser()+fun.getPoint_plus());
	  	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	  	    	us.setLast_update(tstamp.getTime());
	  	    	 userRepository.save(us);
	  	    	NotificationGeneration(us,"Invitación consultada","Invitación consultada",
	   						"La invitación a sido consultada. Por favor presione el botón de los puntos para actualizar los mismos.",
	   						"Informativa",us.getIduser(),null,null,null,"Informativo",null,null,us);
	  	    	}	
	  	    	  	    			
	  		}
	    @GetMapping("/users/getpoint/{iduser}")
  		public Integer getpointuser(@PathVariable (value = "iduser") Integer iduser) {  	    	
  	    	User us = userRepository.findById(iduser).get();	  	    	
  	    	return us.getPtosUser(); 	    	  	    			
  		}
	    @GetMapping("/users/lesspoint/{iduser}/{point}")
  		public Integer lesspoint(@PathVariable (value = "iduser") Integer iduser,@PathVariable (value = "point") Integer point) {  	    	
  	    	User us = userRepository.findById(iduser).get();	  	    	
  			us.setPtosUser(us.getPtosUser() - point);
  			Timestamp tstamp = new Timestamp(System.currentTimeMillis());
  			us.setLast_update(tstamp.getTime());
  			userRepository.save(us);
  			return us.getPtosUser(); 
  		}


	    private String getJWTToken(String username,User user) {
			String secretKey = "mySecretKey";
			if (user.getRole() == null) throw new InsufficientAuthenticationException("User has no roles assigned");
			List<GrantedAuthority> grantedAuthorities = AuthorityUtils
					.commaSeparatedStringToAuthorityList("ROLE_"+user.getRole().getDesc_rol());
			
			String token = Jwts
					.builder()
					.setId("softtekJWT")
					.setSubject(username)
					.claim("authorities",
							grantedAuthorities.stream()
									.map(GrantedAuthority::getAuthority)
									.collect(Collectors.toList()))
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + 43200000))
					.signWith(SignatureAlgorithm.HS512,
							secretKey.getBytes()).compact();

			return token;
		}
	    
	    
}
