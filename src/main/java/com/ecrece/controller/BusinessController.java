/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecrece.controller;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;

import org.springframework.web.multipart.MultipartFile;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Business;
import com.ecrece.model.BusinessStates;
import com.ecrece.model.BusinessSubcategory;
import com.ecrece.model.BusinessTop;
import com.ecrece.model.Clasification;
import com.ecrece.model.Client;
import com.ecrece.model.Country;
import com.ecrece.model.Funcionality;
import com.ecrece.model.FuncionalityUserBusiness;
import com.ecrece.model.ModDir;
import com.ecrece.model.ModReport;
import com.ecrece.model.Municipality;
import com.ecrece.model.Notification;
import com.ecrece.model.NotificationState;
import com.ecrece.model.NotificationType;
import com.ecrece.model.ReplyComent;
import com.ecrece.model.Reservation;
import com.ecrece.model.ReservationCharacteristics;
import com.ecrece.model.ScheduleBusiness;
import com.ecrece.model.SearchBusiness;
import com.ecrece.model.Subcategory;
import com.ecrece.model.User;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.BusinessStatesRepository;
import com.ecrece.repository.BusinessSubcategoryRepository;
import com.ecrece.repository.ClasificationRepository;
import com.ecrece.repository.ClientReposity;
import com.ecrece.repository.FuncionalityRepository;
import com.ecrece.repository.FuncionalityUserBusinessRepository;
import com.ecrece.repository.MunicipalityRepository;
import com.ecrece.repository.NotificationRepository;
import com.ecrece.repository.NotificationStatesRepository;
import com.ecrece.repository.NotificationTypeRepository;
import com.ecrece.repository.ReplyComentReposity;
import com.ecrece.repository.ReservationCharacteristicsRepository;
import com.ecrece.repository.ReservationReposity;
import com.ecrece.repository.ScheduleBusinessRepository;
import com.ecrece.repository.SubcategoryRepository;
import com.ecrece.repository.UserRepository;
import com.ecrece.util.QRCode;
import com.ecrece.util.SendMail;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 *
 * @author disney
 */
@RestController
@CrossOrigin
@RequestMapping("/api")
public class BusinessController {	
	
	@Autowired
	private BusinessRepository businessRepository;
	
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BusinessStatesRepository businessStatesRepository;
	@Autowired
	private MunicipalityRepository municipalityRepository;
	@Autowired
	private NotificationRepository notificationRepository;
	@Autowired
	private NotificationStatesRepository notificationStatesRepository;
//	@Autowired
//	private UploadFileService uploadFileService;
	@Autowired
    private ClientReposity clientReposity;

	@Autowired
	private SubcategoryRepository subcategoryRepository;

	@Autowired
	private NotificationTypeRepository notificationtypeRepository;

	@Autowired
	private BusinessSubcategoryRepository businessSubcategoryRepository;
	@Autowired
	private FuncionalityRepository funcionalityRepository;
	@Autowired
	private FuncionalityUserBusinessRepository funcionalityUserBusinessRepository;

	@Autowired
	private ClasificationRepository clasificationRepository;

	@Autowired
	private ScheduleBusinessRepository scheduleBusinessRepository;
	
	@Autowired
	private ReplyComentReposity replyComentReposity;
	
	@Autowired
	private ReservationCharacteristicsRepository reservationCharacteristicsRepository;
	
	@Autowired
	private ReservationReposity reservationReposity;
		
	@GetMapping("/business")
	public List<Business> findAll() {
		List<Business> busi = businessRepository.findAll().stream()
				.filter(business -> business.getIs_deleted().equals(false))
				.collect(Collectors.toList());
		return busi;	 
	}

	@GetMapping("/business/user/{iduser}")
	public List<Business> findAllbyUser(@PathVariable(value = "iduser") Integer iduser) {
		return businessRepository.findnBusinessbyUser(iduser);
	}
	@GetMapping("/business/user/login/{iduser}")
	public List<Business> findAllbyUsertoLogin(@PathVariable(value = "iduser") Integer iduser) {
		return businessRepository.findnBusinessbyUsertoLogin(iduser);
	}
	@GetMapping("/business/user/{iduser}/mybusiness")
	public List<Business> findAllmybusiness(@PathVariable(value = "iduser") Integer iduser) {
		return businessRepository.findnmybusiness(iduser);
	}
	
	@GetMapping("/business/active")
	public List<Business> findnBusinessbyActive() {
		return businessRepository.findnBusinessbyActive();
	}
	
	@GetMapping("/business/{idbusiness}")
	public Business getBusiness(@PathVariable(value = "idbusiness") Integer idbusiness) throws IOException {	
		
		Business busi = businessRepository.findById(idbusiness)
				.orElseThrow(() -> new ResourceNotFoundException("Business not found"));
		busi.setSubcategory(businessSubcategoryRepository.findByBusiness(idbusiness));
        return busi;
        
	}

	@GetMapping("/business/{idbusiness}/funcionalities")
	public List<FuncionalityUserBusiness> getBusinessFuncionalities(@PathVariable(value = "idbusiness") Integer idbusiness) {
		
		List<FuncionalityUserBusiness> funuserbusi = funcionalityUserBusinessRepository.funcionalitybyBusiness(idbusiness);
		return funuserbusi;
	}
	
	@GetMapping("/business/{idbusiness}/listreservation")
	public List<Reservation> getlistreservation(@PathVariable(value = "idbusiness") Integer idbusiness) {
		List<Reservation> listreserv = businessRepository.findlistreservation(idbusiness);
		return listreserv;
	}

	@PostMapping("/business/{idbusiness}/clasification/{calification}/user/{iduser}")
	public void clasifications(@PathVariable(value = "idbusiness") Integer idbusiness,
			@PathVariable(value = "calification") Integer calification,
			@PathVariable(value = "iduser") Integer iduser) {

		User user = userRepository.findById(iduser).get();
		Business business = businessRepository.findById(idbusiness).get();
		Clasification clasif = new Clasification();
		clasif.setBusiness(business);
		clasif.setPtosclasification(calification);
		clasif.setUser(user);
		clasif.setCreateDate(new Date());
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		clasif.setCreate_at(tstamp);
		clasif.setLast_update(tstamp.getTime());
		clasif.setIs_deleted(false);
		clasificationRepository.save(clasif);
		
		User userbus = business.getUser();
		  Funcionality  func = funcionalityRepository.findById(6).get();
		  userbus.setPtosUser(userbus.getPtosUser() + func.getPoint_plus());
		  userbus.setLast_update(tstamp.getTime());
		  userRepository.save(userbus);
		  
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification("Su negocio a sido calificado por el usuario " + user.getNameUser());
		notification.setSubject_notification("Calificación de negocio");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("Se negocio se ha calificado con "+clasif.getPtosclasification()+" puntos");		
		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Informativa")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Informativa");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);
		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate1 = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Informativo")).findFirst();
		if (notistate1.isPresent()) {
			notification.setNotification_state(notistate1.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Informativo");
			notistate2.setNameNotificationstate("Informativo");
			notificationStatesRepository.save(notistate2);
			notification.setNotification_state(notistate2);

		}
		notification.setIdbusiness_to(business);
		notification.setIduser_to(business.getUser());
		notification.setUsernotification(user);
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
		
	}
	
	@GetMapping("/business/{idbusiness}/avgclasification/")
	public Double avgclasification(@PathVariable(value = "idbusiness") Integer idbusiness) {
		Double avg = 0.0;
		List<Clasification> listclasification = clasificationRepository.findbyBusiness(idbusiness);
		if(listclasification.size()>0)
		{
			for (Clasification clasification : listclasification) {
				avg += clasification.getPtosclasification();
			}
			avg = Math.round((avg / listclasification.size()) * 10d) / 10d;
		}
		
		if(avg == 0)
		{
			return 0.0;
		}

		return avg;
	}
	@GetMapping("/business/{idbusiness}/countcalification/")
	public Integer countcalification(@PathVariable(value = "idbusiness") Integer idbusiness) {		
		List<Clasification> listclasification = clasificationRepository.findbyBusiness(idbusiness);
		
		return listclasification.size();
	}
	@PostMapping("/sendmail/aceptBusiness/{idnotification}")
	public void sendmailaceptbusines( @PathVariable(value = "idnotification") Integer idnotification, @Valid @RequestBody ModDir modDir)
	{		
		Notification noti = notificationRepository.findById(idnotification).get();
		String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
    	  		"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
    	  		" <head> \r\n" + 
    	  		"  <meta charset=\"UTF-8\"> \r\n" + 
    	  		"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
    	  		"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
    	  		"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
    	  		"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
    	  		"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
    	  		"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
    	  		"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
    	  		"  <!--[if !mso]><!-- --> \r\n" + 
    	  		"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
    	  		"  <!--<![endif]--> \r\n" + 
    	  		"  <style type=\"text/css\">\r\n" + 
    	  		"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
    	  		"#outlook a {\r\n" + 
    	  		"	padding:0;\r\n" + 
    	  		"}\r\n" + 
    	  		".ExternalClass {\r\n" + 
    	  		"	width:100%;\r\n" + 
    	  		"}\r\n" + 
    	  		".ExternalClass,\r\n" + 
    	  		".ExternalClass p,\r\n" + 
    	  		".ExternalClass span,\r\n" + 
    	  		".ExternalClass font,\r\n" + 
    	  		".ExternalClass td,\r\n" + 
    	  		".ExternalClass div {\r\n" + 
    	  		"	line-height:100%;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button {\r\n" + 
    	  		"	mso-style-priority:100!important;\r\n" + 
    	  		"	text-decoration:none!important;\r\n" + 
    	  		"}\r\n" + 
    	  		"a[x-apple-data-detectors] {\r\n" + 
    	  		"	color:inherit!important;\r\n" + 
    	  		"	text-decoration:none!important;\r\n" + 
    	  		"	font-size:inherit!important;\r\n" + 
    	  		"	font-family:inherit!important;\r\n" + 
    	  		"	font-weight:inherit!important;\r\n" + 
    	  		"	line-height:inherit!important;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-desk-hidden {\r\n" + 
    	  		"	display:none;\r\n" + 
    	  		"	float:left;\r\n" + 
    	  		"	overflow:hidden;\r\n" + 
    	  		"	width:0;\r\n" + 
    	  		"	max-height:0;\r\n" + 
    	  		"	line-height:0;\r\n" + 
    	  		"	mso-hide:all;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button-border:hover {\r\n" + 
    	  		"	border-style:solid solid solid solid!important;\r\n" + 
    	  		"	background:#d6a700!important;\r\n" + 
    	  		"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button-border:hover a.es-button {\r\n" + 
    	  		"	background:#d6a700!important;\r\n" + 
    	  		"	border-color:#d6a700!important;\r\n" + 
    	  		"}\r\n" + 
    	  		"</style> \r\n" + 
    	  		" </head> \r\n" + 
    	  		" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
    	  		"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
    	  		"   <!--[if gte mso 9]>\r\n" + 
    	  		"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
    	  		"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
    	  		"			</v:background>\r\n" + 
    	  		"		<![endif]--> \r\n" + 
    	  		"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
    	  		"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
    	  		"           <div> \r\n" + 
    	  		"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
    	  		"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                      </tr> \r\n" + 
    	  		"                    </table></td> \r\n" + 
    	  		"                  </tr> \r\n" + 
    	  		"                </table></td> \r\n" + 
    	  		"              </tr> \r\n" + 
    	  		"            </table> \r\n" + 
    	  		"           </div> \r\n" + 
    	  		"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, su negocio ha sido aceptado, gracias por inscribirlo en eCrece </p></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 

	"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
	"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
	"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
	"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
	"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
	"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
	"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"><a href=\""+modDir.getDir()+"\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FFC80A;border-width:10px 20px 10px 20px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;\">Ver Negocio</a></span></td> \r\n" + 
	"                     </tr> \r\n" + 
	"                   </table></td> \r\n" + 
	"                 </tr> \r\n" + 
	"               </table></td> \r\n" + 
	"             </tr> \r\n" + 
	"           </table></td> \r\n" + 
	"         </tr> \r\n" + 
    	  		
    	  		
    	  		
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"></span></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"           </table></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
    	  		"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" +    	  		
    	  		"                       </td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"           </table></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table></td> \r\n" + 
    	  		"     </tr> \r\n" + 
    	  		"   </table> \r\n" + 
    	  		"  </div>  \r\n" + 
    	  		" </body>\r\n" + 
    	  		"</html>";
		
		SendMail email = new SendMail();	    
	  String subject = "Solicitud de Negocio Aceptado";		 
	  String[] to = {noti.getUsernotification().getMail()};
	  email.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);	
	}
	

	@GetMapping("/aceptBusiness/{idnotification}/business")
	public void acceptBusines(@PathVariable(value = "idnotification") Integer idnotification) {
		
		Business busi = notificationRepository.findById(idnotification).get().getIdbusiness_fk();
		List<BusinessStates> bustates = businessStatesRepository.findAll();
		Optional<BusinessStates> notistate = bustates.stream()
				.filter(p -> p.getName_state_business().equals("Aceptado")).findFirst();
		if (notistate.isPresent()) {
			busi.setBusinessStates(notistate.get());
		} else {
			BusinessStates bustate2 = new BusinessStates();
			bustate2.setName_state_business("Aceptado");
			businessStatesRepository.save(bustate2);
			busi.setBusinessStates(bustate2);
		}
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		busi.setLast_update(tstamp.getTime());
		businessRepository.save(busi);

		Notification oldnotification = notificationRepository.findById(idnotification).get();
		
		List<NotificationState> notificationstate1 = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate11 = notificationstate1.stream()
				.filter(p -> p.getNameNotificationstate().equals("Aceptada")).findFirst();
		if (notistate11.isPresent()) {
			oldnotification.setNotification_state(notistate11.get());
			oldnotification.setLast_update(tstamp.getTime());
			notificationRepository.save(oldnotification);
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Aceptada");
			notistate2.setNameNotificationstate("Aceptada");
			notificationStatesRepository.save(notistate2);
			oldnotification.setNotification_state(notistate2);
			oldnotification.setLast_update(tstamp.getTime());
			notificationRepository.save(oldnotification);
		}

//	  	  	se crea una notificacion de confimacion del negocio
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification("Usuario " + oldnotification.getUsernotification().getNameUser());
		notification.setSubject_notification( "Negocio Aceptado.");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("Su negocio ha sido Aceptado. Nombre del negocio: " + oldnotification.getIdbusiness_fk().getName_business()+". Dueño del negocio: "+oldnotification.getUsernotification().getNameUser());
		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Confirmación")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Confirmación");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);

		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate1 = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Aceptada")).findFirst();
		if (notistate1.isPresent()) {
			notification.setNotification_state(notistate1.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Aceptada");
			notistate2.setNameNotificationstate("Aceptada");
			notificationStatesRepository.save(notistate2);
			notification.setNotification_state(notistate2);

		}
		notification.setIdbusiness_to(busi);
		notification.setIduser_to(busi.getUser());
		notification.setLast_update(tstamp.getTime());
		notificationRepository.save(notification);

	}

	@GetMapping("/business/{idbusiness}/notification/user/{iduser}")
	public void NotificationGeneration(@PathVariable(value = "idbusiness") Integer idbusiness,
			@PathVariable(value = "iduser") Integer iduser) {
		Business busi = new Business();
		busi = businessRepository.findById(idbusiness).get();

		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification("Moderador");
		notification.setSubject_notification("Creación de Negocio con nombre " + busi.getName_business()+ ". Por favor revisar el negocio en el listado de notificaciones");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("Se ha creado un nuevo negocio");
		notification.setIdpointuser(null);
		notification.setIdpayment_form(null);
		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Creación de Negocio")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Creación de Negocio");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);

		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Pendiente")).findFirst();
		if (notistate.isPresent()) {
			notification.setNotification_state(notistate.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Pendiente");
			notistate2.setNameNotificationstate("Pendiente");
			notificationStatesRepository.save(notistate2);
			notification.setNotification_state(notistate2);

		}
		notification.setIdbusiness_fk(businessRepository.findById(idbusiness).get());
		notification.setUsernotification(userRepository.findById(iduser).get());
		notification.setIduser_to(null);
		notification.setIdbusiness_to(null);
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
	}

	@GetMapping("/business/schedule/{idbusiness}")
	public List<ScheduleBusiness> schedule(@PathVariable(value = "idbusiness") Integer idbusiness) {
		List<ScheduleBusiness> listschedule = scheduleBusinessRepository.findByBusiness(idbusiness);
		return listschedule;

	}
	
	@GetMapping("/business/top/")
	public List<BusinessTop> topbusiness() {	
		List<BusinessTop> listreturned = new ArrayList<BusinessTop>();
		List<Business> bus= businessRepository.findnBusinessbyActive();
		for (Business business : bus) {
			BusinessTop btop = new BusinessTop();
			Double avg = 0.0;
			List<Clasification> listclasification = clasificationRepository.findbyBusiness(business.getIdbusiness());
			if(listclasification.size()>0)
			{
				for (Clasification clasification : listclasification) {
					avg += clasification.getPtosclasification();
				}
				avg = Math.round((avg / listclasification.size()) * 10d) / 10d;
			}
			btop.setBusiness(business);
			btop.setAvg(avg);
			btop.setCountcalif(listclasification.size());
			listreturned.add(btop);
		}
		return listreturned;
	}
	@GetMapping("/business/favo/{iduser}")
	public List<Business> favobusiness(@PathVariable(value = "iduser") Integer iduser) {	
		return businessRepository.findfavobusiness(iduser);
	}
	@GetMapping("/business/lastsearch/{iduser}")
	public List<Business> lastsearch(@PathVariable(value = "iduser") Integer iduser) {
		List<Business> allbusi = businessRepository.findnBusinessbyActive();
		for (Business business : allbusi) {
			business.setSubcategory(businessSubcategoryRepository.findByBusiness(business.getIdbusiness()));			
		}
		List<Business> businessfound = new ArrayList<>();
		SearchBusiness ultimabusqueda = null;
		List<SearchBusiness> search = businessRepository.findlastsearch(iduser);
		if(search.size()>0)
		{
			ultimabusqueda = search.get(0);
		}
		if(ultimabusqueda != null)
		{		
			if(ultimabusqueda.getIdCategory()!=null){
				for (Business bus : allbusi) {
					for (Subcategory subcat : bus.getSubcategory()) {
						if (subcat.getCategory().getIdcategory().equals(ultimabusqueda.getIdCategory())) {
							businessfound.add(bus);
						}
					}
				}
			}		
			if(ultimabusqueda.getDesc_search()!=null)
			{
				List<Business> aux = new ArrayList<Business>();
				if(businessfound.size() > 0)
				{
					for (Business busfounded : businessfound) {
						if(busfounded.getName_business().toLowerCase().contains(ultimabusqueda.getDesc_search().toLowerCase()) 
							|| busfounded.getDesc_business().toLowerCase().contains(ultimabusqueda.getDesc_search().toLowerCase())
							|| busfounded.getDir_business().toLowerCase().contains(ultimabusqueda.getDesc_search().toLowerCase()))
						{
							aux.add(busfounded);
						}
					}
					if(aux.size()>0)
					{
						businessfound = aux;
					}
				}
				else {
					for (Business busfounded : allbusi) {
						if(busfounded.getName_business().toLowerCase().contains(ultimabusqueda.getDesc_search().toLowerCase()) 
							|| busfounded.getDesc_business().toLowerCase().contains(ultimabusqueda.getDesc_search().toLowerCase())
							|| busfounded.getDir_business().toLowerCase().contains(ultimabusqueda.getDesc_search().toLowerCase()))
						{
							aux.add(busfounded);
						}
					}
					if(aux.size()>0)
					{
						businessfound = aux;
					}
				}
			}
			if (businessfound.size()==0)
			{
				businessfound = allbusi;
			}
		}
		else {
			businessfound = allbusi;
		}
		return businessfound;
	}

	@PostMapping("/business/{idbusiness}/subcategory/{idsubcategory}")
	public void Addsubcategorybusiness(@PathVariable(value = "idbusiness") Integer idbusiness,
			@PathVariable(value = "idsubcategory") Integer idsubcategory) {
		
		BusinessSubcategory busisubcate = new BusinessSubcategory();
		busisubcate.setIdbusiness(idbusiness);
		Subcategory subcate = subcategoryRepository.findById(idsubcategory).get();
		busisubcate.setIdsubcategory(subcate);
    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
    	busisubcate.setCreate_at(tstamp);
    	busisubcate.setLast_update(tstamp.getTime());
    	busisubcate.setIs_deleted(false);
		businessSubcategoryRepository.save(busisubcate);
	}

	public String[] cortarCadenaPorPuntos(String cadena) {
		return cadena.split("\\.");
	}
	private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public String generateCode(int length) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }
        return builder.toString();
    }
    ApplicationHome home = new ApplicationHome(this.getClass()); 
    private String UPLOAD_FOLDER = home.getDir().toString().replace("com"+File.separator+"ecrece"+File.separator+"controller","")+File.separator+"static"+File.separator+"files"+File.separator;
	    
	@PostMapping("/business/municipality/{idmunicipality}/user/{iduser}")
	public Business createBusiness(@PathVariable(value = "idmunicipality") Integer idmunicipality,
			@PathVariable(value = "iduser") Integer iduser,
			@RequestParam(value = "formulario", required = true) String store, @RequestPart("file") MultipartFile file)
			throws Exception {

		if (!file.isEmpty()) {
			try {
				JsonObject jsonObj = new JsonParser().parse(store).getAsJsonObject();
				Business busi = new Business();
				busi.setDesc_business(jsonObj.get("desc_business").getAsString());
				busi.setDir_business(jsonObj.get("dir_business").getAsString());
				busi.setLicence_business(jsonObj.get("licence_business").getAsString());
				busi.setName_business(jsonObj.get("name_business").getAsString());
				
				busi.setName_image((jsonObj.get("name_business").getAsString()+generateCode(4)+ "." + cortarCadenaPorPuntos(
						file.getOriginalFilename())[cortarCadenaPorPuntos(file.getOriginalFilename()).length - 1]).replace(" ","_"));
				
				byte[] bFile = file.getBytes();
				Path path = Paths.get(UPLOAD_FOLDER + busi.getName_image());
				Files.write(path, bFile);
				
				busi.setPhone_business(jsonObj.get("phone_business").getAsString());
				busi.setWeb_site(jsonObj.get("web_site").getAsString());

				if (Float.parseFloat(jsonObj.get("latitude").getAsString()) == 0) {
					busi.setLatitude(null);
					busi.setLongitude(null);
				} else {
					busi.setLatitude(jsonObj.get("latitude").getAsString());
					busi.setLongitude(jsonObj.get("longitude").getAsString());

				}
				busi.setCrate_date(new Date());

//		        	 el estado del negocio que hay q definir uno inicial
				User user = userRepository.findById(iduser).get();
				BusinessStates businessStates = businessStatesRepository.findById(2).get();
				Municipality municipality = municipalityRepository.findById(idmunicipality).get();

				busi.setUser(user);
				busi.setBusinessStates(businessStates);

				busi.setMunicipality(municipality);
				Timestamp tstamp = new Timestamp(System.currentTimeMillis());
				busi.setCreate_at(tstamp);
				busi.setLast_update(tstamp.getTime());
				busi.setIs_deleted(false);
				return businessRepository.save(busi);
			} catch (Exception e) {
				throw new Exception("Error" + e);
			}
		} else {
			throw new Exception("El archivo esta vacio");
		}
	}
	
	@PostMapping("/business/acceptreservation/{idreservation}")
	public Reservation acceptreservation(@PathVariable(value = "idreservation") Integer idreservation)
	{
		Reservation reser = reservationReposity.findById(idreservation).get();
		reser.setAccept(true);
		reser.setPend(false);
		reser.setReject(false);
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		reser.setLast_update(tstamp.getTime());
		Reservation reservationaccept = reservationReposity.save(reser);
		
		User us = reser.getBusiness().getUser();
		Funcionality fun = funcionalityRepository.findById(2).get();
		us.setPtosUser(us.getPtosUser() + fun.getPoint_plus());
		us.setLast_update(tstamp.getTime());
		userRepository.save(us);
		
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification(reser.getUsertoreservation().getNameUser());
		notification.setSubject_notification("Reservación");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("La reservación ha sido aceptada.");

		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Reservacion")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Reservacion");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);
		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Aceptada")).findFirst();
		if (notistate.isPresent()) {
			notification.setNotification_state(notistate.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Aceptada");
			notistate2.setNameNotificationstate("Aceptada");
			notificationStatesRepository.save(notistate2);			
			notification.setNotification_state(notistate2);
		}
		notification.setIduser_to(reser.getUsertoreservation());
		notification.setUsernotification(reser.getBusiness().getUser());
		notification.setReservation(reser);
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
		
		 Client cli = clientReposity.findbyBusiness(reser.getBusiness().getIdbusiness(), reser.getUsertoreservation().getIduser());
    	 if(cli == null)
    	 {
    		 Client newclient = new Client();
    		 newclient.setBusiness(reser.getBusiness());
    		 newclient.setIduser(reser.getUsertoreservation());
    		 newclient.setCreate_at(tstamp);
    		 newclient.setLast_update(tstamp.getTime());
    		 newclient.setIs_deleted(false);
    		 clientReposity.save(newclient);
    	 }
		
		return reservationaccept;
	}
	
	@PostMapping("/business/sendmailacceptreservation/{idreservation}")
	public void sendmailacceptreservation(@PathVariable(value = "idreservation") Integer idreservation)
	{
		Reservation reser = reservationReposity.findById(idreservation).get();
		String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
    	  		"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
    	  		" <head> \r\n" + 
    	  		"  <meta charset=\"UTF-8\"> \r\n" + 
    	  		"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
    	  		"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
    	  		"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
    	  		"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
    	  		"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
    	  		"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
    	  		"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
    	  		"  <!--[if !mso]><!-- --> \r\n" + 
    	  		"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
    	  		"  <!--<![endif]--> \r\n" + 
    	  		"  <style type=\"text/css\">\r\n" + 
    	  		"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
    	  		"#outlook a {\r\n" + 
    	  		"	padding:0;\r\n" + 
    	  		"}\r\n" + 
    	  		".ExternalClass {\r\n" + 
    	  		"	width:100%;\r\n" + 
    	  		"}\r\n" + 
    	  		".ExternalClass,\r\n" + 
    	  		".ExternalClass p,\r\n" + 
    	  		".ExternalClass span,\r\n" + 
    	  		".ExternalClass font,\r\n" + 
    	  		".ExternalClass td,\r\n" + 
    	  		".ExternalClass div {\r\n" + 
    	  		"	line-height:100%;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button {\r\n" + 
    	  		"	mso-style-priority:100!important;\r\n" + 
    	  		"	text-decoration:none!important;\r\n" + 
    	  		"}\r\n" + 
    	  		"a[x-apple-data-detectors] {\r\n" + 
    	  		"	color:inherit!important;\r\n" + 
    	  		"	text-decoration:none!important;\r\n" + 
    	  		"	font-size:inherit!important;\r\n" + 
    	  		"	font-family:inherit!important;\r\n" + 
    	  		"	font-weight:inherit!important;\r\n" + 
    	  		"	line-height:inherit!important;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-desk-hidden {\r\n" + 
    	  		"	display:none;\r\n" + 
    	  		"	float:left;\r\n" + 
    	  		"	overflow:hidden;\r\n" + 
    	  		"	width:0;\r\n" + 
    	  		"	max-height:0;\r\n" + 
    	  		"	line-height:0;\r\n" + 
    	  		"	mso-hide:all;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button-border:hover {\r\n" + 
    	  		"	border-style:solid solid solid solid!important;\r\n" + 
    	  		"	background:#d6a700!important;\r\n" + 
    	  		"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button-border:hover a.es-button {\r\n" + 
    	  		"	background:#d6a700!important;\r\n" + 
    	  		"	border-color:#d6a700!important;\r\n" + 
    	  		"}\r\n" + 
    	  		"</style> \r\n" + 
    	  		" </head> \r\n" + 
    	  		" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
    	  		"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
    	  		"   <!--[if gte mso 9]>\r\n" + 
    	  		"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
    	  		"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
    	  		"			</v:background>\r\n" + 
    	  		"		<![endif]--> \r\n" + 
    	  		"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
    	  		"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
    	  		"           <div> \r\n" + 
    	  		"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
    	  		"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                      </tr> \r\n" + 
    	  		"                    </table></td> \r\n" + 
    	  		"                  </tr> \r\n" + 
    	  		"                </table></td> \r\n" + 
    	  		"              </tr> \r\n" + 
    	  		"            </table> \r\n" + 
    	  		"           </div> \r\n" + 
    	  		"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, su reservación ha sido aceptada, gracias por reservar en "+reser.getBusiness().getName_business()+"</p></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"></span></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"           </table></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
    	  		"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" +    	  		
    	  		"                       </td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"           </table></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table></td> \r\n" + 
    	  		"     </tr> \r\n" + 
    	  		"   </table> \r\n" + 
    	  		"  </div>  \r\n" + 
    	  		" </body>\r\n" + 
    	  		"</html>";
		
		SendMail email = new SendMail();	    
	  String subject = "Solicitud de Reservación Aceptada";	
	  User us = userRepository.findById(reser.getUsertoreservation().getIduser()).get();
	  Funcionality  func = funcionalityRepository.findById(2).get();
	  us.setPtosUser(us.getPtosUser() + func.getPoint_plus());
	  Timestamp tstamp = new Timestamp(System.currentTimeMillis()); 
	  us.setLast_update(tstamp.getTime());
	  userRepository.save(us);
//	  String body = "Hola el negocio "+reser.getBusiness().getName_business()+" aceptó su solicitud de reservación";
	  String[] to = {reser.getUsertoreservation().getMail()};
	  email.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);	
	}
	@PostMapping("/business/{idbusiness}/madereservation/iduserreservation/{iduser}")
	public Reservation createreservation(@PathVariable(value = "idbusiness") Integer idbusiness,
									@PathVariable(value = "iduser") Integer iduser,
								@Valid @RequestBody Reservation reservationRequest){
		
		Business busi = businessRepository.findById(idbusiness).get();
		User userreservatio = userRepository.findById(iduser).get();		
		Reservation reser = new Reservation();
		reser = reservationRequest;
		reser.setBusiness(busi);
		reser.setUsertoreservation(userreservatio);
		reser.setReject(false);
		reser.setAccept(false);
		reser.setPend(true);
		reser.setCreatedate(new Date());
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		reser.setCreate_at(tstamp);
		reser.setLast_update(tstamp.getTime());
		reser.setIs_deleted(false);
		Reservation newreservation = reservationReposity.save(reser);
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification(busi.getName_business());
		notification.setSubject_notification("Reservación");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("Solicitud de reservación de "+userreservatio.getNameUser()+","+
				" toda la información está en: ");

		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Reservacion")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Reservacion");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);
		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Aceptada")).findFirst();
		if (notistate.isPresent()) {
			notification.setNotification_state(notistate.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Aceptada");
			notistate2.setNameNotificationstate("Aceptada");
			notificationStatesRepository.save(notistate2);			
			notification.setNotification_state(notistate2);
		}
		notification.setIduser_to(busi.getUser());
		notification.setUsernotification(userreservatio);
		notification.setReservation(newreservation);
		notification.setUrltodetail(newreservation.getUrldir()+newreservation.getIdreservation());
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
		
		return newreservation;
	}

//enviar el mensaje de rechazada...
	@PostMapping("/business/sendmailrejectreservation/{idreservation}")
	public void sendmailrejectreservation(@PathVariable(value = "idreservation") Integer idreservation)
	{
	  Reservation reser = reservationReposity.findById(idreservation).get();
	  
	  String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
  	  		"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
  	  		" <head> \r\n" + 
  	  		"  <meta charset=\"UTF-8\"> \r\n" + 
  	  		"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
  	  		"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
  	  		"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
  	  		"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
  	  		"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
  	  		"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
  	  		"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
  	  		"  <!--[if !mso]><!-- --> \r\n" + 
  	  		"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
  	  		"  <!--<![endif]--> \r\n" + 
  	  		"  <style type=\"text/css\">\r\n" + 
  	  		"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
  	  		"#outlook a {\r\n" + 
  	  		"	padding:0;\r\n" + 
  	  		"}\r\n" + 
  	  		".ExternalClass {\r\n" + 
  	  		"	width:100%;\r\n" + 
  	  		"}\r\n" + 
  	  		".ExternalClass,\r\n" + 
  	  		".ExternalClass p,\r\n" + 
  	  		".ExternalClass span,\r\n" + 
  	  		".ExternalClass font,\r\n" + 
  	  		".ExternalClass td,\r\n" + 
  	  		".ExternalClass div {\r\n" + 
  	  		"	line-height:100%;\r\n" + 
  	  		"}\r\n" + 
  	  		".es-button {\r\n" + 
  	  		"	mso-style-priority:100!important;\r\n" + 
  	  		"	text-decoration:none!important;\r\n" + 
  	  		"}\r\n" + 
  	  		"a[x-apple-data-detectors] {\r\n" + 
  	  		"	color:inherit!important;\r\n" + 
  	  		"	text-decoration:none!important;\r\n" + 
  	  		"	font-size:inherit!important;\r\n" + 
  	  		"	font-family:inherit!important;\r\n" + 
  	  		"	font-weight:inherit!important;\r\n" + 
  	  		"	line-height:inherit!important;\r\n" + 
  	  		"}\r\n" + 
  	  		".es-desk-hidden {\r\n" + 
  	  		"	display:none;\r\n" + 
  	  		"	float:left;\r\n" + 
  	  		"	overflow:hidden;\r\n" + 
  	  		"	width:0;\r\n" + 
  	  		"	max-height:0;\r\n" + 
  	  		"	line-height:0;\r\n" + 
  	  		"	mso-hide:all;\r\n" + 
  	  		"}\r\n" + 
  	  		".es-button-border:hover {\r\n" + 
  	  		"	border-style:solid solid solid solid!important;\r\n" + 
  	  		"	background:#d6a700!important;\r\n" + 
  	  		"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
  	  		"}\r\n" + 
  	  		".es-button-border:hover a.es-button {\r\n" + 
  	  		"	background:#d6a700!important;\r\n" + 
  	  		"	border-color:#d6a700!important;\r\n" + 
  	  		"}\r\n" + 
  	  		"</style> \r\n" + 
  	  		" </head> \r\n" + 
  	  		" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
  	  		"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
  	  		"   <!--[if gte mso 9]>\r\n" + 
  	  		"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
  	  		"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
  	  		"			</v:background>\r\n" + 
  	  		"		<![endif]--> \r\n" + 
  	  		"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
  	  		"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
  	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
  	  		"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
  	  		"           <div> \r\n" + 
  	  		"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
  	  		"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
  	  		"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
  	  		"                      </tr> \r\n" + 
  	  		"                    </table></td> \r\n" + 
  	  		"                  </tr> \r\n" + 
  	  		"                </table></td> \r\n" + 
  	  		"              </tr> \r\n" + 
  	  		"            </table> \r\n" + 
  	  		"           </div> \r\n" + 
  	  		"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
  	  		"         </tr> \r\n" + 
  	  		"       </table> \r\n" + 
  	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
  	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
  	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
  	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table></td> \r\n" + 
  	  		"             </tr> \r\n" + 
  	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
  	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, su reservación ha sido rechazada, disculpe por las molestias ocacionadas.</p></td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table></td> \r\n" + 
  	  		"             </tr> \r\n" + 
  	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
  	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"></span></td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table></td> \r\n" + 
  	  		"             </tr> \r\n" + 
  	  		"           </table></td> \r\n" + 
  	  		"         </tr> \r\n" + 
  	  		"       </table> \r\n" + 
  	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
  	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
  	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
  	  		"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
  	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table> \r\n" + 
  	  		"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
  	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  	
  	  		"                       </td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table> \r\n" + 
  	  		"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
  	  		"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table> \r\n" + 
  	  		"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
  	  		"             </tr> \r\n" + 
  	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
  	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
  	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
  	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
  	  		"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
  	  		"                     </tr> \r\n" + 
  	  		"                   </table></td> \r\n" + 
  	  		"                 </tr> \r\n" + 
  	  		"               </table></td> \r\n" + 
  	  		"             </tr> \r\n" + 
  	  		"           </table></td> \r\n" + 
  	  		"         </tr> \r\n" + 
  	  		"       </table></td> \r\n" + 
  	  		"     </tr> \r\n" + 
  	  		"   </table> \r\n" + 
  	  		"  </div>  \r\n" + 
  	  		" </body>\r\n" + 
  	  		"</html>";
	  SendMail email = new SendMail();	    
	  String subject = "Solicitud de Reservación Rechazada";	      
//	  String body = "Hola el negocio "+reser.getBusiness().getName_business()+" ha rechazado su solicitud de reservación. Disculpe por las molestias ocacionadas.";
	  String[] to = {reser.getUsertoreservation().getMail()};
	  email.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);		
	}
	@PostMapping("/business/sendnotificationrejectreservation/{idreservation}")
	public void sendnotificationrejectreservation(@PathVariable(value = "idreservation") Integer idreservation)
	{
		Reservation reser = reservationReposity.findById(idreservation).get();
		reser.setReject(true);
		reser.setPend(false);
		reser.setAccept(false);
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		reser.setLast_update(tstamp.getTime());
		reservationReposity.save(reser);
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification(reser.getUsertoreservation().getNameUser());
		notification.setSubject_notification("Reservación Rechazada");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("La reservación ha sido rechazada.");
		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Reservacion")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Reservacion");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);
		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Aceptada")).findFirst();
		if (notistate.isPresent()) {
			notification.setNotification_state(notistate.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Aceptada");
			notistate2.setNameNotificationstate("Aceptada");
			notificationStatesRepository.save(notistate2);			
			notification.setNotification_state(notistate2);
		}
		notification.setIduser_to(reser.getUsertoreservation());
		notification.setUsernotification(reser.getBusiness().getUser());
		notification.setReservation(reser);
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
	}
	@PostMapping("/business/sendmailreservation/{idreservation}")
	public void sendmail(@PathVariable(value = "idreservation") Integer idreservation)
	{
		Reservation reser = reservationReposity.findById(idreservation).get();
		String tobody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
    	  		"<html style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">\r\n" + 
    	  		" <head> \r\n" + 
    	  		"  <meta charset=\"UTF-8\"> \r\n" + 
    	  		"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \r\n" + 
    	  		"  <meta name=\"x-apple-disable-message-reformatting\"> \r\n" + 
    	  		"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \r\n" + 
    	  		"  <meta content=\"telephone=no\" name=\"format-detection\"> \r\n" + 
    	  		"  <title>Nueva plantilla de correo electrónico 2019-10-02</title> \r\n" + 
    	  		"  <!--[if (mso 16)]>    <style type=\"text/css\">    a {text-decoration: none;}    </style>    <![endif]--> \r\n" + 
    	  		"  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> \r\n" + 
    	  		"  <!--[if !mso]><!-- --> \r\n" + 
    	  		"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\"> \r\n" + 
    	  		"  <!--<![endif]--> \r\n" + 
    	  		"  <style type=\"text/css\">\r\n" + 
    	  		"@media only screen and (max-width:600px) {.st-br { padding-left:10px!important; padding-right:10px!important } p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important; text-align:center } h2 a { font-size:26px!important; text-align:center } h3 a { font-size:20px!important; text-align:center } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:16px!important; display:block!important; border-left-width:0px!important; border-right-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }\r\n" + 
    	  		"#outlook a {\r\n" + 
    	  		"	padding:0;\r\n" + 
    	  		"}\r\n" + 
    	  		".ExternalClass {\r\n" + 
    	  		"	width:100%;\r\n" + 
    	  		"}\r\n" + 
    	  		".ExternalClass,\r\n" + 
    	  		".ExternalClass p,\r\n" + 
    	  		".ExternalClass span,\r\n" + 
    	  		".ExternalClass font,\r\n" + 
    	  		".ExternalClass td,\r\n" + 
    	  		".ExternalClass div {\r\n" + 
    	  		"	line-height:100%;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button {\r\n" + 
    	  		"	mso-style-priority:100!important;\r\n" + 
    	  		"	text-decoration:none!important;\r\n" + 
    	  		"}\r\n" + 
    	  		"a[x-apple-data-detectors] {\r\n" + 
    	  		"	color:inherit!important;\r\n" + 
    	  		"	text-decoration:none!important;\r\n" + 
    	  		"	font-size:inherit!important;\r\n" + 
    	  		"	font-family:inherit!important;\r\n" + 
    	  		"	font-weight:inherit!important;\r\n" + 
    	  		"	line-height:inherit!important;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-desk-hidden {\r\n" + 
    	  		"	display:none;\r\n" + 
    	  		"	float:left;\r\n" + 
    	  		"	overflow:hidden;\r\n" + 
    	  		"	width:0;\r\n" + 
    	  		"	max-height:0;\r\n" + 
    	  		"	line-height:0;\r\n" + 
    	  		"	mso-hide:all;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button-border:hover {\r\n" + 
    	  		"	border-style:solid solid solid solid!important;\r\n" + 
    	  		"	background:#d6a700!important;\r\n" + 
    	  		"	border-color:#42d159 #42d159 #42d159 #42d159!important;\r\n" + 
    	  		"}\r\n" + 
    	  		".es-button-border:hover a.es-button {\r\n" + 
    	  		"	background:#d6a700!important;\r\n" + 
    	  		"	border-color:#d6a700!important;\r\n" + 
    	  		"}\r\n" + 
    	  		"</style> \r\n" + 
    	  		" </head> \r\n" + 
    	  		" <body style=\"width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\"> \r\n" + 
    	  		"  <div class=\"es-wrapper-color\" style=\"background-color:#F6F6F6;\"> \r\n" + 
    	  		"   <!--[if gte mso 9]>\r\n" + 
    	  		"			<v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">\r\n" + 
    	  		"				<v:fill type=\"tile\" color=\"#f6f6f6\"></v:fill>\r\n" + 
    	  		"			</v:background>\r\n" + 
    	  		"		<![endif]--> \r\n" + 
    	  		"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"      <td class=\"st-br\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-header\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;background-color:#FFE09B;\" bgcolor=\"#ffe09b\"> \r\n" + 
    	  		"           <!--[if gte mso 9]><v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:204px;\"><v:fill type=\"tile\" src=\"https://pics.esputnik.com/repository/home/17278/common/images/1546958148946.jpg\" color=\"#343434\" origin=\"0.5, 0\" position=\"0.5,0\" ></v:fill><v:textbox inset=\"0,0,0,0\"><![endif]--> \r\n" + 
    	  		"           <div> \r\n" + 
    	  		"            <table bgcolor=\"transparent\" class=\"es-header-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"              <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"               <td align=\"left\" style=\"padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;\"> \r\n" + 
    	  		"                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                  <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                   <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                    <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                      <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                       <td align=\"center\" height=\"66\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                      </tr> \r\n" + 
    	  		"                    </table></td> \r\n" + 
    	  		"                  </tr> \r\n" + 
    	  		"                </table></td> \r\n" + 
    	  		"              </tr> \r\n" + 
    	  		"            </table> \r\n" + 
    	  		"           </div> \r\n" + 
    	  		"           <!--[if gte mso 9]></v:textbox></v:rect><![endif]--></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"Margin:0;padding-bottom:10px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-position:center bottom;background-color:#FFFFFF;\" bgcolor=\"#ffffff\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"left\" style=\"padding:0;Margin:0;\"><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">Bienvenido(a)</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;text-align:center;\">eCrece</h1><h1 style=\"Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#212121;\"><br></h1></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"Margin:0;padding-top:20px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:center bottom;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"540\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;\">Hola, el usuario "+reser.getUsertoreservation().getNameUser()+" ha realizado una solicitud de reservación, para ver los detalles de click en el botón.</p></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FAFAFA;\" bgcolor=\"#fafafa\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"560\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:10px;Margin:0;\"><span class=\"es-button-border\" style=\"border-style:solid;border-color:#2CB543;background:#FFC80A;border-width:0px;display:inline-block;border-radius:3px;width:auto;\"><a href=\""+reser.getUrldir()+reser.getIdreservation()+"\" class=\"es-button\" target=\"_blank\" style=\"mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FFC80A;border-width:10px 20px 10px 20px;display:inline-block;background:#FFC80A;border-radius:3px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;\">Ver Reservación</a></span></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"           </table></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table> \r\n" + 
    	  		"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-footer\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:#F6F6F6;background-repeat:repeat;background-position:center top;\"> \r\n" + 
    	  		"         <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"          <td align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"           <table bgcolor=\"#31cb4b\" class=\"es-footer-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;\"> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td style=\"Margin:0;padding-top:30px;padding-bottom:30px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#EFEFEF;\" align=\"left\" bgcolor=\"#efefef\"> \r\n" + 
    	  		"               <!--[if mso]><table width=\"540\" cellpadding=\"0\"                             cellspacing=\"0\"><tr><td width=\"186\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td class=\"es-m-p0r es-m-p20b\" width=\"166\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                  <td class=\"es-hidden\" width=\"20\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td><td width=\"165\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td class=\"es-m-p20b\" width=\"165\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td class=\"es-m-txt-c\" align=\"left\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		
    	  		"                       </td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td><td width=\"20\"></td><td width=\"169\" valign=\"top\"><![endif]--> \r\n" + 
    	  		"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"169\" align=\"center\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table> \r\n" + 
    	  		"               <!--[if mso]></td></tr></table><![endif]--></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"             <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"              <td align=\"left\" style=\"padding:0;Margin:0;background-position:left top;\"> \r\n" + 
    	  		"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                 <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                  <td width=\"600\" align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;\"> \r\n" + 
    	  		"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;\"> \r\n" + 
    	  		"                     <tr style=\"border-collapse:collapse;\"> \r\n" + 
    	  		"                      <td align=\"center\" height=\"40\" style=\"padding:0;Margin:0;\"></td> \r\n" + 
    	  		"                     </tr> \r\n" + 
    	  		"                   </table></td> \r\n" + 
    	  		"                 </tr> \r\n" + 
    	  		"               </table></td> \r\n" + 
    	  		"             </tr> \r\n" + 
    	  		"           </table></td> \r\n" + 
    	  		"         </tr> \r\n" + 
    	  		"       </table></td> \r\n" + 
    	  		"     </tr> \r\n" + 
    	  		"   </table> \r\n" + 
    	  		"  </div>  \r\n" + 
    	  		" </body>\r\n" + 
    	  		"</html>";
		SendMail email = new SendMail();	    
	  String subject = "Solicitud de Reservación";	      
//	  String body = "Hola el usuario "+reser.getUsertoreservation().getNameUser()+" ha solicitado una reservación. Los detalles de la reservación se encuentran en la siquiente derección "+reser.getUrldir()+reser.getIdreservation();
	  String[] to = {reser.getBusiness().getUser().getMail()};
	  email.sendFromGMail(SendMail.USER_NAME, SendMail.PASSWORD, to , subject, tobody);		
	}
	
	@GetMapping("/business/detailreservation/{idreservation}")
	public Reservation getreservation(@PathVariable(value = "idreservation") Integer idreservation)
	{
		 return reservationReposity.findById(idreservation)
	                .orElseThrow(() -> new ResourceNotFoundException("Reservation not found"));
		
	}
	
	@PostMapping("/asigfuncionality/{idfuncionality}/business/{idbusiness}/dateend/{dateend}")
	public void asigfuncionalitymoderador(@PathVariable(value = "idfuncionality") Integer idfuncionality,
			@PathVariable(value = "idbusiness") Integer idbusiness, @PathVariable(value = "dateend") Date dateend) throws ParseException {
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");    	
		Business business = businessRepository.findById(idbusiness).get();
		Funcionality funcionality = funcionalityRepository.findById(idfuncionality).get();
		FuncionalityUserBusiness funuserBusiness = funcionalityUserBusinessRepository.selectoneFuncionalityUserBusiness(business.getIdbusiness(), funcionality.getIdfuncionality());
		if (funuserBusiness != null) {
			String todateend = format.format(funuserBusiness.getDate_end());
	    	Date dateendfun = format.parse(todateend);
			if(dateend.compareTo(dateendfun) == -1)
			{
//				funuserBusiness.setDate_end(dateend);
				funuserBusiness.setActive(true);
				funuserBusiness.setLast_update(tstamp.getTime());
				funcionalityUserBusinessRepository.save(funuserBusiness);
			}
			else
			{
				funuserBusiness.setDate_end(dateend);
				funuserBusiness.setActive(true);
				funuserBusiness.setLast_update(tstamp.getTime());
				funcionalityUserBusinessRepository.save(funuserBusiness);
			}
						
			
		} else {
			FuncionalityUserBusiness funcionalityuserbusiness = new FuncionalityUserBusiness();
			
			funcionalityuserbusiness.setDate_start(new Date());
			funcionalityuserbusiness.setDate_end(dateend);
			funcionalityuserbusiness.setIdbusiness(business);
			funcionalityuserbusiness.setIdfuncionality(funcionality);
			funcionalityuserbusiness.setActive(true);
			funcionalityuserbusiness.setCreate_at(tstamp);
			funcionalityuserbusiness.setLast_update(tstamp.getTime());
			funcionalityuserbusiness.setIs_deleted(false);
			funcionalityUserBusinessRepository.save(funcionalityuserbusiness);
		}
		if(funcionality.getIdfuncionality()==2)
		{
			ReservationCharacteristics resercharacteristics = new ReservationCharacteristics();
			resercharacteristics.setBusiness(business);
			resercharacteristics.setDate(true);
			resercharacteristics.setDescription(true);
			resercharacteristics.setArticle(false);
			resercharacteristics.setDateend(false);
			resercharacteristics.setNight(false);
			resercharacteristics.setPersonsamount(false);
			resercharacteristics.setSchedule(false);
			resercharacteristics.setScheduleend(false);
			resercharacteristics.setCreate_at(tstamp);
			resercharacteristics.setLast_update(tstamp.getTime());
			resercharacteristics.setIs_deleted(false);
			reservationCharacteristicsRepository.save(resercharacteristics);
		}
		
//		User us = business.getUser();
//		us.setPtosUser(us.getPtosUser() - funcionality.getPointFuncionality());
//		userRepository.save(us);

	}
	@PostMapping("/business/{idbusiness}/listfuncionality")
	public void sendnotificationfuncionality(@PathVariable(value = "idbusiness") Integer idbusiness,
																@Valid @RequestBody List<Funcionality> listfuncionality) {
		Business busi = businessRepository.findById(idbusiness).get();
		String listFun="";
		for (Funcionality funcionality : listfuncionality) {
			listFun += funcionality.getNameFuncionality()+", ";
		}
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification("Moderador");
		notification.setSubject_notification("Funcionalidades asigandas por el moderador");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("Hola, " + busi.getUser().getNameUser()+" se le han asiganado las siguentes funcionalidades: "+ listFun);

		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Confirmación")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Confirmación");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);

		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Informativo")).findFirst();
		if (notistate.isPresent()) {
			notification.setNotification_state(notistate.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Informativo");
			notistate2.setNameNotificationstate("Informativo");
			notificationStatesRepository.save(notistate2);
			notification.setNotification_state(notistate2);

		}
		notification.setIdbusiness_fk(busi);
		notification.setIduser_to(busi.getUser());
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
		
	}
	
	@PostMapping("/business/{idbusiness}/reservationcharacteristics")
	public ReservationCharacteristics createReservationCharacteristics(@PathVariable(value = "idbusiness") Integer idbusiness,
																@Valid @RequestBody ReservationCharacteristics reservationcharacte) {
		Business busi = businessRepository.findById(idbusiness).get();
		
		ReservationCharacteristics resercharacteristics = new ReservationCharacteristics();
		resercharacteristics = reservationcharacte;
		resercharacteristics.setBusiness(busi);		
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		resercharacteristics.setCreate_at(tstamp);
		resercharacteristics.setLast_update(tstamp.getTime());
		resercharacteristics.setIs_deleted(false);
		return reservationCharacteristicsRepository.save(resercharacteristics);
	}
	
	@PostMapping("/business/{idbusiness}/reservationcharacteristics/actualizar")
	public ReservationCharacteristics actualizarReservationCharacteristics(@PathVariable(value = "idbusiness") Integer idbusiness,
																@Valid @RequestBody ReservationCharacteristics reservationcharacte) {
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		Business busi = businessRepository.findById(idbusiness).get();		
		ReservationCharacteristics resercharacteristics = new ReservationCharacteristics();
		resercharacteristics = reservationcharacte;
		resercharacteristics.setBusiness(busi);	
		resercharacteristics.setLast_update(tstamp.getTime());
		return reservationCharacteristicsRepository.save(resercharacteristics);
	}
	
	@GetMapping("/business/{idbusiness}/loadreservationcharacteristics")
	public ReservationCharacteristics getloadreservationcharacteristics(@PathVariable(value = "idbusiness") Integer idbusiness)
	{
		ReservationCharacteristics res = reservationCharacteristicsRepository.findbyidbusiness(idbusiness);
		return res;
	}
	
	@GetMapping("/business/{idbusiness}/listfuncionality/singel")
	public List<Funcionality> getlistfuncionality(@PathVariable(value = "idbusiness") Integer idbusiness)
	{
		return funcionalityUserBusinessRepository.funcionalitybyBusinessentity(idbusiness);
	}
	
	@PostMapping("/asigfuncionality/{idfuncionality}/business/{idbusiness}/dateend/{dateend}/user")
	public void comprarfuncionalityuser(@PathVariable(value = "idfuncionality") Integer idfuncionality,
			@PathVariable(value = "idbusiness") Integer idbusiness, @PathVariable(value = "dateend") Date dateend) {
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		Business business = businessRepository.findById(idbusiness).get();
		Funcionality funcionality = funcionalityRepository.findById(idfuncionality).get();
		FuncionalityUserBusiness funuserBusiness = funcionalityUserBusinessRepository
				.selectoneFuncionalityUserBusiness(business.getIdbusiness(), funcionality.getIdfuncionality());
		if (funuserBusiness != null) {
			
			funuserBusiness.setDate_end(dateend);
			funuserBusiness.setActive(true);
			funuserBusiness.setLast_update(tstamp.getTime());
			funcionalityUserBusinessRepository.save(funuserBusiness);
		} else {
			FuncionalityUserBusiness funcionalityuserbusiness = new FuncionalityUserBusiness();
			funcionalityuserbusiness.setDate_start(new Date());
			funcionalityuserbusiness.setDate_end(dateend);
			funcionalityuserbusiness.setIdbusiness(business);
			funcionalityuserbusiness.setIdfuncionality(funcionality);
			funcionalityuserbusiness.setActive(true);
			funcionalityuserbusiness.setCreate_at(tstamp);
			funcionalityuserbusiness.setLast_update(tstamp.getTime());
			funcionalityuserbusiness.setIs_deleted(false);
			funcionalityUserBusinessRepository.save(funcionalityuserbusiness);
		}		
	}
	
	@PostMapping("/business/{idbusiness}/createqr")
	 public  void createqr(@PathVariable(value = "idbusiness") Integer idbusiness,@Valid @RequestBody ModDir moddir)
	 {
		try {
		Business busi = businessRepository.findById(idbusiness).get();
		
			QRCode qr = new QRCode();
//			como primer argumento va la url del usuario su quiere
			qr.writeQR(moddir.getDir(), busi.getName_business());

		} catch (Exception e) {
			e.printStackTrace();
		}
	 }
	@PostMapping("/business/edit/withoutfile")
	public Business EditBusinesswithoutfile(@RequestParam(value = "formulario", required = true) String store) {
		
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		JsonObject jsonObj = new JsonParser().parse(store).getAsJsonObject();
		Business busi = new Business();

		Integer id = Integer.parseInt(jsonObj.get("idbusiness").getAsString());
		busi = businessRepository.findById(id).get();
		busi.setDir_business(jsonObj.get("dir_business").getAsString());
		busi.setLicence_business(jsonObj.get("licence_business").getAsString());
		busi.setName_business(jsonObj.get("name_business").getAsString());
		busi.setDesc_business(jsonObj.get("desc_business").getAsString());
		busi.setPhone_business(jsonObj.get("phone_business").getAsString());
		busi.setWeb_site(jsonObj.get("web_site").getAsString());
		busi.setClosefor(jsonObj.get("closefor").getAsString());
		busi.setLast_update(tstamp.getTime());
		if (Float.parseFloat(jsonObj.get("latitude").getAsString()) == 0) {
			busi.setLatitude(null);
			busi.setLongitude(null);
		} else {
			busi.setLatitude(jsonObj.get("latitude").getAsString());
			busi.setLongitude(jsonObj.get("longitude").getAsString());

		}
		
		Integer idmunicipality = Integer.parseInt(jsonObj.get("municipality").getAsString());
		BusinessStates businessStates = businessStatesRepository.findById(2).get();
		Municipality municipality = municipalityRepository.findById(idmunicipality).get();

		busi.setBusinessStates(businessStates);

		busi.setMunicipality(municipality);

		Business editbusi = businessRepository.save(busi);

		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification("Moderador");
		notification.setSubject_notification("Edición de Negocio con nombre " + busi.getName_business());
		notification.setCreate_date(new Date());
		notification.setDesc_notification("Se edito un negocio");

		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Edición de Negocio")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Edición de Negocio");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);

		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Pendiente")).findFirst();
		if (notistate.isPresent()) {
			notification.setNotification_state(notistate.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Pendiente");
			notistate2.setNameNotificationstate("Pendiente");
			notificationStatesRepository.save(notistate2);
			notification.setNotification_state(notistate2);

		}
		notification.setIdbusiness_fk(businessRepository.findById(busi.getIdbusiness()).get());
		notification.setUsernotification(userRepository.findById(busi.getUser().getIduser()).get());
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
		return editbusi;
	}

	@PostMapping("/business/edit/withfile")
	 public  Business EditBusinesswithfile(
				@RequestParam(value = "formulario", required = true)String store, 
				@RequestPart("file") MultipartFile file) throws Exception{

				if (!file.isEmpty()) {
				try {
				File convFile = new File(file.getOriginalFilename());
				convFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(convFile);
				//aqui salvo la imagen
				fos.write(file.getBytes());
				fos.close();
				
				JsonObject jsonObj = new JsonParser().parse(store).getAsJsonObject();
				Business busi = new Business();

				Integer id = Integer.parseInt(jsonObj.get("idbusiness").getAsString());
				busi = businessRepository.findById(id).get();
				busi.setDesc_business(jsonObj.get("desc_business").getAsString());
				busi.setDir_business(jsonObj.get("dir_business").getAsString());
				busi.setLicence_business(jsonObj.get("licence_business").getAsString());
				busi.setName_business(jsonObj.get("name_business").getAsString());
				     	
				busi.setName_image((jsonObj.get("name_business").getAsString()+generateCode(4)+ "." + cortarCadenaPorPuntos(
						file.getOriginalFilename())[cortarCadenaPorPuntos(file.getOriginalFilename()).length - 1]).replace(" ","_"));
				
				byte[] bFile = file.getBytes();
				Path path = Paths.get(UPLOAD_FOLDER + busi.getName_image());
				Files.write(path, bFile);
				
				busi.setPhone_business(jsonObj.get("phone_business").getAsString());
				busi.setWeb_site(jsonObj.get("web_site").getAsString());
				busi.setClosefor(jsonObj.get("closefor").getAsString());				
				Timestamp tstamp = new Timestamp(System.currentTimeMillis());
				busi.setLast_update(tstamp.getTime());
				if (Float.parseFloat(jsonObj.get("latitude").getAsString()) == 0) {
					busi.setLatitude(null);
					busi.setLongitude(null);
				} else {
					busi.setLatitude(jsonObj.get("latitude").getAsString());
					busi.setLongitude(jsonObj.get("longitude").getAsString());

				}			
				
				busi.setCrate_date(new Date());
				
				//el estado del negocio que hay q definir uno inicial
				Integer idmunicipality = Integer.parseInt(jsonObj.get("municipality").getAsString());
				BusinessStates businessStates = businessStatesRepository.findById(2).get();
				Municipality municipality = municipalityRepository.findById(idmunicipality).get();

				busi.setBusinessStates(businessStates);

				busi.setMunicipality(municipality);
				
				Notification notification = new Notification();
				notification.setRead(false);
				notification.setFor_notification("Moderador");
				notification.setSubject_notification("Edición de Negocio con nombre " + busi.getName_business());
				notification.setCreate_date(new Date());
				notification.setDesc_notification("Se edito un negocio");

				List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
				Optional<NotificationType> notitype = notificationtypes.stream()
						.filter(p -> p.getDesc_notification_type().equals("Edición de Negocio")).findFirst();
				if (notitype.isPresent()) {
					notification.setNotificationsType(notitype.get());
				} else {
					NotificationType notitype2 = new NotificationType();
					notitype2.setDesc_notification_type("Edición de Negocio");
					notificationtypeRepository.save(notitype2);
					notification.setNotificationsType(notitype2);

				}
				List<NotificationState> notificationstate = notificationStatesRepository.findAll();
				Optional<NotificationState> notistate = notificationstate.stream()
						.filter(p -> p.getNameNotificationstate().equals("Pendiente")).findFirst();
				if (notistate.isPresent()) {
					notification.setNotification_state(notistate.get());
				} else {
					NotificationState notistate2 = new NotificationState();
					notistate2.setDescNotificationstate("En estado Pendiente");
					notistate2.setNameNotificationstate("Pendiente");
					notificationStatesRepository.save(notistate2);
					notification.setNotification_state(notistate2);

				}
				notification.setIdbusiness_fk(businessRepository.findById(busi.getIdbusiness()).get());
				notification.setUsernotification(userRepository.findById(busi.getUser().getIduser()).get());
				notification.setCreate_at(tstamp);
				notification.setLast_update(tstamp.getTime());
				notification.setIs_deleted(false);
				notificationRepository.save(notification);
				
				Business editbusi = businessRepository.save(busi);
				return editbusi;				
				
				} catch (Exception e) {
				throw new Exception("Error" + e);
				}
				} else {
				throw new Exception("El archivo esta vacio");
				}
	}
	
	@GetMapping("/business/getreplys/{idcoment}")
	public List<ReplyComent> getReplys(@PathVariable(value = "idcoment") Integer idcoment) {
		return replyComentReposity.findbyComent(idcoment);
	}
	
	@PostMapping("/business/deletesubcategory/{idbusiness}")
	public void deletesubcategorybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
		businessRepository.deletesubcategoryforbusiness(idbusiness);
	}
	@PostMapping("/business/deleteschedule/{idbusiness}")
	public boolean deleteschedulebusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
		businessRepository.deletesscheduleforbusiness(idbusiness);
		return true;
	}
	
	@PostMapping("/business/{idbusiness}/report/user/{iduser}")
	public void reportbusiness(@PathVariable(value = "idbusiness") Integer idbusiness,
							@PathVariable(value = "iduser") Integer iduser,
							@Valid @RequestBody ModReport modReport) {
		
		Business business = businessRepository.findById(idbusiness).get();
		User user = userRepository.findById(iduser).get();
		
		Notification notification = new Notification();
		notification.setRead(false);
		notification.setFor_notification("El usuario " + user.getNameUser()+" está reportando el negocio "+business.getName_business());
		notification.setSubject_notification("Un negocio ha sido reportado");
		notification.setCreate_date(new Date());
		notification.setDesc_notification("El negocio "+business.getName_business()+" ha sido reportado por el usuario "+user.getNameUser()+" .Motivo:"+modReport.getDesc_report());
		notification.setIdpointuser(null);
		notification.setIdpayment_form(null);
		List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
		Optional<NotificationType> notitype = notificationtypes.stream()
				.filter(p -> p.getDesc_notification_type().equals("Informativa")).findFirst();
		if (notitype.isPresent()) {
			notification.setNotificationsType(notitype.get());
		} else {
			NotificationType notitype2 = new NotificationType();
			notitype2.setDesc_notification_type("Informativa");
			notificationtypeRepository.save(notitype2);
			notification.setNotificationsType(notitype2);
		}
		List<NotificationState> notificationstate = notificationStatesRepository.findAll();
		Optional<NotificationState> notistate1 = notificationstate.stream()
				.filter(p -> p.getNameNotificationstate().equals("Informativo")).findFirst();
		if (notistate1.isPresent()) {
			notification.setNotification_state(notistate1.get());
		} else {
			NotificationState notistate2 = new NotificationState();
			notistate2.setDescNotificationstate("En estado Informativo");
			notistate2.setNameNotificationstate("Informativo");
			notificationStatesRepository.save(notistate2);
			notification.setNotification_state(notistate2);
		}
		notification.setIdbusiness_fk(business);
		notification.setIdbusiness_to(null);
		notification.setIduser_to(null);
		notification.setUsernotification(user);
		Timestamp tstamp = new Timestamp(System.currentTimeMillis());
		notification.setCreate_at(tstamp);
		notification.setLast_update(tstamp.getTime());
		notification.setIs_deleted(false);
		notificationRepository.save(notification);
	}

//	prueba para devolver una imagen
		@PostMapping("/image/path")
		public Country getImage(){		
		
		  ApplicationHome home = new ApplicationHome(this.getClass()); 
//		  File jarFile = home.getSource(); 
//		  File jarDir = home.getDir(); 
//		  File uploadDir = new  File(jarDir, "upload-dir");
		 
//		  System.out.print(home.getSource()+File.separator +"src"+File.separator+"main"+File.separator+"resources"+File.separator+"static"+File.separator+"files"+File.separator);		
//		String ruta = new File("").getAbsolutePath()+ File.separator +"src"+File.separator+"main"+File.separator+"resources"+File.separator+"static"+File.separator+"files"+File.separator+"Morla.png";
//	    InputStream in = getClass()
//	      .getResourceAsStream(ruta);
//	    return IOUtils.toByteArray(in);
//		return home.getSource()+File.separator +"src"+File.separator+"main"+File.separator+"resources"+File.separator+"static"+File.separator+"files"+File.separator;
//			Images ima = imageRepository.findById(1).get();
		  
		  Country cou = new Country();
		  System.out.print(new File("").getAbsolutePath());
		 
		  cou.setName_country(home.getDir().toString());
//		  cou.setName_country(env.getProperty("pathtocopy"));		 

		 
		return cou;
	}
		
		@PostMapping("/image")
		public ResponseEntity<byte[]> download() throws IOException {
	        File file = new File("C:\\Users\\Carlos\\Downloads\\usuarios.png");

	        HttpHeaders header = new HttpHeaders();
	        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=img.jpg");
	        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
	        header.add("Pragma", "no-cache");
	        header.add("Expires", "0");

	        Path path = Paths.get(file.getAbsolutePath());
	        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
	        
	        byte[] encoded = Base64.getEncoder().encode(resource.getByteArray());
	        
	        return ResponseEntity.ok()
	                .headers(header)
	                .contentLength(file.length())
	                .contentType(MediaType.parseMediaType("application/octet-stream"))
	                .body(encoded);
	    }


}
