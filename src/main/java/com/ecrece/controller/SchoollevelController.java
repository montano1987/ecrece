package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Schoollevel;
import com.ecrece.repository.SchoollevelRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class SchoollevelController {	
	   
	    @Autowired
	    private SchoollevelRepository schoollevelRepository;	   
		        
	   
		@GetMapping("/schoollevel")
	    public  List<Schoollevel> getschoollevels() {
			List<Schoollevel> schoollevels = schoollevelRepository.findAll().stream()
					.filter(schoollevel -> schoollevel.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return schoollevels;	    
			}
	
	    @GetMapping("/schoollevel/{idschoollevel}")
	    public Schoollevel getSchoollevel(@PathVariable(value = "idschoollevel") Integer idschoollevel) {
	        return schoollevelRepository.findById(idschoollevel)
	                .orElseThrow(() -> new ResourceNotFoundException("Schoollevel"));
	    }
    
	    @PostMapping("/schoollevel")
	    public Schoollevel createSchoollevel(@Valid @RequestBody Schoollevel schoollevel) {
	    	Schoollevel newschoollevel = schoollevel;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	newschoollevel.setCreate_at(tstamp);
	    	newschoollevel.setLast_update(tstamp.getTime());
	    	newschoollevel.setIs_deleted(false);
	        return schoollevelRepository.save(newschoollevel);
	    }

	    @PutMapping("/schoollevel/{idschoollevel}")
	    public Schoollevel updateScheduleJobOffer(@PathVariable(value = "idschoollevel") Integer idschoollevel,
	                                   @Valid @RequestBody Schoollevel SchoollevelRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return schoollevelRepository.findById(idschoollevel)
	                .map(schoollevel -> {
	                	schoollevel.setDesc_schoollevel(SchoollevelRequest.getDesc_schoollevel());
	                	schoollevel.setLast_update(tstamp.getTime());
	                    return schoollevelRepository.save(schoollevel);
	                }).orElseThrow(() -> new ResourceNotFoundException("Schoollevel not found with id " + idschoollevel));
	    }


	    @DeleteMapping("/schoollevel/{idschoollevel}")
	    public ResponseEntity<?> deleteJob(@PathVariable(value = "idschoollevel") Integer idschoollevel) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	return schoollevelRepository.findById(idschoollevel)
	                .map(schoolitem -> {
	                	schoolitem.setLast_update(tstamp.getTime());
	                	schoolitem.setIs_deleted(true);
	                	schoollevelRepository.save(schoolitem);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Schoollevel not found with id " + idschoollevel));
	    }

}
