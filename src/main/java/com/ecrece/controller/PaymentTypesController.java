package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.PaymentTypes;
import com.ecrece.repository.PaymentTypesRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class PaymentTypesController {	

	    @Autowired
	    private PaymentTypesRepository paymentTypesRepository;
	    
	
	        
	    @GetMapping("/paymentTypes")
	    public  List<PaymentTypes> getpaymentTypes() {
	    	List<PaymentTypes> ptype = paymentTypesRepository.findAll().stream()
					.filter(paymentT -> paymentT.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return ptype;	
	    }

	    @GetMapping("/paymentTypes/{idtipo_pago}")
	    public PaymentTypes getpaymentTypes(@PathVariable(value = "idtipo_pago") Long idtipo_pago) {
	        return paymentTypesRepository.findById(idtipo_pago)
	                .orElseThrow(() -> new ResourceNotFoundException("PaymentTypes"));
	    }
	    
	    @PostMapping("/paymentTypes")
	    public PaymentTypes createpaymentTypes(@Valid @RequestBody PaymentTypes paymentTypes) {
	    	PaymentTypes ptypes = paymentTypes;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	ptypes.setCreate_at(tstamp);
	    	ptypes.setLast_update(tstamp.getTime());
	    	ptypes.setIs_deleted(false);
	        return paymentTypesRepository.save(ptypes);
	    }

	    @PutMapping("/paymentTypes/{idtipo_pago}")
	    public PaymentTypes updatepaymentTypes(@PathVariable Long idtipo_pago,
	                                   @Valid @RequestBody PaymentTypes paymentTypesRequest) {
	    	
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return paymentTypesRepository.findById(idtipo_pago)
	                .map(paymentTypes -> {
	                	paymentTypes.setDesc_tipo_pago(paymentTypesRequest.getDesc_tipo_pago());
	                	paymentTypes.setNom_tipo_pago(paymentTypesRequest.getNom_tipo_pago());
	                	paymentTypes.setLast_update(tstamp.getTime());
	                    return paymentTypesRepository.save(paymentTypes);
	                }).orElseThrow(() -> new ResourceNotFoundException("paymentTypes not found with id " + idtipo_pago));
	    }


	    @DeleteMapping("/paymentTypes/{idtipo_pago}")
	    public ResponseEntity<?> deletepaymentTypes(@PathVariable Long idtipo_pago) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return paymentTypesRepository.findById(idtipo_pago)
	                .map(paymentTypes -> {
	                	paymentTypes.setLast_update(tstamp.getTime());
	                	paymentTypes.setIs_deleted(true);
	                	paymentTypesRepository.save(paymentTypes);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("paymentForms not found with id " + idtipo_pago));
	    }

}
