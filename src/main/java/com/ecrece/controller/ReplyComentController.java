package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.Coment;
import com.ecrece.model.Notification;
import com.ecrece.model.NotificationState;
import com.ecrece.model.NotificationType;
import com.ecrece.model.ReplyComent;
import com.ecrece.repository.ComentReposity;
import com.ecrece.repository.NotificationRepository;
import com.ecrece.repository.NotificationStatesRepository;
import com.ecrece.repository.NotificationTypeRepository;
import com.ecrece.repository.ReplyComentReposity;
import com.ecrece.repository.UserRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ReplyComentController {	

	    @Autowired
	    private ComentReposity comentRepository;
	    	    
	    @Autowired
	    private NotificationTypeRepository notificationtypeRepository;
	    
	    @Autowired
	    private NotificationStatesRepository notificationStatesRepository;
	    
	    @Autowired
	    private NotificationRepository notificationRepository;
	    
	    @Autowired
	    private ReplyComentReposity  replyComentReposity;
	    
	    @Autowired
	    private UserRepository  userRepository;
	  
	    @GetMapping("/replycoment/coment/{idcoment}")
	    public  List<ReplyComent> getComentsbyUser(@PathVariable(value = "idcoment") Integer idcoment) {
	        return replyComentReposity.findbyComent(idcoment);
	    }
	  	    
	    @PostMapping("/replycoment/idcoment/{idcoment}/iduser/{iduser}")
	    public ReplyComent createReplyComment(@PathVariable(value = "idcoment") Integer idcoment,
	    		@PathVariable(value = "iduser") Integer iduser,
	    		@Valid @RequestBody Coment replyComentpost) {	    	
	    	
	    	Coment coment = comentRepository.findById(idcoment).get();
	    	ReplyComent replycoment = new ReplyComent();
	    	replycoment.setBusiness(coment.getBusiness());
	    	replycoment.setComent(coment);
	    	replycoment.setDate_replycoment(new Timestamp(new Date().getTime()));
	    	replycoment.setDesc_replycoment(replyComentpost.getDesc_coment());	    	
	    	replycoment.setReplyforiduser(userRepository.findById(iduser).get());
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	replycoment.setCreate_at(tstamp);
	    	replycoment.setLast_update(tstamp.getTime());
	    	replycoment.setIs_deleted(false);
	    	
	    	Notification notification = new Notification();
	    	notification.setRead(false);
			notification.setFor_notification("Respuesta del Comentario "+coment.getTitle_coment());
			notification.setSubject_notification(
					"Respuesta del Comentario");
			notification.setCreate_date(new Date());
			notification.setDesc_notification("Se respondio el comentario del negocio "+ coment.getBusiness().getName_business());
			notification.setIdpointuser(null);
			notification.setIdpayment_form(null);
			List<NotificationType> notificationtypes = notificationtypeRepository.findAll();
			Optional<NotificationType> notitype = notificationtypes.stream()
					.filter(p -> p.getDesc_notification_type().equals("Informativa")).findFirst();
			if (notitype.isPresent()) {
				notification.setNotificationsType(notitype.get());
			} else {
				NotificationType notitype2 = new NotificationType();
				notitype2.setDesc_notification_type("Informativa");
				notificationtypeRepository.save(notitype2);
				notification.setNotificationsType(notitype2);

			}
			List<NotificationState> notificationstate = notificationStatesRepository.findAll();
			Optional<NotificationState> notistate1 = notificationstate.stream()
					.filter(p -> p.getNameNotificationstate().equals("Informativo")).findFirst();
			if (notistate1.isPresent()) {
				notification.setNotification_state(notistate1.get());
			} else {
				NotificationState notistate2 = new NotificationState();
				notistate2.setDescNotificationstate("En estado Informativo");
				notistate2.setNameNotificationstate("Informativo");
				notificationStatesRepository.save(notistate2);
				notification.setNotification_state(notistate2);

			}
			notification.setIdbusiness_to(coment.getBusiness());
			notification.setIduser_to(coment.getIduser());
			notification.setCreate_at(tstamp);
			notification.setLast_update(tstamp.getTime());
			notification.setIs_deleted(false);
			notificationRepository.save(notification);			
			
	        return replyComentReposity.save(replycoment);
	    }
}
