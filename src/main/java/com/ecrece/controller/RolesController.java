package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Role;
import com.ecrece.repository.RolesRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class RolesController {	

	    @Autowired
	    private RolesRepository rolesRepository;
	    
	
	        
	    @GetMapping("/roles")
	    public  List<Role> getRoles() {
	    	List<Role> roles = rolesRepository.findAll().stream()
					.filter(rol -> rol.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return roles;	
	    }

	    @GetMapping("/roles/{idrol}")
	    public Role getRole(@PathVariable(value = "idrol") Integer idrol) {
	        return rolesRepository.findById(idrol)
	                .orElseThrow(() -> new ResourceNotFoundException("Role"));
	    }
	    
	    @PostMapping("/roles")
	    public Role createRole(@Valid @RequestBody Role Role1) {
	    	Role ro = Role1;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	ro.setCreate_at(tstamp);
	    	ro.setLast_update(tstamp.getTime());
	    	ro.setIs_deleted(false);
	        return rolesRepository.save(ro);
	    }

	    @PutMapping("/roles/{idrol}")
	    public Role updateRole(@PathVariable Integer idrol,
	                                   @Valid @RequestBody Role RoleRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return rolesRepository.findById(idrol)
	                .map(Role -> {
	                	Role.setDesc_rol(RoleRequest.getDesc_rol());
	                	Role.setLast_update(tstamp.getTime());
	                    return rolesRepository.save(Role);
	                }).orElseThrow(() -> new ResourceNotFoundException("Role not found with id " + idrol));
	    }


	    @DeleteMapping("/roles/{idrol}")
	    public ResponseEntity<?> deleteRole(@PathVariable Integer idrol) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return rolesRepository.findById(idrol)
	                .map(Role -> {
	                	Role.setLast_update(tstamp.getTime());
	                	Role.setIs_deleted(true);
	                	rolesRepository.save(Role);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Role not found with id " + idrol));
	    }

}
