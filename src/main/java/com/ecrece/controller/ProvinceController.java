package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Country;
import com.ecrece.model.Province;
import com.ecrece.repository.CountryRepository;
import com.ecrece.repository.ProvinceRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ProvinceController {


	    @Autowired
	    private ProvinceRepository provinceRepository;
	    
	    @Autowired
	    private CountryRepository countryRepository;
	        
	    @GetMapping("/provinces")
	    public  List<Province> getProvinces() {
	    	List<Province> prov = provinceRepository.findAll().stream()
					.filter(province -> province.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return prov;	
	    }

	    @GetMapping("/provinces/{idprovince}")
	    public Province getProvince(@PathVariable(value = "idprovince") Integer idprovince) {
	        return provinceRepository.findById(idprovince)
	                .orElseThrow(() -> new ResourceNotFoundException("Province not found"+ idprovince));
	    }	    

	    
	    @PostMapping("countries/{idcountry}/provinces")
	    public Province createProvince(@PathVariable (value = "idcountry") Integer idcountry,
	                                 @Valid @RequestBody Province province) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        Province prov = province;
	        Country cou = countryRepository.findById(idcountry).get();
	        prov.setCountry(cou);
	        prov.setCreate_at(tstamp);
	        prov.setLast_update(tstamp.getTime());
	        prov.setIs_deleted(false);       
	        
	        return provinceRepository.save(prov);
	    }
	    
	    @PutMapping("countries/{idcountry}/provinces/{idprovince}")
	    public Province updateProvince(@PathVariable (value = "idcountry") Integer idcountry,
	                                 @PathVariable (value = "idprovince") Integer idprovince,
	                                 @Valid @RequestBody Province ProvinceRequest) {
	    	
	    
	        if(!countryRepository.existsById(idcountry)) {
	            throw new ResourceNotFoundException("Country "+"id"+idcountry);
	        }
	        Timestamp tstamp = new Timestamp(System.currentTimeMillis());
            Country cou = countryRepository.findById(idcountry).get();   
	        return provinceRepository.findById(idprovince).map(province -> {
	        	province.setName_province(ProvinceRequest.getName_province());
	        	province.setCountry(cou);
	        	province.setLast_update(tstamp.getTime());
	            return provinceRepository.save(province);
	        }).orElseThrow(() -> new ResourceNotFoundException("Province" + "id"+ idprovince));
	    }


	    @DeleteMapping("/provinces/{idprovince}")
	    public ResponseEntity<?> deleteSubcategoria(@PathVariable(value = "idprovince") Integer idprovince) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return provinceRepository.findById(idprovince)
	                .map(prov -> {
	                	prov.setLast_update(tstamp.getTime());
	                	prov.setIs_deleted(true);
	                	provinceRepository.save(prov);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Country not found with id " + idprovince));
	    }
}
