package com.ecrece.controller;

import java.sql.Timestamp;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.SearchBusiness;
import com.ecrece.repository.SearchBusinesRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class SearchBusinessController {	

	    @Autowired
	    private SearchBusinesRepository searchBusinesRepository;    
		   	    
	    @PostMapping("/business/search")
	    public SearchBusiness createCategory(@Valid @RequestBody SearchBusiness searchbusiness) {
	    	SearchBusiness sbusiness = searchbusiness;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	sbusiness.setCreate_at(tstamp);
	    	sbusiness.setLast_update(tstamp.getTime());
	    	sbusiness.setIs_deleted(false);
	        return searchBusinesRepository.save(sbusiness);
	    }

	    
}
