package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.ContractType;
import com.ecrece.repository.ContractTypeRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ContractTypeController {	
	   
	    @Autowired
	    private ContractTypeRepository contractTypeRepository;	   
		        
	   
		@GetMapping("/contracttype")
	    public  List<ContractType> getContractType() {
			List<ContractType> contracttypes = contractTypeRepository.findAll().stream()
					.filter(contracttype -> contracttype.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return contracttypes;	    
			}
	
	    @GetMapping("/contracttype/{idcontracttype}")
	    public ContractType getContractType(@PathVariable(value = "idcontracttype") Integer idcontracttype) {
	        return contractTypeRepository.findById(idcontracttype)
	                .orElseThrow(() -> new ResourceNotFoundException("ContractType"));
	    }
    
	    @PostMapping("/contracttype")
	    public ContractType createContractType(@Valid @RequestBody ContractType contractType) {
	    	ContractType newcontractType = contractType;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	newcontractType.setCreate_at(tstamp);
	    	newcontractType.setLast_update(tstamp.getTime());
	    	newcontractType.setIs_deleted(false);
	        return contractTypeRepository.save(newcontractType);
	    }

	    @PutMapping("/contracttype/{idcontracttype}")
	    public ContractType updateContractType(@PathVariable(value = "idcontracttype") Integer idcontracttype,
	                                   @Valid @RequestBody ContractType contractTypeRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return contractTypeRepository.findById(idcontracttype)
	                .map(contracttype -> {
	                	contracttype.setDesc_contracttype(contractTypeRequest.getDesc_contracttype());
	                	contracttype.setLast_update(tstamp.getTime());
	                    return contractTypeRepository.save(contracttype);
	                }).orElseThrow(() -> new ResourceNotFoundException("ContractType not found with id " + idcontracttype));
	    }
	    
	    @DeleteMapping("/contracttype/{idcontracttype}")
	    public ResponseEntity<?> deleteJob(@PathVariable(value = "idcontracttype") Integer idcontracttype) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	return contractTypeRepository.findById(idcontracttype)
	                .map(contracttype -> {
	                	contracttype.setLast_update(tstamp.getTime());
	                	contracttype.setIs_deleted(true);
	                	contractTypeRepository.save(contracttype);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("ContractType not found with id " + idcontracttype));
	    }

}
