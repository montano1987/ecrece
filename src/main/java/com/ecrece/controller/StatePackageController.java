package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.StatePackage;
import com.ecrece.repository.StatePackageRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class StatePackageController {	

	    @Autowired
	    private StatePackageRepository statePackageRepository;
	    
	
	        
	    @GetMapping("/statepackage")
	    public  List<StatePackage> getStatePackage() {
	    	List<StatePackage> spack = statePackageRepository.findAll().stream()
					.filter(statep -> statep.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return spack;	
	    }

	    @GetMapping("/statepackage/{id_state_package}")
	    public StatePackage getStatePackage(@PathVariable(value = "id_state_package") Integer id_state_package) {
	        return statePackageRepository.findById(id_state_package)
	                .orElseThrow(() -> new ResourceNotFoundException("StatePackage"));
	    }
	    
	    @PostMapping("/statepackage")
	    public StatePackage createStatePackage(@Valid @RequestBody StatePackage statePackage) {
	    	StatePackage spackage = statePackage;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	spackage.setCreate_at(tstamp);
	    	spackage.setLast_update(tstamp.getTime());
	    	spackage.setIs_deleted(false);
	        return statePackageRepository.save(spackage);
	    }

	    @PutMapping("/statepackage/{id_state_package}")
	    public StatePackage updateStatePackage(@PathVariable (value = "id_state_package") Integer id_state_package,
	                                   @Valid @RequestBody StatePackage StatePackageRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return statePackageRepository.findById(id_state_package)
	                .map(statePackage -> {
	                	statePackage.setState(StatePackageRequest.getState());
	                	statePackage.setLast_update(tstamp.getTime());
	                    return statePackageRepository.save(statePackage);
	                }).orElseThrow(() -> new ResourceNotFoundException("statePackage not found with id " + id_state_package));
	    }


	    @DeleteMapping("/statepackage/{id_state_package}")
	    public ResponseEntity<?> deleteStatePackage(@PathVariable (value = "id_state_package") Integer id_state_package) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return statePackageRepository.findById(id_state_package)
	                .map(StatePackage -> {
	                	StatePackage.setLast_update(tstamp.getTime());
	                	StatePackage.setIs_deleted(true);
	                	statePackageRepository.save(StatePackage);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("StatePackage not found with id " + id_state_package));
	    }

}
