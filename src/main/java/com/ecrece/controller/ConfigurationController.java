package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Configuration;
import com.ecrece.repository.ConfigurationRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ConfigurationController {	

	    @Autowired
	    private ConfigurationRepository configurationRepository;
	    
	
	        
	    @GetMapping("/configuration")
	    public  List<Configuration> getConfiguration() {
	    	List<Configuration> con = configurationRepository.findAll().stream()
					.filter(country -> country.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return con;	  
	    }

	    @GetMapping("/configuration/{id_configuration}")
	    public Configuration getConfiguration(@PathVariable(value = "id_configuration") Integer id_configuration) {
	        return configurationRepository.findById(id_configuration)
	                .orElseThrow(() -> new ResourceNotFoundException("Configuration"));
	    }
	    
	    @PostMapping("/configuration")
	    public Configuration createConfiguration(@Valid @RequestBody Configuration configuration) {
	    	Configuration conf = configuration;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	conf.setCreate_at(tstamp);
	    	conf.setLast_update(tstamp.getTime());
	    	conf.setIs_deleted(false);	    	
	        return configurationRepository.save(conf);
	    }

	    @PutMapping("/configuration/{id_configuration}")
	    public Configuration updateConfiguration(@PathVariable (value = "id_configuration") Integer id_configuration,
	                                   @Valid @RequestBody Configuration configurationRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return configurationRepository.findById(id_configuration)
	                .map(configuration -> {
	                	configuration.setLatitude(configurationRequest.getLatitude());
	                	configuration.setLongitude(configurationRequest.getLongitude());
	                	configuration.setPtos_star(configurationRequest.getPtos_star());
	                	configuration.setDaysfuncionality(configurationRequest.getDaysfuncionality());
	                	configuration.setRecordbypage(configurationRequest.getRecordbypage());
	                	configuration.setLast_update(tstamp.getTime());
	                    return configurationRepository.save(configuration);
	                }).orElseThrow(() -> new ResourceNotFoundException("Configuration not found with id " + id_configuration));
	    }


	    @DeleteMapping("/configuration/{id_configuration}")
	    public ResponseEntity<?> deleteConfiguration(@PathVariable (value = "id_configuration") Integer id_configuration) {
	        return configurationRepository.findById(id_configuration)
	                .map(configuration -> {
	                    configurationRepository.delete(configuration);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Configuration not found with id " + id_configuration));
	    }

}
