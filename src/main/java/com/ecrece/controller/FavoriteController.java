package com.ecrece.controller;


import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.Business;
import com.ecrece.model.Client;
import com.ecrece.model.Favorite;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.ClientReposity;
import com.ecrece.repository.FavoriteRepository;
import com.ecrece.repository.UserRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class FavoriteController {
	
	    @Autowired
	    private FavoriteRepository favoriteRepository;
	    
	    @Autowired
	    private BusinessRepository businessRepository;
	    
	    @Autowired
	    private UserRepository userRepository;
	    
	    @Autowired
	    private ClientReposity clientReposity;
	        
	    @GetMapping("/favorite/business/{idbusiness}/user/{iduser}")
	    public boolean  isfavorite(@PathVariable (value = "idbusiness") Integer idbusiness,
	    		@PathVariable (value = "iduser") Integer iduser) {
		        Favorite fav = favoriteRepository.findByBusiness(idbusiness,iduser);
		        if(fav != null) 
		        {
		        	return true;
		        }
		        else
		        {
		        	return false;
		        }
	    }
	    
	    @PostMapping("/favorite/check/business/{idbusiness}/user/{iduser}")
	    public boolean createFavorite(@PathVariable (value = "idbusiness") Integer idbusiness,
	    		@PathVariable (value = "iduser") Integer iduser) {
	    	
	    	Favorite fav = new Favorite();
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	fav.setCreate_at(tstamp);
	    	fav.setLast_update(tstamp.getTime());
	    	fav.setIs_deleted(false);
	    	Business busi = businessRepository.findById(idbusiness).get();
	    	
	    	fav.setBusiness(busi);
	    	fav.setUser(userRepository.findById(iduser).get());
	    	 favoriteRepository.save(fav);
	    	 Client cli = clientReposity.findbyBusiness(idbusiness,iduser);
	    	 if(cli == null)
	    	 {
	    		 Client newclient = new Client();
	    		 newclient.setBusiness(busi);
	    		 newclient.setIduser(userRepository.findById(iduser).get());
	    		 clientReposity.save(newclient);
	    	 }
	    		 
	    	 
	    	 return true;
	    }
	    @PostMapping("/favorite/uncheck/business/{idbusiness}/user/{iduser}")
	    public boolean uncheckFavorite(@PathVariable (value = "idbusiness") Integer idbusiness,
	    		@PathVariable (value = "iduser") Integer iduser) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	Favorite fav = favoriteRepository.findByBusiness(idbusiness,iduser);
	    	fav.setLast_update(tstamp.getTime());
	    	fav.setIs_deleted(true);
	    	favoriteRepository.save(fav);
	    	return false;
	    }

	   
}
