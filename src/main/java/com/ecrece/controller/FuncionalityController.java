package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Funcionality;
import com.ecrece.repository.FuncionalityRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class FuncionalityController {	

	    @Autowired
	    private FuncionalityRepository funcionalityRepository;
	    
	
	        
	    @GetMapping("/funcionalities")
	    public  List<Funcionality> getfuncionalities() {
	    	List<Funcionality> fun = funcionalityRepository.findAll().stream()
					.filter(funcio -> funcio.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return fun;
	    }
	    
	    @GetMapping("/funcionalities/notsystem")
	    public  List<Funcionality> getfuncionalitiesnotsystem() {
	        return funcionalityRepository.findfuncionalitynotsystem();
	    }
	    @GetMapping("/funcionalities/findall/{namefunc}/{descfunc}/{pointfunc}/{plusfunc}/{lessfunc}/{dayfunc}")
	    public  List<Funcionality> findforall(@PathVariable(value = "namefunc") String namefunc,
	    		@PathVariable(value = "descfunc") String descfunc,
	    		@PathVariable(value = "pointfunc") Integer pointfunc,
	    		@PathVariable(value = "plusfunc") Integer plusfunc,
	    		@PathVariable(value = "lessfunc") Integer lessfunc,
	    		@PathVariable(value = "dayfunc") Integer dayfunc) {
	        return funcionalityRepository.findforall(namefunc,descfunc,pointfunc,plusfunc,lessfunc,dayfunc);
	    }
	    @GetMapping("/funcionalities/system")
	    public  List<Funcionality> findfuncionalitysystem() {
	        return funcionalityRepository.findfuncionalitysystem();
	    }

	    @GetMapping("/funcionalities/{idfuncionality}")
	    public Funcionality getFuncionality(@PathVariable(value = "idfuncionality") Integer idfuncionality) {
	        return funcionalityRepository.findById(idfuncionality)
	                .orElseThrow(() -> new ResourceNotFoundException("funcionality"));
	    }
	    
	    @PostMapping("/funcionalities")
	    public Funcionality createFuncionality(@Valid @RequestBody Funcionality funcionality) {
	    	Funcionality funcio = funcionality;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	funcio.setCreate_at(tstamp);
	    	funcio.setLast_update(tstamp.getTime());
	    	funcio.setIs_deleted(false);
	        return funcionalityRepository.save(funcio);
	    }

	    @PutMapping("/funcionalities/{idfuncionality}")
	    public Funcionality updateFuncionality(@PathVariable Integer idfuncionality,
	                                   @Valid @RequestBody Funcionality funcionalityRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return funcionalityRepository.findById(idfuncionality)
	                .map(funcionality -> {
	                	funcionality.setDescFuncionality(funcionalityRequest.getDescFuncionality());
	                	funcionality.setNameFuncionality(funcionalityRequest.getNameFuncionality());
	                	funcionality.setPointFuncionality(funcionalityRequest.getPointFuncionality());
	                	funcionality.setPoint_less(funcionalityRequest.getPoint_less());
	                	funcionality.setPoint_plus(funcionalityRequest.getPoint_plus());
	                	funcionality.setFuncionality_days(funcionalityRequest.getFuncionality_days());
	                	funcionality.setLast_update(tstamp.getTime());
	                    return funcionalityRepository.save(funcionality);
	                }).orElseThrow(() -> new ResourceNotFoundException("Funcionality not found with id " + idfuncionality));
	    }

	    @DeleteMapping("/funcionalities/{idfuncionality}")
	    public ResponseEntity<?> deleteFuncionality(@PathVariable Integer idfuncionality) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return funcionalityRepository.findById(idfuncionality)
	                .map(funcionality -> {
	                	funcionality.setLast_update(tstamp.getTime());
	                	funcionality.setIs_deleted(true);
	                	funcionalityRepository.save(funcionality);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Funcionality not found with id " + idfuncionality));
	    }

}
