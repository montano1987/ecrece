package com.ecrece.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.Clasification;
import com.ecrece.repository.ClasificationRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ClasificationController {	

	    @Autowired
	    private ClasificationRepository clasificationRepository;
	   	           
	    @GetMapping("/clasification")
	    public  List<Clasification> getAll() {
	        List<Clasification> calif = clasificationRepository.findAll().stream()
					.filter(calification -> calification.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return calif;	
	    }
	    
	    @GetMapping("/clasificationbyuser/{iduser}")
	    public  List<Clasification> getclasificationbyuser(@PathVariable(value = "iduser") Integer iduser) {
	        return clasificationRepository.findclasificationbyuser(iduser);
	    }
	    @GetMapping("/clasificationbybusiness/{idbusiness}")
	    public  List<Clasification> getclasificationbybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	        return clasificationRepository.findclasificationbybusiness(idbusiness);
	    }
	    
}
