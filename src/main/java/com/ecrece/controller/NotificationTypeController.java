package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.NotificationType;
import com.ecrece.repository.NotificationTypeRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class NotificationTypeController {	

	    @Autowired
	    private NotificationTypeRepository typenotificationRepository;
	    
	
	        
	    @GetMapping("/typeNotification")
	    public  List<NotificationType> getTypeNotification() {
	    	List<NotificationType> ntype = typenotificationRepository.findAll().stream()
					.filter(notitype -> notitype.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return ntype;	
	    }

	    @GetMapping("/typeNotification/{idnotification_form}")
	    public NotificationType getTypeNotification(@PathVariable(value = "idnotification_form") Integer idnotification_form) {
	        return typenotificationRepository.findById(idnotification_form)
	                .orElseThrow(() -> new ResourceNotFoundException("TypeNotification not found"));
	    }
	    
	    @PostMapping("/typeNotification")
	    public NotificationType createTypeNotification(@Valid @RequestBody NotificationType notificationType) {
	    	NotificationType ntype = notificationType;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	ntype.setCreate_at(tstamp);
	    	ntype.setLast_update(tstamp.getTime());
	    	ntype.setIs_deleted(false);
	        return typenotificationRepository.save(ntype);
	    }

	    @PutMapping("/typeNotification/{idnotification_form}")
	    public NotificationType updateTypeNotification(@PathVariable Integer idnotification_form,
	                                   @Valid @RequestBody NotificationType typeNotificationRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return typenotificationRepository.findById(idnotification_form)
	                .map(tipo_notificacion -> {
	                	tipo_notificacion.setDesc_notification_type(typeNotificationRequest.getDesc_notification_type());
	                	tipo_notificacion.setName_notification_type(typeNotificationRequest.getName_notification_type());
	                	tipo_notificacion.setLast_update(tstamp.getTime());
	                    return typenotificationRepository.save(tipo_notificacion);
	                }).orElseThrow(() -> new ResourceNotFoundException("tipo_notificacion not found with id " + idnotification_form));
	    }


	    @DeleteMapping("/typeNotification/{idnotification_form}")
	    public ResponseEntity<?> deletetypeNotification(@PathVariable Integer idnotification_form) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return typenotificationRepository.findById(idnotification_form)
	                .map(tipo_notificacion -> {
	                	tipo_notificacion.setLast_update(tstamp.getTime());
	                	tipo_notificacion.setIs_deleted(true);
	                	typenotificationRepository.save(tipo_notificacion);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("tipo_notificacion not found with id " + idnotification_form));
	    }

}
