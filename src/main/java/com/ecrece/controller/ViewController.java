package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.View;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.UserRepository;
import com.ecrece.repository.ViewRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ViewController {	
	    @Autowired
	    private ViewRepository visitsRepository;   
	    
	    @Autowired
	    private UserRepository userRepository;  
	    
	    @Autowired
	    private BusinessRepository businessRepository;
	
	    @GetMapping("/views")
	    public  List<View> getViews() {
	    	List<View> vie = visitsRepository.findAll().stream()
					.filter(view -> view.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return vie;	
	    }
	    
	    @PostMapping("/views/iduser/{iduser}/business/{idbusiness}")
	    public View createView(@PathVariable(value = "iduser") Integer iduser,
	    						@PathVariable(value = "idbusiness") Integer idbusiness) {
	    	View visi = new View();
	    	visi.setBusiness(businessRepository.findById(idbusiness).get());
	    	visi.setCreate_date(new Date());
	    	visi.setUser(userRepository.findById(iduser).get());	 
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	visi.setCreate_at(tstamp);
	    	visi.setLast_update(tstamp.getTime());
	    	visi.setIs_deleted(false);
	        return visitsRepository.save(visi);
	    }

}
