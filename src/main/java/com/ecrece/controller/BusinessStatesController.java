package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.BusinessStates;
import com.ecrece.repository.BusinessStatesRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class BusinessStatesController {	

	    @Autowired
	    private BusinessStatesRepository businessStatesRepository;
	    
	
	        
	    @GetMapping("/businessStates")
	    public  List<BusinessStates> getBusinessStates() {	        
	        List<BusinessStates> bstatess = businessStatesRepository.findAll().stream()
					.filter(bstate -> bstate.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return bstatess;
	    }

	    @GetMapping("/businessStates/{idstate_business}")
	    public BusinessStates getBusinessStates(@PathVariable(value = "idstate_business") Integer idstate_business) {
	        return businessStatesRepository.findById(idstate_business)
	                .orElseThrow(() -> new ResourceNotFoundException("BusinessStates not found"));
	    }
	    @GetMapping("/businessStates/findall/{descbusinessstate}")
	    public List<BusinessStates> findall(@PathVariable(value = "descbusinessstate") String descbusinessstate) {
	        List<BusinessStates> sta = businessStatesRepository.findforAll(descbusinessstate);	    	
	    	return sta;
	    }
	    
	    @PostMapping("/businessStates")
	    public BusinessStates createBusinessStates(@Valid @RequestBody BusinessStates businessStates) {
	    	
	    	BusinessStates bstate = businessStates;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	bstate.setCreate_at(tstamp);
	    	bstate.setLast_update(tstamp.getTime());
	    	bstate.setIs_deleted(false);
	        return businessStatesRepository.save(businessStates);
	    }

	    @PutMapping("/businessStates/{idstate_business}")
	    public BusinessStates updateBusinessStates(@PathVariable Integer idstate_business,
	                                   @Valid @RequestBody BusinessStates businessStatesRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return businessStatesRepository.findById(idstate_business)
	                .map(businessStates -> {
	                	businessStates.setName_state_business(businessStatesRequest.getName_state_business());
	                	businessStates.setLast_update(tstamp.getTime());
	                    return businessStatesRepository.save(businessStates);
	                }).orElseThrow(() -> new ResourceNotFoundException("BusinessStates not found with id " + idstate_business));
	    }


	    @DeleteMapping("/businessStates/{idstate_business}")
	    public ResponseEntity<?> deleteBusinessStates(@PathVariable Integer idstate_business) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return businessStatesRepository.findById(idstate_business)
	                .map(businessStates -> {
	                	businessStates.setLast_update(tstamp.getTime());
	                	businessStates.setIs_deleted(true);
	                	businessStatesRepository.save(businessStates);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("businessStates not found with id " + idstate_business));
	    }

}
