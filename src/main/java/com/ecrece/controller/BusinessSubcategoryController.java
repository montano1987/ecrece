/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.BusinessSubcategory;
import com.ecrece.model.Subcategory;
import com.ecrece.repository.BusinessSubcategoryRepository;

/**
 *
 * @author disney
 */
@RestController
@CrossOrigin
@RequestMapping("/api")
public class BusinessSubcategoryController {

	@Autowired
	private BusinessSubcategoryRepository businessSubcategoryRepository;



	@GetMapping("/businessSubcategory/business/{idbusiness}")
    public List<Subcategory> getbusinessSubcategory(@PathVariable(value = "idbusiness") Integer idbusiness) {
    	return businessSubcategoryRepository.findByBusiness(idbusiness);
    }

	@PostMapping("/businessSubcategory")
	public BusinessSubcategory createBusiness(@Valid @RequestBody BusinessSubcategory businessSubcategory) {
		BusinessSubcategory bsubcategory = businessSubcategory;
    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
    	bsubcategory.setCreate_at(tstamp);
    	bsubcategory.setLast_update(tstamp.getTime());
    	bsubcategory.setIs_deleted(false);
		return businessSubcategoryRepository.save(bsubcategory);
	}


}
