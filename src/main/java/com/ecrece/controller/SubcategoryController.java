package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Subcategory;
import com.ecrece.repository.CategoryRepository;
import com.ecrece.repository.SubcategoryRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class SubcategoryController {
	    @Autowired
	    private SubcategoryRepository subcategoryRepository;
	    
	    @Autowired
	    private CategoryRepository categoryRepository;
	        
	    @GetMapping("/subcategories")
	    public  List<Subcategory> getSubcategories() {
	    	List<Subcategory> subca = subcategoryRepository.findAll().stream()
					.filter(country -> country.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return subca;	 
	    }

	    @GetMapping("/subcategories/{idsubcategory}")
	    public Subcategory getSubcategory(@PathVariable(value = "idsubcategory") Integer idsubcategory) {
	        return subcategoryRepository.findById(idsubcategory)
	                .orElseThrow(() -> new ResourceNotFoundException("Subcategory not found"+ idsubcategory));
	    }
	    @GetMapping("/subcategories/category/{idcategory}")
	    public List<Subcategory> getListSubcategory(@PathVariable(value = "idcategory") Integer idcategory) {
	        return subcategoryRepository.findByCategory(idcategory);
	    }
	    
//	    @GetMapping("/categories/{idcategory}/subcategories")
//	    public List<Subcategory> getSubcategoriesByCategoryId(@PathVariable (value = "idcategory") Integer idcategory) {
//	        return subcategoryRepository.findByCategoria(idcategory);
//	        }
	    
	    @PostMapping("categories/{idcategory}/subcategories")
	    public Subcategory createSubcategory(@PathVariable (value = "idcategory") Integer idcategory,
	                                 @Valid @RequestBody Subcategory Subcategory) {
	    	
	        Subcategory subcategory1 = Subcategory;
	        categoryRepository.findById(idcategory).map(category -> {
	        	subcategory1.setCategory(category);
	        	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        	subcategory1.setCreate_at(tstamp);
	        	subcategory1.setLast_update(tstamp.getTime());
	        	subcategory1.setIs_deleted(false);
	            return subcategory1;
	        }).orElseThrow(() -> new ResourceNotFoundException("Categories "+"id"+idcategory));
	        
	        
	        return subcategoryRepository.save(subcategory1);
	    }

	    @PutMapping("categories/{idcategory}/subcategories/{idsubcategory}")
	    public Subcategory updateSubcategory(@PathVariable (value = "idcategory") Integer idcategory,
	                                 @PathVariable (value = "idsubcategory") Integer idsubcategory,
	                                 @Valid @RequestBody Subcategory SubcategoryRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return subcategoryRepository.findById(idsubcategory)
	                .map(subcate -> {
	                	subcate.setName_subcategory(SubcategoryRequest.getName_subcategory());
	                	subcate.setCategory(categoryRepository.findById(idcategory).get());
	                	subcate.setLast_update(tstamp.getTime());
	                    return subcategoryRepository.save(subcate);
	                }).orElseThrow(() -> new ResourceNotFoundException("Country not found with id " + idsubcategory));

	    }


	    @DeleteMapping("/subcategories/{idsubcategory}")
	    public ResponseEntity<?> deleteSubcategoria(@PathVariable(value = "idsubcategory") Integer idsubcategory) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return subcategoryRepository.findById(idsubcategory)
	                .map(subcate -> {
	                	subcate.setLast_update(tstamp.getTime());
	                	subcate.setIs_deleted(true);
	                	subcategoryRepository.save(subcate);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Subcategory not found with id " + idsubcategory));
	    }
}
