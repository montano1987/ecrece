package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.PointPackage;
import com.ecrece.repository.PointPackageRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class PointPackageController {	

	
	    @Autowired
	    private PointPackageRepository pointPackageRepository;
	    
	
	        
	    @GetMapping("/pointpackage")
	    public  List<PointPackage> getCountries() {
	    	List<PointPackage> pointP = pointPackageRepository.findAll().stream()
					.filter(ppackege -> ppackege.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return pointP;	 
	    }

	    @GetMapping("/pointpackage/{id_package}")
	    public PointPackage getPointPackage(@PathVariable(value = "id_package") Integer id_package) {
	        return pointPackageRepository.findById(id_package)
	                .orElseThrow(() -> new ResourceNotFoundException("PointPackage"));
	    }
	    
	    @PostMapping("/pointpackage")
	    public PointPackage createPointPackage(@Valid @RequestBody PointPackage pointPackage) {
	    	PointPackage ppackage = pointPackage;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	ppackage.setCreate_at(tstamp);
	    	ppackage.setLast_update(tstamp.getTime());
	    	ppackage.setIs_deleted(false);
	        return pointPackageRepository.save(ppackage);
	    }

	    @PutMapping("/pointpackage/{id_package}")
	    public PointPackage updatePointPackage(@PathVariable Integer id_package,
	                                   @Valid @RequestBody PointPackage pointPackageRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return pointPackageRepository.findById(id_package)
	                .map(pointpackage -> {
	                	pointpackage.setCost(pointPackageRequest.getCost());
	                	pointpackage.setPoint_value(pointPackageRequest.getPoint_value());
	                	pointpackage.setLast_update(tstamp.getTime());
	                    return pointPackageRepository.save(pointpackage);
	                }).orElseThrow(() -> new ResourceNotFoundException("PointPackage not found with id " + id_package));
	    }


	    @DeleteMapping("/pointpackage/{id_package}")
	    public ResponseEntity<?> deletePointPackage(@PathVariable Integer id_package) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return pointPackageRepository.findById(id_package)
	                .map(pointpackage -> {
	                	pointpackage.setLast_update(tstamp.getTime());
	                	pointpackage.setIs_deleted(true);
	                	pointPackageRepository.save(pointpackage);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("PointPackage not found with id " + id_package));
	    }

}
