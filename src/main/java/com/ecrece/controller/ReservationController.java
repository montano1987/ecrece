package com.ecrece.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.Reservation;
import com.ecrece.repository.ReservationReposity;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ReservationController {	

	    @Autowired
	    private ReservationReposity reservationReposity;
	        
	    @GetMapping("/reservation")
	    public  List<Reservation> getRoles() {
	    	List<Reservation> rese = reservationReposity.findAll().stream()
					.filter(reserv -> reserv.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return rese;	
	    }

	    @GetMapping("/reservation/{iduser}")
	    public List<Reservation> getreservationbyuser(@PathVariable(value = "iduser") Integer iduser) {
	       List<Reservation> rese = reservationReposity.getallreservationbyuser(iduser);
	    	return rese;
	    }
	    @GetMapping("/reservation/business/{idbusiness}")
	    public List<Reservation> getreservationbybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	       List<Reservation> rese = reservationReposity.getallreservationbybusiness(idbusiness);
	    	return rese;
	    }

}
