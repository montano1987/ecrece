package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.ScheduleJobOffer;
import com.ecrece.repository.ScheduleJobOfferRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ScheduleJobOfferController {	
	   
	    @Autowired
	    private ScheduleJobOfferRepository scheduleJobOfferRepository;	   
		        
	   
		@GetMapping("/schedulejoboffer")
	    public  List<ScheduleJobOffer> getScheduleJobOffer() {
			List<ScheduleJobOffer> schedulejoboffers = scheduleJobOfferRepository.findAll().stream()
					.filter(schedulejoboff -> schedulejoboff.getIs_deleted().equals(false))
					.collect(Collectors.toList());
			return schedulejoboffers;	    
			}
	
	    @GetMapping("/schedulejoboffer/{idschedulejoboffer}")
	    public ScheduleJobOffer getScheduleJobOffer(@PathVariable(value = "idschedulejoboffer") Integer idschedulejoboffer) {
	        return scheduleJobOfferRepository.findById(idschedulejoboffer)
	                .orElseThrow(() -> new ResourceNotFoundException("ScheduleJobOffer"));
	    }
    
	    @PostMapping("/schedulejoboffer")
	    public ScheduleJobOffer createScheduleJobOffer(@Valid @RequestBody ScheduleJobOffer scheduleJobOffer) {
	    	ScheduleJobOffer newscheduleJobOffer = scheduleJobOffer;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	newscheduleJobOffer.setCreate_at(tstamp);
	    	newscheduleJobOffer.setLast_update(tstamp.getTime());
	    	newscheduleJobOffer.setIs_deleted(false);
	        return scheduleJobOfferRepository.save(newscheduleJobOffer);
	    }

	    @PutMapping("/schedulejoboffer/{idschedulejoboffer}")
	    public ScheduleJobOffer updateScheduleJobOffer(@PathVariable(value = "idschedulejoboffer") Integer idschedulejoboffer,
	                                   @Valid @RequestBody ScheduleJobOffer ScheduleJobOfferRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return scheduleJobOfferRepository.findById(idschedulejoboffer)
	                .map(schejoboffer -> {
	                	schejoboffer.setDesc_schedu(ScheduleJobOfferRequest.getDesc_schedu());
	                	schejoboffer.setLast_update(tstamp.getTime());
	                    return scheduleJobOfferRepository.save(schejoboffer);
	                }).orElseThrow(() -> new ResourceNotFoundException("ScheduleJobOffer not found with id " + idschedulejoboffer));
	    }


	    @DeleteMapping("/schedulejoboffer/{idschedulejoboffer}")
	    public ResponseEntity<?> deleteJob(@PathVariable(value = "idschedulejoboffer") Integer idschedulejoboffer) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	return scheduleJobOfferRepository.findById(idschedulejoboffer)
	                .map(jobitem -> {
	                	jobitem.setLast_update(tstamp.getTime());
	                	jobitem.setIs_deleted(true);
	                	scheduleJobOfferRepository.save(jobitem);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("ScheduleJobOffer not found with id " + idschedulejoboffer));
	    }

}
