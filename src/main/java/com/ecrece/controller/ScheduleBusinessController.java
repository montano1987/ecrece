package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.model.ScheduleBusiness;
import com.ecrece.repository.BusinessRepository;
import com.ecrece.repository.ScheduleBusinessRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class ScheduleBusinessController {
	
		
		@Autowired
	    private ScheduleBusinessRepository scheduleBusinessRepository;
	    
	    @Autowired
	    private BusinessRepository businessRepository;
	    
	    @GetMapping("/scheduleBusiness/business/{idbusiness}")
	    public List<ScheduleBusiness> getScheduleBusinessforBusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	    	return scheduleBusinessRepository.findByBusiness(idbusiness);
	    }
	    
	    @PostMapping("/business/{idbusiness}/schedule")
	    public ScheduleBusiness createScheduleBusiness(@PathVariable(value = "idbusiness") Integer idbusiness,
	    										@Valid @RequestBody ScheduleBusiness scheduleBusinessrequest) {    
	
	    	ScheduleBusiness scheduleBusiness = new ScheduleBusiness();
	    	scheduleBusiness = scheduleBusinessrequest;
	    	scheduleBusiness.setBusiness(businessRepository.findById(idbusiness).get());	    	
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	scheduleBusiness.setCreate_at(tstamp);
	    	scheduleBusiness.setLast_update(tstamp.getTime());
	    	scheduleBusiness.setIs_deleted(false);
	    	return scheduleBusinessRepository.save(scheduleBusiness);
	        
	    }
	    
	   
}
