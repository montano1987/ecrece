package com.ecrece.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecrece.exception.ResourceNotFoundException;
import com.ecrece.model.Offer;
import com.ecrece.repository.OfferRepository;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class OfferController {	

	    @Autowired
	    private OfferRepository offerRepository;
	   	           
	    @GetMapping("/offersall")
	    public  List<Offer> getAll() {
	        return offerRepository.getoffersadmin();
	    }
	    
	    @GetMapping("/offers/business/{idbusiness}")
	    public  List<Offer> getoffersbybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	        return offerRepository.findAllbybusiness(idbusiness);
	    }
	    @GetMapping("/offers/user/{iduser}")
	    public  List<Offer> getoffersbyuser(@PathVariable(value = "iduser") Integer iduser) {
	    	List<Offer> offe = offerRepository.getoffersbyuser(iduser);
	        return offe;
	    }	    
	    @GetMapping("/offers/all/business/{idbusiness}")
	    public  List<Offer> getalloffersbybusiness(@PathVariable(value = "idbusiness") Integer idbusiness) {
	    	List<Offer> offe = offerRepository.getallofferbybusiness(idbusiness);
	        return offe;
	    }
	    
	    @GetMapping("/offers/active")
	    public  List<Offer> getoffersactive (){
	        return offerRepository.getoffersactive();
	    }

	    @GetMapping("/offers/{idoffer}")
	    public Offer getOffer(@PathVariable(value = "idoffer") Integer idoffer) {
	        return offerRepository.findById(idoffer)
	                .orElseThrow(() -> new ResourceNotFoundException("idoffer"));
	    }
	    
	    @PostMapping("/offers")
	    public Offer createOffer(@Valid @RequestBody Offer offer) {
	    	Offer off = offer;
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	    	off.setCreate_at(tstamp);
	    	off.setLast_update(tstamp.getTime());
	    	off.setIs_deleted(false);
	        return offerRepository.save(off);
	    }
	    
	    @PutMapping("/offers/{idoffer}")
	    public Offer updateOffer(@PathVariable (value = "idoffer") Integer idoffer,
	                                   @Valid @RequestBody Offer offerRequest) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return offerRepository.findById(idoffer)
	                .map(offer -> {
	                	offer.setDate_end(offerRequest.getDate_end());
	                	offer.setDate_start(offerRequest.getDate_start());
	                	offer.setDescOffer(offerRequest.getDescOffer());
	                	offer.setNameOffer(offerRequest.getNameOffer());
	                	offer.setLast_update(tstamp.getTime());
	                    return offerRepository.save(offer);
	                }).orElseThrow(() -> new ResourceNotFoundException("Offer not found with id " + idoffer));
	    }

	 

	    @DeleteMapping("/offers/{idoffer}")
	    public ResponseEntity<?> deleteOffer(@PathVariable Integer idoffer) {
	    	Timestamp tstamp = new Timestamp(System.currentTimeMillis());
	        return offerRepository.findById(idoffer)
	                .map(offer -> {
	                	offer.setLast_update(tstamp.getTime());
	                	offer.setIs_deleted(true);
	                	offerRepository.save(offer);
	                    return ResponseEntity.ok().build();
	                }).orElseThrow(() -> new ResourceNotFoundException("Offer not found with id " + idoffer));
	    }

}
