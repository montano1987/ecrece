package com.ecrece;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.ecrece.security.JWTAuthorizationFilter;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaAuditing
@EnableScheduling
public class EcreceApplication extends SpringBootServletInitializer{
	
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	        return application.sources(EcreceApplication.class);
	    }
	public static void main(String[] args) {		
		SpringApplication.run(EcreceApplication.class, args);
				
	}

	@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers("/api/user/and/pass","/api/users/exist/{nameUser}/email/{mail}",
						"/api/users/rol/{idrol}",
						"/api/business/active",
						"/api/users/forgotpass/{email}",
						"/api/configuration",
						"/api/categories",
						"/api/municipality",
						"/api/languages",
						"/api/roles","/api/pointpackage","/api/paymentForms",
						"/api/activationcode/{activationcode}",
						"/api/users/exist/{nameUser}",
						"/api/asigpointuser/{iduser}",
						"/api/users/",
						"/api/business/top/",
						"/api/funcionalities",
						"/api/business/{idbusiness}",
						"/api/business/{idbusiness}/funcionalities",
						"/api/business/{idbusiness}/avgclasification/",
						"/api/business/{idbusiness}/countcalification/",
						"/api/coment/business/{idbusiness}",
						"/api/promotions",	
						"/api/promotion/details/{idpromotion}",
						"/api/offers/{idoffer}",
						"/api/offers/active",
						"/api/funcionalities/notsystem",
						"/api/activeuser",
						"/api/business/{idbusiness}/listreservation",
						"/api/users/sendmailregistration",	
						"/api/users/{iduser}/sendmailwithtoken",
						"/api/users/newpass/{newpass}",
						"/api/funcionalities/system",
						"/api/listofferbybusiness/{idbusiness}",
						"/api/listpromobybusiness/{idbusiness}",
						"/api/user/invited/{usercode}",
						"/api/image/path",
						"/api/business/schedule/{idbusiness}",
						"/api/businessSubcategory/business/{idbusiness}",
						"/api/favorite/business/{idbusiness}/user/{iduser}",
						"/api/users/token/{token}",
						"/api/visitqr/business/{idbusiness}/qr",
						"/api/job/offer",
						"/",
						"/css/**",
						"/img/**",
						"/js/**",
						"/fonts/**",
						"/i18n/**",
						"/resources/static/**",
						"/favicon.ico",
						"/vedor/fontawesome-free/css/**",
						"/vendor/fontawesome-free/webfonts/**",
						"/vendor/bootstrap/js/**",
						"/vendor/bootstrap/css/**",
						"/vendor/simple-line-icons/css/**",
						"/vendor/fontawesome-free/css/**",
						"/vendor/jquery/**",
						"/html/home/head-home.html",
						"/html/home/sessiontwo-home.html",
						"/html/home/sessionthree-home.html",
						"/html/home/sessionfour-home.html",
						"/html/users/login.html",
						"/html/business/business-list-search.html",
						"/html/business/business-det.html",
						"/html/promotions/promotion-detail.html",
						"/html/offers/offer-detail.html",
						"/html/modificationuser/forgopassword.html",
						"/html/modificationuser/changepass.html",
						"/html/static-pages/invitado.html",
						"/html/reservation/reservation-detail.html",
						"/files/**",
						"/img/**",
						"/html/nomencladores/registeruser/**").permitAll()
				.anyRequest().authenticated();
		}
	}
	
	
}
