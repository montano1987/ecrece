package com.ecrece.util;

import java.io.IOException;

public class Idioma extends java.util.Properties {
	
	private static final long serialVersionUID = 1L;
		 
    public Idioma(String idioma){ 
        //Modificar si quieres añadir mas idiomas
        //Cambia el nombre de los ficheros o añade los necesarios
    	 if(idioma.equals("ES")){//español
             getProperties("_ES.properties");
         }else if(idioma.equals("EN")){//Ingles
             getProperties("_EN.properties");
         }else{//sino por default idioma español
             getProperties("ES");
         }
 
    }
 
    public Idioma() {
		// TODO Auto-generated constructor stub
    	 getProperties("ES");
	}

	private void getProperties(String idioma) {
        try {
            this.load( getClass().getResourceAsStream(idioma)
            		);
        } catch (IOException ex) {
 
        }
   }
}

