package com.ecrece.util;
import com.google.zxing.*;
import com.google.zxing.Reader;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;

import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.*;

@Service
public class QRCode {
	private String UPLOAD_FOLDER;
	ApplicationHome home = new ApplicationHome(this.getClass()); 
	 public void writeQR(String text, String namefile) throws WriterException, IOException {
	
		 UPLOAD_FOLDER = home.getDir().toString().replace("com"+File.separator+"ecrece"+File.separator+"util","")+File.separator+"static"+File.separator+"files"+File.separator;
		 System.out.print(home.getDir().toString());
	        int width = 600;
	        int height = 400;
	        namefile = namefile.replace(" ","_");
	        
	        String imageFormat = "png"; // "jpeg" "gif" "tiff"

	        BitMatrix bitMatrix = new QRCodeWriter().encode(text, BarcodeFormat.QR_CODE, width, height);
	        FileOutputStream outputStream = new FileOutputStream(new File(UPLOAD_FOLDER +namefile+"QR"+".png"));
	        MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, outputStream);
	    }

	 public String readQR(String pathname) throws FormatException, ChecksumException, NotFoundException, IOException {

	        InputStream qrInputStream = new FileInputStream(pathname);
	        BufferedImage qrBufferedImage = ImageIO.read(qrInputStream);

	        LuminanceSource source = new BufferedImageLuminanceSource(qrBufferedImage);
	        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
	        Reader reader = new MultiFormatReader();
	        Result stringBarCode = reader.decode(bitmap);

	        return stringBarCode.getText();
	    }
}
