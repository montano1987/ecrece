package com.ecrece.util;

import java.io.File;

public class Constantes {
 
	//SUPER ADMINISTRADOR
	public static final String C_ROL_SUPER_ADMIN ="ROLE_ADMIN";
	
	//ADMINISTRADOR DE LA APLICACION (Moderador)
	public static final String C_ROL_ADMIN_APP    ="ROLE_ADMIN_APP";
	
	//Properties 
	public static final String PATH_PROPERTIES    = new File("").getAbsolutePath()+ "\\src\\main\\resources\\";
	
	
}
