package com.ecrece.service;

import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class UploadFileService {
	
	ApplicationHome home = new ApplicationHome(this.getClass()); 
	
	private String UPLOAD_FOLDER;
    public boolean uploadfile(File archivo, String nombrearchivo) {
    	 boolean forretun = true;
        FileInputStream fileInputStream = null;
//        ApplicationHome home = new ApplicationHome(this.getClass());        
        UPLOAD_FOLDER = home.getDir().toString().replace("com\\ecrece\\service","")+File.separator+"static"+File.separator+"files"+File.separator;
      
        try {
//          File file = new File(ruta);
          byte[] bFile = new byte[(int) archivo.length()];

          //read file into bytes[]
          fileInputStream = new FileInputStream(archivo);
          fileInputStream.read(bFile);

          //save bytes[] into a file
//          writeBytesToFile(bFile, UPLOAD_FOLDER + nombrearchivo);
          
          Files.write(Paths.get(UPLOAD_FOLDER + nombrearchivo) , bFile);
            return forretun;

        } catch (IOException e) {        	
            e.printStackTrace();
            return forretun = false;
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return forretun = false;
                }
            }

        }
    }

//    //Since JDK 7 - try resources
//    private static void writeBytesToFile(byte[] bFile, String fileDest) {
//
//        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
//            fileOuputStream.write(bFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    


}
