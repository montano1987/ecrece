package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.BusinessSubcategory;
import com.ecrece.model.Subcategory;

public interface BusinessSubcategoryRepository extends JpaRepository<BusinessSubcategory, Integer> {
	
	@Query("SELECT s FROM BusinessSubcategory t INNER JOIN"
			+ " Subcategory s ON s.idsubcategory = t.idsubcategory  where t.idbusiness = :id AND t.is_deleted = FALSE") 
	List<Subcategory> findByBusiness( Integer id);
	
	@Query("SELECT s FROM BusinessSubcategory t INNER JOIN"
			+ " Subcategory s ON s.idsubcategory = t.idsubcategory.idsubcategory  where t.idsubcategory.category.idcategory = :id AND t.is_deleted = FALSE") 
	List<Subcategory> findBySearch( Integer id);
}
