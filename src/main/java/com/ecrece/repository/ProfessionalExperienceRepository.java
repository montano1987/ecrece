package com.ecrece.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.ProfessionalExperience;

public interface ProfessionalExperienceRepository extends JpaRepository<ProfessionalExperience, Integer> {
	
}
