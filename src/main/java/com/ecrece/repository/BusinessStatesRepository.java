package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.BusinessStates;

public interface BusinessStatesRepository extends JpaRepository<BusinessStates, Integer> {
	
	@Query("SELECT b FROM BusinessStates b where UPPER(b.name_state_business) LIKE CONCAT('%',UPPER(:descbusinessstate),'%') AND b.is_deleted = FALSE") 
	List<BusinessStates> findforAll( String descbusinessstate);
}
