package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Promotion;


public interface PromotionRepository extends JpaRepository<Promotion, Integer> {
	
	@Query("SELECT p FROM Promotion p where  p.activePromotion = TRUE AND p.is_deleted = FALSE") 
	List<Promotion> findPromotionActive( );
	
	@Query("SELECT p FROM Promotion p where  p.activePromotion = FALSE AND p.is_deleted = FALSE") 
	List<Promotion> findPromotionDesable( );
		
	@Query("SELECT p FROM Promotion p where  p.business.idbusiness  = :idbusiness AND p.is_deleted = FALSE") 
	List<Promotion> findAllbybusiness( Integer idbusiness);
	
	@Query("SELECT p FROM Promotion p where p.business.user.iduser = :iduser AND p.is_deleted = FALSE") 
	List<Promotion> findPromotionbyUser( Integer iduser);
	
	@Query("SELECT p FROM Promotion p where p.business IS NULL AND p.is_deleted = FALSE") 
	List<Promotion> findPromotionbyUserAdmin();
	
	@Query("SELECT p FROM Promotion p where p.business.idbusiness  = :idbusiness AND  p.activePromotion = TRUE AND p.is_deleted = FALSE") 
	List<Promotion> findpromotionbybusiness(Integer idbusiness);

}
