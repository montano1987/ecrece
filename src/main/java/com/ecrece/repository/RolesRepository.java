package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Role;

public interface RolesRepository extends JpaRepository<Role, Integer> {

}
