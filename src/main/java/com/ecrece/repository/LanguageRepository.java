package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Language;

public interface LanguageRepository extends JpaRepository<Language, Integer> {
	
}
