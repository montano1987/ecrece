package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.NotificationState;

public interface NotificationStatesRepository extends JpaRepository<NotificationState, Integer> {

}
