package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Country;

public interface CountryRepository extends JpaRepository<Country, Integer> {
	@Query("SELECT b FROM Country b where UPPER(b.name_country) LIKE CONCAT('%',UPPER(:descountry),'%') AND b.is_deleted != TRUE") 
	List<Country> findforall( String descountry);
}
