package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.NotificationType;

public interface NotificationTypeRepository extends JpaRepository<NotificationType, Integer> {

}
