package com.ecrece.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Client;


public interface ClientReposity extends JpaRepository<Client, Integer> {

	@Query("SELECT c FROM Client c where  c.business.idbusiness = :idbusiness AND c.iduser.iduser = :iduser AND c.is_deleted = FALSE") 
	Client findbyBusiness( Integer idbusiness, Integer iduser);
	
	@Query("SELECT c FROM Client c where c.business.idbusiness = :idbusiness AND c.is_deleted = FALSE") 
	List<Client> findClientbyBusiness( Integer idbusiness);
}
