package com.ecrece.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Municipality;


public interface MunicipalityRepository extends JpaRepository<Municipality, Integer> {
	@Query("SELECT t FROM Municipality t where  t.province.idprovince = :idprovince AND t.is_deleted = FALSE") 
	List<Municipality> findByProvince( Integer idprovince);	
}
