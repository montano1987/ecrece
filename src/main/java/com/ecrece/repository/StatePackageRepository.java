package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.StatePackage;

public interface StatePackageRepository extends JpaRepository<StatePackage, Integer> {

}
