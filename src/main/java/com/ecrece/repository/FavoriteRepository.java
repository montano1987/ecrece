package com.ecrece.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Favorite;


public interface FavoriteRepository extends JpaRepository<Favorite, Integer> {	
	@Query("SELECT f FROM Favorite f where  f.business.idbusiness = :idbusiness AND  f.user.iduser = :iduser AND f.is_deleted = FALSE") 
	Favorite findByBusiness( Integer idbusiness, Integer iduser);
}
