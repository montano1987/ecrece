package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Visitedqr;

public interface VisitqrRepository extends JpaRepository<Visitedqr, Integer> {

}
