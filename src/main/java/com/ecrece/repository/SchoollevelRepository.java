package com.ecrece.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.ecrece.model.Schoollevel;

public interface SchoollevelRepository extends JpaRepository<Schoollevel, Integer> {
	
}
