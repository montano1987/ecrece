package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

}
