package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Reservation;


public interface ReservationReposity extends JpaRepository<Reservation, Integer> {	
	@Query("SELECT r FROM Reservation r where  r.usertoreservation.iduser = :iduser AND r.is_deleted = FALSE") 
	List<Reservation> getallreservationbyuser(Integer iduser);
	
	@Query("SELECT r FROM Reservation r where  r.business.idbusiness = :idbusiness AND r.is_deleted = FALSE") 
	List<Reservation> getallreservationbybusiness(Integer idbusiness);


}
