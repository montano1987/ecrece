package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ecrece.model.ScheduleBusiness;
public interface ScheduleBusinessRepository extends JpaRepository<ScheduleBusiness, Integer> {
	
	
	@Query("SELECT t FROM ScheduleBusiness t where t.business.idbusiness = :id AND t.is_deleted = FALSE ORDER BY t.weekday") 
	List<ScheduleBusiness> findByBusiness( Integer id);
}
