package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.ReservationCharacteristics;

public interface ReservationCharacteristicsRepository extends JpaRepository<ReservationCharacteristics, Integer> {
	
	@Query("SELECT r FROM ReservationCharacteristics r where  r.business.idbusiness = :idbusiness AND r.is_deleted = FALSE") 
	ReservationCharacteristics findbyidbusiness( Integer idbusiness);
}
