package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.SearchBusiness;

public interface SearchBusinesRepository extends JpaRepository<SearchBusiness, Integer> {
	
}
