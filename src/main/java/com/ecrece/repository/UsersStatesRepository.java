package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.UsersStates;

public interface UsersStatesRepository extends JpaRepository<UsersStates, Integer> {

}
