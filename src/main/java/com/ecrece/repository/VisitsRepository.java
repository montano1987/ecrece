package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Visits;

public interface VisitsRepository extends JpaRepository<Visits, Integer> {

}
