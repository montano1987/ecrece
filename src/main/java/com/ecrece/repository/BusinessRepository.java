package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.ecrece.model.Business;
import com.ecrece.model.Reservation;
import com.ecrece.model.SearchBusiness;

public interface BusinessRepository extends JpaRepository<Business, Integer> {
	
	@Query("SELECT b FROM Business b where  b.user.iduser = :iduser AND b.businessStates.idstate_business = 1 AND b.is_deleted = FALSE ORDER BY b.businessStates.idstate_business, b.crate_date") 
	List<Business> findnBusinessbyUser( Integer iduser);
	
	@Query("SELECT b FROM Business b where  b.user.iduser = :iduser AND b.is_deleted = FALSE ORDER BY b.crate_date") 
	List<Business> findnBusinessbyUsertoLogin( Integer iduser);
	
	@Query("SELECT b FROM Business b where  b.user.iduser = :iduser AND b.is_deleted = FALSE ORDER BY b.crate_date") 
	List<Business> findnmybusiness( Integer iduser);
	
	@Transactional
	  @Modifying
	@Query("DELETE FROM BusinessSubcategory b where  b.idbusiness = :idbusiness AND b.is_deleted = FALSE") 
	void deletesubcategoryforbusiness( Integer idbusiness);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM ScheduleBusiness b where  b.business.idbusiness = :idbusiness AND b.is_deleted = FALSE") 
	void deletesscheduleforbusiness( Integer idbusiness);
	
		@Query("SELECT b FROM Business b where  b.businessStates.idstate_business = 1 AND b.is_deleted = FALSE") 
	List<Business> findnBusinessbyActive();
		
	@Query("SELECT res FROM Reservation res  WHERE res.business.idbusiness = :idbusiness and res.accept = TRUE AND res.is_deleted = FALSE") 
	List<Reservation> findlistreservation(Integer idbusiness);
	
	@Query("SELECT b FROM SearchBusiness b where  b.idUser =:iduser AND b.is_deleted = FALSE ORDER BY b.date_search DESC") 
	List<SearchBusiness> findlastsearch(Integer iduser);
	
	@Query("SELECT f.business FROM Favorite f where f.user.iduser = :iduser AND f.business.businessStates.idstate_business = 1 AND f.is_deleted = FALSE") 
	List<Business> findfavobusiness(Integer iduser);
		
}
