package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {	
	@Query("SELECT b FROM Category b where UPPER(b.name_category) LIKE CONCAT('%',UPPER(:descategory),'%') AND b.is_deleted = FALSE") 
	List<Category> findforAll( String descategory);
}
