package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Request;

public interface RequestRepository extends JpaRepository<Request, Integer> {

}
