package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Offer;

public interface OfferRepository extends JpaRepository<Offer, Integer> {
	
	@Query("SELECT o FROM Offer o where  o.business.idbusiness = :idbusiness AND o.is_deleted = FALSE ORDER BY o.date_start ") 
	List<Offer> findAllbybusiness( Integer idbusiness);
	
	@Query("SELECT o FROM Offer o where  o.activeOffer = TRUE AND o.is_deleted = FALSE ORDER BY o.date_start") 
	List<Offer> getoffersactive( );
	
	@Query("SELECT o FROM Offer o where  o.activeOffer = FALSE AND o.is_deleted = FALSE ORDER BY o.date_start") 
	List<Offer> getoffersdesable( );
	
	@Query("SELECT o FROM Offer o where  o.business IS NULL AND o.is_deleted = FALSE ORDER BY o.date_start ") 
	List<Offer> getoffersadmin();
	
	@Query("SELECT o FROM Offer o where  o.business.user.iduser = :iduser AND o.is_deleted = FALSE ORDER BY o.date_start") 
	List<Offer> getoffersbyuser(Integer iduser);
	
	@Query("SELECT o FROM Offer o where  o.business.idbusiness = :idbusiness and o.activeOffer = TRUE AND o.is_deleted = FALSE") 
	List<Offer> getallofferbybusiness( Integer idbusiness);

}
