package com.ecrece.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Configuration;

public interface ConfigurationRepository extends JpaRepository<Configuration, Integer> {
	
}
