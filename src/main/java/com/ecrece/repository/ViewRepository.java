package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.View;

public interface ViewRepository extends JpaRepository<View, Integer> {

}
