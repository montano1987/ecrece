package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Clasification;

public interface ClasificationRepository extends JpaRepository<Clasification, Integer> {
	

	@Query("SELECT b FROM Clasification b where  b.business.idbusiness = :idbusiness AND b.is_deleted = FALSE") 
	List<Clasification> findbyBusiness( Integer idbusiness);
	
	@Query("SELECT b FROM Clasification b where  b.business.user.iduser = :idsuer AND b.is_deleted = FALSE") 
	List<Clasification> findclasificationbyuser( Integer idsuer);
	
	@Query("SELECT b FROM Clasification b where  b.business.idbusiness = :idbusiness AND b.is_deleted = FALSE") 
	List<Clasification> findclasificationbybusiness( Integer idbusiness);
	
}
