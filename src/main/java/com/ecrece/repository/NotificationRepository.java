package com.ecrece.repository;




import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Notification;


public interface NotificationRepository extends JpaRepository<Notification, Integer> {	
	
	@Query("SELECT n FROM Notification n WHERE n.iduser_to.iduser IS NULL OR n.iduser_to.iduser = :iduser OR n.notificationsType.desc_notification_type = 'Promoción' AND n.is_deleted = FALSE  Order By n.create_date DESC , n.notification_state.nameNotificationstate ") 
	List<Notification> findallformoderator(Integer iduser);
	
	@Query("SELECT n FROM Notification n WHERE n.iduser_to.iduser = :iduser AND n.read = FALSE AND n.is_deleted = FALSE Order By n.create_date DESC") 
	List<Notification> getallnotificationbyuser(Integer iduser);
	
	@Query("SELECT n FROM Notification n WHERE (n.iduser_to.iduser = :iduser OR n.iduser_to = null)  AND n.read = FALSE AND n.is_deleted = FALSE Order By n.create_date DESC") 
	List<Notification> getallnotificationbyuseradmin(Integer iduser);
	
	@Query("SELECT n FROM Notification n WHERE n.iduser_to.iduser = :iduser AND n.is_deleted = FALSE Order By n.create_date DESC , n.notification_state.nameNotificationstate ") 
	List<Notification> findallforuser(Integer iduser);
	
	@Query("SELECT n FROM Notification n WHERE n.idbusiness_to.idbusiness = :idbusiness  AND n.is_deleted = FALSE Order By n.create_date DESC , n.notification_state.nameNotificationstate ") 
	List<Notification> findallforbusiness(Integer idbusiness);
	
	@Query("SELECT n FROM Notification n WHERE n.iduser_to.iduser IS NULL OR n.iduser_to.iduser = :iduser AND n.is_deleted = FALSE Order By n.create_date DESC") 
	List<Notification> findallorder(Integer iduser);
}
