package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.ecrece.model.PaymentForms;

public interface PaymentFormsRepository extends JpaRepository<PaymentForms, Integer> {

}
