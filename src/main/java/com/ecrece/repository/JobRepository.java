package com.ecrece.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Job;

public interface JobRepository extends JpaRepository<Job, Integer> {
	
}
