package com.ecrece.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Subcategory;


public interface SubcategoryRepository extends JpaRepository<Subcategory, Integer> {
	
	@Query("SELECT t FROM Subcategory t where  t.category.idcategory = :idcategory AND t.is_deleted = FALSE") 
	List<Subcategory> findByCategory( Integer idcategory);
}
