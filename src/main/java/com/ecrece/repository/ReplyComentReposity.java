package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.ReplyComent;


public interface ReplyComentReposity extends JpaRepository<ReplyComent, Integer> {
	
	@Query("SELECT c FROM ReplyComent c where  c.coment.idcoment = :idcoment AND c.is_deleted = FALSE") 
	List<ReplyComent> findbyComent( Integer idcoment);

}
