package com.ecrece.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.User;


public interface UserRepository extends JpaRepository<User, Integer> {
	
	public User findByNameUser(String NameUser);
	
	@Query("SELECT u FROM User u where  u.userState.iduser_state = 1 and u.receivepromotions = TRUE AND u.is_deleted = FALSE") 
	List<User> findactiveUserPromotion();
	
	@Query("SELECT u FROM User u where  u.userState.iduser_state = 1 and u.receiveoffer = TRUE AND u.is_deleted = FALSE") 
	List<User> findactiveUserOffer();
	
	@Query("SELECT u FROM User u where  u.userState.iduser_state = 2 and u.code = :code AND u.is_deleted = FALSE") 
	User finduserforactivation(String code);
	
	@Query("SELECT u FROM User u where u.token = :token AND u.is_deleted = FALSE") 
	User getuserbytoken(String token);
	
	@Query("SELECT u FROM User u where u.code = :code AND u.is_deleted = FALSE") 
	User getuserbycode(String code);
	
	@Query("SELECT u FROM User u where u.token = :token and u.userState.iduser_state = 1 AND u.is_deleted = FALSE") 
	List<User> findByToken(String token);
	
	@Query("SELECT u FROM User u where u.token IS NOT NULL AND u.userState.iduser_state = 1 AND u.is_deleted = FALSE") 
	List<User> getUserConnet();
}
