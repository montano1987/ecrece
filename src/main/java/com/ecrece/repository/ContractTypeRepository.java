package com.ecrece.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.ContractType;

public interface ContractTypeRepository extends JpaRepository<ContractType, Integer> {
	
}
