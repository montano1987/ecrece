package com.ecrece.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Funcionality;

public interface FuncionalityRepository extends JpaRepository<Funcionality, Integer> {
	
	@Query("SELECT f FROM Funcionality f where  f.sistema = FALSE AND f.is_deleted = FALSE") 
	List<Funcionality> findfuncionalitynotsystem();
	
	@Query("SELECT f FROM Funcionality f where  f.sistema = TRUE AND f.is_deleted = FALSE") 
	List<Funcionality> findfuncionalitysystem();
	
	@Query("SELECT f FROM Funcionality f where  f.nameFuncionality  = :namefuncionality AND f.is_deleted = FALSE") 
	Funcionality findfuncionalityforname(String namefuncionality);
	
	@Query("SELECT f FROM Funcionality f where  (UPPER(f.descFuncionality) LIKE CONCAT('%',UPPER(:descfunc),'%') OR "
			+ "UPPER(f.nameFuncionality) LIKE CONCAT('%',UPPER(:namefunc),'%') OR "
			+ "UPPER(f.pointFuncionality) LIKE CONCAT('%',UPPER(:pointfunc),'%') OR "
			+ "UPPER(f.point_plus) LIKE CONCAT('%',UPPER(:plusfunc),'%') OR "
			+ "UPPER(f.point_less) LIKE CONCAT('%',UPPER(:lessfunc),'%') OR "
			+ "UPPER(f.funcionality_days) LIKE CONCAT('%',UPPER(:dayfunc),'%')) AND f.is_deleted = FALSE ") 
	List<Funcionality> findforall(String namefunc, String descfunc,Integer pointfunc,
			Integer plusfunc,Integer lessfunc, Integer dayfunc);
}
