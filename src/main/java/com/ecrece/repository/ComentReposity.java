package com.ecrece.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Coment;


public interface ComentReposity extends JpaRepository<Coment, Integer> {
	
	@Query("SELECT c FROM Coment c where  c.iduser.iduser = :iduser AND c.is_deleted = FALSE") 
	List<Coment> findbyUser( Integer iduser);
	
	@Query("SELECT c FROM Coment c where  c.business.idbusiness = :idbusiness AND c.is_deleted = FALSE") 
	List<Coment> findbyBusiness( Integer idbusiness);

}
