package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.PaymentTypes;

public interface PaymentTypesRepository extends JpaRepository<PaymentTypes, Long> {

}
