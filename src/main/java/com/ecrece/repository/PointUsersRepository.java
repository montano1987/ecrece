package com.ecrece.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ecrece.model.PointUsers;

public interface PointUsersRepository extends JpaRepository<PointUsers, Integer> {

}
