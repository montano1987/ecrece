package com.ecrece.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ecrece.model.Province;


public interface ProvinceRepository extends JpaRepository<Province, Integer> {
	List<Province> findByCountry(Integer idcountry);
	

}
