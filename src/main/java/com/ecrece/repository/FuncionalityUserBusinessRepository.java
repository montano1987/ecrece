package com.ecrece.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ecrece.model.Funcionality;
import com.ecrece.model.FuncionalityUserBusiness;

public interface FuncionalityUserBusinessRepository extends JpaRepository<FuncionalityUserBusiness, Integer> {
	
	@Query("Select f FROM FuncionalityUserBusiness f where f.idbusiness.idbusiness = :idbusiness AND f.idfuncionality.idfuncionality = :idfuncionality AND f.is_deleted = FALSE") 
	FuncionalityUserBusiness selectoneFuncionalityUserBusiness( Integer idbusiness, Integer idfuncionality);
	
	@Query("Select f FROM FuncionalityUserBusiness f INNER JOIN Funcionality fun ON  fun.idfuncionality = f.idfuncionality "+
									"where f.idbusiness.idbusiness = :idbusiness AND f.is_deleted = FALSE") 
	List<FuncionalityUserBusiness> funcionalitybyBusiness( Integer idbusiness);
	
	@Query("Select f FROM FuncionalityUserBusiness f where f.active = TRUE") 
	List<FuncionalityUserBusiness> getallfuncionalitybyBusiness( );
	
	@Query("Select fun FROM FuncionalityUserBusiness f INNER JOIN Funcionality fun ON  fun.idfuncionality = f.idfuncionality "+
			"where f.idbusiness.idbusiness = :idbusiness AND f.is_deleted = FALSE") 
	List<Funcionality> funcionalitybyBusinessentity( Integer idbusiness);
}
